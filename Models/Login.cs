﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Login
    {
        public int Idlogin { get; set; }
        public int? Iduser { get; set; }
        public string Token { get; set; }
        public DateTime? TimeLogin { get; set; }
    }
}
