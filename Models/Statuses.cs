﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Statuses
    {
        public int IdStatus { get; set; }
        public string StatusName { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
