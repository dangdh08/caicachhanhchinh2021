﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public partial class Sptrac_nghiem_onlineContext : DbContext
    {
        public Sptrac_nghiem_onlineContext()
        {
        }

        public Sptrac_nghiem_onlineContext(DbContextOptions<Sptrac_nghiem_onlineContext> options)
            : base(options)
        {
        }
        public virtual DbSet<GetDanhSachBaiThi> GetDanhSachBaiThi { get; set; }
        public virtual DbSet<GetChiTietBaiThi> GetChiTietBaiThi { get; set; }
        public virtual DbSet<KetQuaThi> KetQuaThi { get; set; }
        public virtual DbSet<GetDanhSachThiSinhTheoLop> GetDanhSachThiSinhTheoLop { get; set; }
        public virtual DbSet<ThongKeHome> ThongKeHome { get; set; }
        public virtual DbSet<CauHoimf> CauHoimf { get; set; }
        public virtual DbSet<GetDanhSachKetQua> GetDanhSachKetQua { get; set; }
        public virtual DbSet<ThiSinh_BTG> GetThongTinThiSinh_BTG { get; set; }
        public virtual DbSet<KetQuaThi_BTG> KetQuaThi_BTG { get; set; }
        public virtual DbSet<DanhSachThi> DanhSachThi { get; set; }
        public virtual DbSet<DanhSachDonViThi> DanhSachDonViThi { get; set; }
        public virtual DbSet<ThongKeSoLuongCauDung> ThongKeSoLuongCauDung { get; set; }
        public virtual DbSet<DapAn1ThiSinh> DapAn1ThiSinh { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["TracNghiemDatabase"].ConnectionString);
            }
        }
    }
}
