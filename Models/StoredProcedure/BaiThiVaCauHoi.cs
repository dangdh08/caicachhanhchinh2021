﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class BaiThiVaCauHoi
    {
        public string TestName { get; set; }
        public int TestCode { get; set; }
        public string Password { get; set; }
        public int TotalQuestions { get; set; }
        public int TimeToDo { get; set; }
        public string Note { get; set; }
        public int IdStatus { get; set; }
        public DateTime? Timestamps { get; set; }
        public List<CauHoiEdit> datacauhoi { get; internal set; }
        public List<ThiSinh> datathisinh { get; internal set; }
    }
}
