﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class KetQuaThi_BTG
    {
        [Key]
        public int? id_score { get; internal set; }
        public int? id_student { get; internal set; }
        public int? test_code { get; internal set; }
        public int? socaudung { get; internal set; }
        public int? tongcau { get; internal set; }
        public DateTime? time_finish { get; internal set; }
        public double? diemthang10 { get; internal set; }
        public int? dudoanketqua { get; internal set; }
        public string test_name { get; internal set; }
        public string name { get; internal set; }
        public DateTime? timelogin { get; internal set; }
        public string thoigianlam { get; internal set; }
    }
}
