﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class DanhSachDonViThi
    {
        [Key]
        public string donvi { get; set; }
        public int? soluong { get; set; }
    }
}
