﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class GetDanhSachBaiThi
    {
        public string test_name { get; set; }
        public int total_questions { get; set; }
        public int id_student { get; set; }
        [Key]
        public int test_code { get; set; }
        public int time_to_do { get; set; }
        public int id_status { get; set; }
        public string status_name { get; set; }
        public string note { get; set; }
        public string dudoanketqua { get; set; }
    }
}
