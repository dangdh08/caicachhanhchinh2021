﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class DanhSachThi
    {
        [Key]
        public int id_student { get; set; }
        public string username { get; set; }
        public string avatar { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string cmnd { get; set; }
        public DateTime? timelogin { get; set; }
        public string donvi { get; set; }
        public int tongcau { get; set; }
        public int? socaudung { get; set; }
        public DateTime? time_finish { get; set; }
        public int ketqua { get; set; }
    }
}
