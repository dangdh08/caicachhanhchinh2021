﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class GetChiTietBaiThi
    {
        [Key]
        public int ID { get; set; }
        public int id_student { get; set; }
        public int test_code { get; set; }
        public int id_cauhoi { get; set; }
        public string img_content { get; set; }
        public string content { get; set; }
        public string answer_a { get; set; }
        public string answer_b { get; set; }
        public string answer_c { get; set; }
        public string answer_d { get; set; }
        public int id_loaicauhoi { get; set; }
        public string tenloai { get; set; }
        public string test_name { get; set; }
        public string student_answer { get; set; }

    }
}
