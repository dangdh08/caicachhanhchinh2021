﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class ThongKeSoLuongCauDung
    {
        [Key]
        public int? socaudung { get; set; }
        public int? soluong { get; set; }
    }
}
