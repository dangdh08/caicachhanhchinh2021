﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class DapAn1ThiSinh
    {
        [Key]
        public int id_cauhoi { get; set; }
        public string content { get; set; }
        public string student_answer { get; set; }
        public string correct_answer { get; set; }
        public string answer_a { get; set; }
        public string answer_b { get; set; }
        public string answer_c { get; set; }
        public string answer_d { get; set; }
        public int? id_student { get; internal set; }
    }
}
