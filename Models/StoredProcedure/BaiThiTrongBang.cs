﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class BaiThiTrongBang
    {
        public int IdClass { get; set; }
        public string ClassName { get; set; }
        public string GradeName { get; set; }
        public DateTime? Timestamps { get; set; }
        public List<ThiSinh> datathisinh { get; internal set; }
        //public List<ThiSinh> datathisinh { get; internal set; }
    }
}
