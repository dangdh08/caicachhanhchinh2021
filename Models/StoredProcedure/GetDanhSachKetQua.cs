﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class GetDanhSachKetQua
    {
        [Key]
        public int? Rank { get; internal set; }
        public int? IdStudent { get;internal set; }
        public string Username { get; internal set; }
        public string Email { get; internal set; }
        public string Name { get; internal set; }
        public string Gender { get; internal set; }
        public DateTime? Birthday { get; internal set; }
        public string Phone { get; internal set; }
        public string Cmnd { get; internal set; }
        public string Sbd { get; internal set; }
        public DateTime? Timelogin { get; internal set; }
        public string Donvi { get; internal set; }
        public int? Socaudung { get; internal set; }
        public int? TongCau { get; internal set; }
        public DateTime? TimeFinish { get; internal set; }
        public Double? Diemthang10 { get; internal set; }
        public int? TestCode { get; internal set; }
    }
}
