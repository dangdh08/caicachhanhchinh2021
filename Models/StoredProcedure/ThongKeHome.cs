﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class ThongKeHome
    {
        [Key]
        public int idkey { get; set; }
        public string title { get; set; }
        public int total { get; set; }
    }
}
