﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class GetDanhSachThiSinhTheoLop
    {
        public string name { get; set; }
        public string gender { get; set; }
        public string donvi { get; set; }
        public string sbd { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        [Key]
        public int id_chitiet_lop { get; set; }
        public int id_class { get; set; }
        public string class_name { get; set; }
        public string grade_name { get; set; }
        public int id_student { get; set; }
    }
}
