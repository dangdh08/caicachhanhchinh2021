﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class ThiSinh_BTG
    {
        [Key]
        public int id_student { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string cmnd { get; set; }
        public DateTime? timelogin { get; set; }
        public string donvi { get; set; }
    }
}
