﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models.StoredProcedure
{
    public class ThiSinh
    {
        public int IdStudent { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public string Sbd { get; set; }
        public DateTime? Timelogin { get; set; }
        public string Donvi { get; set; }
        public int Rank { get; internal set; }
        public int? Idclass { get; internal set; }
    }
}
