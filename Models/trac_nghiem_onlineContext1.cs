﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ReactJsExample.Models
{
    public partial class trac_nghiem_onlineContext1 : DbContext
    {
        public trac_nghiem_onlineContext1()
        {
        }

        public trac_nghiem_onlineContext1(DbContextOptions<trac_nghiem_onlineContext1> options)
            : base(options)
        {
        }

        public virtual DbSet<Admins> Admins { get; set; }
        public virtual DbSet<Baithi> Baithi { get; set; }
        public virtual DbSet<Cauhoi> Cauhoi { get; set; }
        public virtual DbSet<ChitietLop> ChitietLop { get; set; }
        public virtual DbSet<Chitietbaithi> Chitietbaithi { get; set; }
        public virtual DbSet<Classes> Classes { get; set; }
        public virtual DbSet<Loaicauhoi> Loaicauhoi { get; set; }
        public virtual DbSet<Login> Login { get; set; }
        public virtual DbSet<Scores> Scores { get; set; }
        public virtual DbSet<Specialities> Specialities { get; set; }
        public virtual DbSet<Statuses> Statuses { get; set; }
        public virtual DbSet<StudentTestDetail> StudentTestDetail { get; set; }
        public virtual DbSet<Students> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=10.184.254.149;Database=trac_nghiem_online;User Id=tracnghiem2019;Password=tracnghiem2019@0981234.");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Admins>(entity =>
            {
                entity.HasKey(e => e.IdAdmin)
                    .HasName("PK__admins__89472E9530A157A8");

                entity.ToTable("admins");

                entity.HasIndex(e => new { e.Username, e.Email })
                    .HasName("UQ__admins__B96D23647734DA5F")
                    .IsUnique();

                entity.Property(e => e.IdAdmin).HasColumnName("id_admin");

                entity.Property(e => e.Avatar)
                    .HasColumnName("avatar")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('avatar-default.jpg')");

                entity.Property(e => e.Birthday)
                    .HasColumnName("birthday")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Timelogin)
                    .HasColumnName("timelogin")
                    .HasColumnType("datetime");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Baithi>(entity =>
            {
                entity.HasKey(e => e.TestCode)
                    .HasName("PK__baithi__040975AA5A4C8063");

                entity.ToTable("baithi");

                entity.Property(e => e.TestCode).HasColumnName("test_code");

                entity.Property(e => e.DelStt)
                    .HasColumnName("del_stt")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IdStatus).HasColumnName("id_status");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.TestName)
                    .IsRequired()
                    .HasColumnName("test_name")
                    .HasMaxLength(255);

                entity.Property(e => e.TimeToDo).HasColumnName("time_to_do");

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TotalQuestions).HasColumnName("total_questions");
            });

            modelBuilder.Entity<Cauhoi>(entity =>
            {
                entity.HasKey(e => e.IdCauhoi)
                    .HasName("PK__question__2BD924771B7173C5");

                entity.ToTable("cauhoi");

                entity.Property(e => e.IdCauhoi).HasColumnName("id_cauhoi");

                entity.Property(e => e.AnswerA).HasColumnName("answer_a");

                entity.Property(e => e.AnswerB).HasColumnName("answer_b");

                entity.Property(e => e.AnswerC).HasColumnName("answer_c");

                entity.Property(e => e.AnswerD).HasColumnName("answer_d");

                entity.Property(e => e.Content).HasColumnName("content");

                entity.Property(e => e.CorrectAnswer)
                    .HasColumnName("correct_answer")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DelStt)
                    .HasColumnName("del_stt")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IdLoaicauhoi).HasColumnName("id_loaicauhoi");

                entity.Property(e => e.ImgContent).HasColumnName("img_content");

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<ChitietLop>(entity =>
            {
                entity.HasKey(e => e.IdChitietLop)
                    .HasName("PK__chitiet___3CC70372550C4EEC");

                entity.ToTable("chitiet_lop");

                entity.Property(e => e.IdChitietLop).HasColumnName("id_chitiet_lop");

                entity.Property(e => e.IdClass).HasColumnName("id_class");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Chitietbaithi>(entity =>
            {
                entity.HasKey(e => e.IdChitiet)
                    .HasName("PK__quests_o__3214EC27B8C23CE0");

                entity.ToTable("chitietbaithi");

                entity.Property(e => e.IdChitiet).HasColumnName("ID_chitiet");

                entity.Property(e => e.IdCauhoi).HasColumnName("id_cauhoi");

                entity.Property(e => e.TestCode).HasColumnName("test_code");

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Classes>(entity =>
            {
                entity.HasKey(e => e.IdClass)
                    .HasName("PK__classes__2352EEA93059EA0D");

                entity.ToTable("classes");

                entity.Property(e => e.IdClass).HasColumnName("id_class");

                entity.Property(e => e.ClassName)
                    .HasColumnName("class_name")
                    .HasMaxLength(60);

                entity.Property(e => e.DelStt)
                    .HasColumnName("del_stt")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.GradeName)
                    .HasColumnName("grade_name")
                    .HasMaxLength(60);

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Loaicauhoi>(entity =>
            {
                entity.HasKey(e => e.IdLoaicauhoi)
                    .HasName("PK__subjects__8F848F6098C0F347");

                entity.ToTable("loaicauhoi");

                entity.Property(e => e.IdLoaicauhoi).HasColumnName("id_loaicauhoi");

                entity.Property(e => e.DelStt)
                    .HasColumnName("del_stt")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Tenloai)
                    .IsRequired()
                    .HasColumnName("tenloai")
                    .HasMaxLength(255);

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Login>(entity =>
            {
                entity.HasKey(e => e.Idlogin)
                    .HasName("PK__Login__1EE24E5C9F020625");

                entity.Property(e => e.Idlogin)
                    .HasColumnName("IDlogin")
                    .ValueGeneratedNever();

                entity.Property(e => e.Iduser).HasColumnName("IDUser");

                entity.Property(e => e.TimeLogin).HasColumnType("date");
            });

            modelBuilder.Entity<Scores>(entity =>
            {
                entity.HasKey(e => new { e.IdScore, e.IdStudent, e.TestCode });

                entity.ToTable("scores");

                entity.Property(e => e.IdScore)
                    .HasColumnName("id_score")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.TestCode).HasColumnName("test_code");

                entity.Property(e => e.Diemthang10).HasColumnName("diemthang10");

                entity.Property(e => e.Socaudung).HasColumnName("socaudung");

                entity.Property(e => e.TimeFinish)
                    .HasColumnName("time_finish")
                    .HasColumnType("datetime");

                entity.Property(e => e.Tongcau).HasColumnName("tongcau");
            });

            modelBuilder.Entity<Specialities>(entity =>
            {
                entity.HasKey(e => e.IdSpeciality)
                    .HasName("PK__speciali__CF97EB984CF2A323");

                entity.ToTable("specialities");

                entity.Property(e => e.IdSpeciality).HasColumnName("id_speciality");

                entity.Property(e => e.SpecialityName)
                    .IsRequired()
                    .HasColumnName("speciality_name")
                    .HasMaxLength(255);

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Statuses>(entity =>
            {
                entity.HasKey(e => e.IdStatus)
                    .HasName("PK__statuses__5D2DC6E865E1C90F");

                entity.ToTable("statuses");

                entity.Property(e => e.IdStatus).HasColumnName("id_status");

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasColumnName("status_name")
                    .HasMaxLength(50);

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<StudentTestDetail>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.IdStudent, e.TestCode });

                entity.ToTable("student_test_detail");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.TestCode).HasColumnName("test_code");

                entity.Property(e => e.IdCauhoi).HasColumnName("id_cauhoi");

                entity.Property(e => e.StudentAnswer)
                    .HasColumnName("student_answer")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamps)
                    .HasColumnName("timestamps")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.HasKey(e => e.IdStudent)
                    .HasName("PK__students__2BE2EBB681A432C4");

                entity.ToTable("students");

                entity.HasIndex(e => new { e.Username, e.Email })
                    .HasName("UQ__students__B96D2364BADF7213")
                    .IsUnique();

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Avatar)
                    .HasColumnName("avatar")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('avatar-default.jpg')");

                entity.Property(e => e.Birthday)
                    .HasColumnName("birthday")
                    .HasColumnType("date");

                entity.Property(e => e.Cmnd)
                    .HasColumnName("cmnd")
                    .HasMaxLength(15);

                entity.Property(e => e.Donvi)
                    .HasColumnName("donvi")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnName("gender")
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Sbd)
                    .HasColumnName("sbd")
                    .HasMaxLength(15);

                entity.Property(e => e.Timelogin)
                    .HasColumnName("timelogin")
                    .HasColumnType("datetime");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
        }
    }
}
