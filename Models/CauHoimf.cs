﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models
{
    public class CauHoimf
    {
        public int Rank { get; internal set; }
        [Key]
        
        public int IdCauhoi { get; set; }
        public int IdLoaicauhoi { get; internal set; }
        public string ImgContent { get;internal set; }
        public string Content { get; internal set; }
        public string AnswerA { get; internal set; }
        public string AnswerB { get; internal set; }
        public string AnswerC { get; internal set; }
        public string AnswerD { get; internal set; }
        public string CorrectAnswer { get;internal set; }
        public DateTime? Timestamps { get;internal set; }
        public string TenLoai { get;internal set; }
    }
}
