﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Xacthucotp
    {
        public int Idotp { get; set; }
        public string Sodienthoai { get; set; }
        public string Otp { get; set; }
        public DateTime? Ngayadd { get; set; }
    }
}
