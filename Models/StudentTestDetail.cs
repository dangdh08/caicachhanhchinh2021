﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class StudentTestDetail
    {
        public int Id { get; set; }
        public int IdStudent { get; set; }
        public int TestCode { get; set; }
        public int IdCauhoi { get; set; }
        public string StudentAnswer { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
