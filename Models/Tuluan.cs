﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Tuluan
    {
        public int Idtuluan { get; set; }
        public int IdStudent { get; set; }
        public int? TestCode { get; set; }
        public string Phone { get; set; }
        public string Cautraloi { get; set; }
        public DateTime? Ngaytraloi { get; set; }
        public DateTime? Thoigianketthuc { get; set; }
    }
}
