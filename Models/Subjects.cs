﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Subjects
    {
        public int IdSubject { get; set; }
        public string SubjectName { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
