﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Grades
    {
        public int IdGrade { get; set; }
        public string GradeName { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
