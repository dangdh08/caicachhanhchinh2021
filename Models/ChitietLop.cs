﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class ChitietLop
    {
        public int IdChitietLop { get; set; }
        public int IdClass { get; set; }
        public int IdStudent { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
