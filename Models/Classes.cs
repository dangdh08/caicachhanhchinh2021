﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Classes
    {
        public int IdClass { get; set; }
        public string ClassName { get; set; }
        public string GradeName { get; set; }
        public DateTime? Timestamps { get; set; }
        public bool? DelStt { get; set; }
    }
}
