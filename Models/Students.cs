﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Students
    {
        public int IdStudent { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public string Sbd { get; set; }
        public string Token { get; set; }
        public DateTime? Timelogin { get; set; }
        public string Donvi { get; set; }
    }
}
