﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
