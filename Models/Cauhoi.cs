﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Cauhoi
    {
        public int IdCauhoi { get; set; }
        public int IdLoaicauhoi { get; set; }
        public string ImgContent { get; set; }
        public string Content { get; set; }
        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }
        public string CorrectAnswer { get; set; }
        public DateTime? Timestamps { get; set; }
        public bool? DelStt { get; set; }
    }
}
