﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Specialities
    {
        public int IdSpeciality { get; set; }
        public string SpecialityName { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
