﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Scores
    {
        public int IdScore { get; set; }
        public int IdStudent { get; set; }
        public int TestCode { get; set; }
        public int? Socaudung { get; set; }
        public int? Tongcau { get; set; }
        public DateTime? TimeFinish { get; set; }
        public double? Diemthang10 { get; set; }
        public int? Dudoanketqua { get; set; }
    }
}
