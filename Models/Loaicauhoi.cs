﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Loaicauhoi
    {
        public int IdLoaicauhoi { get; set; }
        public string Tenloai { get; set; }
        public DateTime? Timestamps { get; set; }
        public bool? DelStt { get; set; }
    }
}
