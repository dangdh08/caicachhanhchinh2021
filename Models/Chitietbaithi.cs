﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Chitietbaithi
    {
        public int IdChitiet { get; set; }
        public int TestCode { get; set; }
        public int IdCauhoi { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
