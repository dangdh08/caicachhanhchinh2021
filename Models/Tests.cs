﻿using System;
using System.Collections.Generic;

namespace ReactJsExample.Models
{
    public partial class Tests
    {
        public string TestName { get; set; }
        public int TestCode { get; set; }
        public string Password { get; set; }
        public int IdSubject { get; set; }
        public int TotalQuestions { get; set; }
        public int TimeToDo { get; set; }
        public string Note { get; set; }
        public int IdStatus { get; set; }
        public DateTime? Timestamps { get; set; }
    }
}
