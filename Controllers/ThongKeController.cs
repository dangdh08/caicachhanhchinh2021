﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Moderator")]
    public class ThongKeController : Controller
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        DataAccessLayer obj = new DataAccessLayer();

        [HttpGet("getall")]
        public IEnumerable<DanhSachThi> GetAll()
        {
            
            
                // document-https://docs.microsoft.com/en-us/dotnet/api/system.identitymodel.tokens.jwt.jwtsecuritytokenhandler.readjwttoken?view=azure-dotnet
                var handler = new JwtSecurityTokenHandler();
                string authHeader = Request.Headers["Authorization"];
                authHeader = authHeader.Replace("Bearer ", "");
                var jsonToken = handler.ReadToken(authHeader);
                var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
                //var handler = new JwtSecurityTokenHandler();
                //var jsonToken = handler.ReadToken(token);
                //var jsonToken1 = handler.ReadJwtToken(token);
                //var tokenS = handler.ReadToken(token) as JwtSecurityToken;

                var role = tokenS.Claims.First(claim => claim.Type == "role").Value;
                var id_name = tokenS.Claims.First(claim => claim.Type == "unique_name").Value;
                var id_nam_donvi = tokenS.Claims.First(claim => claim.Type == "given_name").Value;

              return obj.DanhSachThi_DonVi(id_nam_donvi);
               // return Ok(new { error = 200, message = id_nam_donvi.ToString(), data= ketqua });
            
            
           
            
        }
    }
}