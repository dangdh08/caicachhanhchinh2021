﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BanTuyenGiaoController : ControllerBase
    {
        public class paramGetCauHoi
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string Password { get; set; }
            public int TimeFN { get; set; }
        }
        public class paramUpdateDapAn
        {
            public int Id { get; set; }
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string StudentAnswer { get; set; }
        }
        public class paramNopBai
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public int DuDoan { get; set; }
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        BaiThiDataAccessLayer obj_Baithi = new BaiThiDataAccessLayer();
        [AllowAnonymous]
        [HttpPost("lambaithi")]
        public IActionResult LamBaiThi([FromBody] Students st)
        {
            ResultCode rs = new ResultCode();
            if (st.Name == null || st.Donvi == null)
            {
                return BadRequest(new { error = 103, message = "Tên và đơn vị không để trống!" });
            }
            if (st.Phone != null)
            {
                if (st.Phone.Substring(0, 1) != "0" || st.Phone.Length != 10 || IsNumber(st.Phone) == false)
                {
                    return BadRequest(new { error = 101, message = "Di động nhập sai, Nhập lại (Số di động 10 số)!" });
                }
            }
            if (st.Phone==null && st.Email ==null )
            {
                return BadRequest(new { error = 102, message = "Nhập Di động hoặc Email!" });
            }
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                ThiSinh_BTG _user = new ThiSinh_BTG();
                var dbResults = ctx.GetChiTietBaiThi.FromSql("EXEC dbo.InsertPersonal @Name={0}, @CMND ={1}, @Phone ={2}, @DiaChi ={3}, @Email={4}", st.Name, st.Cmnd,st.Phone,st.Donvi,st.Email).ToList();
                //var _user = data.Students.SingleOrDefault(x => x.IdStudent == dbResults[0].id_student);
                using (var _ctx = new Sptrac_nghiem_onlineContext())
                {
                    var _dbResults = ctx.GetThongTinThiSinh_BTG.FromSql("EXEC dbo.GetThongTinThiSinh_BTG @id={0}", dbResults[0].id_student).ToList();
                    _user = _dbResults[0];
                    // return null if user not found
                    if (_user == null)
                        return BadRequest(new { error = 202, message = "Lỗi" });
                }
                return Ok(new { error = 200, message = "Lấy thông tin thành công",user= _user, data = dbResults });
            }
            ////var kq = obj_BTG.ThemThongTin(st);
            ////obj_Baithi.TaoDeThi(kq, 925126);
            //if (kq == -1)
            //{
            //    rs.error = 202;
            //    rs.message = "Thêm lỗi";
            //    return BadRequest(rs);
            //}
            //rs.error = 200;
            //rs.message = "Thêm thành công";
            //return Ok(rs);
        }
        [AllowAnonymous]
        [HttpPost("[action]")]
        public IActionResult NopBai([FromBody] paramNopBai userParam)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.KetQuaThi_BTG.FromSql("EXEC dbo.GetKetQua_BTG @MaThiSinh={0}, @TestCode ={1}, @DuDoan={2}", userParam.IdStudent, userParam.TestCode, userParam.DuDoan).ToList();
                return Ok(new { error = 200, message = "Nộp bài thành công", data = dbResults });
            }
        }
        [AllowAnonymous]
        [HttpPost("[action]")]
        public IActionResult UpdateDapAn_BTG([FromBody] paramUpdateDapAn detail)
        {
            ResultCode rs = new ResultCode();
            try
            {
                var _detail = data.StudentTestDetail.FirstOrDefault(x => x.Id == detail.Id && x.IdStudent == detail.IdStudent);
                _detail.Timestamps = DateTime.Now;
                _detail.StudentAnswer = detail.StudentAnswer;
                data.Entry(_detail).State = EntityState.Modified;
                data.SaveChanges();
                rs.error = 200;
                rs.message = "Success!";
                return Ok(rs);
            }
            catch
            {
                rs.error = 1;
                rs.message = "Update thất bại!";
                return BadRequest(rs);
            }
        }
        [AllowAnonymous]
        [HttpPost("[action]")]
        public IActionResult GetCauHoi_BTG([FromBody] paramGetCauHoi abc)
        {
            try
            {
                using (var ctx = new Sptrac_nghiem_onlineContext())
                {
                    var dbResults = ctx.GetChiTietBaiThi.FromSql("EXEC dbo.GetChiTietBaiThi @MaThiSinh={0}, @TestCode ={1}", abc.IdStudent, abc.TestCode).ToList();
                    return Ok(new { error = 200, message = "Lấy danh sách câu hỏi thành công!", data = dbResults });
                }
            }
            catch
            {
                return BadRequest(new { error = -1, message = "Lỗi!" });
            }

        }
    }
}