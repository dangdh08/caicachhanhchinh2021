﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;
using RestSharp;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "User")]
    public class CaiCachHanhChinhController : Controller
    {
        public class result
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string Didong { get; set; }
        }
        public class _Tuluan
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string Didong { get; set; }
            public string CauTraLoi { get; set; }
        }
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        BaiThiDataAccessLayer obj_Baithi = new BaiThiDataAccessLayer();
        [Authorize(Roles = "User")]
        [HttpPost("lambaituluan")]
        public IActionResult LamBaiTuLuan([FromBody] _Tuluan tl)
        {
            ResultCode rs = new ResultCode();
            var find = data.Tuluan.Count(n => n.IdStudent == tl.IdStudent);
            try
            {
                if (find == 0)
                {
                    tl.CauTraLoi = "";
                    data.Database.ExecuteSqlCommand("dbo.TaoCauTraLoiTuLuan @p0, @p1, @p2, @p3", parameters: new[] { tl.IdStudent.ToString(), tl.TestCode.ToString(), tl.Didong, tl.CauTraLoi });
                    var dbResults = data.Tuluan.SingleOrDefault(n => n.IdStudent == tl.IdStudent);
                    return Ok(new { error = 200, message = "Tạo bài tự luận thành công", data = dbResults });
                }
                else
                {
                    var dbResults = data.Tuluan.SingleOrDefault(n => n.IdStudent == tl.IdStudent);
                    return Ok(new { error = 200, message = "Get bài tự luận thành công", data = dbResults });
                }
            } catch (Exception ex)
            {
                return BadRequest(new { error = 102, message = "Lấy thông tin lỗi./ "+ ex.Message });
            }
         }
        [Authorize(Roles = "User")]
        [HttpPost("capnhattuluan")]
        public IActionResult CapNhatTuLuan([FromBody] Tuluan tl)
        {
            try
            {
                tl.Ngaytraloi = DateTime.Now;
                data.Entry(tl).State = EntityState.Modified;
                data.SaveChanges();
                Tuluan _context = data.Tuluan.Find(tl.Idtuluan);
                return Ok(new { error = 200, message = "Cập nhật thành công", data = _context });
            }
            catch (Exception ex)
            {
                return BadRequest(new { error = 102, message = "Cập nhật lỗi./ " + ex.Message });
            }
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public void guitinmobi(string sdt, string nd)
        {
            var client = new RestClient("http://210.211.109.118/apibrandname/send?wsdl");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("text/xml", "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:send=\"http://send_sms.vienthongdidong.vn/\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <send:send>\r\n         <!--Optional:-->\r\n         <USERNAME>sotttn</USERNAME>\r\n         <!--Optional:-->\r\n         <PASSWORD>sotttn!@#</PASSWORD>\r\n         <!--Optional:-->\r\n         <BRANDNAME>TAYNINHeGov</BRANDNAME>\r\n         <!--Optional:-->\r\n         <MESSAGE>"+ nd +"</MESSAGE>\r\n         <!--Optional:-->\r\n         <TYPE>1</TYPE>\r\n         <!--Optional:-->\r\n         <PHONE>"+ sdt + "</PHONE>\r\n         <!--Optional:-->\r\n         <IDREQ>100000001</IDREQ>\r\n      </send:send>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
           // Console.WriteLine(response.Content);
            
        }
        [HttpPost("[action]")]
        [Authorize(Roles = "User")]
        public IActionResult TaoDeThi([FromBody] Students st)
        {
            BaiThiDataAccessLayer obj = new BaiThiDataAccessLayer();
            result _rs = new result();
            Baithi parameter = new Baithi();
            parameter.TestName = st.Phone;
            parameter.TimeToDo = 15;
            parameter.TotalQuestions = 20;
            parameter.IdStatus = 1;
            parameter.Password = st.Phone;
            parameter.DelStt = true;
            var kq = obj.AddBaiThi(parameter);
            if (kq > 2)
            {
                try {
                    int idst = data.Students.Where(n=>n.Phone== st.Phone).OrderByDescending(x => x.IdStudent).First().IdStudent;
                    //int idst = data.Students.SingleOrDefault(x => x.Phone == st.Phone).IdStudent;
                    // data.Database.ExecuteSqlCommand("dbo.TaoDeCauHoiChoDe @p0, @p1, @p2, @p3, @p4 ,@p5, @p6", parameters: new[] { kq.ToString(), "7", "8", "10", "11", "12", idst.ToString() });
                   //chỉnh mã loại trong sp TaoDeCauHoiChoDe_TpTayNinh
                    data.Database.ExecuteSqlCommand("dbo.TaoDeCauHoiChoDe_TpTayNinh @p0, @p1", parameters: new[] { kq.ToString(), idst.ToString() });
                    _rs.IdStudent = idst;
                    _rs.Didong = st.Phone;
                    _rs.TestCode = kq;
                    return Ok(new { error = 200, message = "Tạo đề thi thành công", data = _rs });
                } catch (Exception ex){
                    return BadRequest(new { error = 203, message = ex.Message });
                }
                
            }
            if (kq == 2)
            {
                //int idst = data.Students.SingleOrDefault(x => x.Phone == st.Phone).IdStudent;
                //_rs.IdStudent = idst;
                //_rs.Didong = st.Phone;
                //_rs.TestCode = data.Baithi.SingleOrDefault(n => n.TestName == st.Phone).TestCode;
                return BadRequest(new { error = 201, message = "Quá 3 lần thi"});
            }
            return BadRequest(new { error = 201, message = "Tạo đề lỗi"});
        }
    }
}