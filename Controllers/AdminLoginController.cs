﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AdminLoginController : Controller
    {
        private IAdminService _userService;

        public AdminLoginController(IAdminService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]Admins userParam)
        {
            var user = _userService.Authenticate(userParam.Username, userParam.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(user);
        }
        [AllowAnonymous]
        [HttpPost("logout")]
        public IActionResult Logout([FromBody]Admins userParam)
        {
            ResultCode rs = new ResultCode();
            var user = _userService.Logout(userParam.Token);
            if (user == null || userParam.Token == "" || userParam.Token == null)
            {
                rs.error = 1;
                rs.message = "Token không chính xác!";
                return BadRequest(rs);
            }
            rs.error = 200;
            rs.message = "Đăng xuất thành công!";
            return Ok(rs);
        }
        [AllowAnonymous]
        [HttpPost("checktoken")]
        public IActionResult CheckToken([FromBody]Admins userParam)
        {
            var user = _userService.CheckToken(userParam.Token);
            ResultCode rs = new ResultCode();
            if (user == null || userParam.Token == "" || userParam.Token == null)
            {
                rs.error = 1;
                rs.message = "Token không chính xác!";
                return BadRequest(rs);
            }
            //https://jwt.io/
            var stream = userParam.Token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            var exp = tokenS.Claims.First(claim => claim.Type == "exp").Value;
            DateTime foo = DateTime.UtcNow;
            long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();

            if (int.Parse(exp) < unixTime)
            {
                rs.error = 2;
                rs.message = "Token hết hạn";
                return Ok(rs);
            }
            rs.error = 200;
            rs.message = "Token đúng";
            return Ok(rs);
        }
        [Authorize(Roles = "Administrator")]
        [HttpGet("[action]")]
        public IEnumerable<ThongKeHome> ThongKeHome()
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.ThongKeHome.FromSql("EXEC dbo.ThongKeHome").ToList();
                return dbResults;
            }
        }
    }
}