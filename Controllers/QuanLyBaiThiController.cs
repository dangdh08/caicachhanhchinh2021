﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class QuanLyBaiThiController : Controller
    {
        public class paramTaoDe
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
        }
        BaiThiDataAccessLayer obj = new BaiThiDataAccessLayer();
        [HttpGet("[action]")]
        public IEnumerable<BaiThiVaCauHoi> GetAll()
        {
            return obj.GetAllBaiThi();
        }
        [HttpGet("[action]")]
        public IEnumerable<Statuses> GetAllStatuses()
        {
            return obj.GetAllTrangThai();
        }
        [HttpPost("[action]")]
        public IActionResult Create([FromBody] Baithi parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddBaiThi(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Tên bài thi tồn tại!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult Details([FromBody] Baithi parameter)
        {
            if (obj.GetDetail(parameter.TestCode) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetDetail(parameter.TestCode) });
        }
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] Baithi parameter)
        {
            var kq = obj.UpdateBaithi(parameter);
            if (kq == 1)
                return Ok(new { error = 200, message = "Cập nhật thành công!", data = obj.GetDetail(parameter.TestCode) });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpDelete("[action]")]
        public IActionResult Delete([FromBody] Baithi parameter)
        {
            var kq = obj.DeleteBaithi(parameter.TestCode);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("[action]")]
        public IActionResult AddCauHoiVaoBaiThi([FromBody] Chitietbaithi parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddCauHoiVaoBaiThi(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Câu hỏi đã có trong bài thi!";
                return BadRequest(rs);
            }
            if (kq == 3)
            {
                rs.error = 203;
                rs.message = "Quá số câu quy định!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpDelete("[action]")]
        public IActionResult DeleteCauHoiTrongBaiThi([FromBody] Chitietbaithi parameter)
        {
            var kq = obj.DeleteCauHoiTrongBaiThi(parameter.IdChitiet);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("[action]")]
        public IActionResult GetCauHoiTheoBaiThi([FromBody] Baithi parameter)
        {
            if (obj.GetCauHoiTheoBaiThi(parameter.TestCode) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetCauHoiTheoBaiThi(parameter.TestCode) });
        }
        [HttpPost("[action]")]
        public IActionResult TaoDeThi([FromBody] paramTaoDe parameter)
        {
            if (obj.TaoDeThi(parameter.IdStudent,parameter.TestCode) == -1)
                return BadRequest(new { error = 201, message = "Lỗi" });
            return Ok(new { error = 200, message = "Thành công"});
        }
        [HttpPost("[action]")]
        public IActionResult GetDanhSachKetQua([FromBody] paramTaoDe parameter)
        {
            try
            {
                return Ok(new { error = 200, message = "Thành công", data = obj.GetDanhSachKetQua(parameter.TestCode) });
            }
            catch
            {
                return BadRequest(new { error = 201, message = "Lỗi" });
            }
        }
    }
}