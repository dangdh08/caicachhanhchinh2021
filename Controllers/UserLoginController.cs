﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;
using RestSharp;

namespace ReactJsExample.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserLoginController : Controller
    {
        public class rsGoogle
        {
            public string success { get; set; }
            public string challenge_ts { get; set; }
            public string hostname { get; set; }
        }
        private IUserService _userService;

        public UserLoginController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]Students userParam)
        {
            var user = _userService.Authenticate(userParam.Username, userParam.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(user);
        }
        [AllowAnonymous]
        [HttpPost("authenticatecchc")]
        public IActionResult AuthenticateCCHC([FromBody]Students userParam)
        {
            ResultCode rs = new ResultCode();
            var user = _userService.AuthenticateCCHC(userParam.Phone, userParam.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(new { error = 200, message = "Đăng nhập thành công", data = user });
        }
        [AllowAnonymous]
        [HttpPost("logout")]
        public IActionResult Logout([FromBody]Students userParam)
        {
            var user = _userService.Logout(userParam.Token);
            ResultCode rs = new ResultCode();
            if (user == null)
            {
                rs.error = 1;
                rs.message = "Token không chính xác!";
                return BadRequest(rs);
            }
            rs.error = 200;
            rs.message = "Đăng xuất thành công!";
            return Ok(rs);
        }
        [AllowAnonymous]
        [HttpPost("checktoken")]
        public IActionResult CheckToken([FromBody]Students userParam)
        {
            var user = _userService.CheckToken(userParam.Token);
            ResultCode rs = new ResultCode();
            if (user == null)
            {
                rs.error = 1;
                rs.message = "Token không chính xác!";
                return BadRequest(rs);
            }
            //https://jwt.io/
            var stream = userParam.Token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            var exp = tokenS.Claims.First(claim => claim.Type == "exp").Value;
            DateTime foo = DateTime.UtcNow;
            long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();

            if (int.Parse(exp) < unixTime)
            {
                rs.error = 2;
                rs.message = "Token hết hạn";
                return Ok(rs);
            }
            rs.error = 200;
            rs.message = "Token đúng";
            return Ok(rs);
        }

        // Cải cách hành chính
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        BaiThiDataAccessLayer obj_Baithi = new BaiThiDataAccessLayer();
        [AllowAnonymous]
        [HttpPost("dangkythi")]
        public IActionResult DangKyThi([FromBody] Students st)
        {
            //check google capchart
            var client = new RestClient("https://www.google.com/recaptcha/api/siteverify");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddParameter("secret", "6LcFXawZAAAAAJTW7sv1NKFZvxpJ56ppxEFcHVoB");
            request.AddParameter("response", st.Token);
            IRestResponse response = client.Execute(request);
            rsGoogle checkspam = Newtonsoft.Json.JsonConvert.DeserializeObject<rsGoogle>(response.Content);
            if (checkspam.success == "false")
            {
                return BadRequest(new { error = 105, message = "Sai mã reCatpcha" });
            }

            ResultCode rs = new ResultCode();
            if(KTCMND(st.Cmnd)== false){
                return BadRequest(new { error = 104, message = "CMND không đúng định dạng (09 hoặc 12 số)" });
            }
            // if (data.Students.Where(n => n.Phone == st.Phone).Count() >= 3 )
            // {
            //     return BadRequest(new { error = 104, message = "User đã đăng ký 3 lần! với số điện thoại" });
            // }
            // if (data.Students.Where(n => n.Cmnd == st.Cmnd).Count() >= 3)
            // {
            //     return BadRequest(new { error = 104, message = "User đã đăng ký 3 lần! với số cmnd" });
            // }
            if (st.Name == null || st.Donvi == null)
            {
                return BadRequest(new { error = 103, message = "Tên và đơn vị không để trống!" });
            }
            if (st.Phone != null)
            {
                if (st.Phone.Substring(0, 1) != "0" || st.Phone.Length != 10 || IsNumber(st.Phone) == false)
                {
                    return BadRequest(new { error = 101, message = "Di động nhập sai, Nhập lại (Số di động 10 số)!" });
                }
            }
            if (st.Phone == null && st.Email == null)
            {
                return BadRequest(new { error = 102, message = "Nhập Di động hoặc Email!" });
            }
            Random rnd = new Random();
            int otp = rnd.Next(100000, 999999);
            var check_otp = "success";
            //check có otp chưa
            var check = data.Xacthucotp.Count(n => n.Sodienthoai == st.Phone);
            if (check == 0)
            {
                data.Database.ExecuteSqlCommand("dbo.InsertXacThucOTP @p0, @p1", parameters: new[] { st.Phone, otp.ToString() });
                guitinmobi(st.Phone, "Dùng " + otp.ToString() + " để đăng nhập hệ thống");
                check_otp = "unsuccess";
                // return BadRequest(new { error = 110, message = "Chưa có otp - đã gửi otp", checkotp = check_otp });
            }
            // else if (check == 1)
            // {
            //     var kiemtraotp = data.Xacthucotp.Count(n => n.Sodienthoai == st.Phone && n.Otp == st.Sbd);
            //      if(kiemtraotp!=1)
            //         return BadRequest(new { error = 109, message = "Nhập sai otp", checkotp = check_otp });
            // }
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                // guitinmobi(st.Phone, "Dùng "+ otp.ToString() + " để đăng nhập hệ thống");
                //var dbResults = ctx.GetChiTietBaiThi.FromSql("EXEC dbo.InsertPersonal @Name={0}, @CMND ={1}, @Phone ={2}, @DiaChi ={3}, @Email={4}, @Pass={5}", st.Name, st.Cmnd, st.Phone, st.Donvi, st.Email, GenerateMD5(otp.ToString())).ToList();
                data.Database.ExecuteSqlCommand("dbo.InsertPersonal @p0, @p1, @p2, @p3, @p4 ,@p5", parameters: new[] { st.Name, st.Cmnd, st.Phone, st.Donvi, st.Email, GenerateMD5(otp.ToString()) });
                //var _user = data.Students.SingleOrDefault(x => x.IdStudent == dbResults[0].id_student);
                data.Database.ExecuteSqlCommand("dbo.InsertXacThucOTP @p0, @p1", parameters: new[] { st.Phone, otp.ToString() });
                return Ok(new { error = 200, message = otp, checkotp = check_otp });
                
            }
            //  return BadRequest(new { error = 104, message = "Cuộc thi đã kết thúc" });
        }
        [AllowAnonymous]
        [HttpGet("danhsachdonvithi")]
        public IActionResult DanhSachDonViThi()
        {
            ResultCode rs = new ResultCode();
            try
            {
                using (var ctx = new Sptrac_nghiem_onlineContext())
                {
                    var dbResults = ctx.DanhSachDonViThi.FromSql("EXEC dbo.DanhSachDonViThi").ToList();
                    return Ok(new { error = 200, message = "Lấy dữ liệu thành công", data = dbResults });
                }
            }catch(Exception ex)
            {
                return BadRequest(new { message = "Lỗi ..."+ ex.ToString() });
            }
        }
        [AllowAnonymous]
        [HttpGet("thongketheodiem")]
        public IActionResult ThongKeTheoDiem()
        {
            ResultCode rs = new ResultCode();
            try
            {
                using (var ctx = new Sptrac_nghiem_onlineContext())
                {
                    var dbResults = ctx.ThongKeSoLuongCauDung.FromSql("EXEC dbo.ThongKeSoLuongCauDung").ToList();
                    return Ok(new { error = 200, message = "Lấy dữ liệu thành công", data = dbResults });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Lỗi ..." + ex.ToString() });
            }
        }


        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public bool KTCMND(string cmnd)
        {
            if (cmnd.Length == 9 || cmnd.Length == 12)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        public void guitinmobi(string sdt, string nd)
        {
            var client = new RestClient("http://210.211.109.118/cskhunicodeapi/send?wsdl");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("text/xml", "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:send=\"http://send_sms.vienthongdidong.vn/\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <send:send>\r\n         <!--Optional:-->\r\n         <USERNAME>sotttn</USERNAME>\r\n         <!--Optional:-->\r\n         <PASSWORD>sotttn!@#</PASSWORD>\r\n         <!--Optional:-->\r\n         <BRANDNAME>TAYNINHeGov</BRANDNAME>\r\n         <!--Optional:-->\r\n         <MESSAGE>" + nd + "</MESSAGE>\r\n         <!--Optional:-->\r\n         <TYPE>1</TYPE>\r\n         <!--Optional:-->\r\n         <PHONE>" + sdt + "</PHONE>\r\n         <!--Optional:-->\r\n         <IDREQ>100000001</IDREQ>\r\n      </send:send>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            // Console.WriteLine(response.Content);

        }

    }
}