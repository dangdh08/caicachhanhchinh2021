﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class QuanLyLopController : Controller
    {
        ClassDataAccessLayer obj = new ClassDataAccessLayer();
        [HttpGet("[action]")]
        public IEnumerable<BaiThiTrongBang> GetAll()
        {
            return obj.GetAllBaiThi();
        }
        [HttpPost("[action]")]
        public IActionResult Create([FromBody] Classes parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddClasses(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Tên bài thi tồn tại!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult Details([FromBody] Classes parameter)
        {
            if (obj.GetDetail(parameter.IdClass) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetDetail(parameter.IdClass) });
        }
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] Classes parameter)
        {
            var kq = obj.UpdateClasses(parameter);
            if (kq == 1)
                return Ok(new { error = 200, message = "Cập nhật thành công!", data = obj.GetDetail(parameter.IdClass) });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpDelete("[action]")]
        public IActionResult Delete([FromBody] Classes parameter)
        {
            var kq = obj.DeleteClasses(parameter.IdClass);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("[action]")]
        public IActionResult AddThiSinhVaoLop([FromBody] ChitietLop parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddThiSinhVaoLop(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Thí sinh đã có trong lớp!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpDelete("[action]")]
        public IActionResult DeleteThiSinhTrongLop([FromBody] ChitietLop parameter)
        {
            var kq = obj.DeleteThiSinhTrongLop(parameter.IdChitietLop);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            if (kq == 2)
                return BadRequest(new { error = 501, message = "Không đúng id thí sinh!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("[action]")]
        public IEnumerable<GetDanhSachThiSinhTheoLop> GetDanhSachThiSinhTheoLop([FromBody] ChitietLop userParam)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.GetDanhSachThiSinhTheoLop.FromSql("EXEC dbo.GetDanhSachThiSinhTheoLop @MaLop={0}", userParam.IdChitietLop).ToList();
                return dbResults;
            }
        }
    }
}