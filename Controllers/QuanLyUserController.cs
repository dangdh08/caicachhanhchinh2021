﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class QuanLyUserController : Controller
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        DataAccessLayer obj = new DataAccessLayer();
        [HttpGet("[action]")]
        public IEnumerable<ThiSinh> GetAll2()
        {
            List<ThiSinh> list = obj.GetAllUser();
            return list.Select((user, index) => new ThiSinh()
            {
                IdStudent = user.IdStudent,
                Avatar = user.Avatar,
                Birthday = user.Birthday,
                Cmnd = user.Cmnd,
                Donvi = user.Donvi,
                Email = user.Email,
                Gender = user.Gender,
                Name = user.Name,
                Phone = user.Phone,
                Sbd = user.Sbd,
                Timelogin = user.Timelogin,
                Username = user.Username,
                Rank = index += 1
            }).ToList();
            //return obj.GetAllUser();
        }
        //Thêm tổng kết điểm 1 thi sinh
        [HttpGet("[action]")]
        public IEnumerable<DanhSachThi> GetAll()
        {
            return obj.DanhSachThi();
        }
        //Thêm tổng kết điểm 1 thi sinh
        [HttpGet("[action]")]
        public IEnumerable<DanhSachThi> GetAll3()
        {
            return obj.DanhSachThi2();
        }
        [HttpPost("[action]")]
        public IActionResult Create([FromBody] Students st)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddUser(st);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Tồn tại user!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult Details([FromBody] Students st)
        {
            if(obj.GetDetail(st.IdStudent)==null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu"});
            return Ok(new { error = 200, message = "Thành công", data = obj.GetDetail(st.IdStudent) });
            //return Ok(obj.GetDetail(st.IdStudent));
        }
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] Students st)
        {
            var kq = obj.UpdateUser(st);
            if (kq == 1)
                return Ok(new { error = 200, message = "Cập nhật thành công!", data = obj.GetDetail(st.IdStudent) });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpDelete("[action]")]
        public IActionResult Delete([FromBody] Students st)
        {
            var kq = obj.DeleteUser(st.IdStudent);
            if (kq == 2)
                return BadRequest(new { error = 502, message = "Không xóa dược, tồn tại user trong bài thi!" });
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!"});
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("themuser")]
        public IActionResult ThemUser([FromBody] Students st)
        {
            ResultCode rs = new ResultCode();
            if (data.Students.Where(n => n.Phone == st.Phone).Count() != 0)
            {
                return BadRequest(new { error = 104, message = "User đã đăng ký!" });
            }
            if (st.Name == null || st.Donvi == null)
            {
                return BadRequest(new { error = 103, message = "Tên và đơn vị không để trống!" });
            }
            if (st.Phone != null)
            {
                if (st.Phone.Substring(0, 1) != "0" || st.Phone.Length != 10 || IsNumber(st.Phone) == false)
                {
                    return BadRequest(new { error = 101, message = "Di động nhập sai, Nhập lại (Số di động 10 số)!" });
                }
            }
            if (st.Phone == null && st.Email == null)
            {
                return BadRequest(new { error = 102, message = "Nhập Di động hoặc Email!" });
            }
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                Random rnd = new Random();
                int otp = rnd.Next(100000, 999999);
                //guitinmobi(st.Phone, "Dùng " + otp.ToString() + " để đăng nhập hệ thống");
                // otp => CMND
                // tên đội trong Email
                data.Database.ExecuteSqlCommand("dbo.InsertPersonalDoi @p0, @p1, @p2, @p3, @p4 ,@p5, @p6", parameters: new[] { st.Name, otp.ToString(), st.Phone, st.Donvi, st.Email, GenerateMD5(otp.ToString()),st.Sbd });
                return Ok(new { error = 200, message = "Nhập thông tin thành công" });
            }
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        [HttpGet("[action]")]
        public IActionResult Thongke()
        {
            //var kq = obj.DeleteUser(st.IdStudent);
            //if (kq == 2)
            //    return BadRequest(new { error = 502, message = "Không xóa dược, tồn tại user trong bài thi!" });
            //if (kq == 1)
            //    return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
    }
}