﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "User")]
    public class BaiThiUserController : Controller
    {
        private IUserService _userService;

        public BaiThiUserController(IUserService userService)
        {
            _userService = userService;
        }
        public class paramGetCauHoi
        {
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string Password { get; set; }
            public int TimeFN { get; set; }
            public int DuDoan { get; set; }
        }
        public class paramUpdateDapAn
        {
            public int Id { get; set; }
            public int IdStudent { get; set; }
            public int TestCode { get; set; }
            public string StudentAnswer { get; set; }
        }
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        [HttpPost("[action]")]
        public IEnumerable<GetDanhSachBaiThi> GetBaiThi([FromBody] Students userParam)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.GetDanhSachBaiThi.FromSql("EXEC dbo.GetDanhSachBaiThi @MaThiSinh={0}", userParam.IdStudent).ToList();
                return dbResults;
            }
        }
        [HttpPost("[action]")]
        public IActionResult GetCauHoi([FromBody] paramGetCauHoi abc)
        {
            var c = abc.Password;
            var a = abc.TestCode;
            ResultCode rs = new ResultCode();
            var bt = data.Baithi.SingleOrDefault(n => n.TestCode == abc.TestCode && n.Password == abc.Password);
            if (bt == null)
            {
                rs.error = 402;
                rs.message = "Sai mật khẩu!";
                return BadRequest(rs);
            }
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.GetChiTietBaiThi.FromSql("EXEC dbo.GetChiTietBaiThi @MaThiSinh={0}, @TestCode ={1}", abc.IdStudent, abc.TestCode).ToList();
                return Ok(dbResults);
            }
        }
        [HttpPost("[action]")]
        public IActionResult UpdateDapAn([FromBody] paramUpdateDapAn detail)
        {
            var rv= _userService.UpdateDapAn(detail.Id, detail.IdStudent, detail.StudentAnswer, detail.TestCode);
            ResultCode rs = new ResultCode();
            if (rv == 2)
            {
                rs.error = 214;
                rs.message = "Hết thời gian làm bài!";
                return BadRequest(rs);
            }
            if (rv == 1)
            {
                rs.error = 200;
                rs.message = "Success!";
                return Ok(rs);
            }
            rs.error = 1;
            rs.message = "Update thất bại!";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult ThoiGinThi([FromBody] paramGetCauHoi userParam)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var bt = data.Baithi.Find(userParam.TestCode).TimeToDo;
                var dbResults = ctx.KetQuaThi.FromSql("EXEC dbo.GetThoiGianThi @MaThiSinh={0}, @TestCode ={1}, @time_finish={2}", userParam.IdStudent, userParam.TestCode,bt).ToList();
                var getTime = data.Scores.Where(n => n.IdStudent == userParam.IdStudent && n.TestCode == userParam.TestCode).FirstOrDefault().TimeFinish;
                System.DateTime date1 = new System.DateTime(getTime.Value.Year, getTime.Value.Month, getTime.Value.Day, getTime.Value.Hour, getTime.Value.Minute, getTime.Value.Second);
                System.TimeSpan diff1 = date1.Subtract(DateTime.Now);
                //return dbResults;
                return Ok(new { error = 200, message = "Get Thanh cong",timecount= diff1.TotalSeconds, data = dbResults });

            }
            //return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        //[HttpPost("[action]")]
        //public IEnumerable<KetQuaThi> DiembaiThi([FromBody] paramGetCauHoi userParam)
        //{
        //    using (var ctx = new Sptrac_nghiem_onlineContext())
        //    {
        //        var dbResults = ctx.KetQuaThi.FromSql("EXEC dbo.GetKetQua @MaThiSinh={0}, @TestCode ={1}", userParam.IdStudent, userParam.TestCode).ToList();
        //        var dbDapan = ctx.DapAn1ThiSinh.FromSql("EXEC dbo.GetDapAn_1ThiSinh @idthisinh={0}", userParam.IdStudent).ToList();
        //        return dbResults;
        //    }
        //}
        [HttpPost("[action]")]
        public IActionResult DiembaiThi([FromBody] paramGetCauHoi userParam)
        {
            try
            {
                using (var ctx = new Sptrac_nghiem_onlineContext())
                {
                   
                    //lấy kq
                    var dbResults = ctx.KetQuaThi.FromSql("EXEC dbo.GetKetQua @MaThiSinh={0}, @TestCode ={1}", userParam.IdStudent, userParam.TestCode).ToList();
                     //cập nhật đáp án
                    var dbResults2 = ctx.KetQuaThi_BTG.FromSql("EXEC dbo.CapNhat_KetQuaDuDoan @MaThiSinh={0}, @TestCode ={1}, @DuDoan={2}", userParam.IdStudent, userParam.TestCode, userParam.DuDoan).ToList();
                    var dbDapan = ctx.DapAn1ThiSinh.FromSql("EXEC dbo.GetDapAn_1ThiSinh @idthisinh={0}", userParam.IdStudent).ToList();
                    return Ok(new { error = 200, message = "Get Thanh cong", dapan = dbDapan, data = dbResults2 });
                }
            }catch (Exception ex)
            {
                return BadRequest(new { error = 200, message = "Lỗi"});
            }

        }
        [HttpPost("[action]")]
        public IActionResult CheckTime([FromBody] paramGetCauHoi userParam)
        {
            var rv = _userService.CheckTime(userParam.IdStudent,userParam.TestCode);
            if(rv==null)
                return Ok(new { error = 200, message = "Tiếp tục làm bài" });
            return BadRequest(new { error = 202, message = "Hết giờ làm bài", data = rv });
        }
        [HttpPost("[action]")]
        public IActionResult CheckDiem([FromBody] paramUpdateDapAn detail)
        {
            var rv = _userService.CheckDiem(detail.IdStudent, detail.TestCode);
            ResultCode rs = new ResultCode();
            if (rv == 1)
            {
                rs.error = 201;
                rs.message = "Chưa làm bài thi";
                return Ok(rs);
            }
            if (rv == 2)
            {
                rs.error = 200;
                rs.message = "Success!";
                return Ok(rs);
            }
            if (rv == 3)
            {
                rs.error = 203;
                rs.message = "Đã nộp bài";
                return Ok(rs);
            }
            rs.error = -201;
            rs.message = "Lỗi";
            return BadRequest(rs);
        }
    }
}