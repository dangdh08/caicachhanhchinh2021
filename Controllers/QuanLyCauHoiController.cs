﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReactJsExample.Models;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class QuanLyCauHoiController : Controller
    {
        CauHoiDataAccessLayer obj = new CauHoiDataAccessLayer();
        [HttpGet("[action]")]
        public IEnumerable<CauHoimf> GetAll()
        {
            return obj.GetAllCauHoi();
        }
        [HttpPost("[action]")]
        public IActionResult GetCauHoiTheoLoai([FromBody] Cauhoi parameter)
        {
            if (obj.GetCauHoiTheoLoai(parameter.IdLoaicauhoi) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetCauHoiTheoLoai(parameter.IdLoaicauhoi) });
        }
        [HttpPost("[action]")]
        public IActionResult Create([FromBody] Cauhoi parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddCauhoi(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Tên tồn tại!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult Details([FromBody] Cauhoi parameter)
        {
            if (obj.GetDetail(parameter.IdCauhoi) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetDetail(parameter.IdCauhoi) });
        }
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] Cauhoi parameter)
        {
            var kq = obj.UpdateCauhoi(parameter);
            if (kq == 1)
                return Ok(new { error = 200, message = "Cập nhật thành công!", data = obj.GetDetail(parameter.IdCauhoi) });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpDelete("[action]")]
        public IActionResult Delete([FromBody] Cauhoi parameter)
        {
            var kq = obj.DeleteCauhoi(parameter.IdCauhoi);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpPost("[action]"), DisableRequestSizeLimit]
        public IActionResult UploadImg()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }
    }
}