﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReactJsExample.Models;
using ReactJsExample.Services;

namespace ReactJsExample.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class QuanLyLoaiCauHoiController : Controller
    {
        LoaiCauHoiDataAccessLayer obj = new LoaiCauHoiDataAccessLayer();
        [HttpGet("[action]")]
        public IEnumerable<Loaicauhoi> GetAll()
        {
            return obj.GetAllLoaiCauHoi();
        }
        [HttpPost("[action]")]
        public IActionResult Create([FromBody] Loaicauhoi parameter)
        {
            ResultCode rs = new ResultCode();
            var kq = obj.AddLoaicauhoi(parameter);
            if (kq == 1)
            {
                rs.error = 200;
                rs.message = "Thêm thành công";
                return Ok(rs);
            }
            if (kq == 2)
            {
                rs.error = 201;
                rs.message = "Tên tồn tại!";
                return BadRequest(rs);
            }
            rs.error = 202;
            rs.message = "Thêm lỗi";
            return BadRequest(rs);
        }
        [HttpPost("[action]")]
        public IActionResult Details([FromBody] Loaicauhoi parameter)
        {
            if (obj.GetDetail(parameter.IdLoaicauhoi) == null)
                return BadRequest(new { error = 201, message = "Không có dữ liệu" });
            return Ok(new { error = 200, message = "Thành công", data = obj.GetDetail(parameter.IdLoaicauhoi) });
        }
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] Loaicauhoi parameter)
        {
            var kq = obj.UpdateLoaicauhoi(parameter);
            if (kq == 1)
                return Ok(new { error = 200, message = "Cập nhật thành công!", data = obj.GetDetail(parameter.IdLoaicauhoi) });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
        [HttpDelete("[action]")]
        public IActionResult Delete([FromBody] Loaicauhoi parameter)
        {
            var kq = obj.DeleteLoaicauhoi(parameter.IdLoaicauhoi);
            if (kq == 1)
                return Ok(new { error = 200, message = "Xóa thành công!" });
            return BadRequest(new { error = 501, message = "Lỗi cmnr!" });
        }
    }
}