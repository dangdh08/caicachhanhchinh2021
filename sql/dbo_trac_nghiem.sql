/*
Navicat SQL Server Data Transfer

Source Server         : localhost
Source Server Version : 140000
Source Host           : DESKTOP-S4VGAUV:1433
Source Database       : trac_nghiem_online
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 140000
File Encoding         : 65001

Date: 2019-08-01 19:41:02
*/


-- ----------------------------
-- Table structure for __EFMigrationsHistory
-- ----------------------------
DROP TABLE [dbo].[__EFMigrationsHistory]
GO
CREATE TABLE [dbo].[__EFMigrationsHistory] (
[MigrationId] nvarchar(150) NOT NULL ,
[ProductVersion] nvarchar(32) NOT NULL 
)


GO

-- ----------------------------
-- Records of __EFMigrationsHistory
-- ----------------------------

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE [dbo].[admins]
GO
CREATE TABLE [dbo].[admins] (
[id_admin] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NOT NULL ,
[password] varchar(32) NOT NULL ,
[email] varchar(100) NOT NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NULL ,
[birthday] date NULL ,
[phone] varchar(45) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL 
)


GO

-- ----------------------------
-- Records of admins
-- ----------------------------
SET IDENTITY_INSERT [dbo].[admins] ON
GO
INSERT INTO [dbo].[admins] ([id_admin], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [token], [timelogin]) VALUES (N'1', N'admin', N'e10adc3949ba59abbe56e057f20f883e', N'admin@gmail.com', N'avatar-default.jpg', N'admin', N'nam', N'1990-01-01', null, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NjQ0OTU2OTEsImV4cCI6MTYwMDQ5NTY5MSwiaWF0IjoxNTY0NDk1NjkxfQ.W_ueVepnrejJV-q7s-iUlJZT9zl6ZfkxzTaC4H9naeU', N'2019-07-30 21:08:11.867')
GO
GO
SET IDENTITY_INSERT [dbo].[admins] OFF
GO

-- ----------------------------
-- Table structure for baithi
-- ----------------------------
DROP TABLE [dbo].[baithi]
GO
CREATE TABLE [dbo].[baithi] (
[test_name] nvarchar(255) NOT NULL ,
[test_code] int NOT NULL IDENTITY(1,1) ,
[password] varchar(32) NOT NULL ,
[total_questions] int NOT NULL ,
[time_to_do] int NOT NULL ,
[note] nvarchar(MAX) NULL ,
[id_status] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[baithi]', RESEED, 925123)
GO

-- ----------------------------
-- Records of baithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[baithi] ON
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Đề thi thử luật dân sự', N'925113', N'123456', N'4', N'1', N'Thi thử', N'1', N'2019-07-26 14:13:08.387', N'0')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'AAA Thi Test', N'925114', N'123456', N'1', N'10', null, N'1', N'2019-07-31 16:40:35.900', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'adsad', N'925115', N'123456', N'12', N'12', N'dsad', N'1', N'2019-07-25 10:07:04.717', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'sdad', N'925116', N'123456', N'12', N'12', N'dsad', N'1', N'2019-07-25 10:07:23.767', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'dadass', N'925118', N'123456', N'2', N'21', N'sad', N'1', N'2019-07-25 13:52:54.293', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng A 2018', N'925120', N'123456', N'30', N'30', N'Thi thử', N'1', N'2019-07-26 14:12:58.000', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng B 2018', N'925121', N'123456', N'30', N'20', N'Thi thử', N'1', N'2019-07-25 14:56:33.293', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Test', N'925122', N'123456', N'4', N'123', N'Thi thử', N'1', N'2019-07-26 14:12:25.687', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'test mới', N'925123', N'123456', N'2', N'123', N'Thi thử', N'1', N'2019-07-30 19:58:54.830', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[baithi] OFF
GO

-- ----------------------------
-- Table structure for cauhoi
-- ----------------------------
DROP TABLE [dbo].[cauhoi]
GO
CREATE TABLE [dbo].[cauhoi] (
[id_cauhoi] int NOT NULL IDENTITY(1,1) ,
[id_loaicauhoi] int NOT NULL ,
[img_content] nvarchar(MAX) NULL ,
[content] nvarchar(MAX) NULL ,
[answer_a] nvarchar(MAX) NULL ,
[answer_b] nvarchar(MAX) NULL ,
[answer_c] nvarchar(MAX) NULL ,
[answer_d] nvarchar(MAX) NULL ,
[correct_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[cauhoi]', RESEED, 21)
GO

-- ----------------------------
-- Records of cauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cauhoi] ON
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'1', N'1', N'noimage.png', N'Câu hỏi số 1 luật dân sự', N'Đáp Án B', N'ĐÁP án c', N'Đáp án A', N'đáp ÁN d ĐÚNG', N'A', N'2019-07-16 11:29:39.000', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'2', N'1', N'636977821475187024.png', N'Câu hỏi số 2 luật dân sự', N'đáp án d', N'đáp án c đúng', N'đáp án b', N'đáp án a', N'B', N'2019-07-25 19:55:39.600', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'3', N'1', N'dsad', N'câu 3', N'sdadGFG', N'yf', N'g', N'h', N'C', N'2019-07-19 09:02:19.620', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'4', N'1', N'dsadsa', N'Câu 4', N'aada', N'gg', N'gg', N'gh', N'D', N'2019-07-19 09:02:41.850', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'5', N'1', N'cau 1', N'cau nao dung', N'b', N'c', N'a', N'd', N'D', N'2019-07-22 07:30:37.293', N'0')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'8', N'5', N'432', N'Trong Excel,Biểu thức sau cho kết quả là bao nhiêu nếu DTB = 9 và HK loại C trong đó DTB: Điểm trung bình. HK: Hạnh kiểm =If(OR(DTB>=8, HK= "A"),1000,300)', N'Value!', N'300', N'1000', N'False', N'C', N'2019-07-26 08:34:56.423', N'0')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'11', N'5', N'4234', N'Lựa chọn nào sau đây không phải là hiệu ứng tô màu (Fill Effects) dùng cho nền slide?', N'Texture', N'Picture', N'Gradient', N'Brightness', N'D', N'2019-07-26 08:47:02.310', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'12', N'5', N'423', N'Trong hệ điều hành Windows, muốn đóng (thoát) cửa sổ chương trình ứng dụng đang làm việc ta', N'Nhấn tổ hợp phím Alt + F3', N'Nhấn tổ hợp phím Ctrl + F3', N'Nhấn tổ hợp phím Ctrl + F4', N'Nhấn tổ hợp phím Alt + F4', N'D', N'2019-07-26 08:50:03.117', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'13', N'5', N'234', N'Điện thoại thông minh (smartphone) là gì?', N'Bền hơn so với điện thoại di động khác', N'Điện thoại tích hợp một nền tảng hệ điều hành di động với nhiều tính năng hỗ trợ tiên tiến', N'Hỗ trợ tất cả các kiểu hệ điều hành', N'Điện thoại chỉ có chức năng nghe và gọi', N'B', N'2019-07-26 08:54:30.430', N'0')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'14', N'5', N'42', N'Cách chuyển định dạng từ tập tin MSWord 2007 trở lên sang dạng PDF', N'Dùng phần mềm nén tập tin', N'Vào File -> Save as ->Save as Type   ->PDF', N'Sửa phần mở rộng của tập tin thành .pdf', N'MSWord 2010 không hỗ trợ chuyển', N'B', N'2019-07-26 08:55:36.783', N'0')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'15', N'5', N'234', N'Khi đang trình chiếu (Slide Show) một bài trình diễn, muốn chuyển sang màn hình của một  chương trình ứng dụng khác (đã mở trước) để minh họa mà không kết thúc việc trình chiếu, ta phải', N'nhấn tổ hợp phím Alt + Tab', N'nhấn tổ hợp phím Ctrl + Tab', N'nhấn tổ hợp phím Shift + Tab', N'nhấn tổ hợp phím Esc + Tab', N'A', N'2019-07-26 08:59:33.920', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'16', N'5', N'324', N'Phần cứng máy tính là gì?', N'Các bộ phận cụ thể của máy tính về mặt vật lý như màn hình, chuột, bàn phím,...', N'Cấu tạo của phần mềm về mặt vật lý', N'Cấu tạo của phần mềm về mặt logic', N'Cả 3 phương án đều sai', N'A', N'2019-07-26 09:01:41.950', N'0')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'17', N'5', N'4234', N'Thuật ngữ “RAM” là từ viết tắt của cụm từ?', N'Read And Modify', N'Random Access Memory', N'Read Access Memory', N'Recent Access Memory', N'B', N'2019-07-26 09:02:43.620', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'18', N'5', N'324', N'Trong MSWord, khi muốn người sử dụng khác chỉ có thể thay đổi một số nội dung trong văn bản, bạn sử dụng chức năng nào sau đây:', N'Vào File -> Info -> Protect Document -> Restrict Editing', N'Vào File -> Info -> Protect Document -> Encrypt with Password', N'Vào File -> Info -> Protect Document -> Block User', N'Vào File -> Info -> Protect Document -> Mark as Final', N'A', N'2019-07-26 09:03:38.663', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'19', N'5', N'43', N'Để kết thúc việc trình diễn trong Power Point ta bấm: ', N'Phím F10', N'Phím Delete', N'Phím Enter', N'Phím ESC', N'D', N'2019-07-26 09:04:37.160', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'20', N'5', N'4234', N'Chữ viết tắt MB/s thường được đề cập trong lĩnh vực máy tính để nói về đối tượng nào sau đây', N'Tốc độ xử lý.', N'Độ phân giải màn hình.', N'Khả năng lưu trữ.', N'Tốc độ truyền dẫn dữ liệu.', N'D', N'2019-07-26 09:05:43.857', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'21', N'5', N'234', N'Trong Excel,Biểu thức sau cho giá trị là bao nhiêu? =AND(5>4, 6<9, 2<1)', N'# Value!', N'#Name?', N'True', N'False', N'D', N'2019-07-26 09:07:21.670', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[cauhoi] OFF
GO

-- ----------------------------
-- Table structure for chitiet_lop
-- ----------------------------
DROP TABLE [dbo].[chitiet_lop]
GO
CREATE TABLE [dbo].[chitiet_lop] (
[id_chitiet_lop] int NOT NULL IDENTITY(1,1) ,
[id_class] int NOT NULL ,
[id_student] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitiet_lop]', RESEED, 18)
GO

-- ----------------------------
-- Records of chitiet_lop
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitiet_lop] ON
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'2', N'1', N'4', N'2019-07-25 20:49:07.583')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'3', N'1', N'5', N'2019-07-25 20:49:10.587')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'4', N'1', N'9', N'2019-07-25 20:49:15.940')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'5', N'1', N'12', N'2019-07-25 20:49:18.753')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'6', N'5', N'12', N'2019-07-26 14:09:59.143')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'7', N'1', N'14', N'2019-07-31 13:47:20.187')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'8', N'1', N'13', N'2019-07-31 13:47:20.297')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'9', N'1', N'15', N'2019-07-31 13:47:20.303')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'10', N'1', N'16', N'2019-07-31 13:47:20.307')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'11', N'1', N'11', N'2019-07-31 13:47:20.380')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'12', N'1', N'3', N'2019-07-31 13:47:20.427')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'13', N'1', N'20', N'2019-07-31 13:47:21.143')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'14', N'1', N'17', N'2019-07-31 13:47:21.183')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'15', N'1', N'21', N'2019-07-31 13:47:21.433')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'16', N'1', N'18', N'2019-07-31 13:47:21.540')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'17', N'1', N'22', N'2019-07-31 13:47:21.727')
GO
GO
INSERT INTO [dbo].[chitiet_lop] ([id_chitiet_lop], [id_class], [id_student], [timestamps]) VALUES (N'18', N'1', N'19', N'2019-07-31 13:47:21.860')
GO
GO
SET IDENTITY_INSERT [dbo].[chitiet_lop] OFF
GO

-- ----------------------------
-- Table structure for chitietbaithi
-- ----------------------------
DROP TABLE [dbo].[chitietbaithi]
GO
CREATE TABLE [dbo].[chitietbaithi] (
[ID_chitiet] int NOT NULL IDENTITY(1,1) ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitietbaithi]', RESEED, 22)
GO

-- ----------------------------
-- Records of chitietbaithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitietbaithi] ON
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'1', N'925113', N'2', null)
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'2', N'925113', N'1', null)
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'3', N'925113', N'3', N'2019-07-19 09:03:03.800')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'4', N'925113', N'4', N'2019-07-19 09:03:09.213')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'5', N'925114', N'1', N'2019-07-22 07:31:01.670')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'7', N'925121', N'8', N'2019-07-26 15:14:40.597')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'8', N'925121', N'11', N'2019-07-26 15:14:58.400')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'9', N'925121', N'12', N'2019-07-26 15:15:02.107')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'10', N'925121', N'13', N'2019-07-26 15:15:05.397')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'11', N'925121', N'14', N'2019-07-26 15:15:11.393')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'12', N'925121', N'15', N'2019-07-26 15:15:14.310')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'13', N'925121', N'16', N'2019-07-26 15:15:17.767')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'14', N'925121', N'17', N'2019-07-26 15:15:20.600')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'15', N'925121', N'18', N'2019-07-26 15:15:24.160')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'16', N'925121', N'19', N'2019-07-26 15:15:26.987')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'17', N'925121', N'20', N'2019-07-26 15:15:33.530')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'18', N'925121', N'21', N'2019-07-26 15:15:36.253')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'19', N'925123', N'21', N'2019-07-30 20:00:19.840')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'20', N'925123', N'20', N'2019-07-30 20:00:35.293')
GO
GO
SET IDENTITY_INSERT [dbo].[chitietbaithi] OFF
GO

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE [dbo].[classes]
GO
CREATE TABLE [dbo].[classes] (
[id_class] int NOT NULL IDENTITY(1,1) ,
[class_name] nvarchar(60) NULL ,
[grade_name] nvarchar(60) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[classes]', RESEED, 6)
GO

-- ----------------------------
-- Records of classes
-- ----------------------------
SET IDENTITY_INSERT [dbo].[classes] ON
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'1', N'Bảng A', N'Khóa 2018', N'2019-07-25 20:10:40.000', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'3', N'Bảng C', N'Khóa 2018', N'2019-07-25 20:30:52.367', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'4', N'Bảng Cdasd', N'Khóa 2018dasd', N'2019-07-25 20:29:18.927', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'6', N'Bảng Cdasd', N'Khóa 2018dasds', N'2019-07-26 14:09:12.607', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[classes] OFF
GO

-- ----------------------------
-- Table structure for loaicauhoi
-- ----------------------------
DROP TABLE [dbo].[loaicauhoi]
GO
CREATE TABLE [dbo].[loaicauhoi] (
[id_loaicauhoi] int NOT NULL IDENTITY(1,1) ,
[tenloai] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[loaicauhoi]', RESEED, 6)
GO

-- ----------------------------
-- Records of loaicauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[loaicauhoi] ON
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'1', N'Luật Dân Sự', null, N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'2', N'Luật Hình Sự', null, N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'4', N'ahihi12', N'2019-07-25 16:12:56.743', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'5', N'HỘI THI TIN HỌC', N'2019-07-26 08:20:55.870', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'6', N'HỘI THI TIN HỌC DÀNH CHO CBCCVC TỈNH TÂY NINH', N'2019-07-26 14:13:40.100', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[loaicauhoi] OFF
GO

-- ----------------------------
-- Table structure for Login
-- ----------------------------
DROP TABLE [dbo].[Login]
GO
CREATE TABLE [dbo].[Login] (
[IDlogin] int NOT NULL ,
[IDUser] int NULL ,
[Token] nvarchar(MAX) NULL ,
[TimeLogin] date NULL 
)


GO

-- ----------------------------
-- Records of Login
-- ----------------------------

-- ----------------------------
-- Table structure for scores
-- ----------------------------
DROP TABLE [dbo].[scores]
GO
CREATE TABLE [dbo].[scores] (
[id_score] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[socaudung] int NULL ,
[tongcau] int NULL ,
[time_finish] datetime NULL ,
[diemthang10] float(53) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[scores]', RESEED, 16)
GO

-- ----------------------------
-- Records of scores
-- ----------------------------
SET IDENTITY_INSERT [dbo].[scores] ON
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10]) VALUES (N'2', N'4', N'925113', null, null, N'2019-07-03 20:32:02.577', null)
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10]) VALUES (N'13', N'5', N'925113', N'1', N'4', N'2019-08-21 10:39:58.000', N'2.5')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10]) VALUES (N'14', N'5', N'925114', N'0', N'1', N'2019-07-25 11:17:13.827', N'0')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10]) VALUES (N'15', N'22', N'925121', null, null, N'2019-07-30 21:37:28.883', null)
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10]) VALUES (N'16', N'3', N'925114', N'0', N'1', N'2019-07-31 16:52:42.143', N'0')
GO
GO
SET IDENTITY_INSERT [dbo].[scores] OFF
GO

-- ----------------------------
-- Table structure for specialities
-- ----------------------------
DROP TABLE [dbo].[specialities]
GO
CREATE TABLE [dbo].[specialities] (
[id_speciality] int NOT NULL IDENTITY(1,1) ,
[speciality_name] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO

-- ----------------------------
-- Records of specialities
-- ----------------------------
SET IDENTITY_INSERT [dbo].[specialities] ON
GO
INSERT INTO [dbo].[specialities] ([id_speciality], [speciality_name], [timestamps]) VALUES (N'1', N'Luật', null)
GO
GO
SET IDENTITY_INSERT [dbo].[specialities] OFF
GO

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE [dbo].[statuses]
GO
CREATE TABLE [dbo].[statuses] (
[id_status] int NOT NULL IDENTITY(1,1) ,
[status_name] nvarchar(50) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[statuses]', RESEED, 2)
GO

-- ----------------------------
-- Records of statuses
-- ----------------------------
SET IDENTITY_INSERT [dbo].[statuses] ON
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'1', N'Open', N'2019-07-03 20:25:47.597')
GO
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'2', N'Closed', N'2019-07-03 20:25:51.603')
GO
GO
SET IDENTITY_INSERT [dbo].[statuses] OFF
GO

-- ----------------------------
-- Table structure for student_test_detail
-- ----------------------------
DROP TABLE [dbo].[student_test_detail]
GO
CREATE TABLE [dbo].[student_test_detail] (
[ID] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[student_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[student_test_detail]', RESEED, 105)
GO

-- ----------------------------
-- Records of student_test_detail
-- ----------------------------
SET IDENTITY_INSERT [dbo].[student_test_detail] ON
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'1', N'3', N'925113', N'1', null, null)
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'2', N'3', N'925113', N'2', null, null)
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'3', N'4', N'925113', N'1', null, null)
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'4', N'4', N'925113', N'2', N'B', null)
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'7', N'5', N'925113', N'4', N'A', N'2019-07-26 22:46:37.730')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'8', N'5', N'925113', N'3', N'C', null)
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'9', N'5', N'925113', N'2', null, N'2019-07-19 14:09:55.930')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'10', N'5', N'925113', N'1', null, N'2019-07-19 14:09:55.930')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'11', N'57', N'925113', N'2', null, N'2019-07-19 14:10:58.213')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'12', N'57', N'925113', N'4', null, N'2019-07-19 14:10:58.213')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'13', N'57', N'925113', N'1', null, N'2019-07-19 14:10:58.213')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'14', N'57', N'925113', N'3', null, N'2019-07-19 14:10:58.213')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'17', N'5', N'925114', N'5', null, N'2019-07-22 07:32:12.120')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'19', N'22', N'925121', N'8', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'20', N'22', N'925121', N'19', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'21', N'22', N'925121', N'15', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'22', N'22', N'925121', N'16', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'23', N'22', N'925121', N'18', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'24', N'22', N'925121', N'12', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'25', N'22', N'925121', N'20', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'26', N'22', N'925121', N'13', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'27', N'22', N'925121', N'14', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'28', N'22', N'925121', N'21', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'29', N'22', N'925121', N'17', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'30', N'22', N'925121', N'11', null, N'2019-07-26 15:17:09.913')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'79', N'21', N'925121', N'8', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'80', N'21', N'925121', N'18', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'81', N'21', N'925121', N'17', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'82', N'21', N'925121', N'13', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'83', N'21', N'925121', N'20', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'84', N'21', N'925121', N'11', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'85', N'21', N'925121', N'21', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'86', N'21', N'925121', N'15', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'87', N'21', N'925121', N'12', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'88', N'21', N'925121', N'16', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'89', N'21', N'925121', N'19', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'90', N'21', N'925121', N'14', null, N'2019-07-26 16:04:37.847')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'91', N'3', N'925114', N'1', null, N'2019-07-31 16:40:52.633')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'92', N'4', N'925114', N'1', null, N'2019-08-01 10:50:34.303')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'93', N'11', N'925114', N'1', null, N'2019-08-01 10:50:34.310')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'94', N'12', N'925114', N'1', null, N'2019-08-01 10:50:34.950')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'95', N'16', N'925114', N'1', null, N'2019-08-01 10:50:35.270')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'96', N'14', N'925114', N'1', null, N'2019-08-01 10:50:35.410')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'97', N'13', N'925114', N'1', null, N'2019-08-01 10:50:35.483')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'98', N'9', N'925114', N'1', null, N'2019-08-01 10:50:35.573')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'99', N'18', N'925114', N'1', null, N'2019-08-01 10:50:35.677')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'100', N'15', N'925114', N'1', null, N'2019-08-01 10:50:35.760')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'101', N'20', N'925114', N'1', null, N'2019-08-01 10:50:35.863')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'102', N'19', N'925114', N'1', null, N'2019-08-01 10:50:35.990')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'103', N'22', N'925114', N'1', null, N'2019-08-01 10:50:36.930')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'104', N'17', N'925114', N'1', null, N'2019-08-01 10:50:36.187')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'105', N'21', N'925114', N'1', null, N'2019-08-01 10:50:36.397')
GO
GO
SET IDENTITY_INSERT [dbo].[student_test_detail] OFF
GO

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE [dbo].[students]
GO
CREATE TABLE [dbo].[students] (
[id_student] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NOT NULL ,
[password] varchar(32) NOT NULL ,
[email] varchar(100) NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NOT NULL ,
[birthday] date NOT NULL ,
[phone] varchar(45) NULL ,
[cmnd] nvarchar(15) NULL ,
[sbd] nvarchar(15) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL ,
[donvi] nvarchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[students]', RESEED, 22)
GO

-- ----------------------------
-- Records of students
-- ----------------------------
SET IDENTITY_INSERT [dbo].[students] ON
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'4', N'sinhvien2', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien2@gmail.com', N'avatar-default.jpg', N'BBB', N'Nam', N'1997-01-01', null, N'1', N'1', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'5', N'sinhvien3', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien@gmail.com', N'avatar-default.jpg', N'sinhvien3', N'Nam', N'1990-01-01', null, N'1', N'1', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'9', N'sinhvien2312', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien2@gmail.com', N'avatar-default.jpg', N'Son', N'Nam', N'1997-01-01', null, N'1', N'1', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'11', N'dondvl', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien2@gmail.com', N'avatar-default.jpg', N'Son', N'Nam', N'1997-01-01', null, N'1', N'1', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'12', N'sondvl1204qqq', N'e33d974aae13e4d877477d51d8bafdc4', N'sinhvien2@gmail.com11111', N'avatar-default.jpg', N'Son', N'Nam', N'1997-01-01', N'0987654321', N'1', N'1', null, null, N'ahihi đồ ngốc')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'13', N'sondvl', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, N'291013719', N'SBD123', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'14', N'demo1', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn1', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'15', N'demo2', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn2', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'16', N'demo3', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn3', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'17', N'demo4', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn4', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'18', N'demo5', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'19', N'demo6', N'e10adc3949ba59abbe56e057f20f883e', N'sondoan1204@gmail.com', N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, N'291013719', N'SBD1233', null, null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'20', N'demo67', N'e10adc3949ba59abbe56e057f20f883e', null, N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, null, N'SBD1233', null, null, N'dsadsadsa')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'21', N'demo9', N'e10adc3949ba59abbe56e057f20f883e', null, N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, null, N'SBD1233', null, null, N'dsadsadsa')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'22', N'demo10', N'e10adc3949ba59abbe56e057f20f883e', null, N'avatar-default.jpg', N'Sơn Đoàn', N'Nam', N'1993-04-12', null, null, N'SBD1233', N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImRlbW8xMCIsInJvbGUiOiJVc2VyIiwibmJmIjoxNTY0NDk1MDA1LCJleHAiOjE1NjQ1MDU4MDUsImlhdCI6MTU2NDQ5NTAwNX0.JQfeYaWG0A3Zme4wB2D83a2GO-kDWUaTNtJZWJ0xFoo', N'2019-07-30 20:56:45.187', N'dsadsadsa')
GO
GO
SET IDENTITY_INSERT [dbo].[students] OFF
GO

-- ----------------------------
-- Procedure structure for CheckThoiGianLamBai
-- ----------------------------
DROP PROCEDURE [dbo].[CheckThoiGianLamBai]
GO
CREATE PROCEDURE [dbo].[CheckThoiGianLamBai] @MaThiSinh INT,
@TestCode INT 
AS
begin  
declare @setval int  
select @setval= dbo.[fnTime](@MaThiSinh, @TestCode)  
if(@setval>0)
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
End
ELSE BEGIN
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
CAST(null AS int)socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
CAST(null AS float)diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
end 
End
GO

-- ----------------------------
-- Procedure structure for GetAllCauHoi
-- ----------------------------
DROP PROCEDURE [dbo].[GetAllCauHoi]
GO
CREATE  PROCEDURE [dbo].[GetAllCauHoi]
AS
SELECT
--ROW_NUMBER() OVER(ORDER BY dbo.cauhoi.timestamps DESC)AS sothutu,
cast (ROW_NUMBER() over(ORDER BY dbo.cauhoi.timestamps DESC) as int) Rank,
dbo.cauhoi.id_cauhoi as IdCauhoi,
dbo.cauhoi.id_loaicauhoi as IdLoaicauhoi,
dbo.cauhoi.img_content as ImgContent,
dbo.cauhoi.content as Content,
dbo.cauhoi.answer_a as AnswerA,
dbo.cauhoi.answer_b as AnswerB,
dbo.cauhoi.answer_c as AnswerC,
dbo.cauhoi.answer_d as AnswerD,
dbo.cauhoi.correct_answer as CorrectAnswer,
dbo.cauhoi.timestamps as Timestamps,
dbo.loaicauhoi.tenloai as TenLoai
FROM
dbo.cauhoi
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.cauhoi.del_stt = 1
ORDER BY dbo.cauhoi.timestamps DESC

GO

-- ----------------------------
-- Procedure structure for GetChiTietBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetChiTietBaiThi]
GO
CREATE PROCEDURE [dbo].[GetChiTietBaiThi] @MaThiSinh int, @TestCode int
AS
SELECT
dbo.student_test_detail.ID,
dbo.student_test_detail.id_student,
dbo.student_test_detail.test_code,
dbo.student_test_detail.id_cauhoi,
dbo.cauhoi.img_content,
dbo.cauhoi.content,
dbo.cauhoi.answer_a,
dbo.cauhoi.answer_b,
dbo.cauhoi.answer_c,
dbo.cauhoi.answer_d,
dbo.cauhoi.id_loaicauhoi,
dbo.loaicauhoi.tenloai,
dbo.baithi.test_name,
dbo.student_test_detail.student_answer

FROM
dbo.student_test_detail
INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
INNER JOIN dbo.baithi ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.student_test_detail.test_code = @TestCode AND
dbo.student_test_detail.id_student = @MaThiSinh
ORDER BY dbo.student_test_detail.ID

GO

-- ----------------------------
-- Procedure structure for GetDanhSachBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachBaiThi]
GO
CREATE PROCEDURE [dbo].[GetDanhSachBaiThi] @MaThiSinh int
AS
SELECT DISTINCT
dbo.baithi.test_name,
dbo.baithi.total_questions,
dbo.student_test_detail.id_student,
dbo.baithi.test_code,
dbo.baithi.time_to_do,
dbo.baithi.id_status,
dbo.statuses.status_name,
dbo.baithi.note

FROM
dbo.baithi
INNER JOIN dbo.student_test_detail ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.statuses ON dbo.baithi.id_status = dbo.statuses.id_status
WHERE
dbo.baithi.id_status=1 AND dbo.baithi.del_stt = 1 AND
dbo.student_test_detail.id_student = @MaThiSinh

GO

-- ----------------------------
-- Procedure structure for GetDanhSachKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachKetQua]
GO
CREATE PROCEDURE [dbo].[GetDanhSachKetQua]
 @TestCode INT 
AS 
BEGIN
SELECT
cast (ROW_NUMBER() over(ORDER BY dbo.students.sbd DESC) as int) Rank,
dbo.students.id_student as IdStudent,
dbo.students.username as Username,
dbo.students.email as Email,
dbo.students.name as Name,
dbo.students.gender as Gender,
dbo.students.birthday as Birthday,
dbo.students.phone as Phone,
dbo.students.cmnd as Cmnd,
dbo.students.sbd as Sbd,
dbo.students.timelogin as Timelogin,
dbo.students.donvi as Donvi,
dbo.scores.socaudung as Socaudung,
dbo.scores.tongcau as TongCau,
dbo.scores.time_finish as TimeFinish,
dbo.scores.diemthang10 as Diemthang10,
dbo.scores.test_code as TestCode
FROM
dbo.students
INNER JOIN dbo.scores ON dbo.students.id_student = dbo.scores.id_student
WHERE
dbo.scores.test_code =@TestCode

END

GO

-- ----------------------------
-- Procedure structure for GetDanhSachThiSinhTheoLop
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop]
GO
CREATE PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop] @MaLop int
AS
SELECT
dbo.students.name,
dbo.students.gender,
dbo.students.donvi,
dbo.students.sbd,
dbo.students.avatar,
dbo.students.email,
dbo.students.username,
dbo.chitiet_lop.id_chitiet_lop,
dbo.chitiet_lop.id_class,
dbo.classes.class_name,
dbo.classes.grade_name,
dbo.students.id_student

FROM
dbo.chitiet_lop
INNER JOIN dbo.classes ON dbo.classes.id_class = dbo.chitiet_lop.id_class
INNER JOIN dbo.students ON dbo.students.id_student = dbo.chitiet_lop.id_student
WHERE
dbo.classes.id_class = @MaLop
ORDER BY
dbo.students.name ASC

GO

-- ----------------------------
-- Procedure structure for GetKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetKetQua]
GO
CREATE PROCEDURE [dbo].[GetKetQua] @MaThiSinh INT,
 @TestCode INT 
AS 
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
End
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode


GO

-- ----------------------------
-- Procedure structure for GetThoiGianThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetThoiGianThi]
GO
CREATE PROCEDURE [dbo].[GetThoiGianThi] @MaThiSinh INT,
 @TestCode INT , @time_finish int
AS
IF NOT EXISTS ( SELECT * FROM scores 
                   WHERE id_student = @MaThiSinh
                   AND test_code = @TestCode)
BEGIN
	INSERT INTO scores (
	id_student,
	test_code,
	time_finish
	)
VALUES
	(
		@MaThiSinh,
		@TestCode,
		DATEADD(MINUTE,@time_finish,GETDATE())
	);
END
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode


GO

-- ----------------------------
-- Procedure structure for ScoresSelectInsertUpdate
-- ----------------------------
DROP PROCEDURE [dbo].[ScoresSelectInsertUpdate]
GO
CREATE PROCEDURE [dbo].[ScoresSelectInsertUpdate] (
	@id_stduent int,
	@test_code int,
	@time_finish int,
	@StatementType nvarchar (20) = ''
) AS
BEGIN
IF @StatementType = 'Insert'
BEGIN
	INSERT INTO scores (
	id_student,
	test_code,
	time_finish
	)
VALUES
	(
		@id_stduent,
		@test_code,
		DATEADD(MINUTE,@time_finish,GETDATE())
	)
END
IF @StatementType = 'Select'
BEGIN
	SELECT
		*
	FROM
		scores
	WHERE
		id_student=@id_stduent and test_code=@test_code
	END
	IF @StatementType = 'Update'
	BEGIN
		UPDATE scores
	SET score_number = dbo.fnGetCauDung(@id_stduent,@test_code)
WHERE
	id_student=@id_stduent and test_code=@test_code
END
ELSE
IF @StatementType = 'Delete'
BEGIN
	DELETE
FROM
	scores
WHERE
	id_student=@id_stduent and test_code=@test_code
END
END

GO

-- ----------------------------
-- Procedure structure for TaoDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[TaoDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[TaoDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
INSERT INTO student_test_detail (id_student, test_code, id_cauhoi)
SELECT
@MaThisinh as id_students,
dbo.chitietbaithi.test_code,
dbo.cauhoi.id_cauhoi
FROM
dbo.cauhoi
INNER JOIN dbo.chitietbaithi ON dbo.chitietbaithi.id_cauhoi = dbo.cauhoi.id_cauhoi
WHERE
dbo.chitietbaithi.test_code=@MaCode
ORDER BY NEWID()
END

GO

-- ----------------------------
-- Procedure structure for ThongKeHome
-- ----------------------------
DROP PROCEDURE [dbo].[ThongKeHome]
GO
CREATE PROC [dbo].[ThongKeHome]
AS
BEGIN
	IF OBJECT_ID('tempdb..#tthongke') IS NOT NULL DROP TABLE #ttthongke
	CREATE TABLE #tthongke(idkey int,title nvarchar(40), total int)
	INSERT INTO #tthongke SELECT 1,N'Bài thi',(SELECT COUNT(*) FROM baithi WHERE del_stt = 1) GO
	INSERT INTO #tthongke SELECT 2,N'Câu hỏi',(SELECT COUNT(*) FROM cauhoi WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 3,N'Thí sinh',(SELECT COUNT(*) FROM students)GO
	INSERT INTO #tthongke SELECT 4,N'Bảng',(SELECT COUNT(*) FROM classes WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 5,N'Loại Câu hỏi',(SELECT COUNT(*) FROM loaicauhoi)GO
	SELECT * FROM #tthongke 
END


GO

-- ----------------------------
-- Procedure structure for XoaDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[XoaDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[XoaDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
DELETE FROM [dbo].[student_test_detail] Where id_student = @MaThisinh AND test_code=@MaCode
END

GO

-- ----------------------------
-- Function structure for fnGetCauDung
-- ----------------------------
DROP FUNCTION [dbo].[fnGetCauDung]
GO
CREATE FUNCTION [dbo].[fnGetCauDung] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		AND UPPER(dbo.student_test_detail.student_answer) = UPPER(dbo.cauhoi.correct_answer)
		RETURN @NumCount
		END

GO

-- ----------------------------
-- Function structure for fnGetTongCau
-- ----------------------------
DROP FUNCTION [dbo].[fnGetTongCau]
GO
CREATE FUNCTION [dbo].[fnGetTongCau] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		RETURN @NumCount
		END

GO

-- ----------------------------
-- Function structure for fnTime
-- ----------------------------
DROP FUNCTION [dbo].[fnTime]
GO
CREATE FUNCTION [dbo].[fnTime] (@MaThiSinh INT,
@TestCode INT ) RETURNS INT AS
BEGIN
	DECLARE
		@TimeCount INT
SELECT
@TimeCount=DATEDIFF(MINUTE, dbo.scores.time_finish,GETDATE())
FROM
dbo.scores
WHERE
dbo.scores.id_student=@MaThiSinh AND
dbo.scores.test_code=@TestCode
RETURN @TimeCount
		END

GO

-- ----------------------------
-- Indexes structure for table __EFMigrationsHistory
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table __EFMigrationsHistory
-- ----------------------------
ALTER TABLE [dbo].[__EFMigrationsHistory] ADD PRIMARY KEY ([MigrationId])
GO

-- ----------------------------
-- Indexes structure for table admins
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD PRIMARY KEY ([id_admin])
GO

-- ----------------------------
-- Uniques structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD UNIQUE ([username] ASC, [email] ASC)
GO

-- ----------------------------
-- Indexes structure for table baithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table baithi
-- ----------------------------
ALTER TABLE [dbo].[baithi] ADD PRIMARY KEY ([test_code])
GO

-- ----------------------------
-- Indexes structure for table cauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table cauhoi
-- ----------------------------
ALTER TABLE [dbo].[cauhoi] ADD PRIMARY KEY ([id_cauhoi])
GO

-- ----------------------------
-- Indexes structure for table chitiet_lop
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitiet_lop
-- ----------------------------
ALTER TABLE [dbo].[chitiet_lop] ADD PRIMARY KEY ([id_chitiet_lop])
GO

-- ----------------------------
-- Indexes structure for table chitietbaithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitietbaithi
-- ----------------------------
ALTER TABLE [dbo].[chitietbaithi] ADD PRIMARY KEY ([ID_chitiet])
GO

-- ----------------------------
-- Indexes structure for table classes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table classes
-- ----------------------------
ALTER TABLE [dbo].[classes] ADD PRIMARY KEY ([id_class])
GO

-- ----------------------------
-- Indexes structure for table loaicauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table loaicauhoi
-- ----------------------------
ALTER TABLE [dbo].[loaicauhoi] ADD PRIMARY KEY ([id_loaicauhoi])
GO

-- ----------------------------
-- Indexes structure for table Login
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Login
-- ----------------------------
ALTER TABLE [dbo].[Login] ADD PRIMARY KEY ([IDlogin])
GO

-- ----------------------------
-- Indexes structure for table scores
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table scores
-- ----------------------------
ALTER TABLE [dbo].[scores] ADD PRIMARY KEY ([id_score], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table specialities
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table specialities
-- ----------------------------
ALTER TABLE [dbo].[specialities] ADD PRIMARY KEY ([id_speciality])
GO

-- ----------------------------
-- Indexes structure for table statuses
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table statuses
-- ----------------------------
ALTER TABLE [dbo].[statuses] ADD PRIMARY KEY ([id_status])
GO

-- ----------------------------
-- Indexes structure for table student_test_detail
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table student_test_detail
-- ----------------------------
ALTER TABLE [dbo].[student_test_detail] ADD PRIMARY KEY ([ID], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table students
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD PRIMARY KEY ([id_student])
GO

-- ----------------------------
-- Uniques structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD UNIQUE ([username] ASC, [email] ASC)
GO
