/*
Navicat SQL Server Data Transfer

Source Server         : TracNghiemBanTuyenGiao-149
Source Server Version : 105000
Source Host           : 10.184.254.149:1433
Source Database       : TracNghiemBanTuyenGiao
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 105000
File Encoding         : 65001

Date: 2019-10-20 19:55:43
*/


-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE [dbo].[admins]
GO
CREATE TABLE [dbo].[admins] (
[id_admin] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NOT NULL ,
[password] varchar(32) NOT NULL ,
[email] varchar(100) NOT NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NULL ,
[birthday] date NULL ,
[phone] varchar(45) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL 
)


GO

-- ----------------------------
-- Records of admins
-- ----------------------------
SET IDENTITY_INSERT [dbo].[admins] ON
GO
INSERT INTO [dbo].[admins] ([id_admin], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [token], [timelogin]) VALUES (N'1', N'admin', N'ceea23519f6f86ad67e9f798bf8002cb', N'admin@gmail.com', N'avatar-default.jpg', N'admin', N'nam', N'1990-01-01', null, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NzEzNjAxMzMsImV4cCI6MTYwNzM2MDEzMywiaWF0IjoxNTcxMzYwMTMzfQ.nBRK42kpYgVb1hwOUygBTSlwWt9d4cqqQGMmYm4wSF4', N'2019-10-18 07:55:33.453')
GO
GO
SET IDENTITY_INSERT [dbo].[admins] OFF
GO

-- ----------------------------
-- Table structure for baithi
-- ----------------------------
DROP TABLE [dbo].[baithi]
GO
CREATE TABLE [dbo].[baithi] (
[test_name] nvarchar(255) NOT NULL ,
[test_code] int NOT NULL IDENTITY(1,1) ,
[password] varchar(32) NOT NULL ,
[total_questions] int NOT NULL ,
[time_to_do] int NOT NULL ,
[note] nvarchar(MAX) NULL ,
[id_status] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[baithi]', RESEED, 925134)
GO

-- ----------------------------
-- Records of baithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[baithi] ON
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Tìm hiểu 60 năm Chiến thắng Tua Hai - Câu hỏi tháng 10', N'925134', N'', N'4', N'999999', N'', N'1', N'2019-10-17 14:47:41.693', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[baithi] OFF
GO

-- ----------------------------
-- Table structure for cauhoi
-- ----------------------------
DROP TABLE [dbo].[cauhoi]
GO
CREATE TABLE [dbo].[cauhoi] (
[id_cauhoi] int NOT NULL IDENTITY(1,1) ,
[id_loaicauhoi] int NOT NULL ,
[img_content] nvarchar(MAX) NULL ,
[content] nvarchar(MAX) NULL ,
[answer_a] nvarchar(MAX) NULL ,
[answer_b] nvarchar(MAX) NULL ,
[answer_c] nvarchar(MAX) NULL ,
[answer_d] nvarchar(MAX) NULL ,
[correct_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[cauhoi]', RESEED, 259)
GO

-- ----------------------------
-- Records of cauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cauhoi] ON
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'256', N'7', null, N'Tháng 02/1957, Ban Địch tình Tỉnh uỷ Tây Ninh được Ban Địch tình Xứ uỷ giao nhiệm vụ diệt Ngô Đình Diệm, tạo nên cục diện tình hình có lợi cho cách mạng Miền Nam. Ông (bà) hãy cho biết, nhiệm vụ tổ chức diệt Ngô Đình Diệm đã được Ban Địch tình Tỉnh ủy giao cho ai trực tiếp thực hiện?', N'Đồng chí Trần Minh Tri', N'Đồng chí Nguyễn Thị Văn', N'Đồng chí Hà Minh Trí', N'Đồng chí Bảy Nhanh', N'A', N'2019-10-17 14:41:15.507', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'257', N'7', null, N'Tháng 10/1959, đồng chí Nguyễn Văn Linh, Bí thư Xứ uỷ Nam bộ, nhận được điện của Ban Bí thư Trung ương Đảng về nội dung cơ bản của Nghị quyết Hội nghị Ban Chấp hành Trung ương Đảng lần thứ 15. Ông (bà) hãy cho biết biết, nội dung cơ bản của Nghị quyết này là gì?', N'Con đường phát triển cơ bản cùa cách mạng Việt Nam ở miền Nam là khởi nghĩa giành chính quyền về tay Nhân dân. Lấy sức mạnh của quần chúng kết hợp lực lượng vũ trang đê đánh đổ quyền thống trị cùa để quốc và phong kiến, dựng lên chính quyền cách mạng cùa Nhân dân.', N'Con đường phát triển cơ bản cùa cách mạng Việt Nam ở miên Nam lậ dựa vào lực lượng chính trị của quan chúng là chủ yếu, kết hợp lực lượng vũ trang đê đánh đô quyên thông trị của để quốc và phong kiến, dựng lên chính quyền cách mạng của Nhân dân.', N'Con đường phát triển cơ bản cùa cách mạng Việt Nam ở miền Nam là khởi nghĩa giành chính quyền về tay Nhân dân. Theo tỉnh hình cụ thể và yêu cầu hiện nay cùa cách mạng, thì con đường đó là lấy sức mạnh cùa quần chúng, dựa vào lực lượng chính trị cùa quân chúng là chù yếu, kết hợp lực lượng vũ trang để đánh đồ quyền thống trị cùa đế quốc và phong kiến, dựng lên chính quyền cách mạng cùa Nhân dân.', N'Cà 3 đáp án trên đều đúng', N'D', N'2019-10-17 14:42:44.230', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'258', N'7', null, N'Tháng 11/1959, Hội nghị Xứ uỷ Nam bộ đã nghiên cứu, quán triệt và bàn biên pháp tổ chức thực hiện Nghị quyết Hội nghị lần thứ 15 Ban Chấp hành Trung ương Đảng, đề ra nhiệm vụ trước mắt của cách mạng miền Nam. Ông (bà) hãy cho biết, đó là nhiệm vụ gì?', N'Giữ vừng và đẩy mạnh phong trào cách mạng cùa quần chúng, lấy đấu tranh chính trị rộng rãi và mạnh mẽ cùa quần chúng làm chính, đông thời kêt hợp với hoạt động vũ trang tuyên truyền để chống chính sách khủng bố tàn bạo, chính sách bóc lột, vơ vét của Mỹ-Diệm', N'Ngăn chặn và đẩy lùi từng bước mọi chính sách cùa địch, đẩy địch vào thế bị động, cô lập hơn nữa về chính trị, tạo điều kiện thuận lợi tiến tới đánh đô chính quyền Mỹ-Diệm.', N'Ra sức xây dựng, cùng cố và phát triển lực lượng cách mạng, tăng cường sự lãnh đạo của Đảng, chuẩn bị sẵn sàng nắm lấy thời cơ đánh bại hoàn toàn kẻ địch”', N'Cả 3 đáp án trên đều đúng.', N'D', N'2019-10-17 14:44:06.490', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'259', N'7', null, N'Thực hiện Nghị quyết Hội nghị lần thứ 15 Ban Chấp hành Trung ương Đảng, Ban Quân sự Miền đề ra các phương án để đánh cho được một trận chấn động, mờ màn cho Nhân dân đồng loạt khởi nghĩa giành chinh quyền ở nông thôn. Ông (bà) hãy cho biết, nội dung của các phương án này là gì?', N'Đánh từ 1-2 quận lỵ và 4-5 cứ điểm', N'Đánh căn cứ Tua Hai và 1-2 quận lỵ, cứ diểm', N'Đánh căn cứ Tua Hai', N'Đáp án a và c đúng.', N'D', N'2019-10-17 14:46:05.937', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[cauhoi] OFF
GO

-- ----------------------------
-- Table structure for chitiet_lop
-- ----------------------------
DROP TABLE [dbo].[chitiet_lop]
GO
CREATE TABLE [dbo].[chitiet_lop] (
[id_chitiet_lop] int NOT NULL IDENTITY(1,1) ,
[id_class] int NOT NULL ,
[id_student] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitiet_lop]', RESEED, 18)
GO

-- ----------------------------
-- Records of chitiet_lop
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitiet_lop] ON
GO
SET IDENTITY_INSERT [dbo].[chitiet_lop] OFF
GO

-- ----------------------------
-- Table structure for chitietbaithi
-- ----------------------------
DROP TABLE [dbo].[chitietbaithi]
GO
CREATE TABLE [dbo].[chitietbaithi] (
[ID_chitiet] int NOT NULL IDENTITY(1,1) ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitietbaithi]', RESEED, 224)
GO

-- ----------------------------
-- Records of chitietbaithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitietbaithi] ON
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'221', N'925134', N'259', N'2019-10-17 14:49:23.570')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'222', N'925134', N'258', N'2019-10-17 14:49:23.570')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'223', N'925134', N'256', N'2019-10-17 14:49:23.977')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'224', N'925134', N'257', N'2019-10-17 14:49:23.997')
GO
GO
SET IDENTITY_INSERT [dbo].[chitietbaithi] OFF
GO

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE [dbo].[classes]
GO
CREATE TABLE [dbo].[classes] (
[id_class] int NOT NULL IDENTITY(1,1) ,
[class_name] nvarchar(60) NULL ,
[grade_name] nvarchar(60) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[classes]', RESEED, 9)
GO

-- ----------------------------
-- Records of classes
-- ----------------------------
SET IDENTITY_INSERT [dbo].[classes] ON
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'7', N'Bảng A', N'2019', N'2019-09-09 07:48:28.770', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'8', N'Bảng B', N'2019', N'2019-09-09 07:48:34.157', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'9', N'Bảng C', N'2019', N'2019-09-09 07:48:39.597', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[classes] OFF
GO

-- ----------------------------
-- Table structure for loaicauhoi
-- ----------------------------
DROP TABLE [dbo].[loaicauhoi]
GO
CREATE TABLE [dbo].[loaicauhoi] (
[id_loaicauhoi] int NOT NULL IDENTITY(1,1) ,
[tenloai] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[loaicauhoi]', RESEED, 13)
GO

-- ----------------------------
-- Records of loaicauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[loaicauhoi] ON
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'7', N'Câu hỏi tua hai', N'2019-09-09 07:48:10.510', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[loaicauhoi] OFF
GO

-- ----------------------------
-- Table structure for Login
-- ----------------------------
DROP TABLE [dbo].[Login]
GO
CREATE TABLE [dbo].[Login] (
[IDlogin] int NOT NULL ,
[IDUser] int NULL ,
[Token] nvarchar(MAX) NULL ,
[TimeLogin] date NULL 
)


GO

-- ----------------------------
-- Records of Login
-- ----------------------------

-- ----------------------------
-- Table structure for scores
-- ----------------------------
DROP TABLE [dbo].[scores]
GO
CREATE TABLE [dbo].[scores] (
[id_score] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[socaudung] int NULL ,
[tongcau] int NULL ,
[time_finish] datetime NULL ,
[diemthang10] float(53) NULL ,
[dudoanketqua] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[scores]', RESEED, 156)
GO

-- ----------------------------
-- Records of scores
-- ----------------------------
SET IDENTITY_INSERT [dbo].[scores] ON
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'121', N'207', N'925134', N'4', N'4', N'2019-10-17 14:51:21.660', N'10', N'798998')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'122', N'208', N'925134', N'2', N'4', N'2019-10-17 14:56:50.653', N'5', N'6777788')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'123', N'209', N'925134', N'0', N'4', N'2019-10-17 14:59:21.770', N'0', N'987654376')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'124', N'210', N'925134', N'0', N'4', N'2019-10-17 15:31:06.190', N'0', N'9000')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'125', N'211', N'925134', N'0', N'4', N'2019-10-17 15:32:36.560', N'0', N'125478')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'126', N'212', N'925134', N'0', N'4', N'2019-10-17 15:37:55.830', N'0', N'987441')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'127', N'213', N'925134', N'0', N'4', N'2019-10-17 15:39:46.433', N'0', N'98745512')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'128', N'214', N'925134', N'0', N'4', N'2019-10-17 15:41:25.273', N'0', N'321321')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'129', N'215', N'925134', N'0', N'4', N'2019-10-17 15:43:17.753', N'0', N'333113')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'130', N'216', N'925134', N'0', N'4', N'2019-10-17 15:45:11.643', N'0', N'122')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'131', N'217', N'925134', N'3', N'4', N'2019-10-17 16:24:14.713', N'7.5', N'223113')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'132', N'218', N'925134', N'3', N'4', N'2019-10-17 16:26:07.613', N'7.5', N'500')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'133', N'219', N'925134', N'1', N'4', N'2019-10-17 16:26:41.400', N'2.5', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'134', N'220', N'925134', N'0', N'4', N'2019-10-17 16:28:22.747', N'0', N'300')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'135', N'221', N'925134', N'1', N'4', N'2019-10-17 16:30:28.707', N'2.5', N'767')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'136', N'222', N'925134', N'1', N'4', N'2019-10-17 16:34:46.217', N'2.5', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'137', N'223', N'925134', N'0', N'4', N'2019-10-17 16:38:23.777', N'0', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'138', N'224', N'925134', N'1', N'4', N'2019-10-18 07:32:19.640', N'2.5', N'20000')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'139', N'225', N'925134', N'4', N'4', N'2019-10-18 07:36:37.590', N'10', N'23333')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'140', N'226', N'925134', N'3', N'4', N'2019-10-18 07:43:13.180', N'7.5', N'75645')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'141', N'227', N'925134', N'1', N'4', N'2019-10-18 07:44:51.993', N'2.5', N'20000')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'142', N'228', N'925134', N'2', N'4', N'2019-10-18 07:54:46.223', N'5', N'1000')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'143', N'229', N'925134', N'2', N'4', N'2019-10-18 08:07:22.270', N'5', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'144', N'230', N'925134', N'0', N'4', N'2019-10-18 08:10:55.827', N'0', N'122')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'145', N'231', N'925134', null, null, null, null, null)
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'146', N'232', N'925134', N'0', N'4', N'2019-10-18 08:32:51.730', N'0', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'147', N'233', N'925134', N'2', N'4', N'2019-10-18 08:35:06.803', N'5', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'148', N'234', N'925134', N'1', N'4', N'2019-10-18 08:36:13.127', N'2.5', N'900')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'149', N'235', N'925134', N'3', N'4', N'2019-10-18 08:39:20.240', N'7.5', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'150', N'236', N'925134', N'1', N'4', N'2019-10-18 08:51:55.100', N'2.5', N'10022')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'151', N'237', N'925134', null, null, null, null, null)
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'152', N'238', N'925134', N'2', N'4', N'2019-10-18 09:03:42.830', N'5', N'99')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'153', N'239', N'925134', N'4', N'4', N'2019-10-18 09:17:07.100', N'10', N'100')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'154', N'240', N'925134', N'1', N'4', N'2019-10-18 09:22:19.990', N'2.5', N'9000')
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'155', N'241', N'925134', null, null, null, null, null)
GO
GO
INSERT INTO [dbo].[scores] ([id_score], [id_student], [test_code], [socaudung], [tongcau], [time_finish], [diemthang10], [dudoanketqua]) VALUES (N'156', N'242', N'925134', N'0', N'4', N'2019-10-18 12:12:06.230', N'0', N'1234')
GO
GO
SET IDENTITY_INSERT [dbo].[scores] OFF
GO

-- ----------------------------
-- Table structure for specialities
-- ----------------------------
DROP TABLE [dbo].[specialities]
GO
CREATE TABLE [dbo].[specialities] (
[id_speciality] int NOT NULL IDENTITY(1,1) ,
[speciality_name] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO

-- ----------------------------
-- Records of specialities
-- ----------------------------
SET IDENTITY_INSERT [dbo].[specialities] ON
GO
SET IDENTITY_INSERT [dbo].[specialities] OFF
GO

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE [dbo].[statuses]
GO
CREATE TABLE [dbo].[statuses] (
[id_status] int NOT NULL IDENTITY(1,1) ,
[status_name] nvarchar(50) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[statuses]', RESEED, 2)
GO

-- ----------------------------
-- Records of statuses
-- ----------------------------
SET IDENTITY_INSERT [dbo].[statuses] ON
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'1', N'Open', N'2019-07-03 20:25:47.597')
GO
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'2', N'Closed', N'2019-07-03 20:25:51.603')
GO
GO
SET IDENTITY_INSERT [dbo].[statuses] OFF
GO

-- ----------------------------
-- Table structure for student_test_detail
-- ----------------------------
DROP TABLE [dbo].[student_test_detail]
GO
CREATE TABLE [dbo].[student_test_detail] (
[ID] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[student_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[student_test_detail]', RESEED, 6292)
GO

-- ----------------------------
-- Records of student_test_detail
-- ----------------------------
SET IDENTITY_INSERT [dbo].[student_test_detail] ON
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6149', N'207', N'925134', N'256', N'A', N'2019-10-17 14:51:16.850')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6150', N'207', N'925134', N'257', N'D', N'2019-10-17 14:51:18.353')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6151', N'207', N'925134', N'258', N'D', N'2019-10-17 14:51:19.380')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6152', N'207', N'925134', N'259', N'D', N'2019-10-17 14:51:20.577')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6153', N'208', N'925134', N'256', N'A', N'2019-10-17 14:56:46.393')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6154', N'208', N'925134', N'257', N'D', N'2019-10-17 14:56:48.100')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6155', N'208', N'925134', N'258', N'C', N'2019-10-17 14:56:49.233')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6156', N'208', N'925134', N'259', N'C', N'2019-10-17 14:56:50.377')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6157', N'209', N'925134', N'256', N'B', N'2019-10-17 14:59:15.340')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6158', N'209', N'925134', N'257', N'B', N'2019-10-17 14:59:16.713')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6159', N'209', N'925134', N'258', N'B', N'2019-10-17 14:59:18.607')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6160', N'209', N'925134', N'259', N'B', N'2019-10-17 14:59:19.907')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6161', N'210', N'925134', N'256', N'B', N'2019-10-17 15:30:52.747')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6162', N'210', N'925134', N'257', N'B', N'2019-10-17 15:30:55.350')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6163', N'210', N'925134', N'258', N'A', N'2019-10-17 15:30:57.693')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6164', N'210', N'925134', N'259', N'B', N'2019-10-17 15:31:01.547')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6165', N'211', N'925134', N'256', N'B', N'2019-10-17 15:32:25.343')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6166', N'211', N'925134', N'257', N'A', N'2019-10-17 15:32:30.800')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6167', N'211', N'925134', N'258', N'A', N'2019-10-17 15:32:33.467')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6168', N'211', N'925134', N'259', N'C', N'2019-10-17 15:32:34.517')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6169', N'212', N'925134', N'256', N'C', N'2019-10-17 15:37:47.100')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6170', N'212', N'925134', N'257', N'B', N'2019-10-17 15:37:48.543')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6171', N'212', N'925134', N'258', N'A', N'2019-10-17 15:37:49.530')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6172', N'212', N'925134', N'259', N'C', N'2019-10-17 15:37:51.440')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6173', N'213', N'925134', N'256', N'B', N'2019-10-17 15:39:40.243')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6174', N'213', N'925134', N'257', N'A', N'2019-10-17 15:39:41.573')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6175', N'213', N'925134', N'258', N'B', N'2019-10-17 15:39:43.227')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6176', N'213', N'925134', N'259', N'C', N'2019-10-17 15:39:44.290')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6177', N'214', N'925134', N'256', N'B', N'2019-10-17 15:41:22.280')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6178', N'214', N'925134', N'257', N'A', N'2019-10-17 15:41:24.427')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6179', N'214', N'925134', N'258', N'A', N'2019-10-17 15:41:25.257')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6180', N'214', N'925134', N'259', N'C', N'2019-10-17 15:41:26.970')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6181', N'215', N'925134', N'256', N'B', N'2019-10-17 15:43:13.493')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6182', N'215', N'925134', N'257', N'A', N'2019-10-17 15:43:14.867')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6183', N'215', N'925134', N'258', N'A', N'2019-10-17 15:43:15.873')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6184', N'215', N'925134', N'259', N'A', N'2019-10-17 15:43:17.313')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6185', N'216', N'925134', N'256', N'B', N'2019-10-17 15:45:05.137')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6186', N'216', N'925134', N'257', N'C', N'2019-10-17 15:45:06.877')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6187', N'216', N'925134', N'258', N'B', N'2019-10-17 15:45:07.997')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6188', N'216', N'925134', N'259', N'A', N'2019-10-17 15:45:09.583')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6189', N'217', N'925134', N'256', N'A', N'2019-10-17 16:24:08.390')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6190', N'217', N'925134', N'257', N'B', N'2019-10-17 16:24:09.980')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6191', N'217', N'925134', N'258', N'D', N'2019-10-17 16:24:11.480')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6192', N'217', N'925134', N'259', N'D', N'2019-10-17 16:24:12.670')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6193', N'218', N'925134', N'256', N'B', N'2019-10-17 16:26:03.960')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6194', N'218', N'925134', N'257', N'D', N'2019-10-17 16:26:05.317')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6195', N'218', N'925134', N'258', N'D', N'2019-10-17 16:26:06.583')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6196', N'218', N'925134', N'259', N'D', N'2019-10-17 16:26:07.153')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6197', N'219', N'925134', N'256', N'B', N'2019-10-17 16:26:37.823')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6198', N'219', N'925134', N'257', N'B', N'2019-10-17 16:26:38.723')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6199', N'219', N'925134', N'258', N'D', N'2019-10-17 16:26:40.203')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6200', N'219', N'925134', N'259', N'B', N'2019-10-17 16:26:40.883')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6201', N'220', N'925134', N'256', null, N'2019-10-17 16:27:41.527')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6202', N'220', N'925134', N'257', null, N'2019-10-17 16:27:41.527')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6203', N'220', N'925134', N'258', null, N'2019-10-17 16:27:41.527')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6204', N'220', N'925134', N'259', null, N'2019-10-17 16:27:41.527')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6205', N'221', N'925134', N'256', N'B', N'2019-10-17 16:30:26.240')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6206', N'221', N'925134', N'257', N'A', N'2019-10-17 16:30:27.863')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6207', N'221', N'925134', N'258', N'B', N'2019-10-17 16:30:28.687')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6208', N'221', N'925134', N'259', N'D', N'2019-10-17 16:30:29.440')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6209', N'222', N'925134', N'256', N'B', N'2019-10-17 16:34:44.143')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6210', N'222', N'925134', N'257', N'B', N'2019-10-17 16:34:45.290')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6211', N'222', N'925134', N'258', N'C', N'2019-10-17 16:34:46.170')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6212', N'222', N'925134', N'259', N'D', N'2019-10-17 16:34:47.230')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6213', N'223', N'925134', N'256', N'C', N'2019-10-17 16:38:22.253')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6214', N'223', N'925134', N'257', N'B', N'2019-10-17 16:38:23.387')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6215', N'223', N'925134', N'258', N'A', N'2019-10-17 16:38:24.200')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6216', N'223', N'925134', N'259', N'B', N'2019-10-17 16:38:25.570')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6217', N'224', N'925134', N'256', N'A', N'2019-10-18 07:32:07.787')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6218', N'224', N'925134', N'257', N'A', N'2019-10-18 07:32:09.230')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6219', N'224', N'925134', N'258', N'A', N'2019-10-18 07:32:10.273')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6220', N'224', N'925134', N'259', N'B', N'2019-10-18 07:32:11.350')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6221', N'225', N'925134', N'256', N'A', N'2019-10-18 07:36:23.723')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6222', N'225', N'925134', N'257', N'D', N'2019-10-18 07:36:26.677')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6223', N'225', N'925134', N'258', N'D', N'2019-10-18 07:36:29.420')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6224', N'225', N'925134', N'259', N'D', N'2019-10-18 07:36:28.163')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6225', N'226', N'925134', N'256', null, N'2019-10-18 07:42:55.870')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6226', N'226', N'925134', N'257', N'D', N'2019-10-18 07:43:00.290')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6227', N'226', N'925134', N'258', N'D', N'2019-10-18 07:43:03.447')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6228', N'226', N'925134', N'259', N'D', N'2019-10-18 07:43:05.203')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6229', N'227', N'925134', N'256', N'A', N'2019-10-18 07:44:35.167')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6230', N'227', N'925134', N'257', N'C', N'2019-10-18 07:44:36.807')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6231', N'227', N'925134', N'258', N'C', N'2019-10-18 07:44:38.900')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6232', N'227', N'925134', N'259', N'C', N'2019-10-18 07:44:40.680')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6233', N'228', N'925134', N'256', N'A', N'2019-10-18 07:54:24.793')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6234', N'228', N'925134', N'257', N'B', N'2019-10-18 07:54:26.417')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6235', N'228', N'925134', N'258', N'B', N'2019-10-18 07:54:28.603')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6236', N'228', N'925134', N'259', N'D', N'2019-10-18 07:54:30.153')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6237', N'229', N'925134', N'256', N'C', N'2019-10-18 08:07:18.210')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6238', N'229', N'925134', N'257', N'D', N'2019-10-18 08:07:20.667')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6239', N'229', N'925134', N'258', N'B', N'2019-10-18 08:07:21.413')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6240', N'229', N'925134', N'259', N'D', N'2019-10-18 08:07:19.920')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6241', N'230', N'925134', N'256', N'D', N'2019-10-18 08:09:59.953')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6242', N'230', N'925134', N'257', N'C', N'2019-10-18 08:10:08.153')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6243', N'230', N'925134', N'258', N'A', N'2019-10-18 08:10:40.107')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6244', N'230', N'925134', N'259', N'B', N'2019-10-18 08:10:46.623')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6245', N'231', N'925134', N'256', null, N'2019-10-18 08:15:31.190')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6246', N'231', N'925134', N'257', null, N'2019-10-18 08:15:31.190')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6247', N'231', N'925134', N'258', null, N'2019-10-18 08:15:31.190')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6248', N'231', N'925134', N'259', null, N'2019-10-18 08:15:31.190')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6249', N'232', N'925134', N'256', N'B', N'2019-10-18 08:32:47.587')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6250', N'232', N'925134', N'257', N'C', N'2019-10-18 08:32:49.797')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6251', N'232', N'925134', N'258', N'B', N'2019-10-18 08:32:48.670')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6252', N'232', N'925134', N'259', N'A', N'2019-10-18 08:32:47.893')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6253', N'233', N'925134', N'256', N'A', N'2019-10-18 08:35:01.453')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6254', N'233', N'925134', N'257', N'B', N'2019-10-18 08:35:02.207')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6255', N'233', N'925134', N'258', N'A', N'2019-10-18 08:35:06.170')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6256', N'233', N'925134', N'259', N'D', N'2019-10-18 08:35:03.380')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6257', N'234', N'925134', N'256', N'A', N'2019-10-18 08:35:43.753')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6258', N'234', N'925134', N'257', null, N'2019-10-18 08:35:38.577')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6259', N'234', N'925134', N'258', N'C', N'2019-10-18 08:36:10.787')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6260', N'234', N'925134', N'259', N'B', N'2019-10-18 08:36:09.763')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6261', N'235', N'925134', N'256', N'A', N'2019-10-18 08:38:38.920')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6262', N'235', N'925134', N'257', N'D', N'2019-10-18 08:38:42.107')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6263', N'235', N'925134', N'258', null, N'2019-10-18 08:38:34.743')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6264', N'235', N'925134', N'259', N'D', N'2019-10-18 08:38:44.970')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6265', N'236', N'925134', N'256', N'A', N'2019-10-18 08:51:46.173')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6266', N'236', N'925134', N'257', N'B', N'2019-10-18 08:51:47.100')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6267', N'236', N'925134', N'258', N'B', N'2019-10-18 08:51:49.907')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6268', N'236', N'925134', N'259', N'B', N'2019-10-18 08:51:50.910')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6269', N'237', N'925134', N'256', null, N'2019-10-18 08:59:27.130')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6270', N'237', N'925134', N'257', null, N'2019-10-18 08:59:27.130')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6271', N'237', N'925134', N'258', null, N'2019-10-18 08:59:27.130')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6272', N'237', N'925134', N'259', null, N'2019-10-18 08:59:27.130')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6273', N'238', N'925134', N'256', N'B', N'2019-10-18 09:03:32.183')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6274', N'238', N'925134', N'257', N'A', N'2019-10-18 09:03:34.370')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6275', N'238', N'925134', N'258', N'D', N'2019-10-18 09:03:35.887')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6276', N'238', N'925134', N'259', N'D', N'2019-10-18 09:03:37.130')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6277', N'239', N'925134', N'256', N'A', N'2019-10-18 09:16:25.750')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6278', N'239', N'925134', N'257', N'D', N'2019-10-18 09:16:32.630')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6279', N'239', N'925134', N'258', N'D', N'2019-10-18 09:17:02.440')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6280', N'239', N'925134', N'259', N'D', N'2019-10-18 09:16:39.617')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6281', N'240', N'925134', N'256', N'A', N'2019-10-18 09:21:57.510')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6282', N'240', N'925134', N'257', N'B', N'2019-10-18 09:21:16.930')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6283', N'240', N'925134', N'258', N'A', N'2019-10-18 09:21:18.593')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6284', N'240', N'925134', N'259', N'B', N'2019-10-18 09:21:20.157')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6285', N'241', N'925134', N'256', N'A', N'2019-10-18 12:11:44.510')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6286', N'241', N'925134', N'257', null, N'2019-10-18 12:11:42.300')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6287', N'241', N'925134', N'258', null, N'2019-10-18 12:11:42.300')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6288', N'241', N'925134', N'259', null, N'2019-10-18 12:11:42.300')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6289', N'242', N'925134', N'256', null, N'2019-10-18 12:11:42.300')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6290', N'242', N'925134', N'257', N'B', N'2019-10-18 12:11:51.403')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6291', N'242', N'925134', N'258', N'A', N'2019-10-18 12:11:54.727')
GO
GO
INSERT INTO [dbo].[student_test_detail] ([ID], [id_student], [test_code], [id_cauhoi], [student_answer], [timestamps]) VALUES (N'6292', N'242', N'925134', N'259', N'B', N'2019-10-18 12:11:57.143')
GO
GO
SET IDENTITY_INSERT [dbo].[student_test_detail] OFF
GO

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE [dbo].[students]
GO
CREATE TABLE [dbo].[students] (
[id_student] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NULL ,
[password] varchar(32) NULL ,
[email] varchar(100) NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NULL ,
[birthday] date NULL ,
[phone] varchar(45) NULL ,
[cmnd] nvarchar(15) NULL ,
[sbd] nvarchar(15) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL ,
[donvi] nvarchar(255) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[students]', RESEED, 242)
GO

-- ----------------------------
-- Records of students
-- ----------------------------
SET IDENTITY_INSERT [dbo].[students] ON
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'207', null, null, null, N'avatar-default.jpg', N'Sơn', null, null, N'0878485454', N'32132132131', null, null, N'2019-10-17 14:50:20.990', N'Sở Thông tin')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'208', null, null, null, N'avatar-default.jpg', N'Trần Thanh Sơn', null, null, N'0938474444', N'2981171743', null, null, N'2019-10-17 14:56:40.637', N'Sở Tài Nguyên')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'209', null, null, null, N'avatar-default.jpg', N'Đàm Hải Đăng', null, null, N'098452125', N'295757574', null, null, N'2019-10-17 14:59:09.843', N'Trung tâm CNTT')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'210', null, null, null, N'avatar-default.jpg', N'Nguyễn Hoàng Tấn', null, null, N'0874512544', N'290877445', null, null, N'2019-10-17 15:30:46.113', N'Ban Tuyên Giáo tỉnh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'211', null, null, null, N'avatar-default.jpg', N'Lê Hoàng Nam', null, null, N'0987451266', N'288741155', null, null, N'2019-10-17 15:32:19.607', N'Trung tâm Tỉnh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'212', null, null, null, N'avatar-default.jpg', N'Sơn Đoàn', null, null, N'09855425415', N'294854747', null, null, N'2019-10-17 15:37:39.320', N'Sở Kế Hoạch')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'213', null, null, null, N'avatar-default.jpg', N'Hải Đăng', null, null, N'098545125', N'29884454', null, null, N'2019-10-17 15:39:34.757', N'Hoài Nam')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'214', null, null, null, N'avatar-default.jpg', N'Hoài', null, null, N'1991911333', N'29994488', null, null, N'2019-10-17 15:41:16.613', N'Sở')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'215', null, null, null, N'avatar-default.jpg', N'Vinh', null, null, N'2832193123', N'298745145', null, null, N'2019-10-17 15:43:07.660', N'Sở Nam')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'216', null, null, null, N'avatar-default.jpg', N'Hảo', null, null, N'095545147', N'2908847473', null, null, N'2019-10-17 15:44:59.300', N'Sở Nữ')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'217', null, null, null, N'avatar-default.jpg', N'Minh', null, null, N'28727733231', N'2928747547', null, null, N'2019-10-17 16:24:01.690', N'Sở Tài nguyên')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'218', null, null, null, N'avatar-default.jpg', N'Trung', null, null, N'09887455112', N'2938383771', null, null, N'2019-10-17 16:25:49.443', N'Ban Cơ Yếu Chính Phủ')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'219', null, null, null, N'avatar-default.jpg', N'Test', null, null, N'321321321', N'8283838383', null, null, N'2019-10-17 16:26:32.360', N'331321321')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'220', null, null, null, N'avatar-default.jpg', N'OK', null, null, N'13213125441', N'2321312312', null, null, N'2019-10-17 16:27:41.523', N'11kk11')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'221', null, null, null, N'avatar-default.jpg', N'dấdđ', null, null, N'1321321', N'2313213123', null, null, N'2019-10-17 16:30:20.827', N'2132')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'222', null, null, null, N'avatar-default.jpg', N'Nhậm', null, null, N'29933133211', N'19292999', null, null, N'2019-10-17 16:34:38.807', N'SO')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'223', null, null, null, N'avatar-default.jpg', N'Hoang Nam Nhan', null, null, N'1549987899', N'2093938881', null, null, N'2019-10-17 16:38:16.523', N'So TTTT')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'224', null, null, null, N'avatar-default.jpg', N'son nguyen', null, null, N'0912365478', N'2907894561', null, null, N'2019-10-18 07:32:03.770', N'Tay ninh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'225', null, null, null, N'avatar-default.jpg', N'son đoàn', null, null, N'0964632232', N'2910131754', null, null, N'2019-10-18 07:36:23.800', N'Sở TTTT')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'226', null, null, null, N'avatar-default.jpg', N'Sơn', null, null, N'0965535758', N'08795464646', null, null, N'2019-10-18 07:42:55.870', N'So tt')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'227', null, null, null, N'avatar-default.jpg', N'son nguyen', null, null, N'0921365478', N'290123456', null, null, N'2019-10-18 07:44:35.550', N'Tay Ninh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'228', null, null, null, N'avatar-default.jpg', N'Châu Huỳnh Minh Tiến', null, null, N'09484874744', N'295858483', null, null, N'2019-10-18 07:54:23.600', N'Trung tâm CNTT')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'229', null, null, null, N'avatar-default.jpg', N'dashhd', null, null, N'1123123', N'23123', null, null, N'2019-10-18 08:07:12.987', N'2cadhajh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'230', null, null, null, N'avatar-default.jpg', N'Huynh Thi Nhu Hao', null, null, N'123456', N'122345', null, null, N'2019-10-18 08:08:18.837', N'Sở Thông tin và Truyền thông')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'231', null, null, null, N'avatar-default.jpg', N'Nguyễn Văn Toàn', null, null, N'0918546852', N'290578654', null, null, N'2019-10-18 08:15:31.190', N'Công nhân')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'232', null, null, null, N'avatar-default.jpg', N'321313', null, null, N'13131313', N'3213123', null, null, N'2019-10-18 08:32:42.470', N'1231313')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'233', null, null, null, N'avatar-default.jpg', N'213123ddd', null, null, N'123123123', N'12311', null, null, N'2019-10-18 08:34:57.257', N'2312312ddd')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'234', null, null, null, N'avatar-default.jpg', N'321312cvvvv', null, null, N'12312312312', N'1123123', null, null, N'2019-10-18 08:35:38.577', N'123123vvvv')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'235', null, null, null, N'avatar-default.jpg', N'312312vvvvvggg', null, null, N'312312344', N'31231', null, null, N'2019-10-18 08:38:34.740', N'eqwuyggek')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'236', null, null, null, N'avatar-default.jpg', N'3123123vvvvvnytfdsf1231', null, null, N'231232131', N'123123123', null, null, N'2019-10-18 08:51:40.797', N'd12313ddsad')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'237', null, null, null, N'avatar-default.jpg', N'', null, null, N'', N'', null, null, N'2019-10-18 08:59:27.100', N'')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'238', null, null, null, N'avatar-default.jpg', N'ada12313', null, null, N'4214124', N'1231312414', null, null, N'2019-10-18 09:03:32.393', N'124124124214')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'239', null, null, null, N'avatar-default.jpg', N'Đàm Hải Đăng', null, null, N'0869221603', N'290988556', null, null, N'2019-10-18 09:15:42.443', N'Sở Thông tin và Truyền thông')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'240', null, null, null, N'avatar-default.jpg', N'Hoàng Văn Lâm', null, null, N'877444444', N'2988447463', null, null, N'2019-10-18 09:19:12.870', N'Sở Thông tin Truyền thông')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'241', null, null, null, N'avatar-default.jpg', N'ggdggd', null, null, N'', N'665646', null, null, N'2019-10-18 12:11:42.300', N'hdhhdh')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin], [donvi]) VALUES (N'242', null, null, null, N'avatar-default.jpg', N'ggdggd', null, null, N'', N'665646', null, null, N'2019-10-18 12:11:42.300', N'hdhhdh')
GO
GO
SET IDENTITY_INSERT [dbo].[students] OFF
GO

-- ----------------------------
-- Procedure structure for CheckThoiGianLamBai
-- ----------------------------
DROP PROCEDURE [dbo].[CheckThoiGianLamBai]
GO
CREATE PROCEDURE [dbo].[CheckThoiGianLamBai] @MaThiSinh INT,
@TestCode INT 
AS
begin  
declare @setval int  
select @setval= dbo.[fnTime](@MaThiSinh, @TestCode)  
if(@setval>0)
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
End
ELSE BEGIN
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
CAST(null AS int)socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
CAST(null AS float)diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
end 
End






GO

-- ----------------------------
-- Procedure structure for GetAllCauHoi
-- ----------------------------
DROP PROCEDURE [dbo].[GetAllCauHoi]
GO
CREATE  PROCEDURE [dbo].[GetAllCauHoi]
AS
SELECT
--ROW_NUMBER() OVER(ORDER BY dbo.cauhoi.timestamps DESC)AS sothutu,
cast (ROW_NUMBER() over(ORDER BY dbo.cauhoi.timestamps DESC) as int) Rank,
dbo.cauhoi.id_cauhoi as IdCauhoi,
dbo.cauhoi.id_loaicauhoi as IdLoaicauhoi,
dbo.cauhoi.img_content as ImgContent,
dbo.cauhoi.content as Content,
dbo.cauhoi.answer_a as AnswerA,
dbo.cauhoi.answer_b as AnswerB,
dbo.cauhoi.answer_c as AnswerC,
dbo.cauhoi.answer_d as AnswerD,
dbo.cauhoi.correct_answer as CorrectAnswer,
dbo.cauhoi.timestamps as Timestamps,
dbo.loaicauhoi.tenloai as TenLoai
FROM
dbo.cauhoi
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.cauhoi.del_stt = 1
ORDER BY dbo.cauhoi.timestamps DESC







GO

-- ----------------------------
-- Procedure structure for GetChiTietBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetChiTietBaiThi]
GO
CREATE PROCEDURE [dbo].[GetChiTietBaiThi] @MaThiSinh int, @TestCode int
AS
SELECT
dbo.student_test_detail.ID,
dbo.student_test_detail.id_student,
dbo.student_test_detail.test_code,
dbo.student_test_detail.id_cauhoi,
dbo.cauhoi.img_content,
dbo.cauhoi.content,
dbo.cauhoi.answer_a,
dbo.cauhoi.answer_b,
dbo.cauhoi.answer_c,
dbo.cauhoi.answer_d,
dbo.cauhoi.id_loaicauhoi,
dbo.loaicauhoi.tenloai,
dbo.baithi.test_name,
dbo.student_test_detail.student_answer

FROM
dbo.student_test_detail
INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
INNER JOIN dbo.baithi ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.student_test_detail.test_code = @TestCode AND
dbo.student_test_detail.id_student = @MaThiSinh
ORDER BY dbo.student_test_detail.ID







GO

-- ----------------------------
-- Procedure structure for GetDanhSachBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachBaiThi]
GO
CREATE PROCEDURE [dbo].[GetDanhSachBaiThi] @MaThiSinh int
AS
SELECT DISTINCT
dbo.baithi.test_name,
dbo.baithi.total_questions,
dbo.student_test_detail.id_student,
dbo.baithi.test_code,
dbo.baithi.time_to_do,
dbo.baithi.id_status,
dbo.statuses.status_name,
dbo.baithi.note

FROM
dbo.baithi
INNER JOIN dbo.student_test_detail ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.statuses ON dbo.baithi.id_status = dbo.statuses.id_status
WHERE
dbo.baithi.id_status=1 AND dbo.baithi.del_stt = 1 AND
dbo.student_test_detail.id_student = @MaThiSinh







GO

-- ----------------------------
-- Procedure structure for GetDanhSachKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachKetQua]
GO
CREATE PROCEDURE [dbo].[GetDanhSachKetQua]
 @TestCode INT 
AS 
BEGIN
SELECT
cast (ROW_NUMBER() over(ORDER BY dbo.students.sbd DESC) as int) Rank,
dbo.students.id_student as IdStudent,
dbo.students.username as Username,
dbo.students.email as Email,
dbo.students.name as Name,
dbo.students.gender as Gender,
dbo.students.birthday as Birthday,
dbo.students.phone as Phone,
dbo.students.cmnd as Cmnd,
dbo.students.sbd as Sbd,
dbo.students.timelogin as Timelogin,
dbo.students.donvi as Donvi,
dbo.scores.socaudung as Socaudung,
dbo.scores.tongcau as TongCau,
dbo.scores.time_finish as TimeFinish,
dbo.scores.diemthang10 as Diemthang10,
dbo.scores.test_code as TestCode
FROM
dbo.students
INNER JOIN dbo.scores ON dbo.students.id_student = dbo.scores.id_student
WHERE
dbo.scores.test_code =@TestCode

END







GO

-- ----------------------------
-- Procedure structure for GetDanhSachThiSinhTheoLop
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop]
GO
CREATE PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop] @MaLop int
AS
SELECT
dbo.students.name,
dbo.students.gender,
dbo.students.donvi,
dbo.students.sbd,
dbo.students.avatar,
dbo.students.email,
dbo.students.username,
dbo.chitiet_lop.id_chitiet_lop,
dbo.chitiet_lop.id_class,
dbo.classes.class_name,
dbo.classes.grade_name,
dbo.students.id_student

FROM
dbo.chitiet_lop
INNER JOIN dbo.classes ON dbo.classes.id_class = dbo.chitiet_lop.id_class
INNER JOIN dbo.students ON dbo.students.id_student = dbo.chitiet_lop.id_student
WHERE
dbo.classes.id_class = @MaLop
ORDER BY
dbo.students.name ASC







GO

-- ----------------------------
-- Procedure structure for GetKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetKetQua]
GO
CREATE PROCEDURE [dbo].[GetKetQua] @MaThiSinh INT,
 @TestCode INT 
AS 
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
End
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode








GO

-- ----------------------------
-- Procedure structure for GetKetQua_BTG
-- ----------------------------
DROP PROCEDURE [dbo].[GetKetQua_BTG]
GO
CREATE PROCEDURE [dbo].[GetKetQua_BTG] @MaThiSinh INT,
 @TestCode INT,
 @DuDoan INT
AS 
DECLARE @Timefn datetime
SET @Timefn=(SELECT time_finish FROM scores WHERE id_student =@MaThiSinh
							AND test_code =@TestCode);
if @Timefn is NULL
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float),
		dudoanketqua=@DuDoan,
		time_finish=GETDATE()
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
End
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.scores.dudoanketqua,
dbo.baithi.test_name,
dbo.students.name,
dbo.students.timelogin,
dbo.fnConvertTime(DATEDIFF(SECOND, dbo.students.timelogin, dbo.scores.time_finish)) As thoigianlam
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode








GO

-- ----------------------------
-- Procedure structure for GetThoiGianThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetThoiGianThi]
GO
CREATE PROCEDURE [dbo].[GetThoiGianThi] @MaThiSinh INT,
 @TestCode INT , @time_finish int
AS
IF NOT EXISTS ( SELECT * FROM scores 
                   WHERE id_student = @MaThiSinh
                   AND test_code = @TestCode)
BEGIN
	INSERT INTO scores (
	id_student,
	test_code,
	time_finish
	)
VALUES
	(
		@MaThiSinh,
		@TestCode,
		DATEADD(MINUTE,@time_finish,GETDATE())
	);
END
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode








GO

-- ----------------------------
-- Procedure structure for GetThongTinThiSinh_BTG
-- ----------------------------
DROP PROCEDURE [dbo].[GetThongTinThiSinh_BTG]
GO
CREATE PROCEDURE [dbo].[GetThongTinThiSinh_BTG]
@id int
AS
SELECT
dbo.students.id_student,
dbo.students.name,
dbo.students.phone,
dbo.students.cmnd,
dbo.students.timelogin,
dbo.students.donvi
FROM [dbo].[students]
WHERE
dbo.students.id_student=@id


GO

-- ----------------------------
-- Procedure structure for InsertPersonal
-- ----------------------------
DROP PROCEDURE [dbo].[InsertPersonal]
GO
CREATE PROCEDURE [dbo].[InsertPersonal]
      -- Add the parameters for the stored procedure here
      @Name nvarchar(100),
      @CMND nvarchar(15),
      @Phone varchar(11),
      @DiaChi nvarchar(255),
			@Email varchar(100)
AS
DECLARE @StID int,
				@TestCode int
BEGIN
      SET NOCOUNT ON;
      INSERT INTO students
             (email,name, cmnd, phone, donvi,timelogin)
      VALUES
             (@Email,@Name, @CMND, @Phone, @DiaChi,GETDATE());
			SET @StID=SCOPE_IDENTITY();
			SET @TestCode =(SELECT TOP 1 test_code
											FROM [dbo].[baithi]
											WHERE id_status=1
											ORDER BY
											dbo.baithi.test_code DESC);
			INSERT INTO student_test_detail (id_student, test_code, id_cauhoi)
			SELECT
			@StID as id_students,
			dbo.chitietbaithi.test_code,
			dbo.cauhoi.id_cauhoi
			FROM
			dbo.cauhoi
			INNER JOIN dbo.chitietbaithi ON dbo.chitietbaithi.id_cauhoi = dbo.cauhoi.id_cauhoi
			WHERE
			dbo.chitietbaithi.test_code=@TestCode;
				INSERT INTO scores (
											id_student,
											test_code
											)
										VALUES
											(
												@StID,
												@TestCode
											);
			SELECT
			dbo.student_test_detail.ID,
			dbo.student_test_detail.id_student,
			dbo.student_test_detail.test_code,
			dbo.student_test_detail.id_cauhoi,
			dbo.cauhoi.img_content,
			dbo.cauhoi.content,
			dbo.cauhoi.answer_a,
			dbo.cauhoi.answer_b,
			dbo.cauhoi.answer_c,
			dbo.cauhoi.answer_d,
			dbo.cauhoi.id_loaicauhoi,
			dbo.loaicauhoi.tenloai,
			dbo.baithi.test_name,
			dbo.student_test_detail.student_answer
			FROM
			dbo.student_test_detail
			INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
			INNER JOIN dbo.baithi ON dbo.student_test_detail.test_code = dbo.baithi.test_code
			INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
			WHERE
			dbo.student_test_detail.test_code = @TestCode AND
			dbo.student_test_detail.id_student = @StID
			ORDER BY dbo.student_test_detail.ID
END


GO

-- ----------------------------
-- Procedure structure for TaoDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[TaoDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[TaoDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
INSERT INTO student_test_detail (id_student, test_code, id_cauhoi)
SELECT
@MaThisinh as id_students,
dbo.chitietbaithi.test_code,
dbo.cauhoi.id_cauhoi
FROM
dbo.cauhoi
INNER JOIN dbo.chitietbaithi ON dbo.chitietbaithi.id_cauhoi = dbo.cauhoi.id_cauhoi
WHERE
dbo.chitietbaithi.test_code=@MaCode
ORDER BY NEWID()
END







GO

-- ----------------------------
-- Procedure structure for ThongKeHome
-- ----------------------------
DROP PROCEDURE [dbo].[ThongKeHome]
GO
CREATE PROC [dbo].[ThongKeHome]
AS
BEGIN
	IF OBJECT_ID('tempdb..#tthongke') IS NOT NULL DROP TABLE #ttthongke
	CREATE TABLE #tthongke(idkey int,title nvarchar(40), total int)
	INSERT INTO #tthongke SELECT 1,N'Bài thi',(SELECT COUNT(*) FROM baithi WHERE del_stt = 1) GO
	INSERT INTO #tthongke SELECT 2,N'Câu hỏi',(SELECT COUNT(*) FROM cauhoi WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 3,N'Thí sinh',(SELECT COUNT(*) FROM students)GO
	INSERT INTO #tthongke SELECT 4,N'Bảng',(SELECT COUNT(*) FROM classes WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 5,N'Loại Câu hỏi',(SELECT COUNT(*) FROM loaicauhoi)GO
	SELECT * FROM #tthongke 
END








GO

-- ----------------------------
-- Procedure structure for XoaDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[XoaDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[XoaDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
DELETE FROM [dbo].[student_test_detail] Where id_student = @MaThisinh AND test_code=@MaCode
END







GO

-- ----------------------------
-- Function structure for fnConvertTime
-- ----------------------------
DROP FUNCTION [dbo].[fnConvertTime]
GO
Create FUNCTION [dbo].[fnConvertTime] (@SecondsInput INT) RETURNS varchar(8) AS
BEGIN
DECLARE @SecondsToConvert int
SET @SecondsToConvert = @SecondsInput
-- Declare variables
DECLARE @Hours int

DECLARE @Minutes int

DECLARE @Seconds int

DECLARE @Time datetime
DECLARE @Timeoutput varchar(8)
-- Set the calculations for hour, minute and second

SET @Hours = @SecondsToConvert/3600

SET @Minutes = (@SecondsToConvert % 3600) / 60

SET @Seconds = @SecondsToConvert % 60

-- Store the datetime information retrieved in the @Time variable

SET @Time = (SELECT

RTRIM(CONVERT(char(8), @Hours) ) + ':' +

CONVERT(char(2), @Minutes) + ':' +

CONVERT(char(2), @Seconds));

-- Display the @Time variable in the format of HH:MMTongue TiedS
SET @Timeoutput = (SELECT CONVERT(varchar(8),CONVERT(datetime,@Time),108))
RETURN @Timeoutput
END

GO

-- ----------------------------
-- Function structure for fnGetCauDung
-- ----------------------------
DROP FUNCTION [dbo].[fnGetCauDung]
GO
CREATE FUNCTION [dbo].[fnGetCauDung] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		AND UPPER(dbo.student_test_detail.student_answer) = UPPER(dbo.cauhoi.correct_answer)
		RETURN @NumCount
		END







GO

-- ----------------------------
-- Function structure for fnGetTongCau
-- ----------------------------
DROP FUNCTION [dbo].[fnGetTongCau]
GO
CREATE FUNCTION [dbo].[fnGetTongCau] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		RETURN @NumCount
		END







GO

-- ----------------------------
-- Function structure for fnTime
-- ----------------------------
DROP FUNCTION [dbo].[fnTime]
GO
CREATE FUNCTION [dbo].[fnTime] (@MaThiSinh INT,
@TestCode INT ) RETURNS INT AS
BEGIN
	DECLARE
		@TimeCount INT
SELECT
@TimeCount=DATEDIFF(MINUTE, dbo.scores.time_finish,GETDATE())
FROM
dbo.scores
WHERE
dbo.scores.id_student=@MaThiSinh AND
dbo.scores.test_code=@TestCode
RETURN @TimeCount
		END







GO

-- ----------------------------
-- Indexes structure for table admins
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD PRIMARY KEY ([id_admin])
GO

-- ----------------------------
-- Uniques structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD UNIQUE ([username] ASC, [email] ASC)
GO

-- ----------------------------
-- Indexes structure for table baithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table baithi
-- ----------------------------
ALTER TABLE [dbo].[baithi] ADD PRIMARY KEY ([test_code])
GO

-- ----------------------------
-- Indexes structure for table cauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table cauhoi
-- ----------------------------
ALTER TABLE [dbo].[cauhoi] ADD PRIMARY KEY ([id_cauhoi])
GO

-- ----------------------------
-- Indexes structure for table chitiet_lop
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitiet_lop
-- ----------------------------
ALTER TABLE [dbo].[chitiet_lop] ADD PRIMARY KEY ([id_chitiet_lop])
GO

-- ----------------------------
-- Indexes structure for table chitietbaithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitietbaithi
-- ----------------------------
ALTER TABLE [dbo].[chitietbaithi] ADD PRIMARY KEY ([ID_chitiet])
GO

-- ----------------------------
-- Indexes structure for table classes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table classes
-- ----------------------------
ALTER TABLE [dbo].[classes] ADD PRIMARY KEY ([id_class])
GO

-- ----------------------------
-- Indexes structure for table loaicauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table loaicauhoi
-- ----------------------------
ALTER TABLE [dbo].[loaicauhoi] ADD PRIMARY KEY ([id_loaicauhoi])
GO

-- ----------------------------
-- Indexes structure for table Login
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Login
-- ----------------------------
ALTER TABLE [dbo].[Login] ADD PRIMARY KEY ([IDlogin])
GO

-- ----------------------------
-- Indexes structure for table scores
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table scores
-- ----------------------------
ALTER TABLE [dbo].[scores] ADD PRIMARY KEY ([id_score], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table specialities
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table specialities
-- ----------------------------
ALTER TABLE [dbo].[specialities] ADD PRIMARY KEY ([id_speciality])
GO

-- ----------------------------
-- Indexes structure for table statuses
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table statuses
-- ----------------------------
ALTER TABLE [dbo].[statuses] ADD PRIMARY KEY ([id_status])
GO

-- ----------------------------
-- Indexes structure for table student_test_detail
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table student_test_detail
-- ----------------------------
ALTER TABLE [dbo].[student_test_detail] ADD PRIMARY KEY ([ID], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table students
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD PRIMARY KEY ([id_student])
GO
