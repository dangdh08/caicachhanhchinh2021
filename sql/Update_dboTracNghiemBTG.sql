/*
Navicat SQL Server Data Transfer

Source Server         : localhost
Source Server Version : 140000
Source Host           : DESKTOP-S4VGAUV:1433
Source Database       : TracNghiemBanTuyenGiao
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 140000
File Encoding         : 65001

Date: 2019-10-16 22:48:07
*/


-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE [dbo].[admins]
GO
CREATE TABLE [dbo].[admins] (
[id_admin] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NOT NULL ,
[password] varchar(32) NOT NULL ,
[email] varchar(100) NOT NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NULL ,
[birthday] date NULL ,
[phone] varchar(45) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL 
)


GO

-- ----------------------------
-- Records of admins
-- ----------------------------
SET IDENTITY_INSERT [dbo].[admins] ON
GO
INSERT INTO [dbo].[admins] ([id_admin], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [token], [timelogin]) VALUES (N'1', N'admin', N'ceea23519f6f86ad67e9f798bf8002cb', N'admin@gmail.com', N'avatar-default.jpg', N'admin', N'nam', N'1990-01-01', null, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NzEyMzE2MDIsImV4cCI6MTYwNzIzMTYwMiwiaWF0IjoxNTcxMjMxNjAyfQ.0IwNCM5XeYLu7Dk9BhpD-fYneXYQSUzo-0gGVhsOkr4', N'2019-10-16 20:13:22.610')
GO
GO
SET IDENTITY_INSERT [dbo].[admins] OFF
GO

-- ----------------------------
-- Table structure for baithi
-- ----------------------------
DROP TABLE [dbo].[baithi]
GO
CREATE TABLE [dbo].[baithi] (
[test_name] nvarchar(255) NOT NULL ,
[test_code] int NOT NULL IDENTITY(1,1) ,
[password] varchar(32) NOT NULL ,
[total_questions] int NOT NULL ,
[time_to_do] int NOT NULL ,
[note] nvarchar(MAX) NULL ,
[id_status] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[baithi]', RESEED, 925133)
GO

-- ----------------------------
-- Records of baithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[baithi] ON
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng A - Đề 1', N'925126', N'banga1@2019', N'30', N'30', N'', N'1', N'2019-09-09 11:11:31.520', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng A - Đề 2', N'925127', N'banga2@2019', N'30', N'30', N'', N'0', N'2019-09-09 11:12:13.540', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng B - Đề 1', N'925128', N'bangb1@2019', N'30', N'30', N'', N'1', N'2019-09-09 11:13:17.903', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng B - Đề 2', N'925129', N'bangb2@2019', N'30', N'30', N'', N'0', N'2019-09-09 11:15:19.873', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng C - Đề 1', N'925130', N'bangc1@2019', N'30', N'30', N'', N'1', N'2019-09-09 11:15:34.950', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'Bảng C - Đề 2', N'925131', N'bangc2@2019', N'30', N'30', N'', N'0', N'2019-09-09 11:15:50.670', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'TestTN', N'925132', N'', N'3', N'9000', N'', N'1', N'2019-10-16 20:14:26.290', N'1')
GO
GO
INSERT INTO [dbo].[baithi] ([test_name], [test_code], [password], [total_questions], [time_to_do], [note], [id_status], [timestamps], [del_stt]) VALUES (N'bai thi số 2', N'925133', N'', N'4', N'999999', N'', N'1', N'2019-10-16 20:54:06.307', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[baithi] OFF
GO

-- ----------------------------
-- Table structure for cauhoi
-- ----------------------------
DROP TABLE [dbo].[cauhoi]
GO
CREATE TABLE [dbo].[cauhoi] (
[id_cauhoi] int NOT NULL IDENTITY(1,1) ,
[id_loaicauhoi] int NOT NULL ,
[img_content] nvarchar(MAX) NULL ,
[content] nvarchar(MAX) NULL ,
[answer_a] nvarchar(MAX) NULL ,
[answer_b] nvarchar(MAX) NULL ,
[answer_c] nvarchar(MAX) NULL ,
[answer_d] nvarchar(MAX) NULL ,
[correct_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[cauhoi]', RESEED, 255)
GO

-- ----------------------------
-- Records of cauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cauhoi] ON
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'113', N'9', null, N'Thiết bị nào sau đây dùng để kết nối mạng?', N' Ram', N' Rom', N' Router', N' CPU', N'C', N'2019-09-09 11:41:58.190', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'114', N'9', null, N'Hệ thống nhớ của máy tính bao gồm:', N' Bộ nhớ trong, Bộ nhớ ngoài', N' Cache, Bộ nhớ ngoài', N' Bộ nhớ ngoài, ROM', N' Đĩa quang, Bộ nhớ trong', N'A', N'2019-09-09 11:41:58.193', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'115', N'9', null, N'Bộ nhớ RAM và ROM là bộ nhớ gì?', N' Primary memory', N' Receive memory', N' Secondary memory', N' Random access memory.', N'A', N'2019-09-09 11:41:58.197', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'116', N'9', null, N'Trong mạng máy tính, thuật ngữ Share có ý nghĩa gì?', N' Chia sẻ tài nguyên', N' Nhãn hiệu của một thiết bị kết nối mạng', N' Thực hiện lệnh in trong mạng cục bộ', N' Một phần mềm hỗ trợ sử dụng mạng cục bộ', N'A', N'2019-09-09 11:41:58.197', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'117', N'9', null, N'Các thiết bị nào thông dụng nhất hiện nay dùng để cung cấp dữ liệu cho máy xử lý?', N' Bàn phím (Keyboard), Chuột (Mouse), Máy in (Printer) .', N' Máy quét ảnh (Scaner).', N' Bàn phím (Keyboard), Chuột (Mouse) và Máy quét ảnh (Scaner).', N' Máy quét ảnh (Scaner), Chuột (Mouse)', N'C', N'2019-09-09 11:41:58.200', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'118', N'9', null, N'Khái niệm hệ điều hành là gì ?', N' Cung cấp và xử lý các phần cứng và phần mềm', N' Nghiên cứu phương pháp, kỹ thuật xử lý thông tin bằng máy tính điện tử', N' Nghiên cứu về công nghệ phần cứng và phần mềm', N' Là một phần mềm chạy trên máy tính, dùng để điều hành, quản lý các thiết bị phần cứng và các tài nguyên phần mềm trên máy tính', N'D', N'2019-09-09 11:41:58.220', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'119', N'9', null, N'Trong Powerpoint để tạo mới 1 Slide ta sử dụng?', N' Home -> Slides -> New Slide', N' Insert -> New Slide', N' Design -> New Slide', N' View -> New Slide', N'A', N'2019-09-09 11:41:58.237', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'120', N'9', null, N'Trong Powerpoint để vẽ đồ thị trong Slide ta chọn:', N' File/ Chart', N' Insert/ Chart', N' View/ Chart', N' Design/ Chart', N'B', N'2019-09-09 11:41:58.240', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'121', N'9', null, N'Công dụng của phím Print Screen là gì?', N' In màn hình hiện hành ra máy in', N' Không có công dụng gì khi sử dụng 1 mình nó.', N' In văn bản hiện hành ra máy in', N' Chụp màn hình hiện hành', N'D', N'2019-09-09 11:41:58.243', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'122', N'9', null, N'Trong soạn thảo Word, công dụng của tổ hợp phím Ctrl – S là:', N' Tạo một văn bản mới', N' Chức năng thay thế nội dung trong soạn thảo', N' Định dạng chữ hoa', N' Lưu nội dung tập tin văn bản vào đĩa', N'D', N'2019-09-09 11:41:58.247', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'123', N'9', null, N'Trong soạn thảo Word, để kết thúc 1 đoạn (Paragraph) và muốn sang 1 đoạn mới :', N' Bấm tổ hợp phím Ctrl – Enter', N' Bấm phím Enter', N' Bấm tổ hợp phím Shift – Enter', N' Word tự động, không cần bấm phím', N'B', N'2019-09-09 11:41:58.247', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'124', N'9', null, N'Trong soạn thảo Word, để chọn một đoạn văn bản ta thực hiện:', N' Click 1 lần trên đoạn', N' Click 2 lần trên đoạn', N' Click 3 lần trên đoạn', N' Click 4 lần trên đoạn.', N'C', N'2019-09-09 11:41:58.267', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'125', N'9', null, N'Bộ nhớ truy nhập trực tiếp RAM được viết tắt từ, bạn sử dụng lựa chọn nào?', N' Read Access Memory.', N' Random Access Memory.', N' Rewrite Access Memory.', N' Cả 3 câu đều đúng.', N'B', N'2019-09-09 11:41:58.287', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'126', N'9', null, N'Khả năng xử lý của máy tính phụ thuộc vào … ban sử dụng lựa chọn nào?', N' Tốc độ CPU, dung lượng bộ nhớ RAM, dung lượng và tốc độ ổ cứng.', N' Yếu tố đa nhiệm', N' Hiện tượng phân mảnh ổ đĩa.', N' Cả 3 phần trên đều đúng.', N'D', N'2019-09-09 11:41:58.287', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'127', N'9', null, N'Trình tự xử lý thông tin của máy tính điện tử', N' Màn hình -> CPU -> Đĩa cứng', N' Đĩa cứng -> Màn hình -. CPU', N' Nhập thông tin -> Xử lý thông tin -> Xuất thông tin', N' Màn hình -> Máy in -> CPU', N'C', N'2019-09-09 11:41:58.290', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'128', N'9', null, N'Trong bảng tính Excel, cho các giá trị như sau: ô A4 = 4, ô A2 = 5, ô A3 = 6, ô A7 = 7 tại vị trí ô B2 lập công thức B2 = Sum(A4,A2,Count(A3,A4)) cho biết kết quả ô B2 sau khi Enter:', N'10', N'9', N'11', N' Lỗi', N'C', N'2019-09-09 11:41:58.297', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'129', N'9', null, N'Trong bảng tính Excel, ô A1 chứa nội dung “TTTH ĐHKHTN”. Khi thực hiện công thức = LEN(A1) thì giá trị trả về kết quả:', N'6', N'11', N'5', N'0', N'B', N'2019-09-09 11:41:58.297', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'130', N'9', null, N'Để đảm bảo an toàn dữ liệu ta chọn cách nào?', N' Đặt thuộc tính hidden.', N' Copy nhiều nơi trên ổ đĩa máy tính', N' Đặt thuộc tính Read only', N' Sao lưu dự phòng', N'D', N'2019-09-09 11:41:58.313', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'131', N'9', null, N'Trong soạn thảo word, công dụng của tổ hợp phím Ctrl + O dùng để', N' Tạo một văn bản mới', N' Đóng văn bản đang làm việc', N' Mở 1 văn bản đã có trên máy tính', N' Lưu văn bản đang làm việc', N'C', N'2019-09-09 11:41:58.333', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'132', N'9', null, N'Trong bảng tính Excel, để lưu tập tin đang mở dưới một tên khác, ta chọn:', N' File / Save As', N' File / Save', N' File / New', N' Edit / Replace', N'A', N'2019-09-09 11:41:58.337', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'133', N'9', null, N'Trong bảng tính Excel, Ô C2 chứa hạng của học sinh. Công thức nào tính học bổng theo điều kiện: Nếu xếp hạng từ hạng một đến hạng ba thì được học bổng là 200000, còn lại thì để trống', N' =IF(C2>=3, 200000, 0)', N' =IF(C2<=3, 200000, “”)', N' =IF(C2<=3, 0, 200000)', N' =IF(C2<3, 200000, “”)', N'B', N'2019-09-09 11:41:58.337', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'135', N'9', null, N'Để hủy bỏ thao tác vừa thực hiện ta nhấn tổ hợp phím:', N' Ctrl + X', N' Ctrl + Z', N' Ctrl + C', N' Ctrl + V', N'B', N'2019-09-09 11:41:58.343', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'136', N'10', null, N'Trong bảng tính Excel, các dạng địa chỉ sau đây, địa chỉ nào là địa chỉ tuyệt đối:', N' B$1$$10$D', N' B$1', N' $B1:$D10', N' $B$1:$D$10', N'D', N'2019-09-09 11:45:31.500', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'137', N'10', null, N'Trong Powerpoint muốn đánh số trang cho từng Slide ta dùng lệnh nào sau đây:', N' Insert\ Bullets and Numbering', N' Insert \ Text \ Slide Number.', N' Format \ Bullets and Number.', N' Các câu trên đều sai', N'B', N'2019-09-09 11:45:31.503', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'138', N'10', null, N'Để trình chiếu một Slide hiện hành, bạn sử dụng lựa chọn nào? (Chọn nhiều lựa chọn)', N' Nhấn tổ hợp phím Shift+F5', N' Nhấn tổ hợp phím Ctrl+Shift+F5', N' Slide Show -> Start Slide Show -> From Current Slide', N' Nhấn phím F5', N'C', N'2019-09-09 11:45:31.507', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'139', N'10', null, N'Khi thiết kế Slide với PowerPoint, muốn thay đổi mẫu nền thiết kế của Slide, ta thực hiện:', N' Design ->Themes …', N' Design ->Background…', N' Insert -> Slide Design …', N' Slide Show -> Themes…', N'A', N'2019-09-09 11:45:31.513', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'140', N'10', null, N'Chức năng công cụ nào sau đây trong nhóm dùng để xem trình chiếu slide đang hiển thị thiết kế (thanh công cụ zoom góc dưới bên phải của giao diện thiết kế):', N' Normal', N' Slide Sorter', N' Slide show', N' Reading view', N'D', N'2019-09-09 11:45:31.520', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'141', N'10', null, N'Chức năng Animations/ Timing/ Delay trong PowerPoint dùng để:', N' Thiết lập thời gian chờ trước khi slide được trình chiếu', N' Thiết lập thời gian chờ trước khi hiệu ứng bắt đầu', N' Thiết lập thời gian hoạt động cho tất cả các hiệu ứng', N' Tất cả đều đúng', N'B', N'2019-09-09 11:45:31.523', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'142', N'10', null, N'Cho biết cách xóa một tập tin hay thư mục mà không di chuyển vào Recycle Bin:?', N' Chọn thư mục hay tâp tin cần xóa -> Delete', N' Chọn thư mục hay tâp tin cần xóa -> Ctrl + Delete', N' Chọn thư mục hay tâp tin cần xóa -> Alt + Delete', N' Chọn thư mục hay tâp tin cần xóa -> Shift + Delete', N'D', N'2019-09-09 11:45:31.550', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'143', N'10', null, N'Danh sách các mục chọn trong thực đơn gọi là :', N' Menu pad', N' Menu options', N' Menu bar', N' Tất cả đều sai', N'C', N'2019-09-09 11:45:31.550', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'144', N'10', null, N'Khi một dòng chủ đề trong thư ta nhận được bắt đầu bằng chữ RE:; thì thông thường thư là:', N' Thư rác, thư quảng cáo', N' Thư mới', N' Thư của nhà cung cấp dịch vụ E-mail mà ta đang sử dụng', N' Thư trả lời cho thư mà ta đã gởi trước đó', N'D', N'2019-09-09 11:45:31.553', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'145', N'10', null, N'Chức năng Bookmark của trình duyệt web dùng để:', N' Lưu trang web về máy tính', N' Đánh dấu trang web trên trình duyệt', N' Đặt làm trang chủ', N' Tất cả đều đúng', N'B', N'2019-09-09 11:45:31.563', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'146', N'10', null, N'Nếu không kết nối được mạng, bạn vẫn có thể thực hiện được hoạt động nào sau đây:', N' Gửi email', N' Viết thư', N' Xem 1 trang web', N' In trên may in sử dụng chung cài đặt ở máy khác', N'D', N'2019-09-09 11:45:31.567', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'147', N'10', null, N'Khi muốn tìm kiếm thông tin trên mạng Internet, chúng ta cần', N' Tìm kiếm trên các Websites tìm kiếm chuyên dụng', N' Tùy vào nội dung tìm kiếm mà kết nối đến các Websites cụ thể.', N' Tìm kiếm ở bất kỳ một Websites nào', N' Tìm trong các sách danh bạ internet', N'A', N'2019-09-09 11:45:31.570', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'148', N'10', null, N'Trong soạn thảo Word, muốn tách một ô trong Table thành nhiều ô, ta thực hiện:', N' Table – Merge Cells', N' Table – Split Cells', N' Tools – Split Cells', N' Table – Cells', N'B', N'2019-09-09 11:45:31.597', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'149', N'10', null, N'Trong soạn thảo Word, thao tác nào sau đây sẽ kích hoạt lệnh Paste', N' Tại thẻ Home, nhóm Clipboard, chọn Paste', N' Bấm tổ hợp phím Ctrl + B', N' Chọn vào mục trong Office Clipboard', N' Tất cả đều đúng', N'A', N'2019-09-09 11:45:31.597', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'150', N'10', null, N'Trong bảng tính Excel, giá trị trả về của công thức =LEN(“TRUNG TAM TIN HOC”) là:', N'15', N'16', N'17', N'18', N'C', N'2019-09-09 11:45:31.600', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'151', N'10', null, N'Thiết bị xuất của máy tính gồm?', N' Bàn phím, màn hình, chuột', N' Màn hình, máy in.', N' Chuột, màn hình, CPU', N' Bàn phím, màn hình, loa', N'B', N'2019-09-09 11:45:31.610', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'152', N'10', null, N'Trong ứng dụng windows Explorer, để chọn nhiều tập tin hay thư mục không liên tục ta thực hiện thao tác kết hợp phím … với click chuột.', N'Shift', N' Alt', N' Tab', N' Ctrl', N'D', N'2019-09-09 11:45:31.617', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'153', N'10', null, N'Trong bảng tính Excel, ô A1 chứa giá trị 7.5. Ta lập công thức tại ô B1 có nội dung như sau =IF(A1>=5, “Trung Bình”, IF(A1>=7, “Khá”, IF(A1>=8, “Giỏi”, “Xuất sắc”))) khi đó kết quả nhận được là:', N' Giỏi.', N' Xuất sắc.', N' Trung Bình', N' Khá.', N'C', N'2019-09-09 11:45:31.617', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'154', N'10', null, N'Trong bảng tính Excel, hàm nào dùng để tìm kiếm:', N' Vlookup', N' IF', N' Left', N' Sum', N'A', N'2019-09-09 11:45:31.643', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'155', N'10', null, N'Khi làm việc với văn bản word, để bật chế độ nhập ký tự Subscript khi tạo ký tự hóa học H2O. Ta sử dụng chức năng nào: (Có thể chọn nhiều câu đúng).', N' Nhấn tổ hợp phím Ctrl + =', N' Nhấn tổ hợp phím Ctrl + Shift + +', N' Click chọn biểu tượng Superscipt trong nhóm Font', N' Click chọn biểu tượng Subscript trong nhóm Font', N'D', N'2019-09-09 11:45:31.643', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'156', N'10', null, N'Trong word, biểu tượng cây chổi có chức năng gì?', N' Sao chép nội dung văn bản', N' Canh lề văn bản', N' Sao chép định dạng', N' Mở văn bản đã có', N'C', N'2019-09-09 11:45:31.647', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'157', N'10', null, N'Thuộc tính phần mềm microsoft word 2010 có phần mở rộng là gì?', N' .dox', N' .docx', N' .xls', N' .xlsx', N'B', N'2019-09-09 11:45:31.653', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'159', N'13', null, N'Địa chỉ hộp thư điện tử của Tây Ninh là gì:', N' tayninh.vn', N' email.tayninh.vn', N' mail.tayninh.gov.vn', N' email.tayninh.gov.vn', N'C', N'2019-09-09 11:46:56.893', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'160', N'13', null, N'Trên địa bàn tỉnh Tây Ninh phần mềm một cửa điện tử đã được triển khai cho bao nhiêu xã, phường, thị trấn:', N' 95 xã, phường, thị trấn', N' 70 xã, phường, thị trấn', N' 40 xã, phường, thị trấn', N' 84 xã, phường, thị trấn', N'A', N'2019-09-09 11:46:56.893', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'161', N'13', null, N'Phát biểu nào sau đây đúng với dịch vụ công trực tuyến mức độ 3:', N' Là dịch vụ đảm bảo cho sự cung cấp đầy đủ các thông tin về những thủ tục hành chính và các văn bản có liên quan đến những quy định về thủ tục hành chính đó', N' Là dịch vụ cho phép người dân thanh toán trực tuyến lệ phí nếu có. Việc trả kết quả cho người dân sẽ được trả trực tuyến hoặc gửi trực tiếp qua đường bưu điện cho người dân.', N' Là dịch vụ cho phép người dân điền và gửi các mẫu văn bản trực tuyến đến các cơ quan, tổ chức nhà nước cung cấp dịch vụ. Trong quá trình xử lý hồ sơ và cung cấp dịch vụ, các giao dịch sẽ được thực hiện trên môi trường mạng internet. Người dân sẽ nhận kết quả và thanh toán lệ phí nếu có trực tiếp tại cơ quan và tổ chức nhà nước cung cấp dịch vụ.', N' Là dịch vụ cho phép người dân tải các mẫu văn bản về để khai báo và hoàn thiện hồ sơ theo yêu cầu. Các hồ sơ đó sau khi hoàn thiện sẽ được gửi trực tuyến đến các cơ quan, tổ chức hành chính nhà nước cung cấp dịch vụ.', N'C', N'2019-09-09 11:46:56.897', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'162', N'13', null, N'Phần mềm eGov đang được sử dụng cho các cơ quan đơn vị nhà nước của tỉnh Tây Ninh là phần mềm gì:', N' Phần mềm nhận thông tin cuộc họp', N' Phần mềm văn phòng điện tử', N' Phần mềm thống kê số liệu kinh tế xã hội', N' Phần mềm xử lý hồ sơ một cửa', N'B', N'2019-09-09 11:46:56.900', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'163', N'13', null, N'Cổng hành chính công tỉnh Tây Ninh trên mạng xã hội Zalo có chức năng nào sau đây(chọn câu trả lời đúng nhất):', N' Nộp hồ sơ, tra cứu tình trạng hồ sơ, gửi phản ánh kiến nghị, trả kết quả trực tuyến', N' Nộp hồ sơ, tra cứu tình trạng hồ sơ, tra cứu giá đất', N' Tra cứu hồ sơ, tra cứu địa điểm du lịch, tra cứu giá đất', N' Nộp hồ sơ trực tuyến, tra cứu địa điểm du lịch, phản ảnh kiến nghị', N'A', N'2019-09-09 11:46:56.903', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'164', N'13', null, N'Địa chỉ truy phần mềm Họp không giấy của tỉnh Tây Ninh là gì:', N' tayninh.hopkhonggiay.vn', N' hopkhonggiay.tayninh.gov', N' khonggiay.tayninh.vn', N' hopkhonggiay.tayninh.gov.vn', N'D', N'2019-09-09 11:46:56.907', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'165', N'13', null, N'Phần mềm Hỏi đáp trực tuyến của tỉnh Tây Ninh là phần mềm tiếp nhận phản ảnh kiến nghị của người dân, các đơn vị được phần công trả lời có thời gian tối đa là bao nhiêu để trả lời cho người dân:', N' 8 ngày làm việc', N' 7 ngày làm việc', N' 5 ngày làm việc', N' 4 ngày làm việc', N'A', N'2019-09-09 11:46:56.940', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'166', N'13', null, N'Phần mềm kinh tế xã hội đang được triển khai ở tỉnh Tây Ninh là phần mềm gì:', N' Cập nhật tin tức, thông văn hóa xã hội của tỉnh', N' Thông kê số liệu kinh tế xã hội của các ngành, địa phương', N' Chỉ đạo điều hành công việc', N' Cập nhật các thông tin văn bản', N'B', N'2019-09-09 11:46:56.940', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'167', N'12', null, N'Để xóa kí tự trong văn bản, ta sử dụng những phím nào trên bàn phím?', N' Backspace, Delete', N' Delete, Insert', N' Backspace, End', N' Insert, End', N'A', N'2019-09-09 13:37:25.980', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'168', N'12', null, N'Trong MS Excel 2007, cột C xuất hiện ngay sau cột A là do:', N' Cột B bị ẩn (Hide)     ', N' Trong Excel 2007 không có cột B', N' Cột B trong Excel 2007 đã bị xoá                    ', N' Cột B bị đổi thành tên khác', N'A', N'2019-09-09 13:37:25.980', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'169', N'12', null, N'Trong MS Excel 2007, để đếm dữ liệu kiểu số ta dùng hàm: ', N' COUNT ', N' RANK', N' SUM', N' AVERAGE', N'A', N'2019-09-09 13:37:25.980', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'170', N'12', null, N'Trong MS Excel 2007, địa chỉ ô $A$10 là: ', N'Địa chỉ tuyệt đối', N' Địa chỉ tương đối       ', N'Địa chỉ hỗn hợp                             ', N' Địa chỉ đã bị viết sai', N'A', N'2019-09-09 13:37:25.980', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'171', N'12', null, N'Trong MS Excel 2007, nếu thấy nội dung của một ô có dạng ##### thì kết luận: ', N' Chiều rộng cột không đủ để hiển thị dữ liệu', N' Dữ liệu trong ô có lỗi', N' Kết quả tính toán trong ô có lỗi', N' Định dạng dữ liệu của ô có lỗi  ', N'A', N'2019-09-09 13:37:25.980', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'172', N'12', null, N'Trong MS Excel 2007, địa chỉ tuyệt đối tại cột, tương đối tại hàng là: ', N' $B1:$D10', N' B$1:D$10', N' B$1$:D$10$', N' $B$1:$D$10', N'A', N'2019-09-09 13:37:27.287', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'173', N'12', null, N'Trong MS PowerPoint 2007, để đánh số thứ tự trang thuyết trình ta vào Insert chọn?', N' Text Box                                       ', N' Slide Number', N' Symbol                                          ', N' Object', N'B', N'2019-09-09 13:37:27.337', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'174', N'12', null, N'Mạng máy tính là?', N'Tập hợp các máy tính được nối với nhau theo một phương thức nào đó.', N'Tập hợp các máy tính.', N' Exit                        ', N' Motion Paths', N'A', N'2019-09-09 13:37:27.337', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'175', N'12', null, N'Trong MS Excel 2007, tại ô G32 ta nhập =(7+9):2 thì kết quả là: ', N' Báo lỗi             ', N' 8                       ', N' False                ', N' (7+9):2', N'A', N'2019-09-09 13:37:27.337', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'176', N'12', null, N'Trong MS PowerPoint 2007, để xem trước bài thuyết trình trước khi in ta dùng tổ hợp phím?', N' Ctrl + F2               ', N' Shift + F2  ', N' Alt + F2                 ', N' Tab + F2', N'A', N'2019-09-09 13:37:27.340', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'177', N'12', null, N'Trong MS PowerPoint 2007, để làm xuất hiện các đối tượng khi trình chiếu ta chọn loại hiệu ứng?', N' Exit                        ', N' Emphasis              ', N'Mạng LAN.', N'Mạng có dây.', N'A', N'2019-09-09 13:37:27.340', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'178', N'12', null, N'Trên mạng Internet, thuật ngữ HTTP (HyperText Transfer Protocol) là:', N' Là giao thức truyền tệp tin siêu văn bản ', N' Là ngôn ngữ để soạn thảo nội dung các trang Web', N' Là tên của trang Web', N' Là địa chỉ của trang Web', N'A', N'2019-09-09 13:37:27.340', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'179', N'12', null, N'Tường lửa (Firewall) là: ', N' Một hệ thống báo cháy', N' Một chương trình cho phép đọc thư điện tử', N' Một hệ thống chống xâm nhập thông tin trái phép', N' Một lệnh hoặc nhóm lệnh cho phép đăng nhập vào phần mềm', N'A', N'2019-09-09 13:37:27.387', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'180', N'12', null, N'Trang Web tĩnh là: ', N' Trang Web có nội dung cố định, không thể tùy biến theo yêu cầu từ phía máy khách (client)', N' Trang Web chỉ có văn bản và các hình ảnh tĩnh có thể tương tác với người sử dụng', N' Trang Web chỉ có văn bản và hình ảnh, không có các đoạn phim hoặc âm thanh', N' Nội dung tương tác với người sử dụng cao', N'A', N'2019-09-09 13:37:27.387', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'181', N'12', null, N'Trên mạng Internet, Máy tìm kiếm (Search engine) không có khả năng:', N'Người dùng có thể nhập mọi loại thông tin để tìm kiếm', N' Lưu trữ thông tin về các Website trên Internet', N' Kết quả tìm kiếm khác nhau do có nhiều máy tìm kiếm khác nhau', N'Tìm các thông tin trên mạng Internet theo nội dung yêu cầu từ người dùng', N'A', N'2019-09-09 13:37:27.390', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'182', N'12', null, N'Khi sử dụng Internet, yếu tố nào cần phải được chú ý?', N' Quản lý hệ thống chiếu sáng', N' Truyền thông kỹ thuật số', N' Bảo vệ thông tin an toàn tuyệt đối cho người dùng', N' Trung tâm lưu trữ, xử lý dữ liệu', N'C', N'2019-09-09 13:37:27.390', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'183', N'12', null, N'Để quản lý người sử dụng Internet, phương án sai là: ', N' Phổ biến, hướng dẫn người sử dụng và khai thác dịch vụ internet đúng qui định pháp luật của nhà nước', N' Phối kết hợp cùng chính quyền, địa phương, cơ quan và gia đình để quản lý người sử dụng Internet', N' Tăng cường quản lý hoạt động kinh doanh Internet trên địa bàn', N' Chính quyền bỏ qua kiểm tra, giám sát việc kinh doanh Internet tạo các điều kiện tốt để kinh doanh Internet phát triển ', N'D', N'2019-09-09 13:37:27.390', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'184', N'12', null, N'Trên mạng Internet, chọn phát biểu sai về tên miền là:', N' Tên miền là tên thay thế về một địa chỉ IP', N' Tên miền là tên giao dịch của 1 công ty hay một tổ chức sử dụng trên Internet', N' Công việc chuyển đổi từ tên miền sang địa chỉ IP do máy chủ DNS đảm trách', N' Có nhiều tên miền giống nhau cùng hoạt động', N'D', N'2019-09-09 13:37:27.390', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'185', N'12', null, N'Website là: ', N' Một giao thức truyền tệp tin siêu văn bản', N' Một hay nhiều trang web được tổ chức dưới một địa chỉ truy cập', N' Một hệ thống chống xâm nhập thông tin trái phép', N' Một liên kết đến một trang Web khác', N'B', N'2019-09-09 13:37:27.433', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'187', N'12', null, N'Trong máy tính CPU là viết tắt của cụm từ Tiếng Anh nào sau đây?', N' Case Processing Unit', N' Command Processing Unit', N'Central Processing Unit', N' Control Processing Unit', N'C', N'2019-09-09 13:37:27.440', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'188', N'12', null, N'Đơn vị đo thông tin nhỏ nhất là:', N'Bit', N' Byte        ', N' GHz       ', N' Hz ', N'A', N'2019-09-09 13:37:27.440', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'189', N'7', null, N'Chương trình dùng để xem các trang Web được gọi là?', N' Trình duyệt Web', N' Bộ duyệt Web', N' Chương trình xem Web', N' Phần mềm xem Web', N'A', N'2019-09-09 13:49:50.117', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'190', N'7', null, N'Cho các ứng dụng: 1. Chromium, 2. Google Chrome, 3. DriverMax, 4. Safari. Ứng dụng nào là trình duyệt Web?', N' Các ứng dụng: 1,2,4.', N' Các ứng dụng: 1,2,3.', N' Các ứng dụng: 1,3,4.', N' Các ứng dụng: 2,3,4.', N'A', N'2019-09-09 13:49:50.123', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'191', N'7', null, N'Tên miền edu.vn được dùng cho: ', N' Các đơn vị, tổ chức giáo dục ', N' Tất cả các cơ quan, tổ chức…', N' Chính phủ, các cơ quan hành chính, tổ chức nhà nước trên lãnh thổ Việt Nam.', N' Các tổ chức phi chính phủ.', N'A', N'2019-09-09 13:49:50.133', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'192', N'7', null, N'Giữa phần tên và phần mở rộng của tệp được phân cách bởi dấu:', N' Dấu chấm (.)                                            ', N' Dấu hai chấm (:)', N' Dấu sao (*)                                               ', N' Dấu phẩy (,)', N'A', N'2019-09-09 13:49:50.137', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'193', N'7', null, N'Trong Control Panel của Hệ điều hành Windows, để thay đổi ngày, giờ hệ thống, ta chọn: ', N' Clock, Language, and Region', N' Program', N' Network', N' Fonts ', N'A', N'2019-09-09 13:49:50.137', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'194', N'7', null, N'Định dạng nào sau đây thuộc định dạng tệp âm thanh: ', N' “.mp3”                                                      ', N' “.xls”', N' “.jpg”                                                         ', N' “.doc”', N'A', N'2019-09-09 13:49:50.160', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'195', N'7', null, N'Trong Hệ điều hành Windows, tệp là khái niệm chỉ?', N' Một đơn vị lưu trữ thông tin trên bộ nhớ ngoài', N' Một văn bản.', N' Một gói tin', N' Một trang web', N'A', N'2019-09-09 13:49:50.170', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'196', N'7', null, N'Trong MS Word, để tìm và thay thế kí tự, từ, cụm từ trong văn bản sử dụng tổ hợp phím: ', N' Ctrl + A                 ', N' Ctrl + F                  ', N' Ctrl + H                 ', N' Ctrl + G', N'C', N'2019-09-09 13:49:50.170', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'197', N'7', null, N'Trong MS Word, tại hộp thoại Paragraph thiết lập khoảng cách giãn đoạn so với đoạn văn bản phía trên chọn: ', N' Alignment ', N' Before                   ', N' After                      ', N' Line Spacing', N'D', N'2019-09-09 13:49:50.180', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'198', N'7', null, N'Trong MS Word, để định dạng khung, viền cho đoạn văn bản thiết lập tại: ', N' Border                   ', N' Shading                 ', N' Buletts                   ', N' Numbering', N'A', N'2019-09-09 13:49:50.183', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'199', N'7', null, N'Trong MS Word, để chèn kí hiệu đặc biệt trong văn bản, tại mục Insert chọn: ', N'Hyperlink  ', N'Page Number         ', N'Symbol                   ', N'Number', N'C', N'2019-09-09 13:49:50.183', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'200', N'7', null, N'Trong MS Word, cỡ chữ thường được sử dụng trong văn bản hành chính là: ', N' 12 - 14                   ', N' 10 – 12                  ', N' 15 – 16                  ', N' 9 - 11', N'A', N'2019-09-09 13:49:50.217', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'201', N'7', null, N'Trong MS Word, tại hộp thoại Page Setup để thiết lập hướng giấy ngang cho văn bản chọn: ', N' Portrait                  ', N' Landscape            ', N' Left                        ', N' Right', N'B', N'2019-09-09 13:49:50.220', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'202', N'7', null, N'Trong MS Excel, giao của một dòng và một cột được gọi là: ', N' Ô                            ', N' Vùng                      ', N' Dữ liệu                  ', N' Trường', N'A', N'2019-09-09 13:49:50.230', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'203', N'7', null, N'Trong MS Excel, địa chỉ ô nào sau đây đúng?', N' EF11                      ', N' 1A                          ', N' $1$A                     ', N' B111A11 ', N'A', N'2019-09-09 13:49:50.230', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'204', N'7', null, N'Trong MS Excel, địa chỉ vùng đúng là: ', N' B1...H15                ', N' B1:H15                  ', N' B1-H15                 ', N' B1..H15', N'B', N'2019-09-09 13:49:50.233', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'205', N'7', null, N'Trong MS Excel, Sheet được chèn thêm vào Sheet hiện hành có vị trí: ', N' Ngay phía sau Sheet hiện hành', N' Cuối tất cả các Sheet', N' Trước tất cả các Sheet', N' Ngay phía trước Sheet hiện hành', N'D', N'2019-09-09 13:49:50.240', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'206', N'7', null, N'Trong MS Excel, nếu thí sinh đạt từ 10 điểm trở lên thì xếp loại Đạt, ngược lại xếp loại Không đạt. Công thức nào dưới đây thể hiện đúng điều này (Giả sử ô G6 đang chứa điểm thi).', N' =IF(G6>=10,"Đạt", "Không đạt")         ', N' =IF(G6>10,"Đạt","Không Đạt")', N' =IF(G6=<10,"Đạt", "Không đạt")       ', N' =IF(G6>=10;”Không đạt”, “Đạt”)', N'A', N'2019-09-09 13:49:50.270', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'207', N'7', null, N'Trong MS PowerPoint, để chèn một siêu liên kết vào đối tượng trên trang thuyết trình ta vào Insert chọn?', N' Table                                             ', N' Picture', N' Chart                                             ', N' Hyper Link', N'D', N'2019-09-09 13:49:50.277', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'208', N'7', null, N'Trong MS PowerPoint, để chèn hộp chứa văn bản vào trang thuyết trình ta vào Insert chọn?', N' Text Box                                       ', N' Slide Number', N' Symbol                                          ', N' Object', N'A', N'2019-09-09 13:49:50.277', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'209', N'7', null, N'Trong MS Excel, List Separator dùng để xác định: ', N' Dấu ngăn cách số hàng ngàn', N' Dấu ngăn cách số thập phân', N' Dấu kết thúc một hàm trong Excel', N' Dấu ngăn cách giữa các đối số của hàm', N'A', N'2019-09-09 13:49:50.283', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'210', N'7', null, N'Trong MS PowerPoint, cách chuyển trang (Transition) được hiểu là?', N' Hiệu ứng chuyển tiếp giữa các trang thuyết trình', N' Hiệu ứng động cho các đối tượng trong trang thuyết trình', N' Hiệu ứng khi bắt đầu trình chiếu', N' Hiệu ứng khi kết thúc trình chiếu', N'A', N'2019-09-09 13:49:50.283', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'211', N'8', null, N'Sử dụng chương trình nào của Windows để quản lý các tệp tin và thư mục?', N' Microsoft Office', N' Accessories', N' Control Panel', N'Windows Explorer / File Explorer ', N'D', N'2019-09-09 13:51:53.273', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'212', N'8', null, N'Có thể khôi phục tệp tin bị xóa nhầm bằng cách mở cửa sổ nào? ', N'Documents', N'Computer', N'Internet Explorer', N'Recycle Bin', N'D', N'2019-09-09 13:51:53.273', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'213', N'8', null, N'Trong hệ điều hành Windows, tên thư mục nào sau đây đặt không hợp lệ? ', N' VANBAN', N' TINTUC', N' HOCTAP', N'BT\TIN', N'D', N'2019-09-09 13:51:53.273', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'214', N'8', null, N'Trong soạn thảo văn bản Word, muốn tắt đánh dấu chọn khối văn bản (tô đen), ta thực hiện: ', N'Bấm phím Enter', N'Bấm phím Space', N' Bấm phím mũi tên di chuyển', N'Bấm phím Tab', N'C', N'2019-09-09 13:51:53.273', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'215', N'8', null, N'Trong soạn thảo Winword, công dụng của tổ hợp phím Ctrl - H là : ', N'Tạo tệp văn bản mới', N'Chức năng thay thế trong soạn thảo', N'Định dạng chữ hoa', N'Lưu tệp văn bản vào đĩa', N'B', N'2019-09-09 13:51:53.637', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'216', N'8', null, N'Trong soạn thảo Word, công dụng của tổ hợp phím Ctrl - O là: ', N' Mở một hồ sơ mới', N' Đóng hồ sơ đang mở', N' Mở một hồ sơ đã có', N' Lưu hồ sơ vào đĩa', N'C', N'2019-09-09 13:51:53.637', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'217', N'8', null, N'Trong  PowerPoint, để thêm số trang cho Slide, thực hiện như thế nào?', N'Insert > Slide number', N' View >Slide number', N' Home>Slide number', N' Design>Slide number', N'A', N'2019-09-09 13:51:54.200', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'218', N'8', null, N'Trong  PowerPoint, thực hiện cách nào để ẩn Slide đang chọn ?', N' Format >Hide Slide', N'Slide Show>Hide Side', N' Slide Show>Hide', N' Format>Hide', N'B', N'2019-09-09 13:51:54.200', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'219', N'8', null, N'Trong soạn thảo Winword, công dụng của tổ hợp Ctrl - F là : ', N' Tạo tệp văn bản mới', N'Lưu tệp văn bản vào đĩa', N'Chức năng tìm kiếm trong soạn thảo', N'Định dạng trang', N'C', N'2019-09-09 13:51:54.200', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'220', N'8', null, N'Trong  PowerPoint, chế độ hiển thị nào cho phép soạn thảo văn bản?', N' Slide Sorter', N'Note Page', N'Normal', N'Reading View', N'C', N'2019-09-09 13:51:54.200', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'221', N'8', null, N'Khi đang làm việc với PowerPoint, muốn copy một Slide, ta chọn Slide cần copy và thực hiện: ', N' Insert>Copy', N'Home>Copy', N' File>Copy', N'View>Copy', N'B', N'2019-09-09 13:51:54.630', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'222', N'8', null, N'Trong bảng tính Excel, tại ô A2 gõ vào công thức =MAX(35,10,87,5) thì nhận được kết quả tại ô A2 là: ', N'35', N'5', N'87', N'110', N'C', N'2019-09-09 13:51:54.800', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'223', N'8', null, N'Trong khi làm việc với Excel, để nhập vào công thức tính toán cho một ô, trước hết ta phải gõ: ', N'Dấu chấm hỏi (?)', N'Dấu bằng (=)', N' Dấu hai chấm (:)', N' Dấu đô la ($)', N'B', N'2019-09-09 13:51:54.800', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'224', N'8', null, N'Trong bảng tính Excel, tại ô A2 có sẵn giá trị chuỗi 2017 ; Tại ô B2 gõ vào công thức  =VALUE(A2) thì nhận được kết quả :', N'#NAME!', null, N'Giá trị kiểu chuỗi 2017', N'Giá trị kiểu số 2017', N'D', N'2019-09-09 13:51:54.800', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'225', N'8', null, N'Trong bảng tính Excel, muốn sắp xếp danh sách dữ liệu theo thứ tự tăng (giảm), ta thực hiện: ', N' Tools - Sort', N' File - Sort', N' Data - Sort', N' Format - Sort', N'C', N'2019-09-09 13:51:54.800', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'226', N'8', null, N'Trong  PowerPoint, phím F12 dùng để thực hiện công việc gì?', N'Mở tệp tin', N'Lưu tệp tin với tên khác', N'Xóa tệp tin', N'Di chuyển tệp tin', N'B', N'2019-09-09 13:51:54.800', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'227', N'8', null, N'Trong bảng tính Excel, hàm nào sau đây cho phép tính tổng các giá trị kiểu số thỏa mãn một điều kiện cho trước?', N' SUM', N' COUNTIF', N' COUNT', N' SUMIF', N'D', N'2019-09-09 13:51:54.123', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'228', N'8', null, N'Trong các dạng địa chỉ sau đây, địa chỉ nào là địa chỉ tuyệt đối?', N'B$1:D$10', N'$B1:$D10', N'B$1$:D$10$', N'$B$1:$D$10', N'D', N'2019-09-09 13:51:54.163', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'229', N'8', null, N'Phần mềm nào sau đây không phải là trình duyệt Web? ', N'Microsoft Internet Explorer', N'Mozilla Firefox', N' Google Chrome', N'Unikey', N'D', N'2019-09-09 13:51:54.163', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'230', N'8', null, N'Thiết bị có khả năng truy cập thông tin nhanh nhất là: ', N' HDD/SSD             ', N' DVD          ', N' CD/VCD               ', N' USB', N'A', N'2019-09-09 13:51:54.163', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'231', N'8', null, N'Ngày nay, một số thiết bị ngoại vi như bàn phím, chuột, máy quét, máy in, webcam, micro, ...  thường được kết nối với máy tính thông qua cổng nào?', N'USB', N' Parallel (cổng song song)', N' LAN', N' Audio', N'A', N'2019-09-09 13:51:54.660', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'232', N'8', null, N'Nhược điểm của máy tính khi kết nối mạng là: ', N' Tạo môi trường dễ lay lan virus ', N' Dễ dàng bảo trì, sửa chữa', N' Thông tin cập nhật đồng bộ  ', N' Dùng chung tài nguyên bao gồm phần cứng và phần mềm', N'A', N'2019-09-09 13:51:54.667', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'233', N'11', null, N'Nhiệm vụ cơ bản của tường lửa là?', N'Kiểm soát giao thông dữ liệu giữa hai vùng có độ tin cậy khác nhau.', N'Chương trình diệt virus.', N'Điều khiển máy tính hoạt động trên mạng.', N'Giao thức để trao đổi thông tin.', N'A', N'2019-09-09 13:52:59.720', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'234', N'11', null, N'Chọn câu đúng về đơn vị đo thông tin', N'1GB = 1024MB.  ', N'1B = 1024 Bit.', N'1KB = 1024MB.              ', N'1Bit= 1024B.', N'A', N'2019-09-09 13:52:59.720', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'235', N'11', null, N'Phần mềm hệ thống:', N'có chức năng giám sát và điều phối thực hiện các chương trình.', N'còn được gọi là chương trình giám sát.', N'còn có tên khác là phần mềm ứng dụng.', N'là phần mềm để giải quyết những việc thường gặp.', N'A', N'2019-09-09 13:52:59.720', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'236', N'11', null, N'Chọn phát biểu sai trong các thuật ngữ sau:', N'Địa chỉ URL là ngôn ngữ toàn cầu của các tài liệu và các nguồn khác trên Web.  ', N'Trang Web là một văn bản trên World Wide Web.  ', N'Website là một địa chỉ trên World Wide Web.  ', N'World Wide Web là một hệ thống các máy chủ trên Internet hỗ trợ các văn bản có định dạng ngôn ngữ đánh dấu siêu văn bản (HTML).  ', N'A', N'2019-09-09 13:52:59.730', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'237', N'11', null, N'Phát biểu nào dưới đây không phải là của CPU?', N'Dùng để nhập và lưu trữ thông tin.', N'Thành phần quan trong nhất của máy tính.', N'Thiết bị chính thực hiện và điều khiển việc thực hiện của chương trình.', N'Gồm hai bộ phận chính là bộ điều kiển và bộ số học logic.', N'A', N'2019-09-09 13:52:59.730', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'238', N'11', null, N'Hệ điều hành là:', N'phần mềm hệ thống.        ', N'phần mềm ứng dụng.', N'phần mềm văn phòng.                ', N'phần mềm công cụ.', N'A', N'2019-09-09 13:52:59.760', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'239', N'11', null, N'Chức năng nào dưới đây không thuộc chức năng hệ điều hành?', N'Tổ chức giao tiếp giữa trang web với hệ thống.', N'Cung cấp tài nguyên cho chương trình.', N'Cung cấp công cụ tìm kiếm và truy cập thông tin.', N'Cung cấp dịch vụ tiện ích hệ thống.', N'A', N'2019-09-09 13:52:59.767', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'240', N'11', null, N'Phần mềm nào sau đây dùng để nén thư mục và tập tin?', N' WinRAR.', N' WindowsRAR.', N' Photoshop.', N' Jet-audio.', N'A', N'2019-09-09 13:52:59.770', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'241', N'11', null, N'Đối với hệ điều hành WINDOWS, tên của một tập tin dài tối đa bao nhiêu kí tự?  ', N' 255  kí tự.', N' 1024 kí tự.', N' 256  kí tự.                         ', N' 8  kí tự.      ', N'A', N'2019-09-09 13:52:59.777', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'242', N'11', null, N'Trong hệ điều hành Windows, muốn đổi tên cho thư mục đang chọn ta:', N'  nhấn phím F2, gõ tên mới cho thư mục và nhấn phím Enter.', N' nhấn tổ hợp phím Ctrl + R, gõ tên mới cho thư mục và nhấn phím Enter.', N' nháy Edit à Rename, gõ tên mới cho thư mục và nhấn phím Enter.', N' nháy Edit à Move to Folder, gõ tên mới cho thư mục và nhấn phím Enter.', N'A', N'2019-09-09 13:52:59.777', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'243', N'11', null, N'Để lưu văn bản sau khi đã soạn thảo trong Microsoft Word ta: ', N' Nhấn tổ hợp phím Ctrl + S.', N' Nhấn tổ hợp phím Ctrl + F.', N' Nháy vào nút lệnh Save trên thanh định dạng.', N' Nháy vào nút lệnh Save trên thanh trạng thái.', N'A', N'2019-09-09 13:52:59.807', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'244', N'11', null, N'Trong Microsoft Word, muốn di chuyển con trỏ soạn thảo về đầu dòng hiện hành ta dùng phím: ', N' Home', N' End', N' Ctrl + Page Up', N' Ctrl + Page Down', N'A', N'2019-09-09 13:52:59.813', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'245', N'11', null, N'Trong Microsoft Word, để mở một văn bản có sẵn ta thực hiện?', N' Mở đường dẫn đến nơi đã lưu, kích đúp lên văn bản đó.', N' Chọn ổ đĩa đã lưu văn bản, tìm đến nơi đã lưu, kích chuột phải lên văn bản đó.', N' Mở phần mềm Microsoft Word, Ctrl + P, tìm đến văn bản đã lưu.', N' Mở phần mềm Microsoft Word, Ctrl + E, tìm đến văn bản đã lưu.', N'A', N'2019-09-09 13:52:59.813', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'246', N'11', null, N'Trong Microsoft Word, để thực hiện ngắt trang văn bản ta nhấn tổ hợp phím?', N' Ctrl + enter.', N' Shift + enter.', N' Shift + alt + enter.', N' Ctrl+ Pagedown.', N'A', N'2019-09-09 13:52:59.817', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'247', N'11', null, N'Trong Microsoft Word, Font chữ nào không thuộc bộ mã theo chuẩn Unicode?', N' Vni-Times.', N' Arial.', N' Times New Roman.', N' Tahoma.', N'A', N'2019-09-09 13:52:59.820', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'248', N'11', null, N'Trong bảng tính MS Excel, giả sử  A1, A2, A3, chứa lần lượt các số: 24, 25, 36. Tại  A4 ta điền công thức = ROUND(AVERAGE(A1:A3),-1) thì hiển thị kết quả là?', N'30', N'28', N'29', N'31', N'A', N'2019-09-09 13:52:59.823', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'249', N'11', null, N'Trong bảng tính MS Excel, các hàm AND, OR, NOT là:', N'Hàm logic.', N'Toán tử.', N'Hàm thống kê.', N'Hàm cơ sở dữ liệu.', N'A', N'2019-09-09 13:52:59.860', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'250', N'11', null, N'Trong bảng tính MS Excel, các địa chỉ sau địa chỉ nào không hợp lệ?', N'A1:A1$A', N'A5', N'AAB$3', N'A5:K$5', N'A', N'2019-09-09 13:52:59.863', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'251', N'11', null, N'Phần mềm nào dưới đây không phải là phần mềm trình chiếu?', N' OpenOffice Slide.', N' Google Docs Presentation.', N' OpenOffice Impress.', N' Microsoft Powerpoint.', N'A', N'2019-09-09 13:52:59.863', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'252', N'11', null, N'Trong phần mềm trình chiếu Power Point, dùng các mẫu slide được xây dựng sẵn kèm theo phần mềm ta chọn?', N' Sample templates.', N' Presentation.', N' All Outlines.', N' Design.', N'A', N'2019-09-09 13:52:59.863', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'253', N'11', null, N'Trong phần mềm trình chiếu Power Point, để chèn biểu đồ vào trong slide ta thực hiện: ', N' chọn Insert ->Chart. ', N' chọn Format->Chart.  ', N' chọn File ->Chart. ', N' chọn Chart->Insert.', N'A', N'2019-09-09 13:52:59.867', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'254', N'11', null, N'Tên miền trong địa chỉ website có .edu cho biết Website đó thuộc về?', N'Lĩnh vực chính phủ', N'Lĩnh vực giáo dục', N'Lĩnh vực cung cấp thông tin', N'Thuộc về các tổ chức khác', N'B', N'2019-09-09 16:17:17.990', N'1')
GO
GO
INSERT INTO [dbo].[cauhoi] ([id_cauhoi], [id_loaicauhoi], [img_content], [content], [answer_a], [answer_b], [answer_c], [answer_d], [correct_answer], [timestamps], [del_stt]) VALUES (N'255', N'12', null, N'Phần mềm nào sau đây không phải trình duyệt WEB?', N'Microsoft Internet Explorer', N'Mozilla Firefox', N'Netcape', N'Unikey', N'D', N'2019-09-09 16:17:52.480', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[cauhoi] OFF
GO

-- ----------------------------
-- Table structure for chitiet_lop
-- ----------------------------
DROP TABLE [dbo].[chitiet_lop]
GO
CREATE TABLE [dbo].[chitiet_lop] (
[id_chitiet_lop] int NOT NULL IDENTITY(1,1) ,
[id_class] int NOT NULL ,
[id_student] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitiet_lop]', RESEED, 18)
GO

-- ----------------------------
-- Records of chitiet_lop
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitiet_lop] ON
GO
SET IDENTITY_INSERT [dbo].[chitiet_lop] OFF
GO

-- ----------------------------
-- Table structure for chitietbaithi
-- ----------------------------
DROP TABLE [dbo].[chitietbaithi]
GO
CREATE TABLE [dbo].[chitietbaithi] (
[ID_chitiet] int NOT NULL IDENTITY(1,1) ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[chitietbaithi]', RESEED, 220)
GO

-- ----------------------------
-- Records of chitietbaithi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[chitietbaithi] ON
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'34', N'925130', N'131', N'2019-09-09 11:42:20.870')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'35', N'925130', N'133', N'2019-09-09 11:42:20.870')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'36', N'925130', N'130', N'2019-09-09 11:42:20.870')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'37', N'925130', N'135', N'2019-09-09 11:42:20.870')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'38', N'925130', N'128', N'2019-09-09 11:42:20.930')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'39', N'925130', N'132', N'2019-09-09 11:42:20.930')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'40', N'925130', N'129', N'2019-09-09 11:42:20.140')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'41', N'925130', N'127', N'2019-09-09 11:42:20.143')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'42', N'925130', N'126', N'2019-09-09 11:42:20.143')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'43', N'925130', N'125', N'2019-09-09 11:42:20.143')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'44', N'925130', N'124', N'2019-09-09 11:42:20.143')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'45', N'925130', N'122', N'2019-09-09 11:42:20.147')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'46', N'925130', N'123', N'2019-09-09 11:42:20.190')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'47', N'925130', N'121', N'2019-09-09 11:42:20.193')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'48', N'925130', N'118', N'2019-09-09 11:42:20.193')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'49', N'925130', N'117', N'2019-09-09 11:42:20.193')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'50', N'925130', N'120', N'2019-09-09 11:42:20.207')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'51', N'925130', N'119', N'2019-09-09 11:42:20.207')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'52', N'925130', N'115', N'2019-09-09 11:42:20.243')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'53', N'925130', N'114', N'2019-09-09 11:42:20.247')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'54', N'925130', N'116', N'2019-09-09 11:42:20.247')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'55', N'925130', N'113', N'2019-09-09 11:42:20.250')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'56', N'925130', N'165', N'2019-09-09 11:47:37.793')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'57', N'925130', N'166', N'2019-09-09 11:47:37.793')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'58', N'925130', N'161', N'2019-09-09 11:47:37.810')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'59', N'925130', N'163', N'2019-09-09 11:47:37.810')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'60', N'925130', N'162', N'2019-09-09 11:47:37.810')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'61', N'925130', N'164', N'2019-09-09 11:47:37.810')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'62', N'925130', N'159', N'2019-09-09 11:47:37.843')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'63', N'925130', N'160', N'2019-09-09 11:47:37.853')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'64', N'925131', N'157', N'2019-09-09 11:48:15.407')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'65', N'925131', N'156', N'2019-09-09 11:48:15.410')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'66', N'925131', N'154', N'2019-09-09 11:48:15.410')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'67', N'925131', N'155', N'2019-09-09 11:48:15.410')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'68', N'925131', N'152', N'2019-09-09 11:48:15.413')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'69', N'925131', N'153', N'2019-09-09 11:48:15.420')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'70', N'925131', N'151', N'2019-09-09 11:48:15.457')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'71', N'925131', N'150', N'2019-09-09 11:48:15.460')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'72', N'925131', N'148', N'2019-09-09 11:48:15.460')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'73', N'925131', N'149', N'2019-09-09 11:48:15.460')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'74', N'925131', N'147', N'2019-09-09 11:48:15.467')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'75', N'925131', N'146', N'2019-09-09 11:48:15.473')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'76', N'925131', N'145', N'2019-09-09 11:48:15.503')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'77', N'925131', N'144', N'2019-09-09 11:48:15.507')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'78', N'925131', N'142', N'2019-09-09 11:48:15.510')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'79', N'925131', N'143', N'2019-09-09 11:48:15.510')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'80', N'925131', N'141', N'2019-09-09 11:48:15.520')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'81', N'925131', N'140', N'2019-09-09 11:48:15.530')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'82', N'925131', N'139', N'2019-09-09 11:48:15.550')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'83', N'925131', N'138', N'2019-09-09 11:48:15.557')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'84', N'925131', N'137', N'2019-09-09 11:48:15.560')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'85', N'925131', N'136', N'2019-09-09 11:48:15.560')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'86', N'925131', N'165', N'2019-09-09 11:48:37.123')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'87', N'925131', N'166', N'2019-09-09 11:48:37.123')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'88', N'925131', N'164', N'2019-09-09 11:48:37.127')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'89', N'925131', N'163', N'2019-09-09 11:48:37.130')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'90', N'925131', N'162', N'2019-09-09 11:48:37.133')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'91', N'925131', N'161', N'2019-09-09 11:48:37.140')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'92', N'925131', N'159', N'2019-09-09 11:48:37.170')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'93', N'925131', N'160', N'2019-09-09 11:48:37.180')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'94', N'925126', N'207', N'2019-09-09 13:56:40.590')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'95', N'925126', N'205', N'2019-09-09 13:56:40.593')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'96', N'925126', N'208', N'2019-09-09 13:56:40.593')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'97', N'925126', N'209', N'2019-09-09 13:56:40.593')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'98', N'925126', N'206', N'2019-09-09 13:56:40.593')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'99', N'925126', N'210', N'2019-09-09 13:56:40.593')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'100', N'925126', N'204', N'2019-09-09 13:56:40.643')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'101', N'925126', N'202', N'2019-09-09 13:56:40.647')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'102', N'925126', N'203', N'2019-09-09 13:56:40.647')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'103', N'925126', N'198', N'2019-09-09 13:56:40.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'104', N'925126', N'201', N'2019-09-09 13:56:40.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'105', N'925126', N'200', N'2019-09-09 13:56:40.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'106', N'925126', N'199', N'2019-09-09 13:56:40.693')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'107', N'925126', N'197', N'2019-09-09 13:56:40.693')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'108', N'925126', N'195', N'2019-09-09 13:56:40.697')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'109', N'925126', N'196', N'2019-09-09 13:56:40.703')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'110', N'925126', N'192', N'2019-09-09 13:56:40.703')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'111', N'925126', N'194', N'2019-09-09 13:56:40.703')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'112', N'925126', N'193', N'2019-09-09 13:56:40.743')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'113', N'925126', N'191', N'2019-09-09 13:56:40.743')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'114', N'925126', N'190', N'2019-09-09 13:56:40.743')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'115', N'925126', N'189', N'2019-09-09 13:56:40.757')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'116', N'925126', N'165', N'2019-09-09 13:57:29.197')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'117', N'925126', N'166', N'2019-09-09 13:57:29.200')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'118', N'925126', N'164', N'2019-09-09 13:57:29.200')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'119', N'925126', N'163', N'2019-09-09 13:57:29.267')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'120', N'925126', N'162', N'2019-09-09 13:57:29.270')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'121', N'925126', N'161', N'2019-09-09 13:57:29.270')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'122', N'925126', N'159', N'2019-09-09 13:57:29.307')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'123', N'925126', N'160', N'2019-09-09 13:57:29.323')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'124', N'925128', N'232', N'2019-09-09 13:59:53.360')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'125', N'925128', N'231', N'2019-09-09 13:59:53.360')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'126', N'925128', N'228', N'2019-09-09 13:59:53.363')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'127', N'925128', N'229', N'2019-09-09 13:59:53.363')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'128', N'925128', N'230', N'2019-09-09 13:59:53.373')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'129', N'925128', N'227', N'2019-09-09 13:59:53.373')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'130', N'925128', N'222', N'2019-09-09 13:59:53.410')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'131', N'925128', N'223', N'2019-09-09 13:59:53.413')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'132', N'925128', N'224', N'2019-09-09 13:59:53.423')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'133', N'925128', N'225', N'2019-09-09 13:59:53.427')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'134', N'925128', N'221', N'2019-09-09 13:59:53.430')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'135', N'925128', N'226', N'2019-09-09 13:59:53.430')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'136', N'925128', N'217', N'2019-09-09 13:59:53.463')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'137', N'925128', N'218', N'2019-09-09 13:59:53.467')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'138', N'925128', N'219', N'2019-09-09 13:59:53.473')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'139', N'925128', N'220', N'2019-09-09 13:59:53.477')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'140', N'925128', N'216', N'2019-09-09 13:59:53.480')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'141', N'925128', N'215', N'2019-09-09 13:59:53.480')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'142', N'925128', N'211', N'2019-09-09 13:59:53.510')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'143', N'925128', N'212', N'2019-09-09 13:59:53.517')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'144', N'925128', N'214', N'2019-09-09 13:59:53.527')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'145', N'925128', N'213', N'2019-09-09 13:59:53.553')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'146', N'925128', N'165', N'2019-09-09 14:00:39.550')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'147', N'925128', N'164', N'2019-09-09 14:00:39.550')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'148', N'925128', N'166', N'2019-09-09 14:00:39.550')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'149', N'925128', N'163', N'2019-09-09 14:00:39.557')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'150', N'925128', N'162', N'2019-09-09 14:00:39.563')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'151', N'925128', N'161', N'2019-09-09 14:00:39.563')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'152', N'925128', N'159', N'2019-09-09 14:00:39.610')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'153', N'925128', N'160', N'2019-09-09 14:00:39.610')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'154', N'925129', N'253', N'2019-09-09 16:14:03.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'155', N'925129', N'250', N'2019-09-09 16:14:03.653')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'156', N'925129', N'251', N'2019-09-09 16:14:03.660')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'157', N'925129', N'252', N'2019-09-09 16:14:03.667')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'158', N'925129', N'249', N'2019-09-09 16:14:03.667')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'159', N'925129', N'248', N'2019-09-09 16:14:03.677')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'160', N'925129', N'247', N'2019-09-09 16:14:03.703')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'161', N'925129', N'246', N'2019-09-09 16:14:03.703')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'162', N'925129', N'244', N'2019-09-09 16:14:03.713')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'163', N'925129', N'245', N'2019-09-09 16:14:03.717')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'164', N'925129', N'243', N'2019-09-09 16:14:03.720')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'165', N'925129', N'241', N'2019-09-09 16:14:03.723')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'166', N'925129', N'242', N'2019-09-09 16:14:03.750')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'167', N'925129', N'240', N'2019-09-09 16:14:03.753')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'168', N'925129', N'239', N'2019-09-09 16:14:03.767')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'169', N'925129', N'238', N'2019-09-09 16:14:03.767')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'170', N'925129', N'236', N'2019-09-09 16:14:03.770')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'171', N'925129', N'237', N'2019-09-09 16:14:03.773')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'172', N'925129', N'233', N'2019-09-09 16:14:03.803')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'173', N'925129', N'234', N'2019-09-09 16:14:03.803')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'174', N'925129', N'235', N'2019-09-09 16:14:03.817')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'175', N'925129', N'165', N'2019-09-09 16:14:41.797')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'176', N'925129', N'166', N'2019-09-09 16:14:41.800')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'177', N'925129', N'164', N'2019-09-09 16:14:41.803')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'178', N'925129', N'163', N'2019-09-09 16:14:41.803')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'179', N'925129', N'162', N'2019-09-09 16:14:41.807')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'180', N'925129', N'161', N'2019-09-09 16:14:41.810')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'181', N'925129', N'159', N'2019-09-09 16:14:41.850')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'182', N'925129', N'160', N'2019-09-09 16:14:41.853')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'183', N'925127', N'187', N'2019-09-09 16:15:45.797')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'184', N'925127', N'188', N'2019-09-09 16:15:45.797')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'185', N'925127', N'185', N'2019-09-09 16:15:45.800')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'186', N'925127', N'181', N'2019-09-09 16:15:45.800')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'187', N'925127', N'183', N'2019-09-09 16:15:45.803')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'188', N'925127', N'182', N'2019-09-09 16:15:45.850')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'189', N'925127', N'184', N'2019-09-09 16:15:45.880')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'190', N'925127', N'180', N'2019-09-09 16:15:45.893')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'191', N'925127', N'179', N'2019-09-09 16:15:45.897')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'192', N'925127', N'177', N'2019-09-09 16:15:45.897')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'193', N'925127', N'176', N'2019-09-09 16:15:45.897')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'194', N'925127', N'178', N'2019-09-09 16:15:45.913')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'195', N'925127', N'173', N'2019-09-09 16:15:45.930')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'196', N'925127', N'174', N'2019-09-09 16:15:45.943')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'197', N'925127', N'172', N'2019-09-09 16:15:45.977')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'198', N'925127', N'168', N'2019-09-09 16:15:45.977')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'199', N'925127', N'175', N'2019-09-09 16:15:45.977')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'200', N'925127', N'167', N'2019-09-09 16:15:45.977')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'201', N'925127', N'169', N'2019-09-09 16:15:45.983')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'202', N'925127', N'170', N'2019-09-09 16:15:46.200')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'203', N'925127', N'171', N'2019-09-09 16:15:46.300')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'204', N'925127', N'165', N'2019-09-09 16:16:13.597')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'205', N'925127', N'166', N'2019-09-09 16:16:13.597')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'206', N'925127', N'164', N'2019-09-09 16:16:13.600')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'207', N'925127', N'163', N'2019-09-09 16:16:13.600')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'208', N'925127', N'162', N'2019-09-09 16:16:13.603')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'209', N'925127', N'161', N'2019-09-09 16:16:13.610')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'210', N'925127', N'159', N'2019-09-09 16:16:13.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'211', N'925127', N'160', N'2019-09-09 16:16:13.650')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'212', N'925129', N'254', N'2019-09-09 16:18:14.530')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'213', N'925127', N'255', N'2019-09-09 16:18:25.990')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'214', N'925132', N'253', N'2019-10-16 20:16:57.690')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'215', N'925132', N'254', N'2019-10-16 20:16:57.690')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'216', N'925132', N'255', N'2019-10-16 20:16:57.690')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'217', N'925133', N'254', N'2019-10-16 20:54:21.653')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'218', N'925133', N'255', N'2019-10-16 20:54:21.757')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'219', N'925133', N'253', N'2019-10-16 20:54:21.760')
GO
GO
INSERT INTO [dbo].[chitietbaithi] ([ID_chitiet], [test_code], [id_cauhoi], [timestamps]) VALUES (N'220', N'925133', N'250', N'2019-10-16 20:54:22.400')
GO
GO
SET IDENTITY_INSERT [dbo].[chitietbaithi] OFF
GO

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE [dbo].[classes]
GO
CREATE TABLE [dbo].[classes] (
[id_class] int NOT NULL IDENTITY(1,1) ,
[class_name] nvarchar(60) NULL ,
[grade_name] nvarchar(60) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[classes]', RESEED, 9)
GO

-- ----------------------------
-- Records of classes
-- ----------------------------
SET IDENTITY_INSERT [dbo].[classes] ON
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'7', N'Bảng A', N'2019', N'2019-09-09 07:48:28.770', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'8', N'Bảng B', N'2019', N'2019-09-09 07:48:34.157', N'1')
GO
GO
INSERT INTO [dbo].[classes] ([id_class], [class_name], [grade_name], [timestamps], [del_stt]) VALUES (N'9', N'Bảng C', N'2019', N'2019-09-09 07:48:39.597', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[classes] OFF
GO

-- ----------------------------
-- Table structure for loaicauhoi
-- ----------------------------
DROP TABLE [dbo].[loaicauhoi]
GO
CREATE TABLE [dbo].[loaicauhoi] (
[id_loaicauhoi] int NOT NULL IDENTITY(1,1) ,
[tenloai] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) ,
[del_stt] bit NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[loaicauhoi]', RESEED, 13)
GO

-- ----------------------------
-- Records of loaicauhoi
-- ----------------------------
SET IDENTITY_INSERT [dbo].[loaicauhoi] ON
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'7', N'Đề A', N'2019-09-09 07:48:10.510', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'8', N'Đề B', N'2019-09-09 07:48:14.280', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'9', N'Đề C', N'2019-09-09 07:48:17.880', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'10', N'Đề C 2', N'2019-09-09 11:38:53.373', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'11', N'Đề B 2', N'2019-09-09 11:38:59.930', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'12', N'Đề A 2', N'2019-09-09 11:39:06.700', N'1')
GO
GO
INSERT INTO [dbo].[loaicauhoi] ([id_loaicauhoi], [tenloai], [timestamps], [del_stt]) VALUES (N'13', N'Câu hỏi dùng chung', N'2019-09-09 11:39:12.837', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[loaicauhoi] OFF
GO

-- ----------------------------
-- Table structure for Login
-- ----------------------------
DROP TABLE [dbo].[Login]
GO
CREATE TABLE [dbo].[Login] (
[IDlogin] int NOT NULL ,
[IDUser] int NULL ,
[Token] nvarchar(MAX) NULL ,
[TimeLogin] date NULL 
)


GO

-- ----------------------------
-- Records of Login
-- ----------------------------

-- ----------------------------
-- Table structure for scores
-- ----------------------------
DROP TABLE [dbo].[scores]
GO
CREATE TABLE [dbo].[scores] (
[id_score] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[socaudung] int NULL ,
[tongcau] int NULL ,
[time_finish] datetime NULL ,
[diemthang10] float(53) NULL ,
[dudoanketqua] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[scores]', RESEED, 100)
GO

-- ----------------------------
-- Records of scores
-- ----------------------------
SET IDENTITY_INSERT [dbo].[scores] ON
GO
SET IDENTITY_INSERT [dbo].[scores] OFF
GO

-- ----------------------------
-- Table structure for specialities
-- ----------------------------
DROP TABLE [dbo].[specialities]
GO
CREATE TABLE [dbo].[specialities] (
[id_speciality] int NOT NULL IDENTITY(1,1) ,
[speciality_name] nvarchar(255) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO

-- ----------------------------
-- Records of specialities
-- ----------------------------
SET IDENTITY_INSERT [dbo].[specialities] ON
GO
SET IDENTITY_INSERT [dbo].[specialities] OFF
GO

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE [dbo].[statuses]
GO
CREATE TABLE [dbo].[statuses] (
[id_status] int NOT NULL IDENTITY(1,1) ,
[status_name] nvarchar(50) NOT NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[statuses]', RESEED, 2)
GO

-- ----------------------------
-- Records of statuses
-- ----------------------------
SET IDENTITY_INSERT [dbo].[statuses] ON
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'1', N'Open', N'2019-07-03 20:25:47.597')
GO
GO
INSERT INTO [dbo].[statuses] ([id_status], [status_name], [timestamps]) VALUES (N'2', N'Closed', N'2019-07-03 20:25:51.603')
GO
GO
SET IDENTITY_INSERT [dbo].[statuses] OFF
GO

-- ----------------------------
-- Table structure for student_test_detail
-- ----------------------------
DROP TABLE [dbo].[student_test_detail]
GO
CREATE TABLE [dbo].[student_test_detail] (
[ID] int NOT NULL IDENTITY(1,1) ,
[id_student] int NOT NULL ,
[test_code] int NOT NULL ,
[id_cauhoi] int NOT NULL ,
[student_answer] char(1) NULL ,
[timestamps] datetime NULL DEFAULT (getdate()) 
)


GO
DBCC CHECKIDENT(N'[dbo].[student_test_detail]', RESEED, 6065)
GO

-- ----------------------------
-- Records of student_test_detail
-- ----------------------------
SET IDENTITY_INSERT [dbo].[student_test_detail] ON
GO
SET IDENTITY_INSERT [dbo].[student_test_detail] OFF
GO

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE [dbo].[students]
GO
CREATE TABLE [dbo].[students] (
[id_student] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NULL ,
[password] varchar(32) NULL ,
[email] varchar(100) NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NULL ,
[birthday] date NULL ,
[phone] varchar(45) NOT NULL ,
[cmnd] nvarchar(15) NOT NULL ,
[sbd] nvarchar(15) NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL ,
[donvi] nvarchar(255) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[students]', RESEED, 186)
GO

-- ----------------------------
-- Records of students
-- ----------------------------
SET IDENTITY_INSERT [dbo].[students] ON
GO
SET IDENTITY_INSERT [dbo].[students] OFF
GO

-- ----------------------------
-- Procedure structure for CheckThoiGianLamBai
-- ----------------------------
DROP PROCEDURE [dbo].[CheckThoiGianLamBai]
GO
CREATE PROCEDURE [dbo].[CheckThoiGianLamBai] @MaThiSinh INT,
@TestCode INT 
AS
begin  
declare @setval int  
select @setval= dbo.[fnTime](@MaThiSinh, @TestCode)  
if(@setval>0)
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
End
ELSE BEGIN
SELECT 
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
CAST(null AS int)socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
CAST(null AS float)diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode
end 
End





GO

-- ----------------------------
-- Procedure structure for GetAllCauHoi
-- ----------------------------
DROP PROCEDURE [dbo].[GetAllCauHoi]
GO
CREATE  PROCEDURE [dbo].[GetAllCauHoi]
AS
SELECT
--ROW_NUMBER() OVER(ORDER BY dbo.cauhoi.timestamps DESC)AS sothutu,
cast (ROW_NUMBER() over(ORDER BY dbo.cauhoi.timestamps DESC) as int) Rank,
dbo.cauhoi.id_cauhoi as IdCauhoi,
dbo.cauhoi.id_loaicauhoi as IdLoaicauhoi,
dbo.cauhoi.img_content as ImgContent,
dbo.cauhoi.content as Content,
dbo.cauhoi.answer_a as AnswerA,
dbo.cauhoi.answer_b as AnswerB,
dbo.cauhoi.answer_c as AnswerC,
dbo.cauhoi.answer_d as AnswerD,
dbo.cauhoi.correct_answer as CorrectAnswer,
dbo.cauhoi.timestamps as Timestamps,
dbo.loaicauhoi.tenloai as TenLoai
FROM
dbo.cauhoi
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.cauhoi.del_stt = 1
ORDER BY dbo.cauhoi.timestamps DESC






GO

-- ----------------------------
-- Procedure structure for GetChiTietBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetChiTietBaiThi]
GO
CREATE PROCEDURE [dbo].[GetChiTietBaiThi] @MaThiSinh int, @TestCode int
AS
SELECT
dbo.student_test_detail.ID,
dbo.student_test_detail.id_student,
dbo.student_test_detail.test_code,
dbo.student_test_detail.id_cauhoi,
dbo.cauhoi.img_content,
dbo.cauhoi.content,
dbo.cauhoi.answer_a,
dbo.cauhoi.answer_b,
dbo.cauhoi.answer_c,
dbo.cauhoi.answer_d,
dbo.cauhoi.id_loaicauhoi,
dbo.loaicauhoi.tenloai,
dbo.baithi.test_name,
dbo.student_test_detail.student_answer

FROM
dbo.student_test_detail
INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
INNER JOIN dbo.baithi ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
WHERE
dbo.student_test_detail.test_code = @TestCode AND
dbo.student_test_detail.id_student = @MaThiSinh
ORDER BY dbo.student_test_detail.ID






GO

-- ----------------------------
-- Procedure structure for GetDanhSachBaiThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachBaiThi]
GO
CREATE PROCEDURE [dbo].[GetDanhSachBaiThi] @MaThiSinh int
AS
SELECT DISTINCT
dbo.baithi.test_name,
dbo.baithi.total_questions,
dbo.student_test_detail.id_student,
dbo.baithi.test_code,
dbo.baithi.time_to_do,
dbo.baithi.id_status,
dbo.statuses.status_name,
dbo.baithi.note

FROM
dbo.baithi
INNER JOIN dbo.student_test_detail ON dbo.student_test_detail.test_code = dbo.baithi.test_code
INNER JOIN dbo.statuses ON dbo.baithi.id_status = dbo.statuses.id_status
WHERE
dbo.baithi.id_status=1 AND dbo.baithi.del_stt = 1 AND
dbo.student_test_detail.id_student = @MaThiSinh






GO

-- ----------------------------
-- Procedure structure for GetDanhSachKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachKetQua]
GO
CREATE PROCEDURE [dbo].[GetDanhSachKetQua]
 @TestCode INT 
AS 
BEGIN
SELECT
cast (ROW_NUMBER() over(ORDER BY dbo.students.sbd DESC) as int) Rank,
dbo.students.id_student as IdStudent,
dbo.students.username as Username,
dbo.students.email as Email,
dbo.students.name as Name,
dbo.students.gender as Gender,
dbo.students.birthday as Birthday,
dbo.students.phone as Phone,
dbo.students.cmnd as Cmnd,
dbo.students.sbd as Sbd,
dbo.students.timelogin as Timelogin,
dbo.students.donvi as Donvi,
dbo.scores.socaudung as Socaudung,
dbo.scores.tongcau as TongCau,
dbo.scores.time_finish as TimeFinish,
dbo.scores.diemthang10 as Diemthang10,
dbo.scores.test_code as TestCode
FROM
dbo.students
INNER JOIN dbo.scores ON dbo.students.id_student = dbo.scores.id_student
WHERE
dbo.scores.test_code =@TestCode

END






GO

-- ----------------------------
-- Procedure structure for GetDanhSachThiSinhTheoLop
-- ----------------------------
DROP PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop]
GO
CREATE PROCEDURE [dbo].[GetDanhSachThiSinhTheoLop] @MaLop int
AS
SELECT
dbo.students.name,
dbo.students.gender,
dbo.students.donvi,
dbo.students.sbd,
dbo.students.avatar,
dbo.students.email,
dbo.students.username,
dbo.chitiet_lop.id_chitiet_lop,
dbo.chitiet_lop.id_class,
dbo.classes.class_name,
dbo.classes.grade_name,
dbo.students.id_student

FROM
dbo.chitiet_lop
INNER JOIN dbo.classes ON dbo.classes.id_class = dbo.chitiet_lop.id_class
INNER JOIN dbo.students ON dbo.students.id_student = dbo.chitiet_lop.id_student
WHERE
dbo.classes.id_class = @MaLop
ORDER BY
dbo.students.name ASC






GO

-- ----------------------------
-- Procedure structure for GetKetQua
-- ----------------------------
DROP PROCEDURE [dbo].[GetKetQua]
GO
CREATE PROCEDURE [dbo].[GetKetQua] @MaThiSinh INT,
 @TestCode INT 
AS 
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float)
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
End
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode







GO

-- ----------------------------
-- Procedure structure for GetKetQua_BTG
-- ----------------------------
DROP PROCEDURE [dbo].[GetKetQua_BTG]
GO
CREATE PROCEDURE [dbo].[GetKetQua_BTG] @MaThiSinh INT,
 @TestCode INT,
 @DuDoan INT
AS 
BEGIN
UPDATE scores
SET socaudung = dbo.fnGetCauDung (@MaThiSinh ,@TestCode),
		tongcau=dbo.fnGetTongCau(@MaThiSinh ,@TestCode),
		diemthang10=cast(10 as float)/cast(dbo.fnGetTongCau(@MaThiSinh ,@TestCode)as float)*cast(dbo.fnGetCauDung (@MaThiSinh ,@TestCode)as float),
		dudoanketqua=@DuDoan,
		time_finish=GETDATE()
WHERE
	id_student =@MaThiSinh
AND test_code =@TestCode;
End
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.scores.dudoanketqua,
dbo.baithi.test_name,
dbo.students.name,
dbo.students.timelogin
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode







GO

-- ----------------------------
-- Procedure structure for GetThoiGianThi
-- ----------------------------
DROP PROCEDURE [dbo].[GetThoiGianThi]
GO
CREATE PROCEDURE [dbo].[GetThoiGianThi] @MaThiSinh INT,
 @TestCode INT , @time_finish int
AS
IF NOT EXISTS ( SELECT * FROM scores 
                   WHERE id_student = @MaThiSinh
                   AND test_code = @TestCode)
BEGIN
	INSERT INTO scores (
	id_student,
	test_code,
	time_finish
	)
VALUES
	(
		@MaThiSinh,
		@TestCode,
		DATEADD(MINUTE,@time_finish,GETDATE())
	);
END
SELECT
dbo.scores.id_score,
dbo.scores.id_student,
dbo.scores.test_code,
dbo.scores.socaudung,
dbo.scores.tongcau,
dbo.scores.time_finish,
dbo.scores.diemthang10,
dbo.baithi.test_name,
dbo.students.name
FROM
dbo.scores
INNER JOIN dbo.baithi ON dbo.scores.test_code = dbo.baithi.test_code
INNER JOIN dbo.students ON dbo.scores.id_student = dbo.students.id_student
WHERE
dbo.scores.id_student = @MaThiSinh AND
dbo.scores.test_code = @TestCode







GO

-- ----------------------------
-- Procedure structure for GetThongTinThiSinh_BTG
-- ----------------------------
DROP PROCEDURE [dbo].[GetThongTinThiSinh_BTG]
GO
CREATE PROCEDURE [dbo].[GetThongTinThiSinh_BTG]
@id int
AS
SELECT
dbo.students.id_student,
dbo.students.name,
dbo.students.phone,
dbo.students.cmnd,
dbo.students.timelogin,
dbo.students.donvi
FROM [dbo].[students]
WHERE
dbo.students.id_student=@id

GO

-- ----------------------------
-- Procedure structure for InsertPersonal
-- ----------------------------
DROP PROCEDURE [dbo].[InsertPersonal]
GO
CREATE PROCEDURE [dbo].[InsertPersonal]
      -- Add the parameters for the stored procedure here
      @Name nvarchar(100),
      @CMND nvarchar(15),
      @Phone varchar(11),
      @DiaChi nvarchar(255)
AS
DECLARE @StID int,
				@TestCode int
BEGIN
      SET NOCOUNT ON;
      INSERT INTO students
             (name, cmnd, phone, donvi,timelogin)
      VALUES
             (@Name, @CMND, @Phone, @DiaChi,GETDATE());
			SET @StID=SCOPE_IDENTITY();
			SET @TestCode =(SELECT TOP 1 test_code
											FROM [dbo].[baithi]
											WHERE id_status=1
											ORDER BY
											dbo.baithi.test_code DESC);
			INSERT INTO student_test_detail (id_student, test_code, id_cauhoi)
			SELECT
			@StID as id_students,
			dbo.chitietbaithi.test_code,
			dbo.cauhoi.id_cauhoi
			FROM
			dbo.cauhoi
			INNER JOIN dbo.chitietbaithi ON dbo.chitietbaithi.id_cauhoi = dbo.cauhoi.id_cauhoi
			WHERE
			dbo.chitietbaithi.test_code=@TestCode;
				INSERT INTO scores (
											id_student,
											test_code
											)
										VALUES
											(
												@StID,
												@TestCode
											);
			SELECT
			dbo.student_test_detail.ID,
			dbo.student_test_detail.id_student,
			dbo.student_test_detail.test_code,
			dbo.student_test_detail.id_cauhoi,
			dbo.cauhoi.img_content,
			dbo.cauhoi.content,
			dbo.cauhoi.answer_a,
			dbo.cauhoi.answer_b,
			dbo.cauhoi.answer_c,
			dbo.cauhoi.answer_d,
			dbo.cauhoi.id_loaicauhoi,
			dbo.loaicauhoi.tenloai,
			dbo.baithi.test_name,
			dbo.student_test_detail.student_answer
			FROM
			dbo.student_test_detail
			INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
			INNER JOIN dbo.baithi ON dbo.student_test_detail.test_code = dbo.baithi.test_code
			INNER JOIN dbo.loaicauhoi ON dbo.cauhoi.id_loaicauhoi = dbo.loaicauhoi.id_loaicauhoi
			WHERE
			dbo.student_test_detail.test_code = @TestCode AND
			dbo.student_test_detail.id_student = @StID
			ORDER BY dbo.student_test_detail.ID
END

GO

-- ----------------------------
-- Procedure structure for TaoDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[TaoDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[TaoDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
INSERT INTO student_test_detail (id_student, test_code, id_cauhoi)
SELECT
@MaThisinh as id_students,
dbo.chitietbaithi.test_code,
dbo.cauhoi.id_cauhoi
FROM
dbo.cauhoi
INNER JOIN dbo.chitietbaithi ON dbo.chitietbaithi.id_cauhoi = dbo.cauhoi.id_cauhoi
WHERE
dbo.chitietbaithi.test_code=@MaCode
ORDER BY NEWID()
END






GO

-- ----------------------------
-- Procedure structure for ThongKeHome
-- ----------------------------
DROP PROCEDURE [dbo].[ThongKeHome]
GO
CREATE PROC [dbo].[ThongKeHome]
AS
BEGIN
	IF OBJECT_ID('tempdb..#tthongke') IS NOT NULL DROP TABLE #ttthongke
	CREATE TABLE #tthongke(idkey int,title nvarchar(40), total int)
	INSERT INTO #tthongke SELECT 1,N'Bài thi',(SELECT COUNT(*) FROM baithi WHERE del_stt = 1) GO
	INSERT INTO #tthongke SELECT 2,N'Câu hỏi',(SELECT COUNT(*) FROM cauhoi WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 3,N'Thí sinh',(SELECT COUNT(*) FROM students)GO
	INSERT INTO #tthongke SELECT 4,N'Bảng',(SELECT COUNT(*) FROM classes WHERE del_stt = 1)GO
	INSERT INTO #tthongke SELECT 5,N'Loại Câu hỏi',(SELECT COUNT(*) FROM loaicauhoi)GO
	SELECT * FROM #tthongke 
END







GO

-- ----------------------------
-- Procedure structure for XoaDeCho1ThiSinh
-- ----------------------------
DROP PROCEDURE [dbo].[XoaDeCho1ThiSinh]
GO
CREATE PROCEDURE [dbo].[XoaDeCho1ThiSinh]
@MaCode int,@MaThisinh int
AS
BEGIN
DELETE FROM [dbo].[student_test_detail] Where id_student = @MaThisinh AND test_code=@MaCode
END






GO

-- ----------------------------
-- Function structure for fnGetCauDung
-- ----------------------------
DROP FUNCTION [dbo].[fnGetCauDung]
GO
CREATE FUNCTION [dbo].[fnGetCauDung] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		AND UPPER(dbo.student_test_detail.student_answer) = UPPER(dbo.cauhoi.correct_answer)
		RETURN @NumCount
		END






GO

-- ----------------------------
-- Function structure for fnGetTongCau
-- ----------------------------
DROP FUNCTION [dbo].[fnGetTongCau]
GO
CREATE FUNCTION [dbo].[fnGetTongCau] (@Idtudent INT, @TestCode INT) RETURNS INT AS
BEGIN
	DECLARE
		@NumCount INT SELECT
			@NumCount = COUNT (*)
		FROM
			dbo.student_test_detail
		INNER JOIN dbo.cauhoi ON dbo.student_test_detail.id_cauhoi = dbo.cauhoi.id_cauhoi
		WHERE
			dbo.student_test_detail.id_student = @Idtudent
		AND dbo.student_test_detail.test_code = @TestCode
		RETURN @NumCount
		END






GO

-- ----------------------------
-- Function structure for fnTime
-- ----------------------------
DROP FUNCTION [dbo].[fnTime]
GO
CREATE FUNCTION [dbo].[fnTime] (@MaThiSinh INT,
@TestCode INT ) RETURNS INT AS
BEGIN
	DECLARE
		@TimeCount INT
SELECT
@TimeCount=DATEDIFF(MINUTE, dbo.scores.time_finish,GETDATE())
FROM
dbo.scores
WHERE
dbo.scores.id_student=@MaThiSinh AND
dbo.scores.test_code=@TestCode
RETURN @TimeCount
		END






GO

-- ----------------------------
-- Indexes structure for table admins
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD PRIMARY KEY ([id_admin])
GO

-- ----------------------------
-- Uniques structure for table admins
-- ----------------------------
ALTER TABLE [dbo].[admins] ADD UNIQUE ([username] ASC, [email] ASC)
GO

-- ----------------------------
-- Indexes structure for table baithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table baithi
-- ----------------------------
ALTER TABLE [dbo].[baithi] ADD PRIMARY KEY ([test_code])
GO

-- ----------------------------
-- Indexes structure for table cauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table cauhoi
-- ----------------------------
ALTER TABLE [dbo].[cauhoi] ADD PRIMARY KEY ([id_cauhoi])
GO

-- ----------------------------
-- Indexes structure for table chitiet_lop
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitiet_lop
-- ----------------------------
ALTER TABLE [dbo].[chitiet_lop] ADD PRIMARY KEY ([id_chitiet_lop])
GO

-- ----------------------------
-- Indexes structure for table chitietbaithi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table chitietbaithi
-- ----------------------------
ALTER TABLE [dbo].[chitietbaithi] ADD PRIMARY KEY ([ID_chitiet])
GO

-- ----------------------------
-- Indexes structure for table classes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table classes
-- ----------------------------
ALTER TABLE [dbo].[classes] ADD PRIMARY KEY ([id_class])
GO

-- ----------------------------
-- Indexes structure for table loaicauhoi
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table loaicauhoi
-- ----------------------------
ALTER TABLE [dbo].[loaicauhoi] ADD PRIMARY KEY ([id_loaicauhoi])
GO

-- ----------------------------
-- Indexes structure for table Login
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Login
-- ----------------------------
ALTER TABLE [dbo].[Login] ADD PRIMARY KEY ([IDlogin])
GO

-- ----------------------------
-- Indexes structure for table scores
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table scores
-- ----------------------------
ALTER TABLE [dbo].[scores] ADD PRIMARY KEY ([id_score], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table specialities
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table specialities
-- ----------------------------
ALTER TABLE [dbo].[specialities] ADD PRIMARY KEY ([id_speciality])
GO

-- ----------------------------
-- Indexes structure for table statuses
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table statuses
-- ----------------------------
ALTER TABLE [dbo].[statuses] ADD PRIMARY KEY ([id_status])
GO

-- ----------------------------
-- Indexes structure for table student_test_detail
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table student_test_detail
-- ----------------------------
ALTER TABLE [dbo].[student_test_detail] ADD PRIMARY KEY ([ID], [id_student], [test_code])
GO

-- ----------------------------
-- Indexes structure for table students
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD PRIMARY KEY ([id_student])
GO
