/*
Navicat SQL Server Data Transfer

Source Server         : localhost
Source Server Version : 140000
Source Host           : DESKTOP-S4VGAUV:1433
Source Database       : trac_nghiem_online
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 140000
File Encoding         : 65001

Date: 2019-07-19 07:42:03
*/


-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE [dbo].[students]
GO
CREATE TABLE [dbo].[students] (
[id_student] int NOT NULL IDENTITY(1,1) ,
[username] varchar(20) NOT NULL ,
[password] varchar(32) NOT NULL ,
[email] varchar(100) NULL ,
[avatar] varchar(255) NULL DEFAULT ('avatar-default.jpg') ,
[name] nvarchar(100) NOT NULL ,
[gender] nvarchar(50) NOT NULL ,
[birthday] date NOT NULL ,
[phone] varchar(45) NULL ,
[cmnd] nvarchar(15) NULL ,
[sbd] int NULL ,
[token] nvarchar(MAX) NULL ,
[timelogin] datetime NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[students]', RESEED, 5)
GO

-- ----------------------------
-- Records of students
-- ----------------------------
SET IDENTITY_INSERT [dbo].[students] ON
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin]) VALUES (N'3', N'sinhvien', N'123456', N'sinhvien@gmail.com', N'avatar-default.jpg', N'sinhvien', N'Nam', N'1997-01-01', null, N'1', N'1', N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjMiLCJuYmYiOjE1NjM0NjAyNDgsImV4cCI6MTU2MzQ2MDMwOCwiaWF0IjoxNTYzNDYwMjQ4fQ.cVPOwqPVe0FrnWXsg30Dh6nFvAbCqUJRIByAx4cseU4', N'2019-07-18 21:30:48.633')
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin]) VALUES (N'4', N'sinhvien2', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien2@gmail.com', N'avatar-default.jpg', N'sinhvien2', N'Nam', N'1997-01-01', null, N'1', N'1', null, null)
GO
GO
INSERT INTO [dbo].[students] ([id_student], [username], [password], [email], [avatar], [name], [gender], [birthday], [phone], [cmnd], [sbd], [token], [timelogin]) VALUES (N'5', N'sinhvien3', N'e10adc3949ba59abbe56e057f20f883e', N'sinhvien@gmail.com', N'avatar-default.jpg', N'sinhvien3', N'Nam', N'1990-01-01', null, N'1', N'1', null, null)
GO
GO
SET IDENTITY_INSERT [dbo].[students] OFF
GO

-- ----------------------------
-- Indexes structure for table students
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD PRIMARY KEY ([id_student])
GO

-- ----------------------------
-- Uniques structure for table students
-- ----------------------------
ALTER TABLE [dbo].[students] ADD UNIQUE ([username] ASC, [email] ASC)
GO
