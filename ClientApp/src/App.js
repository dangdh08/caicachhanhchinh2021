import React, { Component } from 'react';
import { Route,Redirect, Switch } from 'react-router-dom';
import LoginPage from './pages/LoginPage/LoginPage';
import ThiSinhHomePage from './pages/ThiSinhPage/ThiSinhHomePage';
import  ThiSinhLamBai  from './pages/ThiSinhPage/ThiSinhLamBai';
import  ThiSinhKetQua  from './pages/ThiSinhPage/ThiSinhKetQua';
import  AdminHomePage  from './pages/AdminPage/AdminHomePage';
import QuanLyThiSinh from './pages/AdminPage/QuanLyThiSinh';
import QuanLyThiSinh2 from './pages/AdminPage/QuanLyThiSinh2';
import QuanLyCauHoi from './pages/AdminPage/QuanLyCauHoi';
import AppDrawer from './components/AppDrawer';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import MenuAppBar from './components/MenuAppBar';
import QuanLyBaiThi from './pages/AdminPage/QuanLyBaiThi';
import QuanlyLoaiCauHoi from './pages/AdminPage/QuanLyLoaiCauHoi';
import QuanLyBang from './pages/AdminPage/QuanLyBang';
import DangKy from './pages/BanTuyenGiaoPages/DangKy';
import LamBai from './pages/BanTuyenGiaoPages/LamBai';
import HoanThanh from './pages/BanTuyenGiaoPages/HoanThanh';
import Register from './pages/CCHC2020Pages/RegisterPage';
import HomePage from './pages/CCHC2020Pages/HomePage';
import DoingTestPage from './pages/CCHC2020Pages/DoingTestPage';
import ResultPage from './pages/CCHC2020Pages/ResultPage';
import EssayPage from './pages/CCHC2020Pages/EssayPage';

import axios from 'axios';

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem('user')? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/register",
            }}
          />
        )
      }
    />
  );
}

function AdminRoute({ component: Component, ...rest }) {
  return (
    
    <Route
      {...rest}
      render={props =>
        localStorage.getItem('token') && JSON.parse(localStorage.getItem('token')).idAdmin ? (
          
          <Component {...props} />
        
        ) : (
          <Redirect
            to={{
              pathname: "/login",
            }}
          />
        )
      }
    />
  );
}

export default function App () {
      return (
           <Switch>
              <Route exact path='/register' component={Register} /> 
              <PrivateRoute exact path="/" component={HomePage} />
              <PrivateRoute path='/lambai/:id' component={DoingTestPage} />
              <PrivateRoute path='/lambaituluan/:id/:idtuluan/' component={EssayPage} />
              <PrivateRoute path='/ketqua/:idstd/:testcode/:dudoan' component={ResultPage} />
              
              <Route exact path='/login' component={LoginPage} />
              <AdminRoute exact path='/admin' component={AdminHomePage} />
              <AdminRoute exact path='/admin/quanlythisinh' component={QuanLyThiSinh} />
              <AdminRoute path='/admin/quanlycauhoi' component={QuanLyCauHoi} />
              <AdminRoute path='/admin/quanlybaithi' component={QuanLyBaiThi} />
              <AdminRoute path='/admin/quanlyloaicauhoi' component={QuanlyLoaiCauHoi} />
              <AdminRoute path='/admin/quanlybang' component={QuanLyBang} />
              {/* <AdminRoute exact path='/admin' component={QuanLyThiSinh} /> */}
              {/* <AdminRoute exact path='/admin/quanlythisinh2' component={QuanLyThiSinh2} /> */}
          </Switch>
    );
}

