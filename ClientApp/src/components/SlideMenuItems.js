import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PeopleIcon from '@material-ui/icons/People';
import QuestionIcon from '@material-ui/icons/ContactSupport';
import TestIcon from '@material-ui/icons/Description';
import PasswordIcon from '@material-ui/icons/VpnKey';
import LogOutIcon from '@material-ui/icons/PowerSettingsNew';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';


export const thiSinhItems = (
  <div>
    <ListItem>
      <Card style={{width:230}}>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="250"
          image={require('../assets/nam-avatar.jpg')}
          title="Contemplative Reptile"
        />
        </Card>
    </ListItem>
    <Divider />
    <ListItem button>
      <ListItemIcon>
        <LogOutIcon color='secondary'/>
      </ListItemIcon>
      <ListItemText primary="Đăng xuất" />
    </ListItem>
  </div>
);


export const mainListItems = (
  <div>
    <ListItem button>
      <ListItemIcon>
        <PeopleIcon color='primary' />
      </ListItemIcon>
      <ListItemText primary="Quản lý Thí sinh" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <TestIcon color='primary'/>
      </ListItemIcon>
      <ListItemText primary="Quản lý Bài Thi" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <QuestionIcon color='primary'/>
      </ListItemIcon>
      <ListItemText primary="Quản lý Câu hỏi" />
    </ListItem>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Tài khoản</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <PasswordIcon color='secondary' />
      </ListItemIcon>
      <ListItemText primary="Đổi mật khẩu" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <LogOutIcon color='secondary'/>
      </ListItemIcon>
      <ListItemText primary="Đăng xuất" />
    </ListItem>
  </div>
);