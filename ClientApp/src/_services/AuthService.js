import React from 'react';
import decode from 'jwt-decode';
import axios from 'axios';


    export default  function isValidToken()   {
        if(!localStorage.getItem('token')){
            return false
        }
        let token = localStorage.getItem('token')
        //console.log("================token==================>" + token)
        try {
            axios.post('/UserLogin/CheckToken',{
                token:token
            })
            .then(function (response) {
                console.log("================response==================>" + response.data.error)
                if(response.data.error==200){
                    return true
                }else {
                    localStorage.removeItem('token')
                    return false
                }
              })
        }
        catch (err) {
            return false;
        }
    }

   