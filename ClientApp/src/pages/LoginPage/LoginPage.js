﻿import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import BannerLeft from "../../assets/vongxoay.jpg";


export default function LoginPage() {
  const classes = useStyles();

  //================== States=========================//
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [logined, setLogined] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errMessOpen,setErrMessOpen] = useState(false);

  //================== Effect =========================//
  useEffect(() => {
    if( localStorage.getItem('token') && JSON.parse(localStorage.getItem('token')).idAdmin){
        setIsAdmin(true)
        setLogined(true)
    }
    if( localStorage.getItem('token') && JSON.parse(localStorage.getItem('token')).idStudent){
        setLogined(true)
    }

  },[logined,isAdmin])

  //================== Đăng nhập Submit =========================//
  async function handleFormSubmit(e){
    e.preventDefault();
    setLoading(true)
    const authenticateUrl = username.toUpperCase() === 'ADMIN' ? '/AdminLogin/Authenticate' : '/AdminLogin/Authenticate'

    try{
        await axios.post(authenticateUrl, {
            username: username,
            password: password
          })
          .then(function (response) {

            if(response.status===200){
            localStorage.setItem('token', JSON.stringify(response.data))
            setLoading(false)
            response.data.idAdmin ? setIsAdmin(true) : setIsAdmin(false)
            setLogined(true)
            
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
            setErrMessOpen(true)
        
    }
}
     //================== Set state thông báo lỗi =========================//
    function handleErrMessOpen(){
        setErrMessOpen(false)
    }


//==============================Condition Render=============================================//
    if(logined && isAdmin){
        return <Redirect to={{ pathname: "/admin",}}/>
    }
    else if (logined){
        return <Redirect to={{ pathname: "/",}}/>
    }
    else
        return (
            
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            
            {/* <Grid item xs={false} sm={7} md={7} lg={8} xl={9} className={classes.image}>
               
            </Grid>     */}

            <Grid item xs={12} sm={5} md={5} lg={4} xl={3} component={Paper} elevation={6} square className={classes.gridForm}>
            {loading ? <LinearIndeterminate/> : null}  {/* //Render indicator */}
            <ErrorSnackBar errMessOpen={errMessOpen} handleErrMessOpen={handleErrMessOpen}/> {/* //Render thông báo lỗi */}
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Đăng nhập
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Tài khoản"
                            name={username}
                            autoFocus
                            onChange={(input) => setUsername(input.target.value)}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name={password}
                            label="Mật khẩu"
                            type="password"
                            onChange={(input) => setPassword(input.target.value)}
                            
                            
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick = {handleFormSubmit}
                        >
                            Đăng nhập
                        </Button>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
    }

    
 {/* ====== Thông báo lỗi khi đăng nhập sai ========*/}
    function ErrorSnackBar(props){
        const classes = useStyles();
        const {errMessOpen, handleErrMessOpen, ...other } = props;
        
        return(
            <Snackbar
            anchorOrigin={ { vertical: 'bottom', horizontal: 'right' } }
            open={errMessOpen}
            autoHideDuration={3000}
            onClose={handleErrMessOpen}
            >
                <SnackbarContent
                onClose={handleErrMessOpen}
                className={classes.error}
                message={
                            <span  className={classes.message}>
                            <ErrorIcon className={classes.iconVariant} />
                            {"Sai tên đăng nhập hoặc mật khẩu"}
                        </span>
                        }
                />
            </Snackbar>
        );
    }

    {/* ====== Loading indicator ========*/}
    function LinearIndeterminate() {
        return (
          <div>
            <LinearProgress />
          </div>
        );
      }

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    alignContent: "center",
    backgroundImage: `url(${BannerLeft})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% 100%",
  },
  bannerText:{
    backgroundColor: '#007ACC',
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(2),
    color:theme.palette.background.paper,
    padding:theme.spacing(1),

  },
  image: {
      backgroundImage:`url(${BannerLeft})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
  },
  paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
  },
  gridForm:{
    marginRight:"auto",
    marginLeft:"auto"
  },
  avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
  },
  form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
  },
  submit: {
      margin: theme.spacing(3, 0, 2),
  },
  progress: {
    margin: theme.spacing(2),
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));
