import React, { Component,useState,useEffect  } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Create';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import AdminNavBar from './AdminNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from "material-table";
import Footer from '../../components/Footer';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';




const columns =[
  { title: 'STT', field: 'rank'},
  { title: 'Câu hỏi', field: 'content'},
  { title: 'Đáp án A', field: 'answerA' },
  { title: 'Đáp án B', field: 'answerB'},
  {title: 'Đáp án C',field: 'answerC'},
  {title: 'Đáp án D',field: 'answerD'},
  {title: 'Đáp án đúng',field: 'correctAnswer'},
  {title: 'Loại câu hỏi',field: 'idLoaicauhoi'},
]

export default function QuanLyCauHoi() {
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));
  const fileInput = React.createRef();

  // =================== States ===================//
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [openDialog,setOpenDialog] = useState(false);
  const [editAction,setEditAction] = useState(false);
  const [values, setValues] = useState({
    "selectedIdCauhoi": '',
    "selectedIdLoaicauhoi": '',
    "selectedImgContent": '',
    "selectedContent": '',
    "selectedAnswerA": '',
    "selectedAnswerB": '',
    "selectedAnswerC": '',
    "selectedAnswerD": '',
    "selectedCorrectAnswer": '',
  });


 // =============Effects=====================//
 useEffect(() => {
  fetchData();
}, [])

  // =============Fetch Api=====================//
  async function fetchData(){
    setLoading(true)
    try{
        await axios.get('/api/QuanLyCauHoi/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setDataTable(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// =============Add Cau hoi ============//
function handleAdd() {
  setEditAction(false)
  setValues({
    "selectedIdCauhoi": '',
    "selectedIdLoaicauhoi": '',
    "selectedImgContent": '',
    "selectedContent": '',
    "selectedAnswerA": '',
    "selectedAnswerB": '',
    "selectedAnswerC": '',
    "selectedAnswerD": '',
    "selectedCorrectAnswer": '',
  });
  setOpenDialog(true)
}
async function handleSaveAddDialog(){
  setLoading(true)
  try{
      await axios({
        method: 'post',
        url: '/api/QuanLyCauHoi/Create',
        data: {
          "idLoaicauhoi": values.selectedIdLoaicauhoi,
          "content": values.selectedContent,
          "answerA": values.selectedAnswerA,
          "answerB": values.selectedAnswerB,
          "answerC": values.selectedAnswerC,
          "answerD": values.selectedAnswerD,
          "correctAnswer": values.selectedCorrectAnswer,
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          fetchData()
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(error.response.message);
          console.log(userData.token)
          setLoading(false)
        
  }
  setOpenDialog(false)
}

  // =============Delete Cau hoi ============//
  async function handleDelete(rowData) {
    if (window.confirm("Chắc chắn muốn xóa câu hỏi: " + rowData.content)){
       setLoading(true)
    try{
        await axios({
          method: 'delete',
          url: '/api/QuanLyCauHoi/Delete',
          data: {
            idCauhoi: rowData.idCauhoi
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }}
  }
// =============Edit Cau hoi ============//
   function handleEdit(rowData) {
    setEditAction(true);
    setValues({
      "selectedIdCauhoi": rowData.idCauhoi,
      "selectedIdLoaicauhoi":  rowData.idLoaicauhoi,
      "selectedImgContent":  rowData.imgContent,
      "selectedContent":  rowData.content,
      "selectedAnswerA":  rowData.answerA,
      "selectedAnswerB":  rowData.answerB,
      "selectedAnswerC":  rowData.answerC,
      "selectedAnswerD":  rowData.answerD,
      "selectedCorrectAnswer":  rowData.correctAnswer,
    });
    
    setOpenDialog(true)

  }
  async function handleSaveEditDialog(){
    setLoading(true)
    try{
        await axios({
          method: 'put',
          url: '/api/QuanLyCauHoi/Edit',
          data: {
            "idCauhoi": values.selectedIdCauhoi,
            "idLoaicauhoi": values.selectedIdLoaicauhoi,
            "imgContent": values.selectedImgContent,
            "content": values.selectedContent,
            "answerA": values.selectedAnswerA,
            "answerB": values.selectedAnswerB,
            "answerC": values.selectedAnswerC,
            "answerD":values.selectedAnswerD,
            "correctAnswer": values.selectedCorrectAnswer,
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }
    setEditAction(false)
    setOpenDialog(false)

  }

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function handleCloseDialog(){
    setOpenDialog(false);
  }

  // ========================== Import From Excel File ==========//
  function fileHandler(event){
    let fileObj = event.target.files[0];
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if(err){
        console.log(err);            
      }
      else{
          // setCols(resp.cols)
          // setRows(resp.rows)
          console.log(resp.rows)
          addNhieuCauHoi(resp.rows)
      }
    });               

  }

  function openFileBrowser() {
    fileInput.current.click();
  }

  async function addNhieuCauHoi (array){
    setLoading(true)
    let thanhcong = 0
    try{
        await
          array.map((item,index) =>{
            // console.log(item[1])
            // console.log(item[2])
            // console.log(item[3])
            // console.log(item[4])
            // console.log(item[5])
            axios({
              method: 'post',
              url: '/api/QuanLyCauHoi/Create',
              data: {
                "idLoaicauhoi": item[7],
                "content": item[1],
                "answerA": item[2],
                "answerB": item[3],
                "answerC": item[4],
                "answerD": item[5],
                "correctAnswer": item[6],
              },
              headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
            })
              .then(response => {
                if(response.status===200){
                thanhcong = thanhcong + 1
                fetchData()
                setLoading(false)
                console.log("Thêm thành công: " + thanhcong + " câu hỏi")
                }
                else {
                    console.log("===========Reponse FROM TRY ==========:")
                    console.log(response.data.message)
                    setLoading(false)
                    
                }

              }).catch(error=> {
                console.log(error)
                console.log("========CATCH ERROR INDEX "+ index)
                setLoading(false)
              })
            })
    }catch(error) {
            console.log("===========Reponse FROM CATCH ==========:")
            setLoading(false)
          
    }
  }

 // =============================================== Render Main ============================================//
 return (
  <div className={classes.root} >
     {/* ==================Menu Bar======================= */}
    <CssBaseline />
    <AdminNavBar/>
    {/* =====================Main Page==================== */}
    <main className={classes.content}>
    <div className={classes.appBarSpacer} />
    
    {/* <Container> */}
      <Paper className={classes.paperRoot}>
      <MaterialTable
      title="Quản lý câu hỏi"
      columns={columns}
      data={dataTable}
      isLoading={loading}
      // ============== Nút thêm xóa sửa ==================//
      actions={[
        {
          icon: 'add_box',
          tooltip: 'Thêm câu hỏi',
          iconProps:{color:"error"},
          isFreeAction: true,
          onClick: (event) => {handleAdd()}
        },
        {
          icon: 'publish',
          tooltip: 'Thêm nhiều câu hỏi từ excel',
          iconProps:{color:"error"},
          isFreeAction: true,
          onClick: (event) => {openFileBrowser()}
        },
        {
          icon: 'edit',
          tooltip: 'Sửa câu hỏi',
          iconProps:{color:"primary"},
          onClick: (event, rowData) => {handleEdit(rowData)}
        },
        {
          icon: 'delete',
          tooltip: 'Xóa',
          iconProps:{color:"secondary"},
          onClick: (event, rowData) => {handleDelete(rowData)}
        }
      ]}
      // ================ Styles cho bảng ===========//
      options={{
        headerStyle: {
          backgroundColor: '#FF592B',
          color: '#FFF',
          fontSize:14
        },
        actionsColumnIndex: -1,
        pageSize:10,
        pageSizeOptions:[5,10,15,20],
        loadingType: 'overlay',
        
        
      }}
      // ================== Ngôn ngữ ============//
      localization={{
        toolbar: {
          searchTooltip: 'Tìm kiếm',
          searchPlaceholder:"Tìm kiếm"
        },
        pagination: {
          firstTooltip:"Trang đầu",
          previousTooltip:"Trang trước",
          nextTooltip:"Trang sau",
          lastTooltip:"Trang cuối",
          labelRowsSelect:"",
          
        },
        header:{
          actions:""
        },
        body:{
          emptyDataSourceMessage:"Đang tải dữ liệu"
        },
      }}
    />


    {/* ======================================= Dialog =================================== */}

    <Dialog open={openDialog} onClose={handleCloseDialog}  maxWidth="md">
      <DialogTitle id="form-dialog-title">{editAction?"Sửa câu hỏi":"Thêm mới câu hỏi"}</DialogTitle>
      <DialogContent>
        <DialogContentText>
         * : các trường bắt buộc
        </DialogContentText>
        <TextField
          autoFocus
          required
          label="Nội dung câu hỏi"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedContent}
          onChange={handleChange('selectedContent')}
        />
        <TextField
          required
          label="Đáp án A"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedAnswerA}
          onChange={handleChange('selectedAnswerA')}
        />
         <TextField
          required
          label="Đáp án B"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedAnswerB}
          onChange={handleChange('selectedAnswerB')}
        />
         <TextField
          required
          label="Đáp án C"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedAnswerC}
          onChange={handleChange('selectedAnswerC')}
        />
        <TextField
          required  
          label="Đáp án D"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedAnswerD}
          onChange={handleChange('selectedAnswerD')}
        />
        <TextField
          required  
          label="Đáp án Đúng"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedCorrectAnswer}
          onChange={handleChange('selectedCorrectAnswer')}
        />
        <TextField
          required  
          label="Loại câu hỏi"
          fullWidth
          margin="normal"
          variant="outlined"
          type="Number"
          value={values.selectedIdLoaicauhoi}
          onChange={handleChange('selectedIdLoaicauhoi')}
        />
       
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDialog} color="primary">
          Hủy bỏ
        </Button>
        <Button onClick={editAction?handleSaveEditDialog:handleSaveAddDialog} color="primary">
          Lưu 
        </Button>
      </DialogActions>
    </Dialog>
    <input type="file" hidden onChange={fileHandler} ref={fileInput} onClick={(event)=> { event.target.value = null }} style={{"padding":"10px"}} />
  </Paper>
  </main>
  <Footer/>
  </div>
);
}

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  paperRoot: {
      margin:theme.spacing(0,4,0),
    },
    appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
    table: {
      minWidth: 700,
    },
    button: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
    iconSmall: {
      fontSize: 20,
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));

