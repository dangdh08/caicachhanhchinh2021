import React, { Component,useState,useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import CardHeader from '@material-ui/core/CardHeader';
import AdminNavBar from './AdminNavBar';
import axios from 'axios';
import QuanLyThiSinh from './QuanLyThiSinh'
import QuanLyCauHoi from './QuanLyCauHoi'
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer';



export default function AdminHomePage(){
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));
   // =============States=====================//
   const [loading, setLoading] = useState(false);
   const [dataList, setDataList] = useState([])

   // =============Effects=====================//
   useEffect(() => {
    fetchData();
   

  }, [])
  
    // =============Fetch Api=====================//
    async function fetchData(){
      setLoading(true)
      try{
          await axios.get('/AdminLogin/ThongKeHome', 
            {
              headers: {
                  'Authorization': 'Bearer ' + userData.token,
              }
            }
          )
            .then(function (response) {
  
              if(response.status===200){
              setDataList(response.data)
              setLoading(false)
              }else {
                  console.log("===========Reponse==========:")
                  console.log(response.data.message)
              }
  
            })
      }catch(error) {
              console.log("===========Reponse==========:")
              console.log(error.response.status);
              setLoading(false)
          
      }
    }




    // =============Render=====================//
  return (
    <div className={classes.root} >
       {/* ==================Menu Bar======================= */}
      <CssBaseline />
      <AdminNavBar/>
      {/* =====================Main Page==================== */}
      <main className={classes.content}>
      <div className={classes.appBarSpacer} />
      
      <Grid container spacing={3} direction="row" justify="space-evenly" >
          {dataList.slice(1).map(value => (
              <Grid key={value.idkey} item xs={11} sm="auto" md="auto" style={{margin:30}}>
                    <Card className={classes.card} elevation={1}>
                    <CardHeader
                      title= {value.title}
                      titleTypographyProps={{ align: 'left',variant:'h4' }}
                      subheader={ "Tổng số: " + value.total}
                      subheaderTypographyProps={{ align: 'right', color:'inherit',variant:'h6'}}
                      className={value.idkey===1? classes.cardHeader1
                                  : value.idkey===2?classes.cardHeader2
                                  :value.idkey===3?classes.cardHeader3
                                  :value.idkey===4?classes.cardHeader4
                                  :classes.cardHeader5
                                }
                    />
                    <Divider variant="fullWidth" />
                   
                  
                  <CardActions>
                    <Button variant="outlined" color="primary" fullWidth 
                    component={Link} to={value.idkey===3?"/admin/quanlythisinh"
                                          :value.idkey===2?"/admin/quanlycauhoi"
                                          :value.idkey===1?"/admin/quanlybaithi"
                                          :value.idkey===4?"/admin/quanlybang"
                                          :value.idkey===5?"/admin/quanlyloaicauhoi"
                                          :null}>
                      Chi tiết
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
          ))}

      </Grid>
      

      </main>
      <Footer/>                  
    </div>
  );
  }


//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  card: {
    minWidth: 310,
    borderRadius: 3,
    border: 0,
    color: 'white',
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
  cardHeader1: {
    background: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 0%, rgba(7,49,147,1) 26%, rgba(0,212,255,1) 100%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
    height: 180,
  },
  cardHeader2: {
    background: 'linear-gradient(147deg, #FFE53B 0%, #FF2525 74%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
    height: 180,
  },
  cardHeader3: {
    background: 'linear-gradient(19deg, #21D4FD 0%, #B721FF 100%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
    height: 180,
  },
  cardHeader4: {
    background: ' linear-gradient(90deg, #0d4212 0%, #21c131 100%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
    height: 180,
  },
  cardHeader5: {
    background: 'linear-gradient(90deg, rgba(121,82,31,1) 15%, rgba(227,173,91,1) 91%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
    height: 180,
  },
  
}));
