import React, { Component,useState,useEffect  } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Create';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import AdminNavBar from './AdminNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from "material-table";
import Footer from '../../components/Footer';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


const columns =[
  // { title: 'STT', field: 'idCauhoi'},
  { title: 'Mã bài thi', field: 'testCode' },
  { title: 'Tên bài thi', field: 'testName'},
  { title: 'Thời gian(phút)',field: 'timeToDo'},
  { title: 'Mật khẩu',field: 'password'},
  { title: 'Trạng thái',field: 'idStatus'},
  { title: 'Câu hỏi tối đa', field: 'totalQuestions'},
  { title: 'Câu hỏi đã thêm',field: 'datacauhoi',render: rowData => rowData.datacauhoi.length},
  { title: 'Thí sinh',field: 'datathisinh',render: rowData => rowData.datathisinh.length},
]

const columnsCauHoi =[
  // { title: 'STT', field: 'rank'},
  { title: 'Câu hỏi', field: 'content'},
  { title: 'Đáp án A', field: 'answerA' },
  { title: 'Đáp án B', field: 'answerB'},
  {title: 'Đáp án C',field: 'answerC'},
  {title: 'Đáp án D',field: 'answerD'},
  {title: 'Đáp án đúng',field: 'correctAnswer'},
  // {title: 'Loại câu hỏi',field: 'idLoaicauhoi'},
]

const columnsAddCauHoi =[
  { title: 'STT', field: 'rank'},
  { title: 'Câu hỏi', field: 'content'},
  {title: 'Loại câu hỏi',field: 'idLoaicauhoi'},
]

const columnsThiSinh =[
  {title: 'Số Báo Danh', field: 'sbd' },
  {title: 'Tài khoản', field: 'username'},
  {title: 'Họ và tên', field: 'name'},
  {title: 'Giới tính', field: 'gender'},
  {title: 'Năm sinh',field: 'birthday'},
  {title: 'Đơn vị',field: 'donvi'},
]



export default function QuanLyBaiThi() {
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));

  // =================== States ===================//
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [openDialog,setOpenDialog] = useState(false);
  const [openCauHoiDialog,setOpenCauHoiDialog] = useState(false);
  const [openAddCauHoiDialog,setOpenAddCauHoiDialog] = useState(false);
  const [editAction,setEditAction] = useState(false);
  const [values, setValues] = useState({
    selectedTestName:'',
    selectedTestCode: '',
    selectedPassword: '',
    selectedTotalQuestion: '',
    selectedTimeToDo: '',
    selectedNote:'',
    selectedIdStatus:'',
    selectedDataCauHoi:[],
    selectedDataThiSinh:[],
  });
  const [dataAllCauHoi,setDataAllCauHoi] = useState([]);

  
  const [openDetailDialog, setOpenDetailDialog] = useState(false)
  const [openAddThiSinhDialog, setOpenAddThiSinhDialog] = useState(false)
  const [filteredThiSinhData,setFilteredThiSinhData] = useState([]);

 // =============Effects=====================//
 useEffect(() => {
  fetchData();
}, [])

  // =============Fetch Api=====================//
  async function fetchData(){
    setLoading(true)
    try{
        await axios.get('/api/QuanLyBaiThi/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setDataTable(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// =============Add Bai Thi ============//
  function handleAdd() {
    setEditAction(false)
    setValues({
      selectedTestName:'',
      selectedTestCode: '',
      selectedPassword: '',
      selectedTotalQuestion: '',
      selectedTimeToDo: '',
      selectedNote:'',
      selectedIdStatus:'',
    });
    setOpenDialog(true)
  }
  async function handleSaveAddDialog(){
    setLoading(true)
    try{
        await axios({
          method: 'post',
          url: '/api/QuanLyBaiThi/Create',
          data: {
            "testName": values.selectedTestName,
            "password": values.selectedPassword,
            "totalQuestions": values.selectedTotalQuestion,
            "timeToDo": values.selectedTimeToDo,
            "note": values.selectedNote,
            "idStatus": values.selectedIdStatus
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(error.response.message);
            console.log(userData.token)
            setLoading(false)
          
    }
    setOpenDialog(false)
  }

  // =============Delete Bai Thi ============//
  async function handleDelete(rowData) {
    if (window.confirm("Chắc chắn muốn xóa bài thi: " + rowData.testCode)){
       setLoading(true)
    try{
        await axios({
          method: 'delete',
          url: '/api/QuanLyBaiThi/Delete',
          data: {
            testCode: rowData.testCode
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }}
  }
// =============Edit Bai Thi ============//
  function handleEdit(rowData) {
    setEditAction(true);
    setValues({
      selectedTestName:rowData.testName,
      selectedTestCode: rowData.testCode,
      selectedPassword: rowData.password,
      selectedTotalQuestion: rowData.totalQuestions,
      selectedTimeToDo: rowData.timeToDo,
      selectedNote:rowData.note,
      selectedIdStatus:rowData.idStatus,
    });
    
    setOpenDialog(true)

  }
  async function handleSaveEditDialog(){
    setLoading(true)
    try{
        await axios({
          method: 'put',
          url: '/api/QuanLyBaiThi/Edit',
          data: {
            "testName": values.selectedTestName,
            "testCode":  values.selectedTestCode,
            "password":  values.selectedPassword,
            "totalQuestions":  values.selectedTotalQuestion,
            "timeToDo":  values.selectedTimeToDo,
            "note":  values.selectedNote,
            "idStatus":  values.selectedIdStatus
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }
    setEditAction(false)
    setOpenDialog(false)

  }
 
  
  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function handleCloseDialog(){
    setOpenDialog(false);
  }

  // ==================== Hiển thị danh sách câu hỏi của bài thi=====================//
  function handleOpenQuestion(rowData) {
    setValues({
      selectedDataCauHoi:rowData.datacauhoi
    })
    setOpenCauHoiDialog(true)

  }

  function handleCloseCauHoiDialog(){
    setOpenCauHoiDialog(false); 
    setValues({
      selectedDataCauHoi:[]
    })
    
  }

  // ======================= Add câu hỏi vào bài thi ===================//
  function handleOpenAddCauHoiDialog(rowData) {
    fetchAllCauHoi(rowData);
    setValues({
      selectedTestName:rowData.testName,
      selectedTestCode:rowData.testCode
    });
    setOpenAddCauHoiDialog(true); 
  }

  function handleCloseAddCauHoiDialog(){
    setOpenAddCauHoiDialog(false); 
    setValues({
      selectedTestName:'',
      selectedTestCode:''
    })
  }
  
  async function handleAddCauHoiDaChon(data){
    setLoading(true)
      try{
        await data.map(item => {
         axios({
          method: 'post',
          url: '/api/QuanLyBaiThi/AddCauHoiVaoBaiThi',
          data: {
            "TestCode": values.selectedTestCode,
            "IdCauhoi": item.idCauhoi,
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
    })}
    catch(error) {
            console.log("===========Reponse==========:")
            console.log(error);
    }
    fetchData();
    setOpenAddCauHoiDialog(false); 
    setValues({
      selectedTestName:'',
      selectedTestCode:''
    })
    setLoading(false)
    }
  
  

  // =============Fetch All Cau hoi=====================//
  async function fetchAllCauHoi(rowData){
    // setLoading(true)
    let filteredArray =[];
    try{
        await axios.get('/api/QuanLyCauHoi/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
           
            filteredArray  = response.data.filter(function(array1_el){
              return rowData.datacauhoi.filter(function(array2_el){
                 return array2_el.idCauhoi == array1_el.idCauhoi;
              }).length == 0
           });
           console.log(filteredArray.length)
           setDataAllCauHoi(filteredArray)
            
            // setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            // setLoading(false)
        
    }
  }

// ****************************** FUNCTIONS CHO THI SINH **************************************//

// ======================= Function cho xem chi tiet thi sinh trong bang ==========================//
 async function handleOpenDetailDialog(rowData){
  setValues({
    selectedDataThiSinh:rowData.datathisinh
  })
  setOpenDetailDialog(true)

}

function handleCloseDetailDialog(){
  setOpenDetailDialog(false)
  setValues({
    selectedDataThiSinh:[]
  })
  
}
// ======================= Function cho them thi sinh vao bang ==========================//

async function handleOpenAddThiSinhDialog(rowData){
  setValues({
    selectedTestCode:rowData.testCode
  })
  let filteredArray =[];
  setLoading(true)
  try{
      await axios.get('/api/QuanLyUser/GetAll', 
        {
          headers: {
              'Authorization': 'Bearer ' + userData.token,
          }
        }
      )
        .then(function (response) {
          if(response.status===200){
          filteredArray  = response.data.filter(function(array1_el){
            return rowData.datathisinh.filter(function(array2_el){
               return array2_el.idStudent == array1_el.idStudent;
            }).length == 0
         });
          setFilteredThiSinhData(filteredArray)
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          setLoading(false)
      
  }
  setOpenAddThiSinhDialog(true)
}

function handleCloseAddThiSinhDialog(){
  setOpenAddThiSinhDialog(false)
  setFilteredThiSinhData([])
}

async function addSelectedThiSinh(data){
  setLoading(true)
  try{
    await data.map(item => {
     axios({
      method: 'post',
      url: '/api/QuanLyBaiThi/TaoDeThi',
      data: {
        "IdStudent": item.idStudent,
        "TestCode": values.selectedTestCode
      },
      headers: {
        'Authorization': 'Bearer ' + userData.token,
      } 
     })
    })
    setLoading(false)
  }
  catch(error) {
          console.log("===========Reponse==========:")
          console.log(error);
  }
  fetchData()
  setOpenAddThiSinhDialog(false)
  setFilteredThiSinhData([])
}

 // =============================================== Render Main ========================================================//
 return (
  <div className={classes.root} >
     {/* ==================Menu Bar======================= */}
    <CssBaseline />
    <AdminNavBar/>
    {/* =====================Main Page==================== */}
    <main className={classes.content}>
    <div className={classes.appBarSpacer} />
    
    {/* <Container> */}
      <Paper className={classes.paperRoot}>
      <MaterialTable
      title="Quản lý bài thi"
      columns={columns}
      data={dataTable}
      isLoading={loading}
      // ============== Nút thêm xóa sửa ==================//
      actions={[
        {
          icon: 'add_box',
          tooltip: 'Thêm mới bài thi',
          iconProps:{color:"error"},
          isFreeAction: true,
          onClick: (event) => {handleAdd()}
        },
        {
            icon: 'how_to_reg',
            tooltip: 'Xem danh sách thí sinh',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleOpenDetailDialog(rowData)}
          },
          {
            icon: 'person_add',
            tooltip: 'Thêm thí sinh vào bảng',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleOpenAddThiSinhDialog(rowData)}
          },
        {
          icon: 'speaker_notes',
          tooltip: 'Xem chi tiết câu hỏi',
          iconProps:{color:"primary"},
          onClick: (event, rowData) => {handleOpenQuestion(rowData)}
        },
        {
          icon: 'add_comment',
          tooltip: 'Thêm câu hỏi vào bài thi',
          iconProps:{color:"primary"},
          onClick: (event, rowData) => {handleOpenAddCauHoiDialog(rowData)}
        },
        {
          icon: 'edit',
          tooltip: 'Sửa thông tin bài thi',
          iconProps:{color:"primary"},
          onClick: (event, rowData) => {handleEdit(rowData)}
        },
        {
          icon: 'delete',
          tooltip: 'Xóa bài thi',
          iconProps:{color:"secondary"},
          onClick: (event, rowData) => {handleDelete(rowData)}
        }
      ]}
      // ================ Styles cho bảng ===========//
      options={{
        headerStyle: {
          backgroundColor: '#081C85',
          color: '#FFF',
          fontSize:14
        },
        actionsColumnIndex: -1,
        pageSize:10,
        pageSizeOptions:[5,10,15,20],
        loadingType: 'overlay',
        
        
      }}
      // ================== Ngôn ngữ ============//
      localization={{
        toolbar: {
          searchTooltip: 'Tìm kiếm',
          searchPlaceholder:"Tìm kiếm"
        },
        pagination: {
          firstTooltip:"Trang đầu",
          previousTooltip:"Trang trước",
          nextTooltip:"Trang sau",
          lastTooltip:"Trang cuối",
          labelRowsSelect:"",
          
        },
        header:{
          actions:""
        },
        body:{
          emptyDataSourceMessage:"Đang tải dữ liệu"
        },
      }}
    />


    {/* ===================== Dialog thêm mới, sửa bài thi ======================== */}

    <Dialog fullScreen  open={openDialog} onClose={handleCloseDialog}  maxWidth="md">
      <DialogTitle id="form-dialog-title">{editAction?"Sửa thông tin bài thi":"Thêm bài thi"}</DialogTitle>
      <DialogContent>
        <DialogContentText>
         * : các trường bắt buộc
        </DialogContentText>
        <TextField
          autoFocus
          required
          label="Tên Bài thi"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedTestName}
          onChange={handleChange('selectedTestName')}
        />
        <TextField
          required
          label="Tổng số câu"
          fullWidth
          margin="normal"
          variant="outlined"
          type="Number"
          value={values.selectedTotalQuestion}
          onChange={handleChange('selectedTotalQuestion')}
        />
         <TextField
          required
          label="Thời gian làm bài (phút)"
          fullWidth
          margin="normal"
          variant="outlined"
          type="Number"
          value={values.selectedTimeToDo}
          onChange={handleChange('selectedTimeToDo')}
        />
         <TextField
          required
          label="Mật khẩu bài thi"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedPassword}
          onChange={handleChange('selectedPassword')}
        />
        <TextField
          required  
          label="Trạng thái"
          fullWidth
          margin="normal"
          variant="outlined"
          value={values.selectedIdStatus}
          onChange={handleChange('selectedIdStatus')}
        />
        
       
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDialog} color="primary">
          Hủy bỏ
        </Button>
        <Button onClick={editAction?handleSaveEditDialog:handleSaveAddDialog} color="primary">
          Lưu 
        </Button>
      </DialogActions>
    </Dialog>

{/* =============================== Dialog Danh sách cau hoi ============================================ */}

 <Dialog open={openCauHoiDialog} onClose={handleCloseCauHoiDialog}  maxWidth="lg" fullWidth>
   
    <MaterialTable
      title="Danh sách câu hỏi"
      columns={columnsCauHoi}
      data={values.selectedDataCauHoi}
      // ================ Styles cho bảng ===========//
      options={{
        headerStyle: {
          backgroundColor: '#081C85',
          color: '#FFF',
          fontSize:14
        },
        actionsColumnIndex: -1,
        pageSize:5,
        pageSizeOptions:[5,10,15,20],
        loadingType: 'linear',
        search:false
      }}
      // ================== Ngôn ngữ ============//
      localization={{
        toolbar: {
          searchTooltip: 'Tìm kiếm',
          searchPlaceholder:"Tìm kiếm"
        },
        pagination: {
          firstTooltip:"Trang đầu",
          previousTooltip:"Trang trước",
          nextTooltip:"Trang sau",
          lastTooltip:"Trang cuối",
          labelRowsSelect:"",
          
        },
        header:{
          actions:""
        },
        body:{
          emptyDataSourceMessage:"Không có dữ liệu"
        },
      }}
    />  
     <DialogActions>
     <Button onClick={handleCloseCauHoiDialog} color="primary">
      Đóng 
     </Button>
    </DialogActions>
  </Dialog>


      {/* TODO */}
  {/* =============================== Dialog Add câu hỏi vào bài thi ============================================ */}
  <Dialog open={openAddCauHoiDialog} onClose={handleCloseAddCauHoiDialog}  maxWidth="lg" fullWidth>
   
   <MaterialTable
     title={"Thêm câu hỏi vào bài thi: " + values.selectedTestName} 
     columns={columnsAddCauHoi}
     data={dataAllCauHoi}
     // ================ Styles cho bảng ===========//
     options={{
       headerStyle: {
         backgroundColor: '#0387CC',
         color: '#FFF',
         fontSize:14
       },
       actionsColumnIndex: -1,
       pageSize:5,
       pageSizeOptions:[5,10,15,20],
       loadingType: 'linear',
       selection: true
     }}
     actions={[
      {
        tooltip: 'Thêm câu hỏi đã chọn vào bài thi',
        icon: 'add_box',
        onClick: (evt, data) => handleAddCauHoiDaChon(data)
      }
    ]}
     // ================== Ngôn ngữ ============//
     localization={{
       toolbar: {
         searchTooltip: 'Tìm kiếm',
         searchPlaceholder:"Tìm kiếm",
         nRowsSelected:"Đã chọn {0} câu hỏi",
       },
       pagination: {
         firstTooltip:"Trang đầu",
         previousTooltip:"Trang trước",
         nextTooltip:"Trang sau",
         lastTooltip:"Trang cuối",
         labelRowsSelect:"",
         
       },
       header:{
         actions:""
       },
       body:{
         emptyDataSourceMessage:"Không có dữ liệu"
       },
     }}
   />  
    <DialogActions>
    <Button onClick={handleCloseAddCauHoiDialog} color="primary">
     Đóng 
    </Button>
   </DialogActions>
 </Dialog>
 
  </Paper>
  </main>
  <Footer/>
  {/* ======================================= DIALOG CHO THI SINH ================================ */}
  {/* =============================== Dialog Danh sách xem thi sinh trong bang ============================================ */}

  <Dialog open={openDetailDialog} onClose={handleCloseDetailDialog}  maxWidth="lg" fullWidth>
        <MaterialTable
          title="Danh sách thí sinh trong bài thi"
          columns={columnsThiSinh}
          data={values.selectedDataThiSinh}
          // ================ Styles cho bảng ===========//
          options={{
            headerStyle: {
              backgroundColor: '#081C85',
              color: '#FFF',
              fontSize:14
            },
            actionsColumnIndex: -1,
            pageSize:5,
            pageSizeOptions:[5,10,15,20],
            loadingType: 'linear',
            search:false
          }}
          // ================== Ngôn ngữ ============//
          localization={{
            toolbar: {
              searchTooltip: 'Tìm kiếm',
              searchPlaceholder:"Tìm kiếm"
            },
            pagination: {
              firstTooltip:"Trang đầu",
              previousTooltip:"Trang trước",
              nextTooltip:"Trang sau",
              lastTooltip:"Trang cuối",
              labelRowsSelect:"",
              
            },
            header:{
              actions:""
            },
            body:{
              emptyDataSourceMessage:"Không có dữ liệu"
            },
          }}
        />  
          <DialogActions>
          <Button onClick={handleCloseDetailDialog} color="primary">
          Đóng 
          </Button>
        </DialogActions>
      </Dialog>



{/* =============================== Dialog them thi sinh vao bang ============================================ */}

       <Dialog open={openAddThiSinhDialog} onClose={handleCloseAddThiSinhDialog}  maxWidth="lg" fullWidth>
        <MaterialTable
          title="Chọn thí sinh muốn thêm vào bài thi"
          columns={columnsThiSinh}
          data={filteredThiSinhData}
          // ================ Styles cho bảng ===========//
          options={{
            headerStyle: {
              backgroundColor: '#081C85',
              color: '#FFF',
              fontSize:14
            },
            actionsColumnIndex: -1,
            pageSize:5,
            pageSizeOptions:[5,10,15,20],
            loadingType: 'linear',
            selection: true
          }}
          actions={[
            {
              tooltip: 'Thêm thí sinh đã chọn vào bài thi',
              icon: 'person_add',
              onClick: (evt, data) => addSelectedThiSinh(data)
            }
          ]}
          // ================== Ngôn ngữ ============//
          localization={{
            toolbar: {
              searchTooltip: 'Tìm kiếm',
              searchPlaceholder:"Tìm kiếm",
              nRowsSelected:"Đã chọn {0} thí sinh",
            },
            pagination: {
              firstTooltip:"Trang đầu",
              previousTooltip:"Trang trước",
              nextTooltip:"Trang sau",
              lastTooltip:"Trang cuối",
              labelRowsSelect:"",
              
            },
            header:{
              actions:""
            },
            body:{
              emptyDataSourceMessage:"Không có dữ liệu"
            },
          }}
        />  
          <DialogActions>
          <Button onClick={handleCloseAddThiSinhDialog} color="primary">
          Đóng 
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}


//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  paperRoot: {
      margin:theme.spacing(0,4,0),
    },
    appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
    table: {
      minWidth: 700,
    },
    button: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
    iconSmall: {
      fontSize: 20,
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));

