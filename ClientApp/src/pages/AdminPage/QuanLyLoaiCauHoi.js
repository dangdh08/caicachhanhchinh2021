import React, { Component,useState,useEffect,forwardRef  } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Create';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AdminNavBar from './AdminNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from "material-table";
import axios from 'axios';
import Footer from '../../components/Footer';



const columns =[
  { title: 'STT', field: 'idLoaicauhoi'},
  { title: 'Loại câu hỏi', field: 'tenloai'},
]

export default function QuanLyLoaiCauHoi() {
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));

  // =================== States ===================//
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [openDialog,setOpenDialog] = useState(false);
  const [editAction,setEditAction] = useState(false);
  const [values, setValues] = useState({
    "selectedIdLoaicauhoi": '',
    "selectedTenloai": '',
  });


 // =============Effects=====================//
 useEffect(() => {
  fetchData();
}, [])

  // =============Fetch Api=====================//
  async function fetchData(){
    setLoading(true)
    try{
        await axios.get('/Api/QuanLyLoaiCauHoi/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setDataTable(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// =============Add Loai Cau hoi ============//
function handleAdd() {
  setEditAction(false)
  setValues({
    "selectedIdLoaicauhoi": '',
    "selectedTenloai": '',
  });
  setOpenDialog(true)
}
async function handleSaveAddDialog(){
  setLoading(true)
  try{
      await axios({
        method: 'post',
        url: '/api/QuanLyLoaiCauHoi/Create',
        data: {
          "tenloai": values.selectedTenloai
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          fetchData()
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(error.response.message);
          console.log(userData.token)
          setLoading(false)
        
  }
  setOpenDialog(false)
}

  // =============Delete Loại câu hỏi ============//
  async function handleDelete(rowData) {
    if (window.confirm("Chắc chắn muốn xóa loại câu hỏi " + rowData.tenloai)){
    setLoading(true)
    try{
        await axios({
          method: 'delete',
          url: '/api/QuanLyLoaiCauHoi/Delete',
          data: {
            idLoaicauhoi: rowData.idLoaicauhoi
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }}

  }
// =============Edit Loai Cau hoi ============//
function handleEdit(rowData) {
  setEditAction(true);
  setValues({
    "selectedIdLoaicauhoi": rowData.idLoaicauhoi,
    "selectedTenloai": rowData.tenloai,
  });
  
  setOpenDialog(true)

}
async function handleSaveEditDialog(){
  setLoading(true)
  try{
      await axios({
        method: 'put',
        url: '/api/QuanLyLoaiCauHoi/Edit',
        data: {
          "idLoaicauhoi": values.selectedIdLoaicauhoi,
          "tenloai": values.selectedTenloai
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          fetchData()
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(userData.token)
          setLoading(false)
        
  }
  setEditAction(false)
  setOpenDialog(false)

}


  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  }


  function handleCloseDialog(){
    setOpenDialog(false);
  }


  // =============================================================== Render Main =====================================================//
  return (
    <div className={classes.root} >
       {/* ==================Menu Bar======================= */}
      <CssBaseline />
      <AdminNavBar/>
      {/* =====================Main Page==================== */}
      <main className={classes.content}>
      <div className={classes.appBarSpacer} />
      
      {/* <Container> */}
        <Paper className={classes.paperRoot}>
        <MaterialTable
        title="Quản lý  loại câu hỏi"
        columns={columns}
        data={dataTable}
        isLoading={loading}
        // ============== Nút thêm xóa sửa ==================//
        actions={[
          {
            icon: 'add_box',
            tooltip: 'Thêm loại',
            iconProps:{color:"error"},
            isFreeAction: true,
            onClick: (event) => {handleAdd()}
          },
          {
            icon: 'edit',
            tooltip: 'Sửa thông tin loại',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleEdit(rowData)}
          },
          {
            icon: 'delete',
            tooltip: 'Xóa',
            iconProps:{color:"secondary"},
            onClick: (event, rowData) => {handleDelete(rowData)}
          }
        ]}
        // ================ Styles cho bảng ===========//
        options={{
          headerStyle: {
            backgroundColor: '#B98944',
            color: '#FFF',
            fontSize:14
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions:[5,10,15,20],
          loadingType: 'overlay',
          
          
        }}
        // ================== Ngôn ngữ ============//
        localization={{
          toolbar: {
            searchTooltip: 'Tìm kiếm',
            searchPlaceholder:"Tìm kiếm"
          },
          pagination: {
            firstTooltip:"Trang đầu",
            previousTooltip:"Trang trước",
            nextTooltip:"Trang sau",
            lastTooltip:"Trang cuối",
            labelRowsSelect:"",
            
          },
          header:{
            actions:""
          },
          body:{
            emptyDataSourceMessage:"Đang tải dữ liệu"
          },
        }}
      />


      {/* ===================== Dialog ======================== */}

      <Dialog open={openDialog} onClose={handleCloseDialog}  maxWidth="md">
        <DialogTitle id="form-dialog-title">{editAction?"Sửa loại câu hỏi":"Thêm mới loại câu hỏi"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
           * : các trường bắt buộc
          </DialogContentText>
          <TextField
            autoFocus
            required
            label="Tên loại câu hỏi"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedTenloai}
            onChange={handleChange('selectedTenloai')}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Hủy bỏ
          </Button>
          <Button onClick={editAction?handleSaveEditDialog:handleSaveAddDialog} color="primary">
            Lưu 
          </Button>
        </DialogActions>
      </Dialog>
    </Paper>
    </main>
    <Footer/>
    </div>
  );
}

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  paperRoot: {
      margin:theme.spacing(0,4,0),
    },
    appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
    table: {
      minWidth: 700,
    },
    button: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
    iconSmall: {
      fontSize: 20,
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));

const StyledTableCell = withStyles(theme => ({
    head: {
      backgroundColor: '#2196F3',
      color: theme.palette.common.white,
      fontSize: 14,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles(theme => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);