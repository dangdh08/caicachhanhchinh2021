import React, { Component,useState,useEffect,forwardRef  } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Create';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AdminNavBar from './AdminNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from "material-table";
import axios from 'axios';
import Footer from '../../components/Footer';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';



const columns =[
  {
    field: 'timelogin',
    title: 'STT',
    render: rowData => rowData.tableData.id+1,
    export: false
  },
  { title: 'Họ và tên', field: 'name'},
  { title: 'Số điện thoại', field: 'username'},
  { title: 'Số giấy tờ', field: 'cmnd' },
  
  {title: 'Đơn vị',field: 'donvi'},
  {title: 'Điểm thi',field: 'ketqua'},
]

const columnsKetQua =[
  {
    field: 'id',
    title: 'STT',
  },
  { title: 'Tên Đơn vị', field: 'donvi'},
  { title: 'Lượt dự thi', field: 'luot'},
  { title: '25 câu', field: 'diem25' },
  { title: '26 câu', field: 'diem26' },
  { title: '27 câu', field: 'diem27' },
  { title: '28 câu', field: 'diem28' },
  { title: '29 câu', field: 'diem29' },
  { title: '30 câu', field: 'diem30' },
   { title: 'Tổng', field: 'tong' },
    { title: 'Biên chế', field: 'bienche' },
     { title: 'Tỉ lệ', field: 'tile'},

]


const columnsMod =[
  
  {
    field: 'timelogin',
    title: 'STT',
    render: rowData => rowData.tableData.id+1,
    export: false
  },
  { title: 'Họ và tên', field: 'name'},
  { title: 'Số điện thoại', field: 'username'},
  { title: 'Số giấy tờ', field: 'cmnd' },
  // { title: 'Giới tính', field: 'gender'},
  {title: 'Đơn vị',field: 'donvi'},
  
]

    var dataDonvi = [
    {
       "id":"1",
        "donvi": "VP Đoàn ĐBQH, HĐND và UBND tỉnh",
        "bienche": "69",
        "luot": "112",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
        "tong":"0"
    },
    {
          "id":"2",
        "donvi": "Sở Công Thương",
        "bienche": "35",
        "luot": "59",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"

    },
    {
          "id":"3",
        "donvi": "Sở Giáo dục và Đào tạo",
        "bienche": "39",
        "luot": "2058",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"4",
        "donvi": "Sở Giao thông Vận tải",
        "bienche": "61",
        "luot": "167",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"5",
        "donvi": "Sở Kế hoạch và Đầu tư",
        "bienche": "37",
        "luot": "14",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"6",
        "donvi": "Sở Khoa học và Công nghệ",
        "bienche": "37",
        "luot": "65",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    }, {
          "id":"7",
        "donvi": "Sở Lao động - Thương binh và Xã hội",
        "bienche": "42",
        "luot": "224",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"8",
        "donvi": "Sở Ngoại vụ",
        "bienche": "20",
        "luot": "27",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"9",
        "donvi": "Sở Nội vụ",
        "bienche": "58",
        "luot": "123",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },

    
    {
          "id":"10",
        "donvi": "Sở Nông nghiệp và PTNT",
        "bienche": "132",
        "luot": "227",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"11",
        "donvi": "Sở Tài chính",
        "bienche": "52",
        "luot": "107",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"12",
        "donvi": "Sở Tài nguyên và Môi trường",
        "bienche": "224",
        "luot": "5",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"13",
        "donvi": "Sở Thông tin và Truyền thông",
        "bienche": "23",
        "luot": "83",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"14",
        "donvi": "Sở Tư pháp",
        "bienche": "25",
        "luot": "91",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"15",
        "donvi": "Sở Văn hóa - Thể thao - Du lịch",
        "bienche": "38",
        "luot": "54",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"16",
        "donvi": "Sở Xây dựng",
        "bienche": "31",
        "luot": "54",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"17",
        "donvi": "Sở Y tế",
        "bienche": "45",
        "luot": "15",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"18",
        "donvi": "Thanh Tra Tỉnh",
        "bienche": "29",
        "luot": "67",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"19",
        "donvi": "Ban Quản lý khu kinh tế",
        "bienche": "41",
        "luot": "47",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"20",
        "donvi": "Huyện Châu Thành",
        "bienche": "434",
        "luot": "2147",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"21",
        "donvi": "Huyện Bến Cầu",
        "bienche": "1000",
        "luot": "1053",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"22",
        "donvi": "Huyện Dương Minh Châu",
        "bienche": "317",
        "luot": "2743",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"23",
        "donvi": "Huyện Gò Dầu",
        "bienche": "296",
        "luot": "4386",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"24",
        "donvi": "Huyện Tân Biên",
        "bienche": "313",
        "luot": "2209",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"25",
        "donvi": "Huyện Tân Châu",
        "bienche": "368",
        "luot": "1049",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"26",
        "donvi": "Thành Phố Tây Ninh",
        "bienche": "304",
        "luot": "3167",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"27",
        "donvi": "Thị xã Hòa Thành",
        "bienche": "267",
        "luot": "2019",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },
     {
          "id":"28",
        "donvi": "Thị xã Trảng Bàng",
        "bienche": "340",
        "luot": "1897",
        "tile": "0",
        "diem25": "0",
        "diem26": "0",
        "diem27": "0",
        "diem28": "0",
        "diem29": "0",
        "diem30": "0",
         "tong":"0"
    },

    
]

export default function QuanLyThiSinh2() {
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));
  const fileInput = React.createRef();
  const isAdmin = userData.gender.toString()==="Administrator"? true: false;
  const apiUrl = userData.gender.toString()==="Administrator"? "/Api/QuanLyUser/GetAll3": "/api/thongke/getall";

  // =================== States ===================//
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [openDialog,setOpenDialog] = useState(false);
  const [editAction,setEditAction] = useState(false);
  const [cols,setCols] = useState();
  const [rows,setRows] = useState();
  const [values, setValues] = useState({
    selectedIdStudent:'',
    selectedName: '',
    selectedSbd: '',
    selectedGender: '',
    selectedBirthday: '',
    selectedDonvi:'',
    selectedEmail:'',
    selectedAvatar:'',
    selectedPhone:'',
    selectedCmnd:'',
    selectedUsername:''
  });


 // =============Effects=====================//
 useEffect(() => {
  fetchData();
}, [])

  // =============Fetch Api=====================//
  async function fetchData(){
    let count =0;

    setLoading(true)
    try{
        await axios.get(apiUrl, 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            // setDataTable(response.data)
            // setLoading(false)
            response.data.map(item =>{
            for (var i = dataDonvi.length - 1; i >= 0; i--) {
              if(item.ketqua === 30 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem30 =  Number.parseInt(dataDonvi[i].diem30) + 1;
                dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.ketqua === 29 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem29 =  Number.parseInt(dataDonvi[i].diem29) + 1;
               dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.ketqua === 28 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem28 =  Number.parseInt(dataDonvi[i].diem28) + 1;
               dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.ketqua === 27 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem27 =  Number.parseInt(dataDonvi[i].diem27) + 1;
               dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.ketqua === 26 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem26 =  Number.parseInt(dataDonvi[i].diem26) + 1;
               dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.ketqua === 25 && item.donvi === dataDonvi[i].donvi){
               dataDonvi[i].diem25 =  Number.parseInt(dataDonvi[i].diem25) + 1;
               dataDonvi[i].tong = Number.parseInt(dataDonvi[i].tong) + 1;
              }
               if(item.donvi === dataDonvi[i].donvi){
              //  dataDonvi[i].luot =  Number.parseInt(dataDonvi[i].luot) + 1;
               dataDonvi[i].tile =  Math.round(Number.parseInt(dataDonvi[i].tong)/Number.parseInt(dataDonvi[i].bienche)*100) +"%"
              }
              

            }
              
            })
            setDataTable(dataDonvi)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// =============Add User ============//
  function handleAdd() {
    setEditAction(false)
    setValues({
      selectedIdStudent:'',
      selectedName:'',
      selectedSbd:'',
      selectedGender:'',
      selectedDonvi:'',
      selectedBirthday:'',
      selectedEmail:'',
      selectedAvatar:'',
      selectedPhone:'',
      selectedCmnd:'',
      selectedUsername:''
    });
    setOpenDialog(true)
  }

  async function handleSaveAddDialog(){
    setLoading(true)
    try{
        await axios({
          method: 'post',
          url: '/api/QuanLyUser/Create',
          data: {
            "username": values.selectedUsername,
            "email": values.selectedEmail,
            "avatar": values.selectedAvatar,
            "name": values.selectedName,
            "gender": values.selectedGender,
            "birthday": values.selectedBirthday,
            "cmnd": values.selectedUsername,
            "sbd": values.selectedSbd,
            "donvi": values.selectedDonvi,
            "phone":values.selectedUsername
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(error.response.message);
            console.log(userData.token)
            setLoading(false)
          
    }
    setOpenDialog(false)

  }

  // =============Delete User ============//
  async function handleDelete(rowData) {
    if (window.confirm("Chắc chắn muốn xóa thí sinh " + rowData.name)){
    setLoading(true)
    try{
        await axios({
          method: 'delete',
          url: '/api/QuanLyUser/Delete',
          data: {
            idStudent: rowData.idStudent
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }}

  }
// =============Edit User ============//
    function handleEdit(rowData) {
    setEditAction(true)
    setValues({
      selectedIdStudent:rowData.idStudent,
      selectedName:rowData.name,
      selectedSbd:rowData.sbd,
      selectedGender:rowData.gender,
      selectedDonvi:rowData.donvi,
      selectedBirthday:rowData.birthday,
      selectedEmail:rowData.email,
      selectedAvatar:rowData.avatar,
      selectedPhone:rowData.phone,
      selectedCmnd:rowData.cmnd,
      selectedUsername:rowData.username
    });
    
    setOpenDialog(true)

  }
  async function handleSaveEditDialog(){
    setLoading(true)
    try{
        await axios({
          method: 'put',
          url: '/api/QuanLyUser/Edit',
          data: {
            "idStudent": values.selectedIdStudent,
            "username": values.selectedUsername,
            "email": values.selectedEmail,
            "avatar": values.selectedAvatar,
            "name": values.selectedName,
            "gender": values.selectedGender,
            "birthday": values.selectedBirthday,
            "phone": values.selectedPhone,
            "cmnd": values.selectedCmnd,
            "sbd": values.selectedSbd,
            "donvi": values.selectedDonvi
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }
    setEditAction(false)
    setOpenDialog(false)

  }

  
  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function handleCloseDialog(){
    setOpenDialog(false);
  }

  // ========================== Import From Excel File ==========//
  function fileHandler(event){
    let fileObj = event.target.files[0];
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if(err){
        console.log(err);            
      }
      else{
          // setCols(resp.cols)
          // setRows(resp.rows)
          console.log(resp.rows)
          addNhieuThiSinh(resp.rows)
      }
    });               

  }

  function openFileBrowser() {
    fileInput.current.click();
  }

  async function addNhieuThiSinh (array){
    setLoading(true)
    let thanhcong = 0
    try{
        await
          array.map((item,index) =>{
            // console.log(item[1])
            // console.log(item[2])
            // console.log(item[3])
            // console.log(item[4])
            // console.log(item[5])
            axios({
              method: 'post',
              url: '/api/QuanLyUser/Create',
              data: {
                "username": item[5],
                "email": '',
                "avatar": '',
                "name": item[1],
                "gender": item[3],
                "birthday": item[2],
                "cmnd": '',
                "sbd": item[5],
                "donvi": item[4]
              },
              headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
            })
              .then(response => {
                if(response.status===200){
                thanhcong = thanhcong + 1
                fetchData()
                setLoading(false)
                console.log("Thêm thành công: " + thanhcong + " thí sinh")
                }
                else {
                    console.log("===========Reponse FROM TRY ==========:")
                    console.log(response.data.message)
                    setLoading(false)
                    
                }

              }).catch(error=> {
                console.log(error)
                console.log("========CATCH ERROR INDEX "+ index)
                setLoading(false)
              })
            })
    }catch(error) {
            console.log("===========Reponse FROM CATCH ==========:")
            setLoading(false)
          
    }
  }


  // ================= Render Main ======================//
  return (
    <div className={classes.root} >
       {/* ==================Menu Bar======================= */}
      <CssBaseline />
      <AdminNavBar/>
      {/* =====================Main Page==================== */}
      <main className={classes.content}>
      <div className={classes.appBarSpacer} />
      
      {/* <Container> */}
        <Paper className={classes.paperRoot}>
        <MaterialTable
        title="Kết quả thi theo phụ lục 2 (Đã loại bỏ thí sinh không xác thực OTP)"
        columns={isAdmin ? columnsKetQua : columnsMod}
        data={dataTable}
        isLoading={loading}
        // ============== Nút thêm xóa sửa ==================//
        actions={[
          // {
          //   icon: 'add_box',
          //   tooltip: 'Thêm thí sinh',
          //   iconProps:{color:"error"},
          //   isFreeAction: true,
          //   onClick: (event) => {handleAdd()}
          // },
          // {
          //   icon: 'publish',
          //   tooltip: 'Thêm nhiều thí sinh từ Excel',
          //   iconProps:{color:"error"},
          //   isFreeAction: true,
          //   onClick: (event) => {openFileBrowser()}
          // },
          // {
          //   icon: 'edit',
          //   tooltip: 'Sửa thông tin',
          //   iconProps:{color:"primary"},
          //   onClick: (event, rowData) => {handleEdit(rowData)}
          // },
          // {
          //   icon: 'delete',
          //   tooltip: 'Xóa',
          //   iconProps:{color:"secondary"},
          //   onClick: (event, rowData) => {handleDelete(rowData)}
          // }
        ]}
        // ================ Styles cho bảng ===========//
        options={{
          headerStyle: {
            backgroundColor: '#6D7AFE',
            color: '#FFF',
            fontSize:14
          },
          actionsColumnIndex: -1,
          pageSize:30,
          pageSizeOptions:[30,60],
          loadingType: 'overlay',
          grouping: false,
          exportButton: true
          
          
        }}
        // ================== Ngôn ngữ ============//
        localization={{
          toolbar: {
            searchTooltip: 'Tìm kiếm',
            searchPlaceholder:"Tìm kiếm",
            exportTitle:"Xuất dữ liệu",
            exportName:"Xuất dữ liệu ra excel"

          },
          pagination: {
            firstTooltip:"Trang đầu",
            previousTooltip:"Trang trước",
            nextTooltip:"Trang sau",
            lastTooltip:"Trang cuối",
            labelRowsSelect:"",
            
          },
          header:{
            actions:""
          },
          body:{
            emptyDataSourceMessage:"Đang tải dữ liệu"
          },
          grouping:{
            placeholder:"Kéo thả tên cột vào đây để nhóm"
          },
        }}
      />


      {/* ===================== Dialog ======================== */}

      <Dialog open={openDialog} onClose={handleCloseDialog}  maxWidth="md">
        <DialogTitle id="form-dialog-title">{editAction?"Sửa thông tin thí sinh":"Thêm thí sinh"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
          *Mật khẩu mặc định: SBD + năm sinh
          </DialogContentText>
          
          <TextField
            autoFocus
            required
            label="Tài khoản"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedUsername}
            onChange={handleChange('selectedUsername')}
          />
          <TextField
            autoFocus
            required
            label="Họ và Tên"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedName}
            onChange={handleChange('selectedName')}
          />
          <TextField
            required
            label="Đơn vị"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedDonvi}
            onChange={handleChange('selectedDonvi')}
          />
           <TextField
            required
            label="Mã đội"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedSbd}
            onChange={handleChange('selectedSbd')}
          />
           <TextField
            required
            label="Là đội"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedGender}
            onChange={handleChange('selectedGender')}
          />
          <TextField
            required  
            label="Năm Sinh"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedBirthday}
            onChange={handleChange('selectedBirthday')}
          />
         
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Hủy bỏ
          </Button>
          <Button onClick={editAction?handleSaveEditDialog:handleSaveAddDialog} color="primary">
            Lưu 
          </Button>
        </DialogActions>
      </Dialog>

      <input type="file" hidden onChange={fileHandler} ref={fileInput} onClick={(event)=> { event.target.value = null }} style={{"padding":"10px"}} />
    </Paper>
    </main>
    <Footer/>
    </div>
  );
}

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  paperRoot: {
      margin:theme.spacing(0,4,0),
    },
    appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
    table: {
      minWidth: 700,
    },
    button: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
    iconSmall: {
      fontSize: 20,
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));

const StyledTableCell = withStyles(theme => ({
    head: {
      backgroundColor: '#2196F3',
      color: theme.palette.common.white,
      fontSize: 14,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles(theme => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);