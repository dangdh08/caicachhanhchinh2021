import React, { Component,useState,useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PeopleIcon from '@material-ui/icons/People';
import HomeIcon from '@material-ui/icons/Home';
import BangIcon from '@material-ui/icons/TableChart'
import LoaiIcon from '@material-ui/icons/Category'
import QuestionIcon from '@material-ui/icons/ContactSupport';
import TestIcon from '@material-ui/icons/Description';
import PasswordIcon from '@material-ui/icons/VpnKey';
import LogOutIcon from '@material-ui/icons/PowerSettingsNew';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';



  export default function AdminNavBar() {
    const classes = useStyles();

    const [open, setOpen] = useState(false);
    const [logedOut,setLogedOut] = useState(false);

    const handleDrawerOpen = () => {
      setOpen(true);
    };
    const handleDrawerClose = () => {
      setOpen(false);
    };
    
    function logout(){
      localStorage.clear();
      setLogedOut(true)
    }

    

    if(logedOut){
      return <Redirect to={{ pathname: "/login",}}/>
   }
   else
    // ==================================== Render Main =======================================//
    return (
      <div className={classes.root}>
        {/* ================================== App Bar ============================= */}
        <AppBar position="fixed" className={clsx(classes.appBar, open && classes.appBarShift)}>
         <Toolbar className={classes.toolbar}>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleDrawerOpen}
              className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
              <MenuIcon />
            </IconButton>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.menuTitle}>
              Hệ thống thi trắc nghiệm
            </Typography>
          </Toolbar>
        </AppBar>

         {/* ================================== Drawer ============================= */}
        <Drawer
            variant="permanent"
            classes={{
              paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
              <IconButton onClick={handleDrawerClose}>
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />

            <ListItem button  component={Link} to="/admin">
            <ListItemIcon>
              <HomeIcon color='primary' />
            </ListItemIcon>
            <ListItemText primary="Trang chủ" />
          </ListItem>

            {/* <ListItem button  component={Link} to="/admin/quanlythisinh2">
            <ListItemIcon>
              <PeopleIcon color='primary' />
            </ListItemIcon>
            <ListItemText primary="Kết quả đã lọc OTP" />
          </ListItem> */}

          {/* <ListItem button component={Link} to="/admin/quanlybaithi">
            <ListItemIcon>
              <TestIcon color='primary'/>
            </ListItemIcon>
            <ListItemText primary="Quản lý Bài Thi" />
          </ListItem> */}
          <ListItem button component={Link} to="/admin/quanlycauhoi">
            <ListItemIcon>
              <QuestionIcon color='primary'/>
            </ListItemIcon>
            <ListItemText primary="Quản lý Câu hỏi" />
          </ListItem>
          <ListItem button component={Link} to="/admin/quanlyloaicauhoi">
            <ListItemIcon>
              <LoaiIcon color='primary'/>
            </ListItemIcon>
            <ListItemText primary="Quản lý Loại câu hỏi" />
          </ListItem>
          <ListItem button component={Link} to="/admin/quanlybang">
            <ListItemIcon>
              <BangIcon color='primary'/>
            </ListItemIcon>
            <ListItemText primary="Quản lý Bảng thi" />
          </ListItem>
         
            <Divider />

            {/* <ListSubheader inset>Tài khoản</ListSubheader> */}
            {/* <ListItem button>
              <ListItemIcon>
                <PasswordIcon color='secondary' />
              </ListItemIcon>
              <ListItemText primary="Đổi mật khẩu" />
            </ListItem> */}
            <ListItem button onClick={logout}>
              <ListItemIcon>
                <LogOutIcon color='secondary'/>
              </ListItemIcon>
              <ListItemText primary="Đăng xuất" />
            </ListItem>
         </Drawer>
               
        
       </div>
    );
  }


//============================================= Styles =============================================//
const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  root: {
    // padding: theme.spacing(8, 0, 6),
    display: 'flex',
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 275,
    borderRadius: 3,
    border: 0,
    color: 'white',
  },
  title: {
    fontSize: 14,
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  menuTitle: {
    flexGrow: 1,
  },
  paper: {
    height: 350,
    width: 250,
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  drawerPaper: {
    position: 'fixed',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    minHeight: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(1),
    //paddingBottom: theme.spacing(1),
  },
  fixedHeight: {
    height: 240,
  },
  
  
}));
