import React, { Component,useState,useEffect,forwardRef  } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Create';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AdminNavBar from './AdminNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from "material-table";
import axios from 'axios';
import Footer from '../../components/Footer';



const columnsThiSinh =[
  {title: 'Tài khoản', field: 'username'},
  {title: 'Họ và tên', field: 'name'},
  {title: 'Số Báo Danh', field: 'sbd' },
  {title: 'Giới tính', field: 'gender'},
  {title: 'Năm sinh',field: 'birthday'},
  {title: 'Đơn vị',field: 'donvi'},
]


const columns =[
  { title: 'STT', field: 'idClass'},
  { title: 'Tên bảng', field: 'className'},
  { title: 'Năm thi', field: 'gradeName' },
  { title: 'Tổng số thí sinh',field: 'datathisinh',render: rowData => rowData.datathisinh.length},
]

export default function QuanLyBang() {
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));

  // =================== States ===================//
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const [openDialog,setOpenDialog] = useState(false);
  const [openDetailDialog, setOpenDetailDialog] = useState(false)
  const [openAddThiSinhDialog, setOpenAddThiSinhDialog] = useState(false)
  const [editAction,setEditAction] = useState(false);
  const [values, setValues] = useState({
    "selectedIdClass": '',
    "selectedClassName": '',
    "selectedGradeName": '',
    "thiSinhBaiThiData":[],
  });
  const [filteredThiSinhData,setFilteredThiSinhData] = useState([]);


 // =============Effects=====================//
 useEffect(() => {
  fetchData();
}, [])

  // =============Fetch Api=====================//
  async function fetchData(){
    setLoading(true)
    try{
        await axios.get('/api/QuanLyLop/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setDataTable(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// =============Add Bang ============//
function handleAdd() {
  setEditAction(false)
  setValues({
    "selectedIdClass": '',
    "selectedClassName": '',
    "selectedGradeName": '',
  });
  setOpenDialog(true)
}
async function handleSaveAddDialog(){
  setLoading(true)
  try{
      await axios({
        method: 'post',
        url: '/api/QuanLyLop/Create',
        data: {
          "className": values.selectedClassName,
          "gradeName": values.selectedGradeName
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          fetchData()
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(error.response.message);
          console.log(userData.token)
          setLoading(false)
        
  }
  setOpenDialog(false)
}

  // =============Delete User ============//
  async function handleDelete(rowData) {
    if (window.confirm("Chắc chắn muốn xóa bảng thi: " + rowData.className)){
    setLoading(true)
    try{
        await axios({
          method: 'delete',
          url: '/api/QuanLyLop/Delete',
          data: {
            idClass: rowData.idClass
          },
          headers: {
            'Authorization': 'Bearer ' + userData.token,
        }
        })
          .then(function (response) {
            if(response.status===200){
            fetchData()
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
                
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(userData.token)
            setLoading(false)
          
    }}

  }
// =============Edit Bang ============//
function handleEdit(rowData) {
  setEditAction(true);
  setValues({
    "selectedIdClass": rowData.idClass,
    "selectedClassName": rowData.className,
    "selectedGradeName": rowData.gradeName,
  });
  
  setOpenDialog(true)

}
async function handleSaveEditDialog(){
  setLoading(true)
  try{
      await axios({
        method: 'put',
        url: '/api/QuanLyLop/Edit',
        data: {
          "idClass": values.selectedIdClass,
          "className": values.selectedClassName,
          "gradeName": values.selectedGradeName,
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          fetchData()
          setLoading(false)
          }else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(userData.token)
          setLoading(false)
        
  }
  setEditAction(false)
  setOpenDialog(false)

}


  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  }


  function handleCloseDialog(){
    setOpenDialog(false);
  }

// ======================= function cho xem chi tiet thi sinh trong bang ==========================//
 async function handleOpenDetailDialog(rowData){
    setLoading(true)
    try{
      await axios({
        method: 'post',
        url: '/api/QuanLyLop/GetDanhSachThiSinhTheoLop',
        data: {
          "IdChitietLop":rowData.idClass
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
      }
      })
        .then(function (response) {
          if(response.status===200){
          setValues({
            thiSinhBaiThiData:response.data
          })
          setLoading(false)
          }
          else {
              console.log("===========Reponse==========:")
              console.log(response.data.message)
              setLoading(false)
              
          }

        })
  }catch(error) {
          console.log("===========Reponse==========:")
          console.log(error.response.status);
          console.log(error.response.message);
          setLoading(false)
        
  }
    setOpenDetailDialog(true)

  }

  function handleCloseDetailDialog(){
    setOpenDetailDialog(false)
    setValues({
      thiSinhBaiThiData:[]
    })
    
  }

  // ======================= function cho them thi sinh vao bang ==========================//

  async function handleOpenAddThiSinhDialog(rowData){
    setValues({
      selectedIdClass:rowData.idClass
    })
    let filteredArray =[];
    setLoading(true)
    try{
        await axios.get('/api/QuanLyUser/GetAll', 
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {
            if(response.status===200){
            filteredArray  = response.data.filter(function(array1_el){
              return rowData.datathisinh.filter(function(array2_el){
                 return array2_el.idStudent == array1_el.idStudent;
              }).length == 0
           });
            setFilteredThiSinhData(filteredArray)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
                setLoading(false)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
    setOpenAddThiSinhDialog(true)
  }

  function handleCloseAddThiSinhDialog(){
    setOpenAddThiSinhDialog(false)
    setFilteredThiSinhData([])
  }

 async function addSelectedThiSinh(data){
    setLoading(true)
    try{
      await data.map(item => {
       axios({
        method: 'post',
        url: '/api/QuanLyLop/AddThiSinhVaoLop',
        data: {
          "idClass": values.selectedIdClass,
          "IdStudent": item.idStudent
        },
        headers: {
          'Authorization': 'Bearer ' + userData.token,
        } 
       })
      })
      setLoading(false)
    }
    catch(error) {
            console.log("===========Reponse==========:")
            console.log(error);
    }
    fetchData()
    setOpenAddThiSinhDialog(false)
    setFilteredThiSinhData([])
  }


//======================================== Render Main ====================================================//
  return (
    <div className={classes.root} >
       {/* ==================Menu Bar======================= */}
      <CssBaseline />
      <AdminNavBar/>
      {/* =====================Main Page==================== */}
      <main className={classes.content}>
      <div className={classes.appBarSpacer} />
      
      {/* <Container> */}
        <Paper className={classes.paperRoot}>
        <MaterialTable
        title="Quản lý  bảng thi"
        columns={columns}
        data={dataTable}
        isLoading={loading}
        // ============== Nút thêm xóa sửa ==================//
        actions={[
          {
            icon: 'add_box',
            tooltip: 'Thêm bảng',
            iconProps:{color:"error"},
            isFreeAction: true,
            onClick: (event) => {handleAdd()}
          },
          {
            icon: 'how_to_reg',
            tooltip: 'Xem danh sách thí sinh',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleOpenDetailDialog(rowData)}
          },
          {
            icon: 'person_add',
            tooltip: 'Thêm thí sinh vào bảng',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleOpenAddThiSinhDialog(rowData)}
          },
          {
            icon: 'edit',
            tooltip: 'Sửa bảng',
            iconProps:{color:"primary"},
            onClick: (event, rowData) => {handleEdit(rowData)}
          },
          {
            icon: 'delete',
            tooltip: 'Xóa',
            iconProps:{color:"secondary"},
            onClick: (event, rowData) => {handleDelete(rowData)}
          }
        ]}
        // ================ Styles cho bảng ===========//
        options={{
          headerStyle: {
            backgroundColor: '#13691B',
            color: '#FFF',
            fontSize:14
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions:[5,10,15,20],
          loadingType: 'overlay',
          
          
        }}
        // ================== Ngôn ngữ ============//
        localization={{
          toolbar: {
            searchTooltip: 'Tìm kiếm',
            searchPlaceholder:"Tìm kiếm"
          },
          pagination: {
            firstTooltip:"Trang đầu",
            previousTooltip:"Trang trước",
            nextTooltip:"Trang sau",
            lastTooltip:"Trang cuối",
            labelRowsSelect:"",
            
          },
          header:{
            actions:""
          },
          body:{
            emptyDataSourceMessage:"Đang tải dữ liệu"
          },
        }}
      />


      {/* ===================== Dialog ======================== */}

      <Dialog open={openDialog} onClose={handleCloseDialog}  maxWidth="md">
        <DialogTitle id="form-dialog-title">{editAction?"Sửa bảng":"Thêm mới bảng"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
           * : các trường bắt buộc
          </DialogContentText>
          <TextField
            autoFocus
            required
            label="Tên bảng"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedClassName}
            onChange={handleChange('selectedClassName')}
          />
          <TextField
            required
            label="Năm thi"
            fullWidth
            margin="normal"
            variant="outlined"
            value={values.selectedGradeName}
            onChange={handleChange('selectedGradeName')}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Hủy bỏ
          </Button>
          <Button onClick={editAction?handleSaveEditDialog:handleSaveAddDialog} color="primary">
            Lưu 
          </Button>
        </DialogActions>
      </Dialog>
    </Paper>
    </main>
    <Footer/>

    {/* =============================== Dialog Danh sách xem thi sinh trong bang ============================================ */}

      <Dialog open={openDetailDialog} onClose={handleCloseDetailDialog}  maxWidth="lg" fullWidth>
        <MaterialTable
          title="Danh sách thí sinh trong bảng"
          columns={columnsThiSinh}
          data={values.thiSinhBaiThiData}
          // ================ Styles cho bảng ===========//
          options={{
            headerStyle: {
              backgroundColor: '#081C85',
              color: '#FFF',
              fontSize:14
            },
            actionsColumnIndex: -1,
            pageSize:5,
            pageSizeOptions:[5,10,15,20],
            loadingType: 'linear',
            search:false
          }}
          // ================== Ngôn ngữ ============//
          localization={{
            toolbar: {
              searchTooltip: 'Tìm kiếm',
              searchPlaceholder:"Tìm kiếm"
            },
            pagination: {
              firstTooltip:"Trang đầu",
              previousTooltip:"Trang trước",
              nextTooltip:"Trang sau",
              lastTooltip:"Trang cuối",
              labelRowsSelect:"",
              
            },
            header:{
              actions:""
            },
            body:{
              emptyDataSourceMessage:"Không có dữ liệu"
            },
          }}
        />  
          <DialogActions>
          <Button onClick={handleCloseDetailDialog} color="primary">
          Đóng 
          </Button>
        </DialogActions>
      </Dialog>



{/* =============================== Dialog them thi sinh vao bang ============================================ */}

       <Dialog open={openAddThiSinhDialog} onClose={handleCloseAddThiSinhDialog}  maxWidth="lg" fullWidth>
        <MaterialTable
          title="Chọn thí sinh muốn thêm vào bảng"
          columns={columnsThiSinh}
          data={filteredThiSinhData}
          // ================ Styles cho bảng ===========//
          options={{
            headerStyle: {
              backgroundColor: '#081C85',
              color: '#FFF',
              fontSize:14
            },
            actionsColumnIndex: -1,
            pageSize:5,
            pageSizeOptions:[5,10,15,20],
            loadingType: 'linear',
            selection: true
          }}
          actions={[
            {
              tooltip: 'Thêm thí sinh đã chọn vào bảng',
              icon: 'person_add',
              onClick: (evt, data) => addSelectedThiSinh(data)
            }
          ]}
          // ================== Ngôn ngữ ============//
          localization={{
            toolbar: {
              searchTooltip: 'Tìm kiếm',
              searchPlaceholder:"Tìm kiếm",
              nRowsSelected:"Đã chọn {0} thí sinh",
            },
            pagination: {
              firstTooltip:"Trang đầu",
              previousTooltip:"Trang trước",
              nextTooltip:"Trang sau",
              lastTooltip:"Trang cuối",
              labelRowsSelect:"",
              
            },
            header:{
              actions:""
            },
            body:{
              emptyDataSourceMessage:"Không có dữ liệu"
            },
          }}
        />  
          <DialogActions>
          <Button onClick={handleCloseAddThiSinhDialog} color="primary">
          Đóng 
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  paperRoot: {
      margin:theme.spacing(0,4,0),
    },
    appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
    marginLeft:60
  },
    table: {
      minWidth: 700,
    },
    button: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
    iconSmall: {
      fontSize: 20,
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));

const StyledTableCell = withStyles(theme => ({
    head: {
      backgroundColor: '#2196F3',
      color: theme.palette.common.white,
      fontSize: 14,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles(theme => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);