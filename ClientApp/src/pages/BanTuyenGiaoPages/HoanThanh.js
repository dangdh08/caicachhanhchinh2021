import React, { Component,useState,useEffect } from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '../../components/MenuAppBar';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import ThiSinhNavBar from '../ThiSinhPage/ThiSinhNavBar';
import Footer from '../../components/Footer';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Banner from '../../assets/background2.png';



export default function HoanThanh({match,history}){
  const classes = useStyles();
  const idStudent = match.params.idstd.toString();
  const testCode = match.params.testcode.toString();
  const dudoan = match.params.dudoan.toString();
  const [loading, setLoading] = useState(false);
  const [scoreData, setScoreData] = useState([])
  

   // ============= Effects =====================//
   useEffect(() => {
    if(localStorage.getItem("user")){
      localStorage.removeItem("user")
    }
      getTestScore()
  },[])


  // =============Fetch Kết quả từ Api=====================//
  async function getTestScore(){
    setLoading(true)
    try{
        await axios.post('/BanTuyenGiao/NopBai', 
          {
           IdStudent: idStudent,
           TestCode: testCode, 
           DuDoan: dudoan,
          },
         
        )
          .then(function (response) {
            if(response.data.error===200){
            setScoreData(response.data.data)
           
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }
// ======================  onclick =============================//
function onClickBack(){
  history.replace({ pathname: "/"})
  }


  // ============================= Render =======================//
  return (
    <div className={classes.root} >
    {/* ================= Menu Bar =================== */}
    <CssBaseline />
    <ThiSinhNavBar/>
    {/* ================= Main Container =================== */}
    <div className={classes.toolbar} />
    <main className={classes.content} >
   

     {/* ================= Kết quả bài thi =================== */}
      <Grid container component="main" spacing={5} alignItems="center" justify="center">
          {scoreData.map(value => (
              <Grid key={value.id_score} item xs={12} sm={12} md={6}>
                <Card className={classes.card} elevation={5}>
                  <CardContent className={classes.cardContent}>
                    <Typography  variant="h4" align="center" style={{fontWeight:"bold"}}>
                  Chúc mừng {value.name} đã hoàn thành cuộc thi {value.test_name}
                    </Typography>
                  </CardContent>
                      <Divider variant="fullWidth" light={true} />

                      {/*<CardContent className={classes.cardContent}>
                    <Typography  variant="h5" align="center" style={{fontWeight:"bold"}}>
                   Đáp án đúng: {value.socaudung} / {value.tongcau}
                    </Typography>
                  </CardContent>
                  <Divider variant="fullWidth" light={true}/>*/}
                    
                    <CardContent className={classes.cardContent}>
                      <Typography  variant="h5" align="center" style={{fontWeight:"bold"}}>
                     Thời gian làm bài: {value.thoigianlam.toString().slice(3,5)} phút {value.thoigianlam.toString().slice(6)} giây
                      </Typography>
                    </CardContent>
                    <Divider variant="fullWidth" light={true} />

                    <CardContent className={classes.cardContent}>
                      <Typography  variant="h5" align="center" style={{fontWeight:"bold"}}>
                      Dự đoán số lượt trả lời đúng: {value.dudoanketqua}
                      </Typography>
                    </CardContent>
                    <Divider variant="fullWidth" light={true}/>

                    <CardActions>
                    <Button variant="contained" color="primary" fullWidth 
                     onClick={onClickBack}>
                      Về trang chủ
                    </Button>
                  </CardActions>


                </Card>
              </Grid>
          ))}
      </Grid>
      </main>
      <Footer/>
      </div>
  );
  }


//============================================= Styles =============================================//

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 350,
    background: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 0%, rgba(7,49,147,1) 26%, rgba(0,212,255,1) 100%)',
  },
  title: {
    fontSize: 14,
  },
  cardContent:{
    color:"white",
    fontWeight:"bold",
  },
  toolbar: theme.mixins.toolbar,
  content: {
    backgroundImage:`url(${Banner})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    objectFit:'fill',
    flexGrow: 1,
    padding: theme.spacing(5),
    minHeight:"85vh"
  },
  
}));