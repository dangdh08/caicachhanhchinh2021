import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Banner from '../../assets/banner4.jpg';
import BannerLeft from '../../assets/banner-final.png';
import LogoDang from '../../assets/logo-tayninh.png';
import Divider from '@material-ui/core/Divider';
import FlipClock from 'x-react-flipclock';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



export default function DangKy() {
  const classes = useStyles();

  //================== States=========================//
  const [name, setName] = useState(null);
  const [cmnd, setCmnd] = useState(null);
  const [address, setAddress] = useState(null);
  const [email, setEmail] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [logined, setLogined] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errMessOpen,setErrMessOpen] = useState(false);
  const [openDialog,setOpenDialog] = useState(false);
  const [openTheLeDialog,setOpenTheleDialog] = useState(false);
  const [openKetQuaDialog,setOpenKetQuaDialog] = useState(false);
  const [messError, setMessError] = useState("Kiểm tra lại thông tin");


  //================== Effect =========================//
  useEffect(() => {
    if( localStorage.getItem('user')){
        setLogined(true)
    }
  },[logined])

  //================== Đăng nhập Submit =========================//
  async function handleFormSubmit(e){
    e.preventDefault();
    setLoading(true)
    try{
        await axios.post('/BanTuyenGiao/LamBaiThi', {
            name: name,
            cmnd: cmnd,
            phone:phoneNumber,
            donvi:address,
            email:email
          })
          .then(function (response) {

            if(response.data.error===200){
            console.log("===========Reponse==========:")
            console.log(response.data.error)
            localStorage.setItem('user', JSON.stringify(response.data))
            setLoading(false)
            setLogined(true)
            setOpenDialog(false)
            }
          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            console.log(error.response.data.message);
            setMessError(error.response.data.message)
            setLoading(false)
            setErrMessOpen(true)
        
    }
    
}


     //================== Set state thông báo lỗi =========================//
    function handleErrMessOpen(){
        setErrMessOpen(false)
    }

    function handleCloseDialog(){
        setOpenDialog(false);
      }
      function handleCloseTheLeDialog(){
        setOpenTheleDialog(false);
      }
      function handleCloseKetQuaDialog(){
        setOpenKetQuaDialog(false);
      }
      function handleOpenDialog(){
        setOpenDialog(true);
      }

//==============================Condition Render=============================================//
   if (logined){
        return <Redirect to={{ pathname: "/",}}/>
    }
    else
        return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
             
            {/* ================= Dang Ky Form================= */}
            <Grid item xs={12} sm={5} md={5} lg={4} xl={4} component={Paper} elevation={6} square className={classes.rightSide}> 
            {loading ? <LinearIndeterminate/> : null}  {/* //Render indicator */}
            <ErrorSnackBar errMessOpen={errMessOpen} handleErrMessOpen={handleErrMessOpen} messError={messError}/> {/* //Render thông báo lỗi */}
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <img src={LogoDang} className={classes.logo}/>
                    </Avatar>
                    <Typography component="h2" variant="h5" className={classes.testName} align="center">
                         <b>CUỘC THI  TÌM HIỂU <br/> "60 NĂM CHIẾN THẮNG TUA HAI" <br/> TRÊN INTERNET VÀ MẠNG XÃ HỘI</b>
                    </Typography>
                    <Typography component="h2" variant="h6" className={classes.testQuestion} align="center">
                         CÂU HỎI THÁNG 12/2019
                    </Typography>

                    <Typography component="h1" variant="h5" className={classes.timeRemain} >
                         THỜI GIAN CÒN LẠI CỦA CUỘC THI
                    </Typography>
                    <FlipClock 
                        type = "countdown"
                        count_to = "2020-01-26 00:00:00"
                        units = {[
                            {
                                sep: '',
                                type: 'days',
                                title: 'Ngày',
                            },
                            {
                                sep: ' ',
                                type: 'hours',
                                title: 'Giờ',
                            },
                            {
                                sep: ':',
                                type: 'minutes',
                                title: 'Phút',
                            },
                            {
                                sep: ':',
                                type: 'seconds',
                                title: 'Giây',
                            }
                        ]}
                    />
                        <Button
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                            onClick = {handleOpenDialog}
                        >
                            Vào thi
                        </Button>
                        
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick = {()=> setOpenTheleDialog(true)}
                        >
                            Thể lệ
                        </Button>

                        <Button
                            fullWidth
                            variant="contained"
                            color="default"
                            className={classes.submit}
                            onClick = {()=> setOpenKetQuaDialog(true)}
                        >
                            Kết quả tháng 11/2019
                        </Button>
                </div>
            </Grid>
            {/* ============ Banner ======================= */}
            <Grid item xs={false} sm={7} md={7} lg={8} xl={8} className={classes.image} zeroMinWidth>
            
            </Grid>   
             {/* ======================================= Dialog  dang ky thong tin =================================== */}

                <Dialog open={openDialog} onClose={handleCloseDialog}  maxWidth="sm">
                    <DialogTitle id="form-dialog-title" align="center">Đăng ký thông tin</DialogTitle>
                                    <DialogContent>
                                    
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            label="Họ và tên"
                                            name={name}
                                            onChange={(input) => setName(input.target.value)}
                                        />
                                        {/* <TextField
                                            variant="outlined"
                                            margin="normal"
                                            fullWidth
                                            label="Chứng minh nhân dân"
                                            name={cmnd}
                                            type="number"
                                            onChange={(input) => setCmnd(input.target.value)}
                                        /> */}
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            label="Chỗ ở hoặc Cơ quan làm việc"
                                            name={address}
                                            onChange={(input) => setAddress(input.target.value)}
                                        />
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            label="Số điện thoại"
                                            type="number"
                                            name={phoneNumber}
                                            onChange={(input) => setPhoneNumber(input.target.value)}
                                        />
                                        {/* <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            label="Email"
                                            type="email"
                                            name={email}
                                            onChange={(input) => setEmail(input.target.value)}
                                        /> */}
                    </DialogContent>
                    <DialogActions style={{justifyContent:"center"}}>
                        <Button onClick={handleFormSubmit} color="secondary"  variant="contained">
                            Bắt đầu thi
                        </Button>
                    </DialogActions>
            </Dialog>

            {/* ======================================= Dialog  The Le =================================== */}

            <Dialog open={openTheLeDialog} onClose={handleCloseTheLeDialog}  maxWidth="md">
                    <DialogTitle id="form-dialog-title" align="center">THỂ LỆ CUỘC THI</DialogTitle>
                    <DialogContent>
                    <p>
                    <strong>I. Mục đích cuộc thi</strong>
                </p>
                <p>
                    - Tuyên truyền, khẳng định ý nghĩa lịch sử to lớn của Chiến thắng Tua
                    Hai; sự lãnh đạo đúng đắn, sáng suốt của Đảng Cộng sản Việt Nam nói chung
                    và của Đảng bộ tỉnh Tây Ninh nói riêng trong việc chỉ đạo, tổ chức thắng
                    lợi Chiến thắng Tua Hai; giáo dục truyền thống cách mạng, lòng yêu nước;
                    khơi dậy chủ nghĩa anh hùng cách mạng, tinh thần đại đoàn kết, niềm tự hào
                    và ý chí tự lực, tự cường dân tộc; củng cố, bồi đắp niềm tin của Nhân dân
                    vào sự nghiệp xây dựng và bảo vệ Tổ quốc.
                </p>
                <p>
                    - Ca ngợi tinh thần chiến đấu anh dũng, mưu trí của quân và dân Tây Ninh;
                    khẳng định những thành tựu to lớn của Đảng bộ, quân và dân Tây Ninh sau 60
                    năm Chiến thắng Tua Hai; tri ân công lao, đóng góp to lớn của quân và
                    dân ta trong Chiến thắng Tua Hai và trong cuộc kháng chiến chống Mỹ, cứu
                    nước.
                </p>
                <p>
                    - Việc tổ chức Cuộc thi cần đảm bảo trang trọng, tiết kiệm, thiết
                    thực, hiệu quả; thực hiện tốt công tác tuyên truyền, quảng bá rộng rãi về
                    Cuộc thi để cán bộ, đảng viên, đoàn viên, hội viên, chiến sỹ lực lượng vũ
                    trang và Nhân dân trong, ngoài tỉnh biết, tham gia.
                </p>
                <p>
                    <strong>II. Đối tượng dự thi</strong>
                </p>
                <p>
                    Cán bộ, đảng viên, công chức, viên chức, chiến sỹ lực lượng vũ trang, đoàn
                    viên, hội viên và Nhân dân trong và ngoài tỉnh.
                </p>
                <p>
                    Thành viên Ban Tổ chức, Tổ Nội dung, Tổ Thư ký - Kỹ thuật và công chức, người
                    lao động cơ quan Ban Tuyên giáo Tỉnh uỷ, Sở Thông tin và Truyền thông không
                    được tham gia dự thi.
                </p>
                <p>
                    <strong>III. Hình thức thi</strong>
                </p>
                <p>
                    Cuộc thi tìm hiểu “60 năm Chiến thắng Tua Hai” được tổ chức dưới hình thức
                    thi trắc nghiệm hằng tháng trên Internet và mạng xã hội.
                </p>
                <p>
                    <strong>1. Cách thức tham gia Cuộc thi</strong>
                </p>
                <p>
                    Người tham gia Cuộc thi tiến hành các thao tác dưới đây để tham gia trả lời
                    câu hỏi thi của Ban Tổ chức:
                </p>
                <p>
                    <em>a. Đăng nhập tham gia </em>
                </p>
                <p>
                    - Truy cập vào trang web chính thức của Cuộc thi, tại địa chỉ:
                    <a href="https://www.60namtuahai.tayninh.gov.vn">
                        https://www.60namtuahai.tayninh.gov.vn
                    </a>
                </p>
                <p>
                    Hoặc nhấn đúp chuột vào banner (ảnh bìa) Cuộc thi tìm hiểu “60 năm Chiến
                    thắng Tua Hai” trên các website, cổng thông tin điện tử, các trang mạng xã
                    hội sau:
                </p>
                <p>
                + Cổng thông tin điện tử của tỉnh Tây Ninh (    <a href="http://www.tayninh.gov.vn">http://www.tayninh.gov.vn</a>)
                </p>
                <p>
                + Báo Tây Ninh Online (    <a href="http://www.baotayninh.vn">http://www.baotayninh.vn</a>)
                </p>
                <p>
                + Đài Phát thanh và Truyền hình Tây Ninh (    <a href="http://www.ttv11.vn">http://www.ttv11.vn</a>)
                </p>
                <p>
                    + Cổng thông tin điện tử các sở, ban, ngành, địa phương trong tỉnh.
                </p>
                <p>
                    + Các nhóm Facebook: “Chúng tôi yêu Việt Nam” (
                    <a href="http://www.facebook.com/groups/127535717725765">
                        http://www.facebook.com/groups/127535717725765
                    </a>
                    ); “Chúng tôi yêu biên giới và biển, đảo Việt Nam” (
                    <a href="http://www.facebook.com/groups/313090955430812">
                        http://www.facebook.com/groups/313090955430812
                    </a>
                    )
                </p>
                <p>
                    - Bấm chuột vào ô “Bắt đầu thi” và trả lời các câu hỏi.
                </p>
                <p>
                    <em>b</em>
                    . <em>Trả lời câu hỏi </em>
                </p>
                <p>
                    - Mỗi tháng, Ban Tổ chức Cuộc thi đưa ra một số câu hỏi thi trắc nghiệm về
                    lịch sử Chiến thắng Tua Hai, về thành tựu phát triển kinh tế - xã hội, bảo
                    đảm quốc phòng - an ninh, xây dựng Đảng… của Tây Ninh sau 44 năm giải
                    phóng.
                </p>
                <p>
                    Mỗi câu hỏi gồm nhiều đáp án, trong đó có 01 đáp án đúng. Người dự thi lựa
                    chọn một trong số các đáp án, sau đó điền vào ô <strong>“Dự đoán”</strong>
                    số lượt người trả lời đúng.
                </p>
                <p>
                    - Bấm vào ô <strong>“Hoàn thành”</strong> để kết thúc phần thi.
                </p>
                <p>
                    <strong>2. Nguyên tắc trao thưởng </strong>
                    <strong></strong>
                </p>
                <p>
                    Người được trao thưởng là người trả lời đúng các câu hỏi và dự đoán chính
                    xác nhất số lượt người trả lời đúng.
                </p>
                <p>
                    Trong trường hợp số người dự thi (từ 02 người trở lên) cùng trả lời đúng
                    các câu hỏi, cùng dự đoán chính xác số lượt người trả lời đúng, người có thời
                    gian trả lời câu hỏi ngắn nhất sẽ được chọn để trao thưởng (thời gian được
                    tính từ khi bắt đầu đến khi kết thúc cuộc thi tháng).
                </p>
                <p>
                    Trường hợp có nhiều người cùng trả lời đúng các câu hỏi, cùng dự đoán chính
                    xác số lượt người trả lời đúng, cùng thời gian trả lời câu hỏi thì giải thưởng
                    được chia đều cho số người cùng trả lời đúng nêu trên.
                </p>
                <p>
                    <strong>3. </strong>
                    <strong>Tài liệu tham khảo</strong>
                    <strong></strong>
                </p>
                <p>
                    - Lịch sử Đảng Cộng Sản Việt Nam. Nxb. Chính trị Quốc gia.
                </p>
                <p>
                    - Lịch sử Đảng bộ miền Đông Nam bộ lãnh đạo kháng chiến chống thực dân Pháp
                    và đế quốc Mỹ (1945 - 1975). Hội đồng chỉ đạo biên soạn Lịch sử Đảng bộ
                    miền Đông Nam bộ. Nxb Chính trị Quốc gia, Hà Nội, 2003.
                </p>
                <p>
                    - Lịch sử Đảng bộ tỉnh Tây Ninh (1930-2005). Nxb. Chính trị Quốc gia, Hà
                    Nội, 2010.
                </p>
                <p>
                    - Chiến thắng Tua Hai và phong trào Đồng Khởi ở miền Đông Nam bộ. Nxb Quân
                    đội Nhân dân, năm 1999.
                </p>
                <p>
                    - Báo cáo sơ kết giữa nhiệm kỳ Đại hội X của Đảng bộ tỉnh, nhiệm kỳ
                    2015-2020.
                </p>
                <p>
                    Trường hợp có những nội dung sử liệu chưa thống nhất thì căn cứ vào
                    “Lịch sử Đảng bộ tỉnh Tây Ninh” làm gốc đối chiếu.
                </p>
                <p>
                    <strong>IV. Thời gian, giải thưởng Cuộc thi</strong>
                </p>
                <p>
                    <strong>1. Thời gian thi</strong>
                    <strong></strong>
                </p>
                <p>
                    - Cuộc thi tìm hiểu “60 năm Chiến thắng Tua Hai” trên Internet và mạng xã
                hội được tiến hành hằng tháng, <strong>từ ngày 26/10/2019</strong> -    <strong>26/01/2020</strong>.
                </p>
                <p>
                    - Thời gian thi mỗi tháng được tính từ 00 giờ 00’ ngày 26 của tháng và kết
                    thúc vào 23 giờ 59’ ngày 25 của tháng kế tiếp.
                </p>
                <p>
                    - Mỗi người có thể dự thi nhiều lượt/tháng, tuy nhiên chỉ được công nhận 01
                    kết quả đúng nhất và có thời gian trả lời ngắn nhất trong số các lượt dự
                    thi.
                </p>
                <p>
                    <strong>2. Giải thưởng</strong>
                    <strong></strong>
                </p>
                <p>
                    Mỗi tháng có 16 giải thưởng, bao gồm:
                </p>
                <p>
                - <strong>01 giải Nhất:</strong> mỗi giải trị giá 1.000.000 đồng    <em>(Một triệu đồng).</em>
                </p>
                <p>
                - <strong>02 giải Nhì:</strong> mỗi giải trị giá 700.000 đồng    <em>(Bảy trăm ngàn đồng).</em>
                </p>
                <p>
                    - <strong>03 giải Ba:</strong><strong> </strong>mỗi giải trị giá 500.000
                    đồng <em>(Năm trăm ngàn đồng).</em>
                </p>
                <p>
                    - <strong>10 giải Khuyến khích:</strong><strong> </strong>mỗi giải trị giá
                    300.000 đồng <em>(Ba trăm ngàn đồng).</em>
                </p>
                <p>
                    <strong>V. Thông báo kết quả và trao thưởng</strong>
                </p>
                <p>
                    - Kết quả thi trắc nghiệm hằng tháng được công bố trang web chính thức của
                    Cuộc thi ngay sau khi kết thúc (chậm nhất là 12 giờ 00 ngày 26
                    của tháng) tại địa chỉ:
                    <a href="https://www.60namtuahai.tayninh.gov.vn">
                        https://www.60namtuahai.tayninh.gov.vn
                    </a>
                    .
                </p>
                <p>
                    - Kết thúc Cuộc thi, Ban Tổ chức tiến hành Tổng kết Cuộc thi và trao Giấy
                    khen, tiền thưởng cho cá nhân đạt giải các Cuộc thi tháng; dự kiến tổ chức
                    trong tháng 01/2020.
                </p>
                <p>
                    <strong>VI. Tổ chức thực hiện</strong>
                </p>
                <p>
                    Thể lệ Cuộc thi tìm hiểu “60 năm Chiến thắng Tua Hai” trên Internet và mạng
                    xã hội có hiệu lực kể từ ngày ký và được đăng tải công khai trên các phương
                    tiện thông tin đại chúng, trên Internet và mạng xã hội. Việc sửa đổi, điều
                    chỉnh Thể lệ Cuộc thi chỉ được thực hiện khi có sự đồng ý bằng văn bản của
                    Ban Tổ chức Cuộc thi.
                </p>
                    </DialogContent>
                    <DialogActions style={{justifyContent:"center"}}>
                        <Button onClick={handleCloseTheLeDialog} color="secondary"  variant="contained">
                            Đồng ý
                        </Button>
                    </DialogActions>
            </Dialog>



            
            {/* ======================================= Dialog Ket Qua =================================== */}

            <Dialog open={openKetQuaDialog} onClose={handleCloseKetQuaDialog}  maxWidth="lg">
                    <DialogTitle id="form-dialog-title" align="center"><p><strong>DANH S&Aacute;CH</strong></p>
<p>Th&iacute; sinh đạt giải Cuộc thi trắc nhiệm t&igrave;m hiểu</p>
<p>&ldquo;60 năm Chiến thắng Tua Hai&rdquo; - Kết quả thi th&aacute;ng 11/2019</p></DialogTitle>
                    <DialogContent>
                    <table border="1" cellpadding="0" cellspacing="0" width="996">
  <tbody>
    <tr>
      <td align="center" width="4.718875502008032%">
        <p><strong>STT</strong></p>
      </td>
      <td align="center" width="19.377510040160644%">
        <p><strong>Họ và tên</strong></p>
      </td>
      <td align="center" width="10.742971887550201%">
        <p><strong>Số điện thoại</strong></p>
      </td>
      <td align="center" width="7.429718875502008%">
        <p><strong>Số câu đúng</strong></p>
      </td>
      <td align="center" width="6.927710843373494%">
        <p><strong>Kết quả dự đoán</strong></p>
      </td>
      <td align="center" width="6.224899598393574%">
        <p><strong>Sai số</strong></p>
      </td>
      <td colspan="2" align="center" width="10.843373493975903%">
        <p><strong>Thời gian làm bài</strong></p>
      </td>
      <td width="24.096385542168676%" align="center">
        <p><strong>Địa chỉ</strong></p>
      </td>
      <td width="9.63855421686747%" align="center">
        <p><strong>Xếp hạng</strong></p>
      </td>
    </tr>
    <tr>
      <td align="center" width="4.718875502008032%">
        <p>1</p>
      </td>
      <td align="center" width="19.377510040160644%">
        <p>Đinh Thị Hà</p>
      </td>
      <td align="center" width="10.742971887550201%">
        <p>0984085082</p>
      </td>
      <td align="center" width="7.429718875502008%">
        <p>6</p>
      </td>
      <td align="center" width="6.927710843373494%">
        <p>350</p>
      </td>
      <td align="center" width="6.224899598393574%">
        <p>144</p>
      </td>
      <td colspan="2" align="center" width="10.843373493975903%">
        <p>23:51:38</p>
      </td>
      <td align="center" width="24.096385542168676%">
        <p>Sở Văn hóa, Thể thao và Du lịch</p>
      </td>
      <td align="center" width="9.63855421686747%">
        <p>1</p>
      </td>
    </tr>
    <tr>
      <td align="center" width="4.718875502008032%">
        <p>2</p>
      </td>
      <td align="center" width="19.377510040160644%">
        <p>Nguyễn Thị Kim Phượng</p>
      </td>
      <td align="center" width="10.742971887550201%">
        <p>0909741546</p>
      </td>
      <td align="center" width="7.429718875502008%">
        <p>6</p>
      </td>
      <td align="center" width="6.927710843373494%">
        <p>1200</p>
      </td>
      <td align="center" width="6.224899598393574%">
        <p>850</p>
      </td>
      <td colspan="2" align="center" width="10.843373493975903%">
        <p>00:07:58</p>
      </td>
      <td align="center" width="24.096385542168676%">
        <p>Thanh Tra Tỉnh</p>
      </td>
      <td align="center" width="9.63855421686747%">
        <p>2</p>
      </td>
    </tr>
    <tr>
      <td align="center" width="4.718875502008032%">
        <p>3</p>
      </td>
      <td align="center" width="19.377510040160644%">
        <p>Nguyễn Đăng Duy</p>
      </td>
      <td align="center" width="10.742971887550201%">
        <p>0384970710</p>
      </td>
      <td align="center" width="7.429718875502008%">
        <p>6</p>
      </td>
      <td align="center" width="6.927710843373494%">
        <p>1270</p>
      </td>
      <td align="center" width="6.224899598393574%">
        <p>920</p>
      </td>
      <td colspan="2" align="center" width="10.843373493975903%">
        <p>00:00:41</p>
      </td>
      <td align="center" width="24.096385542168676%">
        <p>Thanh tra Tỉnh</p>
      </td>
      <td align="center" width="9.63855421686747%">
        <p>2</p>
      </td>
    </tr>
    
  </tbody>
</table>
                    </DialogContent>
                    <DialogActions style={{justifyContent:"center"}}>
                        <Button onClick={handleCloseKetQuaDialog} color="secondary"  variant="contained">
                            Đồng ý
                        </Button>
                    </DialogActions>
            </Dialog>
        </Grid>
        
    );
    }

    
 {/* ====== Thông báo lỗi khi đăng nhập sai ========*/}
    function ErrorSnackBar(props){
        const classes = useStyles();
        const {errMessOpen, handleErrMessOpen,messError, ...other } = props;
        
        return(
            <Snackbar
            anchorOrigin={ { vertical: 'bottom', horizontal: 'right' } }
            open={errMessOpen}
            autoHideDuration={3000}
            onClose={handleErrMessOpen}
            >
                <SnackbarContent
                onClose={handleErrMessOpen}
                className={classes.error}
                message={
                            <span  className={classes.message}>
                            <ErrorIcon className={classes.iconVariant} />
                            {messError}
                        </span>
                        }
                />
            </Snackbar>
        );
    }

    {/* ====== Loading indicator ========*/}
    function LinearIndeterminate() {
        return (
          <div>
            <LinearProgress />
          </div>
        );
      }

       

//============================================= Styles =============================================//
const useStyles = makeStyles(theme => ({
  root: {
      height: '100vh',
  },
  bannerText:{
    backgroundColor: '#007ACC',
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(2),
    color:theme.palette.background.paper,
    padding:theme.spacing(1),

  },
  image: {
      backgroundImage:`url(${BannerLeft})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: '100% 100%',
      objectFit:'fill',
  },
  rightSide:{
    background: 'linear-gradient(to right, #00c6ff, #0072ff)',
    // background: 'linear-gradient(to right, #00d2ff, #3a7bd5)',
     color:"white"
    // backgroundRepeat: 'no-repeat',
    // backgroundSize: 'cover',
    // backgroundPosition: 'center',
  },
  paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
  },
  avatar: {
      width:130,
      height:130,
      backgroundColor:"transparent",
  },
  logo:{
      width:"100%",
      height:"100%"
  },
  form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
  },
  submit: {
      margin: theme.spacing(1, 0, 1),
  },
  timeRemain:{
    margin: theme.spacing(2, 0, 5),
    fontSize:15,
    fontWeight:"bold",
    color:"white",
    
  },
  testQuestion:{
    fontSize:18,
    fontWeight:"bold",
    fontFamily:"-apple-system",
    color:"white",
  },

  testName:{
    // margin: theme.spacing(0, 0, 5),
    fontSize:20,
    fontWeight:"bold",
    fontFamily:"-apple-system",
    color:"yellow",
  },
  progress: {
    margin: theme.spacing(2),
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));
