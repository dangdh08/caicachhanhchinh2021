import React, { Component,useState,useEffect } from 'react';
import AppBar from '../../components/MenuAppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DoneIcon from '@material-ui/icons/Done';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Container } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CardHeader from '@material-ui/core/CardHeader';
import { flexbox } from '@material-ui/system';
import { Link, Redirect } from 'react-router-dom'
import Footer from '../../components/Footer';
import ThiSinhNavBar from '../ThiSinhPage/ThiSinhNavBar';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Drawer from '@material-ui/core/Drawer';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ReactCountdownClock from 'react-countdown-clock';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import FaceIcon from '@material-ui/icons/Face';
import EmailIcon from '@material-ui/icons/Email';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LamBaiIcon from '@material-ui/icons/Send';
import Avatar from '@material-ui/core/Avatar';
import NamAvatar from '../../assets/nam-avatar.jpg';
import NuAvatar from '../../assets/nu-avatar.jpg';


export default function LamBai({ match,history }) {
    const classes = useStyles();
    
    const userSaved =  JSON.parse(localStorage.getItem('user'));
    const userData =  userSaved.user;
    const questionData = userSaved.data;
    const idStudent = userData.id_student.toString();
    const testCode = userSaved.data[0].test_code.toString();
    

     // ============= States =====================//
    const [value, setValue] = useState();
    const [loading, setLoading] = useState(false);
    const [dudoan, setDuDoan] = useState(null);
    const [open, setOpen] = useState(false);
    const [isSubmited, setIsSubmited] = useState(false);
    const [errMessOpen,setErrMessOpen] = useState(false);
    const [dudoanError,setDudoanError] = useState(false);
    

    // ============= Effects =====================//
    // useEffect(() => {
    //    checkIsSubmited()
    //     fetchQuestions();
     

    // }, [])



    // ============================= Kiểm tra trạng thái bài thi ====================//
    // async function checkIsSubmited() {
    //   try{
    //     await axios.post('/Api/BaiThiUser/CheckDiem', 
    //       {
    //        IdStudent: idStudent,
    //        TestCode: testCode
    //       },
    //       {
    //         headers: {
    //             'Authorization': 'Bearer ' + userData.token,
    //         }
    //       }
    //     )
    //       .then(function (response) {
    //         if(response.data.error===203){
    //           setIsSubmited(true)
    //         }
    //         else setIsSubmited(false)
            
    //       })
    // }catch(error) {
    //         console.log("===========Reponse==========:")
    //         console.log(error.response.status);
        
    // }

    // }


   //============== Cập nhật câu trả lời =========//
  async function handleChange(event,idQuestion) {
      setValue(event.target.value);
      console.log("===========Value radio buuton==========:")
      console.log(event.target.value)
      console.log(idQuestion)
      console.log(idStudent)
      console.log(event.target.value)
      try{
        await axios.post('/BanTuyenGiao/UpdateDapAn_BTG', 
          {
           Id: idQuestion,
           IdStudent: idStudent,
           StudentAnswer: event.target.value 
          },
        )
          .then(function (response) {

            console.log(response.status)
          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
        
    }

    }


// ====================== Nộp bài onclick =============================//
   function onClickNopBai(){
    if(dudoan===null){
      setDudoanError(true)
      setOpen(false)
    }else{
      history.replace({ pathname: "/hoanthanh/" + idStudent + "/" + testCode + "/" + dudoan })
    }
   
    }

// ========================= XÁC NHẬN FUNCTION ========================//
function handleClickOpen() {
  setOpen(true);
}

function handleClose() {
  setOpen(false);
}
 
 //================== Set state thông báo lỗi =========================//
 function handleErrMessOpen(){
  setErrMessOpen(false)
}


// ========================================================== Render ===================================================================//
    
return (
      
      <div className={classes.root} >
        {/* ================= Menu Bar =================== */}
         <CssBaseline />
         <ThiSinhNavBar/>
         {/* ================= Main Container =================== */}
         <main className={classes.content} >
         <div className={classes.toolbar} />

        {/* =======================User Profile========================== */}
        {!isSubmited && (<Grid container spacing={5} direction="row" justify="center" >
          <Grid  item xs={12} sm={6} md={4} lg={3}> 
            <Card className={classes.userCard} elevation={2}>
                          <CardContent  className={classes.media}>
                            <Avatar  src={NamAvatar} className={classes.bigAvatar}/>
                            {userData.name}
                          </CardContent>
                          
                          {/* <ListItem>
                          <ListItemIcon>
                            <FaceIcon color='primary' />
                          </ListItemIcon>
                          <ListItemText primary={"CMND: " + userData.cmnd} />
                        </ListItem> */}
                        <ListItem>
                          <ListItemIcon>
                            <EmailIcon color='primary'/>
                          </ListItemIcon>
                          <ListItemText primary={"Địa chỉ: " + userData.donvi} />
                        </ListItem>
                        <ListItem>
                          <ListItemIcon>
                            <AssignmentIcon color='primary'/>
                          </ListItemIcon>
                          <ListItemText primary={"Điện thoại: " + userData.phone} />
                        </ListItem>
                          
              </Card>
              </Grid>
          
          {/* ================= Danh sách câu hỏi =================== */}
          <Grid item xs={12} sm={6} md={8} lg={9}>
            {questionData.map((item,index)=>{
              return(
                <Card className={classes.card} key={item.id} elevation={5}>
                     <CardHeader
                      title= {`Câu hỏi số ${index+1}: ${item.content}`}
                      className={classes.cardHeader}
                    />
                    <Divider variant="fullWidth" />

                    <CardActions>
                      <RadioGroup
                        className={classes.group}
                        value={value}
                        onChange={(event) => handleChange(event,item.id)}
                        defaultValue={item.student_answer}
                      >
                        <FormControlLabel  value="A" control={<Radio color="secondary"/>} label= {"A. "+item.answer_a} />
                        <FormControlLabel  value="B" control={<Radio color="secondary"/>} label= {"B. "+item.answer_b} />
                        <FormControlLabel  value="C" control={<Radio color="secondary"/>} label= {"C. "+item.answer_c} />
                        <FormControlLabel  value="D" control={<Radio color="secondary"/>} label= {"D. "+item.answer_d} />
                      </RadioGroup>
                    </CardActions>
                  </Card>  
              )
            })}       <Card className={classes.card}  elevation={5}> 
                      <CardContent>
                      <TextField
                            variant="outlined"
                            margin="normal"
                            error={dudoanError}
                            required
                            fullWidth
                            label="Dự đoán số lượt trả lời đúng"
                            type="number"
                            name={dudoan}
                            onChange={(input) => setDuDoan(input.target.value)}
                        />
                      </CardContent>
                      </Card>
                      
                  <Box mt={5} style={{display:"flex",justifyContent:"center"}}>
                    <Button  
                    onClick={handleClickOpen}
                    variant="contained" color="primary" className={classes.button} fullWidth>
                      Hoàn thành
                    </Button>
                  </Box>
                  
          </Grid>
                  
        </Grid>
        )}
        </main>
        {/* =================================== XÁC NHẬN DIALOG ========================= */}
            <Dialog
            open={open}
            onClose={handleClose}
          >
            <DialogTitle >{"Xác nhận hoàn thành bài thi ?"}</DialogTitle>
            <DialogContent >
              <DialogContentText style={{color:'red'}}>
                Sau khi bấm nút "ĐỒNG Ý" kết quả sẽ được ghi nhận, bạn có chắc chắn muốn hoàn thành bài thi ?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary" autoFocus>
                Hủy
              </Button>
              <Button onClick={onClickNopBai} color="primary" >
                Đồng ý
              </Button>
            </DialogActions>
          </Dialog>
        <Footer/>
        </div>
    );
  }



  //============================================= Styles ==============================================//
const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
    root: {
      // padding: theme.spacing(2, 0, 6),
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'left',
      display:'flex',
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'column'
    },
    
    card: {
      minWidth: 275,
      marginBottom:30,
    },
    formControl: {
      margin: theme.spacing(1),
    },
    group: {
      margin: theme.spacing(1, 0),
    },
    button: {
      minWidth: 275,
    },
    cardHeader: {
      backgroundColor: theme.palette.primary.dark,
      color:"white"
    },
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    error: {
      backgroundColor: theme.palette.error.dark,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing(1),
    },
    message: {
      display: 'flex',
      alignItems: 'center',
    },
    userCard: {
        minWidth: 270,
      },
      media: {
        background: 'radial-gradient( circle 502px at 2.7% 23.7%,  rgba(142,5,254,1) 0%, rgba(33,250,214,1) 99.6% )',
        height:210,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        color:'white',
        fontSize:25,
        
      },
      bigAvatar: {
        margin: 15,
        width: 100,
        height: 100,
      },
  }));