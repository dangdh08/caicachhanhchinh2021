import React, { Component,useState,useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import CardHeader from '@material-ui/core/CardHeader';
import { Link } from 'react-router-dom';
import ThiSinhNavBar from './ThiSinhNavBar';
import Footer from '../../components/Footer';
import CardMedia from '@material-ui/core/CardMedia';
import axios from 'axios';
import FaceIcon from '@material-ui/icons/Face';
import EmailIcon from '@material-ui/icons/Email';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LamBaiIcon from '@material-ui/icons/Send';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import NamAvatar from '../../assets/nam-avatar.jpg';
import NuAvatar from '../../assets/nu-avatar.jpg';



export default function ThiSinhHomePage(){
  const classes = useStyles();
  const userData =  JSON.parse(localStorage.getItem('token'));
  const idStudent = Number.parseInt(userData.idStudent,10);

  // =============States=====================//
  const [testData, setTestData] = useState([])
  const [loading, setLoading] = useState(false);

 // =============Effects=====================//
  useEffect(() => {

    fetchTests();

  }, [])
  

   // =============Fetch bài thi từ Api=====================//
  async function fetchTests(){
    setLoading(true)
    try{
        await axios.post('/Api/BaiThiUser/GetBaiThi', 
          {
          IdStudent: idStudent
          },
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setTestData(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }

    // =============Render=====================//
    return (
      <div className={classes.root} >
        {/* ==================Menu Bar======================= */}
        <CssBaseline />
        <ThiSinhNavBar/>
        {/* =====================Main Page==================== */}
        <main className={classes.content} >
        <div className={classes.toolbar} />
        <Grid container spacing={3} direction="row"  justify="center" >

        {/* =======================User Profile========================== */}
            <Grid  item xs={12} sm={12} md="auto"> 
            <Card className={classes.userCard} elevation={2}>
                          <CardContent  className={classes.media}>
                            <Avatar  src={userData.gender==='Nam'?NamAvatar:NuAvatar} className={classes.bigAvatar}/>
                            {userData.name.toUpperCase()}
                          </CardContent>
                          
                          <ListItem>
                          <ListItemIcon>
                            <FaceIcon color='primary' />
                          </ListItemIcon>
                          <ListItemText primary={"Giới tính: " + userData.gender} />
                        </ListItem>
                        <ListItem>
                          <ListItemIcon>
                            <EmailIcon color='primary'/>
                          </ListItemIcon>
                          <ListItemText primary={"Đơn vị: " + userData.donvi} />
                        </ListItem>
                        <ListItem>
                          <ListItemIcon>
                            <AssignmentIcon color='primary'/>
                          </ListItemIcon>
                          <ListItemText primary={"Số báo danh: " + userData.sbd} />
                        </ListItem>
                          
              </Card>
              </Grid>


           {/* =======================Danh sach Bai thi========================== */}
                  <Grid  container spacing={4} direction="row" justify="space-evenly" item xs={12} sm={12} md={8}>
                  {testData.map(value => (
                    <Grid  key={value.test_code} item xs={12} sm={12} md="auto">
                    <Card className={classes.card} elevation={2} key={value.id}>
                        <CardHeader
                          title= {value.test_name}
                          titleTypographyProps={{ align: 'center' }}
                          className={classes.cardHeader}
                        />
                        <Divider variant="fullWidth"/>
                        <CardContent>
                          <Typography  variant="body1" align="center" color="textPrimary">
                          Tình trạng: {value.status_name}
                          </Typography>
                          <Divider variant="fullWidth" />
                        </CardContent>
                        
                        <CardContent>
                          <Typography  variant="body1" align="center" color="textPrimary">
                          Mã bài thi: {value.test_code}
                          </Typography>
                          <Divider variant="fullWidth" />
                        </CardContent>


                        <CardContent>
                          <Typography  variant="body1" align="center" color="textPrimary">
                          Tổng số câu hỏi: {value.total_questions}
                          </Typography>
                          <Divider variant="fullWidth" />
                        </CardContent>

                        <CardContent>
                          <Typography  variant="body1" align="center" color="textPrimary">
                          Thời gian làm bài: {value.time_to_do} phút
                          </Typography>
                          <Divider variant="fullWidth" />
                        </CardContent>

                      
                      <CardActions>
                        <Button variant="contained" color="primary" fullWidth  component={Link} to={"/lambai/"+value.test_code}>
                          Làm bài thi
                          <LamBaiIcon className={classes.rightIcon} />
                        </Button>
                      </CardActions>
                    </Card>
                    </Grid>
              ))}
               </Grid>
        </Grid>
        </main>
        <Footer/>{/* ==============Footer================ */}
        </div>
        
    );
    }

  

//============================================= Styles =============================================//

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 310,
  },
  userCard: {
    minWidth: 300,
  },
  title: {
    fontSize: 14,
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  media: {
    background: 'radial-gradient( circle 502px at 2.7% 23.7%,  rgba(142,5,254,1) 0%, rgba(33,250,214,1) 99.6% )',
    height:210,
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column',
    color:'white',
    fontSize:25,
    
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  bigAvatar: {
    margin: 15,
    width: 100,
    height: 100,
  },
  
}));
