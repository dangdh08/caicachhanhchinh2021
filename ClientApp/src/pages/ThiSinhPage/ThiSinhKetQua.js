import React, { Component,useState,useEffect } from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '../../components/MenuAppBar';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import ThiSinhNavBar from './ThiSinhNavBar';
import Footer from '../../components/Footer';
import axios from 'axios';



export default function ThiSinhKetQua({match}){
  const classes = useStyles();

  const userData =  JSON.parse(localStorage.getItem('token'));
  const idStudent = Number.parseInt(match.params.idstd,10);
  const testCode = Number.parseInt(match.params.testcode,10);
  const [loading, setLoading] = useState(false);
  const [scoreData, setScoreData] = useState([])
  

   // ============= Effects =====================//
   useEffect(() => {
      getTestScore();
  }, [])


  // =============Fetch câu hỏi từ Api=====================//
  async function getTestScore(){
    setLoading(true)
    try{
        await axios.post('/Api/BaiThiUser/DiemBaiThi', 
          {
           IdStudent: idStudent,
           TestCode: testCode, 
          },
          {
            headers: {
                'Authorization': 'Bearer ' + userData.token,
            }
          }
        )
          .then(function (response) {

            if(response.status===200){
            setScoreData(response.data)
            setLoading(false)
            }else {
                console.log("===========Reponse==========:")
                console.log(response.data.message)
            }

          })
    }catch(error) {
            console.log("===========Reponse==========:")
            console.log(error.response.status);
            setLoading(false)
        
    }
  }



  // ============================= Render =======================//
  return (
    <div className={classes.root} >
    {/* ================= Menu Bar =================== */}
    <CssBaseline />
    <ThiSinhNavBar/>
    {/* ================= Main Container =================== */}
    <main className={classes.content} >
    <div className={classes.toolbar} />

     {/* ================= Kết quả bài thi =================== */}
      <Grid container spacing={5} alignItems="flex-end" alignItems="center" justify="center">
          {scoreData.map(value => (
              <Grid key={value.id_score} item xs={12} sm={12} md={4}>
                <Card className={classes.card} elevation={5}>

                  <CardHeader
                      title= "KẾT QUẢ"
                      titleTypographyProps={{ align: 'center' }}
                      className={classes.cardHeader}
                    />
                  <Divider variant="fullWidth" />

                  <CardContent>
                    <Typography  variant="h2" component="h3" align="center">
                      {value.socaudung} / {value.tongcau}
                    </Typography>
                    
                  </CardContent>
                  <Divider variant="fullWidth" />

                    <CardContent>
                      <Typography  variant="body1" align="center"  color="textPrimary">
                      Bài thi: {value.test_name}
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent>

                    <CardContent>
                      <Typography  variant="body1" align="center"  color="textPrimary">
                      Mã Bài thi: {value.test_code}
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent>
                    
                    <CardContent>
                      <Typography  variant="body1" align="center"  color="textPrimary">
                     Thí sinh: {value.name}
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent>

                    {/* <CardContent>
                      <Typography  variant="body1" component="body2" align="center"  color="textPrimary">
                      Số báo danh: {value.SBD}
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent> */}

                </Card>
              </Grid>
          ))}
      </Grid>
      </main>
      <Footer/>
      </div>
  );
  }


//============================================= Styles =============================================//

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
   
    
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  
}));