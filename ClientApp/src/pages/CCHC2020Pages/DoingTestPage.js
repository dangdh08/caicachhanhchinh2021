import React, { Component, useState, useEffect } from "react";
import AppBar from "../../components/MenuAppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import DoneIcon from "@material-ui/icons/Done";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import { flexbox } from "@material-ui/system";
import { Link, Redirect } from "react-router-dom";
import Footer from "../../components/Footer";
import HomePageNavBar from "./HomePageNavBar";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Drawer from "@material-ui/core/Drawer";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ReactCountdownClock from "react-countdown-clock";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Snackbar from "@material-ui/core/Snackbar";
import ErrorIcon from "@material-ui/icons/Error";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';

export default function DoingTestPage({ match, history }) {
  const classes = useStyles();

  const userData = JSON.parse(localStorage.getItem("user"));
  const idStudent = userData.idStudent.toString();
  const testCode = match.params.id.toString(); //==> bắt Testcode truyền từ trang home sang khi user click làm bài
  const phoneNumber = userData.phone.toString();
  // ============= States =====================//
  const [value, setValue] = useState();
  const [questionData, setQuestionData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [inputPass, setInputPass] = useState("");
  const [timeRemain, setTimeRemain] = useState();
  const [open, setOpen] = useState(false);
  const [isSubmited, setIsSubmited] = useState(false);
  const [errMessOpen, setErrMessOpen] = useState(false);
  const [dudoan, setDuDoan] = useState(null);
  const [dudoanError,setDudoanError] = useState(false);

  // ============= Effects =====================//
  useEffect(() => {
    checkIsSubmited();
    fetchQuestions();
  }, []);

  // ============================= Kiểm tra trạng thái bài thi ====================//
  async function checkIsSubmited() {
    setLoading(true);
    try {
      await axios
        .post(
          "/Api/BaiThiUser/CheckDiem",
          {
            IdStudent: idStudent,
            TestCode: testCode
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token
            }
          }
        )
        .then(function(response) {
          if (response.data.error === 203) {
            setIsSubmited(true);
            setLoading(false)
          } else setIsSubmited(false);
        });
    } catch (error) {
       setLoading(false);
    }
  }

  // =============Fetch câu hỏi từ Api=====================//
  async function fetchQuestions() {
    setLoading(true);
    try {
      await axios
        .post(
          "/Api/BaiThiUser/ThoiGinThi",
          {
            IdStudent: idStudent,
            TestCode: testCode
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token
            }
          }
        )
        .then(function(response) {
          if (response.status === 200) {
            // console.log("FETCH QUESTION");
            setTimeRemain(response.data.timecount);
            
            // console.log(response.data.timecount);
          } else {
            // console.log("===========Reponse==========:");
            setTimeRemain(0);
           
            // console.log(response.data.message);
          }
        });
    } catch (error) {
      // console.log("===========Reponse==========:");
      console.log(error.response.status);
      setLoading(false);
    }

    try {
      await axios
        .post(
          "/Api/BaiThiUser/GetCauHoi",
          {
            IdStudent: idStudent,
            TestCode: testCode,
            Password: phoneNumber
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token
            }
          }
        )
        .then(function(response) {
          if (response.status === 200) {
            setQuestionData(response.data);
            setLoading(false);
          } else {
            console.log("===========Reponse==========:");
            console.log(response.data.message);
            setLoading(false);
          }
        });
    } catch (error) {
      console.log("===========Reponse==========:");
      console.log(error.response.status);
      setLoading(false);
    }
  }

  //============== Cập nhật câu trả lời =========//
  async function handleChange(event, idQuestion) {
    setValue(event.target.value);
    // console.log("===========Value radio buuton==========:");
    // console.log(event.target.value);
    // console.log(idQuestion);
    try {
      await axios
        .post(
          "/Api/BaiThiUser/UpdateDapAn",
          {
            Id: idQuestion.toString(),
            IdStudent: idStudent,
            TestCode: testCode,
            StudentAnswer: event.target.value
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token
            }
          }
        )
        .then(function(response) {
          console.log(response.status);
        });
    } catch (error) {
      console.log("===========Reponse==========:");
      console.log(error.response.status);
    }
  }

  // ====================== Nộp bài onclick =============================//
  function onClickNopBai() {
    if(dudoan===null){
      setDudoanError(true)
      setOpen(false)
    }else{
    history.replace({ pathname: "/ketqua/" + idStudent + "/" + testCode  + "/" + dudoan})
  }
  }

  // ========================= XÁC NHẬN FUNCTION ========================//
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  //================== Set state thông báo lỗi =========================//
  function handleErrMessOpen() {
    setErrMessOpen(false);
  }

  // ========================================================== Render ===================================================================//

  if (isSubmited) {
    return (
      <Redirect to={{ pathname: "/ketqua/" + idStudent + "/" + testCode }} />
    )
  }
  else return (
      <div className={classes.root}>
        {/* ================= Menu Bar =================== */}
        <CssBaseline />
        <HomePageNavBar />

        {/* ================= Main Container =================== */}
        <main className={classes.content}>
          <div className={classes.toolbar} />

          {/* ================= Danh sách tóm tắt câu hỏi đã trả lời =================== */}
          {!isSubmited && !loading && (
            <Grid container spacing={1} direction="row" justify="center">
              <Grid item xs={12} sm={12} md={2}>
                <Paper
                  className={classes.paper}
                  elevation={2}
                  style={{ position: "fixed" }}
                >
                  <Typography
                    variant="h6"
                    color="primary"
                    style={{ marginBottom: 10 }}
                  >
                    Thời gian làm bài
                  </Typography>
                  <ReactCountdownClock
                    seconds={timeRemain}
                    color="#303F9F"
                    alpha={1}
                    size={105}
                    weight={15}
                    timeFormat="hms"
                    onComplete={onClickNopBai}
                  />
                </Paper>
              </Grid>

              {/* ================= Danh sách câu hỏi =================== */}
              <Grid item xs={12} sm={12} md={10}>
                {questionData.map((item, index) => {
                  return (
                    <Card className={classes.card} key={item.id} elevation={5}>
                      <CardHeader
                        title={ `Câu hỏi ${index + 1}: ${item.content}`}
                        className={classes.cardHeader}
                      />
                      <Divider variant="fullWidth" />

                      <CardActions>
                        <RadioGroup
                          className={classes.group}
                          value={value}
                          onChange={event => handleChange(event, item.id.toString())}
                          defaultValue={item.student_answer}
                        >
                          <FormControlLabel
                            style={{display:"list-item", marginRight:0, marginLeft:0}}
                            value="A"
                            control={<Radio color="primary" />}
                            label={"A. " + item.answer_a}
                          />
                          <FormControlLabel
                            style={{display:"list-item", marginRight:0, marginLeft:0}}
                            value="B"
                            control={<Radio color="primary" />}
                            label={"B. " + item.answer_b}
                          />
                          <FormControlLabel
                            style={{display:"list-item", marginRight:0, marginLeft:0}}
                            value="C"
                            control={<Radio color="primary" />}
                            label={"C. " + item.answer_c}
                          />
                          <FormControlLabel
                            style={{display:"list-item", marginRight:0, marginLeft:0}}
                            value="D"
                            control={<Radio color="primary" />}
                            label={"D. " + item.answer_d}
                          />
                        </RadioGroup>
                      </CardActions>
                    </Card>
                  );
                })}

                <Card className={classes.card}  elevation={5}> 
                      <CardContent>
                      <TextField
                            variant="outlined"
                            margin="normal"
                            error={dudoanError}
                            required
                            fullWidth
                            label="Dự đoán số lượt dự thi"
                            type="number"
                            name={dudoan}
                            onChange={(input) => setDuDoan(input.target.value)}
                        />
                      </CardContent>
                      </Card>
                <Box mt={5}>
                  <Button
                    onClick={handleClickOpen}
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    fullWidth
                  >
                    Hoàn thành bài thi
                  </Button>
                </Box>
              </Grid>
            </Grid>
          )}

          {!isSubmited && loading && (
            <LinearProgress/>
            )}
        </main>
        {/* =================================== XÁC NHẬN DIALOG ========================= */}
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>{"Xác nhận nộp bài thi ?"}</DialogTitle>
          <DialogContent>
            <DialogContentText style={{ color: "red" }}>
              Sau khi bấm nút "ĐỒNG Ý" kết quả sẽ được ghi nhận, bạn có chắc
              chắn muốn nộp bài ?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" autoFocus>
              Hủy
            </Button>
            <Button onClick={onClickNopBai} color="primary">
              Đồng ý
            </Button>
          </DialogActions>
        </Dialog>
        <Footer />
      </div>
    );
}

{
  /* ====== Thông báo lỗi khi đăng nhập sai ========*/
}
function ErrorSnackBar(props) {
  const classes = useStyles();
  const { errMessOpen, handleErrMessOpen, ...other } = props;

  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      open={errMessOpen}
      autoHideDuration={3000}
      onClose={handleErrMessOpen}
    >
      <SnackbarContent
        onClose={handleErrMessOpen}
        className={classes.error}
        message={
          <span className={classes.message}>
            <ErrorIcon className={classes.iconVariant} />
            {"Sai mật khẩu bài thi"}
          </span>
        }
      />
    </Snackbar>
  );
}

//============================================= Styles ==============================================//
const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "left",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },

  card: {
    minWidth: 275,
    marginBottom: 8
  },
  formControl: {
    margin: theme.spacing(1)
  },
  group: {
    margin: theme.spacing(1, 0)
  },
  button: {
    minWidth: 275
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200]
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));
