import React, { Component, useState, useEffect } from "react";
import AppBar from "../../components/MenuAppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import DoneIcon from "@material-ui/icons/Done";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import { flexbox } from "@material-ui/system";
import { Link, Redirect } from "react-router-dom";
import Footer from "../../components/Footer";
import HomePageNavBar from "./HomePageNavBar";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Drawer from "@material-ui/core/Drawer";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ReactCountdownClock from "react-countdown-clock";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Snackbar from "@material-ui/core/Snackbar";
import ErrorIcon from "@material-ui/icons/Error";
import SnackbarContent from "@material-ui/core/SnackbarContent";

export default function EssayPage({ match, history }) {
  const classes = useStyles();

  const userData = JSON.parse(localStorage.getItem("user"));
  const idStudent = userData.idStudent.toString();
  const testCode = match.params.id.toString(); //==> bắt Testcode truyền từ trang home sang khi user click làm bài
  const idTuluan = match.params.idtuluan.toString(); //==> bắt idtuluan truyền từ trang home sang khi user click làm bài
  const phoneNumber = userData.phone.toString();
  // ============= States =====================//
  const [value, setValue] = useState(null);
  const [questionData, setQuestionData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [inputPass, setInputPass] = useState("");
  const [timeRemain, setTimeRemain] = useState();
  const [open, setOpen] = useState(false);
  const [isSubmited, setIsSubmited] = useState(false);
  const [errMessOpen, setErrMessOpen] = useState(false);

  // ============= Effects =====================//
  useEffect(() => {
    loadCauTraLoi();
  },[]);

  // ================ Load nội dung câu trả lời nếu có ===========//
  function loadCauTraLoi() {
    setLoading(true);
    axios
      .post(
        "/api/CaiCachHanhChinh/LamBaiTuLuan",
        {
          IdStudent: idStudent,
          testcode: testCode,
          didong: phoneNumber
        },
        {
          headers: {
            Authorization: "Bearer " + userData.token
          }
        }
      )
      .then(function(response) {
        if (response.status === 200) {
          setValue(response.data.data.cautraloi.toString());
          setLoading(false);
        }
      })
      .catch(error => {
        console.log("===========Reponse==========:");
        console.log(error.response.status);
        setLoading(false);
      });
  }
  // =============Cập nhật bài thi tự luận=====================//
  async function updateTuLuan() {
    try {
      await axios
        .post(
          "/api/CaiCachHanhChinh/CapNhatTuLuan",
          {
            Idtuluan: idTuluan,
            IdStudent: idStudent,
            TestCode: testCode,
            Phone: phoneNumber,
            Cautraloi: value
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token
            }
          }
        )
        .then(function(response) {
          if (response.status === 200) {
            alert("Cập nhật thành công");
            console.log(response.data);
          }
        });
    } catch (error) {
      console.log("===========Reponse==========:");
      console.log(error.response.status);
    }
  }
  // ====================== Nộp bài onclick =============================//
  function onClickNopBai() {
    updateTuLuan();
  }

  // ========================= XÁC NHẬN FUNCTION ========================//
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  //================== Set state thông báo lỗi =========================//
  function handleErrMessOpen() {
    setErrMessOpen(false);
  }

  const handleChange = event => {
    setValue(event.target.value);
  };

  // ========================================================== Render ===================================================================//

  return (
    <div className={classes.root}>
      {/* ================= Menu Bar =================== */}
      <CssBaseline />
      <HomePageNavBar />

      {/* ================= Main Container =================== */}
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Grid container spacing={1} direction="row" justify="center">
          {/* ================= Bai tu luan =================== */}
          <Grid item xs={12} sm={12} md={10}>
            <Card className={classes.card} elevation={5}>
              <CardHeader
                title="Hãy chia sẻ những cảm nhận của bạn về Cái cách hành chính của tỉnh Tây Ninh."
                className={classes.cardHeader}
              />
              <Divider variant="fullWidth" />

              <CardContent>
                <TextField
                  autoFocus={true}
                  fullWidth
                  id="outlined-multiline-static"
                  label="Câu trả lời của bạn"
                  multiline
                  rows="20"
                  value={value}
                  variant="outlined"
                  onChange={handleChange}
                />
              </CardContent>
              <CardActions>
                <Button
                  onClick={onClickNopBai}
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                >
                  Lưu
                </Button>
                <Button
                  onClick={onClickNopBai}
                  variant="contained"
                  color="primary"
                  className={classes.leftButton}
                >
                  Gửi câu trả lời
                </Button>
              </CardActions>
            </Card>

            <Box mt={5}></Box>
          </Grid>
        </Grid>
      </main>

      {/* =================================== XÁC NHẬN DIALOG ========================= */}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>{"Xác nhận nộp bài thi ?"}</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ color: "red" }}>
            Sau khi bấm nút "ĐỒNG Ý" kết quả sẽ được ghi nhận, bạn có chắc chắn
            muốn nộp bài ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Hủy
          </Button>
          <Button onClick={onClickNopBai} color="primary">
            Đồng ý
          </Button>
        </DialogActions>
      </Dialog>
      <Footer />
    </div>
  );
}

{
  /* ====== Thông báo lỗi khi đăng nhập sai ========*/
}
function ErrorSnackBar(props) {
  const classes = useStyles();
  const { errMessOpen, handleErrMessOpen, ...other } = props;

  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      open={errMessOpen}
      autoHideDuration={3000}
      onClose={handleErrMessOpen}
    >
      <SnackbarContent
        onClose={handleErrMessOpen}
        className={classes.error}
        message={
          <span className={classes.message}>
            <ErrorIcon className={classes.iconVariant} />
            {"Sai mật khẩu bài thi"}
          </span>
        }
      />
    </Snackbar>
  );
}

//============================================= Styles ==============================================//
const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "left",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },

  card: {
    minWidth: 275,
    marginBottom: 8
  },
  formControl: {
    margin: theme.spacing(1)
  },
  group: {
    margin: theme.spacing(1, 0)
  },
  button: {
    minWidth: 275,
    marginRight: "auto"
  },
  leftButton: {
    minWidth: 275,
    marginLeft: "auto"
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200]
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));
