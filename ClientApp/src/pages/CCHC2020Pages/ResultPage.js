import React, { Component, useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import AppBar from "../../components/MenuAppBar";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import CardHeader from "@material-ui/core/CardHeader";
import HomePageNavBar from "./HomePageNavBar";
import Footer from "../../components/Footer";
import axios from "axios";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function ResultPage({ match,history }) {
  const classes = useStyles();
  const dudoan = match.params.dudoan.toString();
  const userData = JSON.parse(localStorage.getItem("user"));
  const idStudent = Number.parseInt(match.params.idstd, 10).toString();
  const testCode = Number.parseInt(match.params.testcode, 10).toString();
  const phone = userData.phone.toString();
  const [loading, setLoading] = useState(false);
  const [scoreData, setScoreData] = useState([]);
  const [dapAnData, setDapAnData] = useState([]);
  const [openDaAnSaiDialog, setOpenDapAnSaiDialog] = useState(false);

  // ============= Effects =====================//
  useEffect(() => {
    if (localStorage.getItem("user")) {
      // localStorage.removeItem("user")
    }
    getTestScore();
  }, []);

  // =============Fetch ket qua Api=====================//
  async function getTestScore() {
    setLoading(true);
    try {
      await axios
        .post(
          "/Api/BaiThiUser/DiemBaiThi",
          {
            IdStudent: idStudent,
            TestCode: testCode,
            DuDoan: dudoan,
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token,
            },
          }
        )
        .then(function (response) {
          if (response.status === 200) {
            setScoreData(response.data.data);
            setDapAnData(response.data.dapan);
            setLoading(false);
          } else {
            console.log("===========Reponse==========:");
            console.log(response.data.message);
          }
        });
    } catch (error) {
      console.log("===========Reponse==========:");
      console.log(error.response.status);
      setLoading(false);
    }
  }


  async function newAttemp() {
    setLoading(true);
    try {
      await axios
        .post(
          "/api/CaiCachHanhChinh/TaoDeThi",
          {
            phone: phone,
          },
          {
            headers: {
              Authorization: "Bearer " + userData.token,
            },
          }
        )
        .then(function (response) {
          if (response.data.error === 200) {
            setLoading(false);
            history.replace({ pathname: "/" });
          }
        });
    } catch (error) {
      console.log("===========Reponse==========:");
      console.log(error.response.status);
      setLoading(false);
    }
  }

  function handleCloseDapAnSaiDialog() {
    setOpenDapAnSaiDialog(false);
  }

  function handleCloseDapAnSaiDialog() {
    setOpenDapAnSaiDialog(false);
  }

  // ============================= Render =======================//
  return (
    <div className={classes.root}>
      {/* ================= Menu Bar =================== */}
      <CssBaseline />
      <HomePageNavBar />
      {/* ================= Main Container =================== */}
      <main className={classes.content}>
        <div className={classes.toolbar} />

        {/* ================= Kết quả bài thi =================== */}
        <Grid
          container
          spacing={5}
          alignItems="flex-end"
          alignItems="center"
          justify="center"
        >
          {scoreData.map((value) => (
            <Grid key={value.id_score} item xs={12} sm={12} md={3}>
              <Card className={classes.card} elevation={5}>
                <CardHeader
                  title="KẾT QUẢ"
                  titleTypographyProps={{ align: "center" }}
                  className={classes.cardHeader}
                />
                <Divider variant="fullWidth" />

                <CardContent>
                  <Typography variant="h2" component="h3" align="center">
                    {value.socaudung} / {value.tongcau}
                  </Typography>
                </CardContent>
                <Divider variant="fullWidth" />

                {/* <CardContent>
                      <Typography  variant="body1" align="center"  color="textPrimary">
                      Bài thi trắc nghiệm 
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent> */}

                <CardContent>
                  <Typography
                    variant="body1"
                    align="center"
                    color="textPrimary"
                  >
                    Họ và tên: {value.name}
                  </Typography>
                  <Divider variant="fullWidth" />
                </CardContent>

                <CardContent>
                  <Typography
                    variant="body1"
                    align="center"
                    color="textPrimary"
                  >
                    Số điện thoại: {value.test_name}
                  </Typography>
                  <Divider variant="fullWidth" />
                </CardContent>
                
                <CardContent>
                  <Typography
                    variant="body1"
                    align="center"
                    color="textPrimary"
                  >
                    Dự đoán tổng lượt thi: {value.dudoanketqua}
                  </Typography>
                  <Divider variant="fullWidth" />
                </CardContent>
                <CardActions>
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={() => setOpenDapAnSaiDialog(true)}
                  >
                    Xem câu trả lời sai
                  </Button>
                </CardActions>
                <CardActions>
                  <Button
                    fullWidth
                    variant="contained"
                    color="secondary"
                    onClick={() => newAttemp()}
                  >
                    Làm bài thi mới
                  </Button>
                </CardActions>
                {/* <CardContent>
                      <Typography  variant="body1" align="center"  color="textPrimary">
                     Đơn vị: {value.donvi}
                      </Typography>
                      <Divider variant="fullWidth" />
                    </CardContent> */}
              </Card>
            </Grid>
          ))}
        </Grid>
        <Dialog
          open={openDaAnSaiDialog}
          onClose={handleCloseDapAnSaiDialog}
          maxWidth="md"
        >
          <DialogTitle id="form-dialog-title" align="center">
            Danh sách câu trả lời sai
          </DialogTitle>
          <DialogContent dividers>
            <TableContainer>
              <Table aria-label="customized table" size="large">
                <TableHead>
                  <TableRow>
                    {/* <StyledTableCell>STT</StyledTableCell> */}
                    <StyledTableCell>Nội dung câu hỏi</StyledTableCell>
                    <StyledTableCell>Đáp án đúng</StyledTableCell>
                    <StyledTableCell>Câu trả lời của bạn</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dapAnData.map((row, index) => {
                    if (row.correct_answer != row.student_answer) {
                      return (
                        <StyledTableRow key={row.idCauhoi}>
                          {/* <StyledTableCell> */}
                          {/*   {index + 1} */}
                          {/* </StyledTableCell> */}
                          <StyledTableCell component="th" scope="row">
                            {row.content}
                          </StyledTableCell>
                          <StyledTableCell>
                            {row.correct_answer === "A"
                              ? row.answer_a
                              : row.correct_answer === "B"
                              ? row.answer_b
                              : row.correct_answer === "C"
                              ? row.answer_c
                              : row.correct_answer === "D"
                              ? row.answer_d
                              : null}
                          </StyledTableCell>
                          <StyledTableCell>
                            {row.student_answer === null
                              ? "Bạn chưa trả lời"
                              : row.student_answer === "A"
                              ? row.answer_a
                              : row.student_answer === "B"
                              ? row.answer_b
                              : row.student_answer === "C"
                              ? row.answer_c
                              : row.student_answer === "D"
                              ? row.answer_d
                              : null}
                          </StyledTableCell>
                        </StyledTableRow>
                      );
                    } else return null;
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleCloseDapAnSaiDialog}
              color="secondary"
              variant="contained"
            >
              Đóng
            </Button>
          </DialogActions>
        </Dialog>
      </main>
      <Footer />
    </div>
  );
}

//============================================= Styles =============================================//

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
