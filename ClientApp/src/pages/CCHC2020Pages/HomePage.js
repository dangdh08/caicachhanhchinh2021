import React, { Component, useState, useEffect } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import CardHeader from "@material-ui/core/CardHeader";
import { Link } from "react-router-dom";
import HomePageNavBar from "./HomePageNavBar";
import Footer from "../../components/Footer";
import CardMedia from "@material-ui/core/CardMedia";
import axios from "axios";
import FaceIcon from "@material-ui/icons/Face";
import EmailIcon from "@material-ui/icons/Email";
import AssignmentIcon from "@material-ui/icons/Assignment";
import LamBaiIcon from "@material-ui/icons/Send";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import NamAvatar from "../../assets/nam-avatar.jpg";
import NuAvatar from "../../assets/nu-avatar.jpg";
import PeopleIcon from "@material-ui/icons/People";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import PhoneIcon from "@material-ui/icons/Phone";

export default function HomePage() {
  const classes = useStyles();
  const userData = JSON.parse(localStorage.getItem("user"));
  const idStudent = userData.idStudent.toString();
  const phoneNumber = userData.phone.toString();
  const isTeam = userData.gender.toString();

  // =============States=====================//
  const [testData, setTestData] = useState([]);
  const [essayData, setEssayData] = useState([]);
  const [loading, setLoading] = useState(false);

  // =============Effects=====================//
  useEffect(() => {
    taoDeThi();
  }, []);

  // ============== Tạo Đề Thi ========================//
  function taoDeThi() {
    setLoading(true);
    // axios
    //   .post(
    //     "/api/CaiCachHanhChinh/TaoDeThi",
    //     {
    //       phone: phoneNumber
    //     },
    //     {
    //       headers: {
    //         Authorization: "Bearer " + userData.token
    //       }
    //     }
    //   )
    //   .then(function(response) {
    //     if (response.status === 200) {
    // ======== Lấy bài trắc nghiệm ======/
    axios
      .post(
        "/Api/BaiThiUser/GetBaiThi",
        {
          IdStudent: idStudent,
        },
        {
          headers: {
            Authorization: "Bearer " + userData.token,
          },
        }
      )
      .then(function (response) {
        if (response.status === 200) {
          setTestData(response.data);
          // ====== Lấy bài tự luận ====//
          // axios
          // .post(
          //   "/api/CaiCachHanhChinh/LamBaiTuLuan",
          //   {
          //     IdStudent: idStudent,
          //     testcode: response.data.test_code,
          //     didong: phoneNumber
          //   },
          //   {
          //     headers: {
          //       Authorization: "Bearer " + userData.token
          //     }
          //   }
          // )
          // .then(function(response) {
          //   if (response.status === 200) {
          //     setEssayData(response.data.data);
          //     setLoading(false);
          //   }
          // })
          // .catch(error => {
          //   console.log("===========Reponse==========:");
          //   console.log(error.response.status);
          //   setLoading(false);
          // });
        }
      })
      .catch((error) => {
        console.log("===========Reponse==========:");
        console.log(error.response.status);
        setLoading(false);
      });
    // }
    // })
    // .catch(error => {
    //   console.log("===========Reponse==========:");
    //   console.log(error.response.status);
    // if(error.response.data.error === 201){
    //   // ========= lấy bài trắc nghiệm ====//
    //   axios
    //     .post(
    //       "/Api/BaiThiUser/GetBaiThi",
    //       {
    //         IdStudent: idStudent
    //       },
    //       {
    //         headers: {
    //           Authorization: "Bearer " + userData.token
    //         }
    //       }
    //     )
    //     .then(function(response) {
    //       if (response.status === 200) {
    //         setTestData(response.data);
    //         // ====== Lấy bài tự luận ====//
    //         axios
    //         .post(
    //           "/api/CaiCachHanhChinh/LamBaiTuLuan",
    //           {
    //             IdStudent: idStudent,
    //             testcode: response.data.test_code,
    //             didong: phoneNumber
    //           },
    //           {
    //             headers: {
    //               Authorization: "Bearer " + userData.token
    //             }
    //           }
    //         )
    //         .then(function(responsedata) {
    //           if (responsedata.status === 200) {
    //             console.log("=========KEETS QUA TU LUAN");
    //             setEssayData(responsedata.data.data);
    //             console.log(essayData);
    //             setLoading(false);
    //           }
    //         })
    //         .catch(error => {
    //           console.log("===========Reponse==========:");
    //           console.log(error);
    //           setLoading(false);
    //         });
    //       }
    //     })
    //     .catch(error => {
    //       console.log("===========Reponse==========:");
    //       console.log(error.response.status);
    //       setLoading(false);
    //     });
    // }
    setLoading(false);
    // });
  }

  // =============Render=====================//
  return (
    <div className={classes.root}>
      {/* ==================Menu Bar======================= */}
      <CssBaseline />
      <HomePageNavBar />
      {/* =====================Main Page==================== */}
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Grid container spacing={3} direction="row" justify="center">
          {/* =======================User Profile========================== */}
          <Grid item xs={12} sm={12} md="auto">
            <Card className={classes.userCard} elevation={2}>
              <CardContent className={classes.media}>
                <Avatar src={NamAvatar} className={classes.bigAvatar} />
                {userData.name.toUpperCase()}
              </CardContent>

              <ListItem>
                <ListItemIcon>
                  <PhoneIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary={"Số điện thoại: " + userData.phone} />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <AccountBalanceIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary={"Đơn vị: " + userData.donvi} />
              </ListItem>
              {/* <ListItem>
                <ListItemIcon>
                  <PeopleIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary={isTeam === "0" ? "Đội:" + userData.sbd : "Thí sinh tự do"} />
              </ListItem> */}
            </Card>
          </Grid>

          {/* =======================Danh sach Bai thi ========================== */}
          <Grid
            container
            spacing={4}
            direction="row"
            justify="space-evenly"
            item
            xs={12}
            sm={12}
            md={8}
          >
            {/* =======================Bai thi trac nghiem ========================== */}
            {testData.map((value) => (
              <Grid key={value.test_code} item xs={12} sm={12} md="auto">
                <Card className={classes.card} elevation={2} key={value.id}>
                  <CardHeader
                    title="Bài thi trắc nghiệm"
                    titleTypographyProps={{ align: "center" }}
                    className={classes.cardHeader}
                  />
                  <Divider variant="fullWidth" />
                  {/* <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Tình trạng: {value.status_name === "Open" ? "Đang mở" : "Đã đóng"}
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent> */}

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Mã bài thi: {value.test_code}
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Tổng số câu hỏi: {value.total_questions}
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Thời gian làm bài: {value.time_to_do} phút
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Số lượt dự đoán:{" "}
                      {value.dudoanketqua === null
                        ? "Chưa thi"
                        : value.dudoanketqua}
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardActions>
                    <Button
                      variant="contained"
                      color="primary"
                      fullWidth
                      component={Link}
                      to={"/lambai/" + value.test_code}
                      disabled={value.dudoanketqua === null ? false : true}
                    >
                      {value.dudoanketqua === null
                        ? "Làm bài thi"
                        : "Đã hoàn thành"}
                      <LamBaiIcon className={classes.rightIcon} />
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
            {/* =======================Bai thi tu luan ========================== */}
            {/* {isTeam === "1" && (
              <Grid key={essayData.idtuluan} item xs={12} sm={12} md="auto">
                <Card className={classes.card} elevation={2}>
                  <CardHeader
                    title="Bài thi tự luận"
                    titleTypographyProps={{ align: "center" }}
                    className={classes.cardHeader}
                  />
                  <Divider variant="fullWidth" />
                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Tình trạng: Đang mở
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Mã bài thi: {essayData.testCode}
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Tổng số câu hỏi: 01
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardContent>
                    <Typography
                      variant="body1"
                      align="center"
                      color="textPrimary"
                    >
                      Thời gian làm bài: đến khi kết thúc cuộc thi
                    </Typography>
                    <Divider variant="fullWidth" />
                  </CardContent>

                  <CardActions>
                    <Button
                      variant="contained"
                      color="primary"
                      fullWidth
                      component={Link} to={"/lambaituluan/"+essayData.testCode+"/"+essayData.idtuluan}
                    >
                      Làm bài thi
                      <LamBaiIcon className={classes.rightIcon} />
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
              )} */}
          </Grid>
        </Grid>
      </main>
      <Footer />
      {/* ==============Footer================ */}
    </div>
  );
}

//============================================= Styles =============================================//

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 0, 6),
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(1),
  },
  card: {
    minWidth: 310,
  },
  userCard: {
    minWidth: 300,
  },
  title: {
    fontSize: 14,
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  media: {
    background:
      "radial-gradient( circle 502px at 2.7% 23.7%,  rgba(142,5,254,1) 0%, rgba(33,250,214,1) 99.6% )",
    height: 210,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    color: "white",
    fontSize: 25,
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  bigAvatar: {
    margin: 15,
    width: 100,
    height: 100,
  },
}));
