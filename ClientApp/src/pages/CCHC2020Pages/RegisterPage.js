import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { Redirect } from "react-router-dom";
import LinearProgress from "@material-ui/core/LinearProgress";
import Snackbar from "@material-ui/core/Snackbar";
import ErrorIcon from "@material-ui/icons/Error";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import Banner from "../../assets/banner4.jpg";
import BannerLeft from "../../assets/bannerthi.jpg";
import LogoDang from "../../assets/logocchc2.png";
import HoaVan from "../../assets/hoavan.png";
import Divider from "@material-ui/core/Divider";
import FlipClock from "x-react-flipclock";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Link from "@material-ui/core/Link";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ReCAPTCHA from "react-google-recaptcha";
import Autocomplete from "@material-ui/lab/Autocomplete";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

const donvi = [
  { tendonvi: "Văn phòng HĐND và UBND thành phố", iddonvi: 1 },
  { tendonvi: "Phòng Nội vụ thành phố", iddonvi: 2 },
  { tendonvi: "Phòng Tư pháp thành phố", iddonvi: 3 },
  { tendonvi: "Phòng Kinh tế thành phố", iddonvi: 4 },
  { tendonvi: "Phòng Y tế thành phố", iddonvi: 1 },
  { tendonvi: "Phòng Tài chính - Kế hoạch thành phố", iddonvi: 2 },
  { tendonvi: "Phòng Giáo dục và Đào tạo thành phố", iddonvi: 3 },
  { tendonvi: "Phòng Tài nguyên và Môi trường thành phố", iddonvi: 4 },
  { tendonvi: "Phòng Y tế thành phố", iddonvi: 1 },
  { tendonvi: "Phòng Quản lý đô thị thành phố", iddonvi: 2 },
  { tendonvi: "Phòng Lao động - Thương binh và Xã hội thành phố", iddonvi: 3 },
  { tendonvi: "Thanh tra thành phố", iddonvi: 4 },
  { tendonvi: "Công an thành phố", iddonvi: 1 },
  { tendonvi: "Quân sự thành phố", iddonvi: 2 },
  {
    tendonvi: "Trung tâm Văn hóa, Thể thao và Truyền thanh thành phố",
    iddonvi: 3,
  },
  { tendonvi: "Trung tâm Phát triển Quỹ đất thành phố", iddonvi: 3 },
  { tendonvi: "Ban Quản lý dự án đầu tư xây dựng thành phố", iddonvi: 3 },
  {
    tendonvi: "Trung tâm Giáo dục nghề nghiệp-Giáo dục thường xuyên thành phố ",
    iddonvi: 3,
  },
  { tendonvi: "Ban Quản lý Chợ Phường 3", iddonvi: 3 },
  { tendonvi: "Ban Quản lý Chợ thành phố", iddonvi: 3 },
  { tendonvi: "Văn phòng Thành ủy", iddonvi: 3 },
  { tendonvi: "Ban Tuyên Giáo Thành ủy", iddonvi: 3 },
  { tendonvi: "Ban Tổ chức Thành ủy", iddonvi: 3 },
  { tendonvi: "Trung tâm Bồi dưỡng Chính trị thành phố", iddonvi: 3 },
  { tendonvi: "Ủy ban Kiểm tra Thành ủy", iddonvi: 3 },
  { tendonvi: "Ủy ban Mặt trận Tổ quốc Việt Nam thành phố", iddonvi: 3 },
  { tendonvi: "Liên đoàn Lao động thành phố", iddonvi: 3 },
  { tendonvi: "Hội Nông dân thành phố", iddonvi: 3 },
  { tendonvi: "Hội Cựu Chiến binh thành phố", iddonvi: 3 },
  { tendonvi: "Hội Liên hiệp Phụ nữ thành phố", iddonvi: 3 },
  { tendonvi: "Ban Dân vận Thành ủy", iddonvi: 3 },
  { tendonvi: "Đoàn Thanh niên Cộng sản Hồ Chí Minh thành phố", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Lê Văn Tám", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Trần  Quốc Toản", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Bùi Thị Xuân", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Kim Đồng", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Võ Thị Sáu", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Nguyễn Hiền", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Tôn Thất Tùng", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Nguyễn Du", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Trần Phú", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Duy Tân", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Võ Trường Toản", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Trương Định", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Hoàng Diệu", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Nguyễn Thị Minh Khai", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Nguyễn Thái Bình", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Nguyễn Khuyến", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học Lê Ngọc Hân", iddonvi: 3 },
  { tendonvi: "Trường Tiểu học La Văn Cầu", iddonvi: 3 },
  { tendonvi: "Trường Mẫu giáo Sơn Ca", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Thực Hành", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Thái Chánh", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Tuổi Ngọc", iddonvi: 3 },
  { tendonvi: "Trường Mầm non 1/6", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Hoa Sen", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Hiệp Ninh", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Hoa Cúc", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Vàng Anh", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Vành Khuyên", iddonvi: 3 },
  { tendonvi: "Trường Mẫu giáo Họa Mi", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Hướng Dương", iddonvi: 3 },
  { tendonvi: "Trường Mầm non Hoa Mai ", iddonvi: 3 },
  { tendonvi: "Trường THCS Phan Bội Châu", iddonvi: 3 },
  { tendonvi: "Trường THCS Trần Hưng Đạo", iddonvi: 3 },
  { tendonvi: "Trường THCS Chu Văn An", iddonvi: 3 },
  { tendonvi: "Trường THCS Võ Văn Kiệt", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Trãi", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Thái Học", iddonvi: 3 },
  { tendonvi: "Trường THCS Bà Đen", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Tri Phương", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Văn Linh", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Văn Trỗi", iddonvi: 3 },
  { tendonvi: "Trường THCS Nguyễn Viết Xuân", iddonvi: 3 },
  { tendonvi: "UBND Phường 1", iddonvi: 3 },
  { tendonvi: "UBND Phường 2", iddonvi: 3 },
  { tendonvi: "UBND Phường 3", iddonvi: 3 },
  { tendonvi: "UBND Phường IV", iddonvi: 3 },
  { tendonvi: "UBND Phường Hiệp Ninh", iddonvi: 3 },
  { tendonvi: "UBND Phường Ninh Thạnh", iddonvi: 3 },
  { tendonvi: "UBND Phường Ninh Sơn", iddonvi: 3 },
  { tendonvi: "UBND xã Bình Minh", iddonvi: 3 },
  { tendonvi: "UBND xã Tân Bình", iddonvi: 3 },
  { tendonvi: "UBND xã Thạnh Tân", iddonvi: 3 },
  { tendonvi: "Hội Chữ thập đỏ thành phố", iddonvi: 3 },
  { tendonvi: "Hội Đông y thành phố", iddonvi: 3 },
  { tendonvi: "Hội Khuyến học thành phố", iddonvi: 3 },
  { tendonvi: "Chi Cục thi hành án dân sự thành phố", iddonvi: 3 },
  { tendonvi: "Tòa án nhân dân thành phố", iddonvi: 3 },
  { tendonvi: "Viện kiểm sát nhân dân thành phố", iddonvi: 3 },
];
export default function RegisterPage() {
  const classes = useStyles();
  const recaptchaRef = React.createRef();
  const [captchaToken, setCaptchaToken] = useState(null);

  //================== States=========================//
  const [name, setName] = useState(null);
  const [cmnd, setCmnd] = useState(null);
  const [address, setAddress] = useState(null);
  const [email, setEmail] = useState(null);
  const [sbd, setSbd] = useState("");
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [passwordOTP, setPasswordOTP] = useState(null);
  const [logined, setLogined] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errMessOpen, setErrMessOpen] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [openTheLeDialog, setOpenTheleDialog] = useState(false);
  const [openThongKeDialog, setOpenThongKeDialog] = useState(false);
  const [openThongKeDiemDialog, setOpenThongKeDiemDialog] = useState(false);
  const [passworDialog, setOpenPassworDialog] = useState(false);
  const [loginDialog, setOpenLoginDialog] = useState(false);
  const [messError, setMessError] = useState("Kiểm tra lại thông tin");
  const [thongKe, setThongKe] = useState([]);
  const [thongKeDiem, setThongKeDiem] = useState([]);
  const [tong, setTong] = useState(0);

  //================== Effect =========================//
  useEffect(() => {
    if (localStorage.getItem("user")) {
      setLogined(true);
    }
  }, [logined]);

  function onChange(value) {
    console.log(value);
    setCaptchaToken(value);
  }
  //================== Đăng ký Submit =========================//
  async function handleFormSubmit(e) {
    const recaptchaValue = recaptchaRef.current.getValue();
    // console.log(recaptchaValue);
    if (recaptchaValue === "" || recaptchaValue != captchaToken) {
      setErrMessOpen(true);
    } else {
      e.preventDefault();
      setLoading(true);
      try {
        await axios
          .post("/UserLogin/DangKyThi", {
            name: name,
            cmnd: cmnd,
            phone: phoneNumber,
            donvi: address,
            email: email,
            sbd: sbd,
            token: recaptchaValue,
          })
          .then(function (response) {
            console.log("========================== " + response.data);
            // nếu đúng otp
            if (response.data.error === 200) {
              handleOTPSubmit(response.data.message.toString());
              setLoading(false);
              setOpenDialog(false);
              setOpenPassworDialog(false);
            }
          });
      } catch (error) {
        if (error.response.data.error === 104) {
          setLoading(false);
          setMessError(error.response.data.message);
          setErrMessOpen(true);
        }
        if (error.response.data.error === 110) {
          // yêu cầu nhập otp
          setLoading(false);
          setMessError(error.response.data.message);
          setErrMessOpen(true);
          setOpenDialog(false);
          setOpenPassworDialog(true);
        }
        if (error.response.data.error === 109) {
          // sai OTP
          setLoading(false);
          setMessError(error.response.data.message);
          setErrMessOpen(true);
          setOpenDialog(false);
          setOpenPassworDialog(true);
        } else {
          setMessError(error.response.data.message);
          setLoading(false);
          setErrMessOpen(true);
        }
      }
    }
  }

  //================== OTP Submit =========================//
  async function handleOTPSubmit(password) {
    setLoading(true);
    try {
      await axios
        .post("/UserLogin/AuthenticateCCHC", {
          phone: phoneNumber,
          password: password,
        })
        .then(function (response) {
          if (response.data.error === 200) {
            localStorage.setItem("user", JSON.stringify(response.data.data));
            axios
              .post(
                "/api/CaiCachHanhChinh/TaoDeThi",
                {
                  phone: response.data.data.phone.toString(),
                },
                {
                  headers: {
                    Authorization: "Bearer " + response.data.data.token,
                  },
                }
              )
              .then(function (response) {
                if (response.data.error === 200) {
                  setLoading(false);
                  setOpenPassworDialog(false);
                  setLogined(true);
                }
              })
              .catch((error) => {
                console.log(error.response.status);
                setLoading(false);
              });
          }
        });
    } catch (error) {
      setMessError(error.response.data.message);
      setLoading(false);
      setErrMessOpen(true);
    }
  }

  //================== Thong ke luot thi=========================//
  async function getThongKeLuotThi() {
    let total = 0;
    setLoading(true);
    try {
      await axios.get("/UserLogin/danhsachdonvithi").then(function (response) {
        if (response.data.error === 200) {
          response.data.data.forEach((element) => {
            total += element.soluong;
          });
          // console.log(total);
          setTong(total);
          setThongKe(response.data.data);
          setOpenThongKeDialog(true);
          setLoading(false);
        }
      });
    } catch (error) {
      setMessError(error.response.data.message);
      setLoading(false);
      setErrMessOpen(true);
    }
  }

  async function getThongKeDiem() {
    setLoading(true);
    try {
      await axios.get("/UserLogin/ThongKeTheoDiem").then(function (response) {
        if (response.data.error === 200) {
          setThongKeDiem(response.data.data);
          setOpenThongKeDiemDialog(true);
          setLoading(false);
        }
      });
    } catch (error) {
      setMessError(error.response.data.message);
      setLoading(false);
      setErrMessOpen(true);
    }
  }

  //================== Set state thông báo lỗi =========================//
  function handleErrMessOpen() {
    setErrMessOpen(false);
  }

  function handleCloseDialog() {
    setOpenDialog(false);
  }
  function handleCloseTheLeDialog() {
    setOpenTheleDialog(false);
  }
  function handleCloseThongKeDialog() {
    setOpenThongKeDialog(false);
  }
  function handleCloseThongKeDiemDialog() {
    setOpenThongKeDiemDialog(false);
  }

  function handleOpenDialog() {
    setOpenDialog(true);
  }

  function handleClosePassworDialog() {
    setOpenPassworDialog(false);
  }
  function handleCloseLoginDialog() {
    setOpenLoginDialog(false);
  }

  //==============================Condition Render=============================================//
  if (logined) {
    return <Redirect to={{ pathname: "/" }} />;
  } else
    return (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />

        {/* ================= Dang Ky Form================= */}
        <Grid
          item
          xs={12}
          sm={8}
          md={4}
          lg={4}
          xl={3}
          component={Paper}
          elevation={12}
          square
          className={classes.rightSide}
        >
          {loading ? <LinearIndeterminate /> : null} {/* //Render indicator */}
          <ErrorSnackBar
            errMessOpen={errMessOpen}
            handleErrMessOpen={handleErrMessOpen}
            messError={messError}
          />{" "}
          {/* //Render thông báo lỗi */}
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <img src={LogoDang} className={classes.logo} />
            </Avatar>

            <Typography
              component="h1"
              variant="h5"
              className={classes.timeRemain}
            >
              <b>CUỘC THI</b>
            </Typography>

            <Typography
              component="h1"
              variant="h3"
              className={classes.testName}
              align="center"
            >
              <b>TÌM HIỂU CẢI CÁCH HÀNH CHÍNH</b>
            </Typography>

            <Typography
              component="h1"
              variant="h5"
              className={classes.timeRemain2}
            >
              <b>NĂM 2021</b>
            </Typography>
            <FlipClock
              type="countdown"
              count_to="2021-11-10 17:00:00"
              units={[
                {
                  sep: "",
                  type: "days",
                  title: "Ngày",
                },
                {
                  sep: "-",
                  type: "hours",
                  title: "Giờ",
                },
                {
                  sep: ":",
                  type: "minutes",
                  title: "Phút",
                },
                {
                  sep: ":",
                  type: "seconds",
                  title: "Giây",
                },
              ]}
            />
            <Button
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
              onClick={() => setOpenDialog(true)}
            >
              Đăng ký
            </Button>

            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => setOpenTheleDialog(true)}
            >
              Thể lệ
            </Button>

            {/* <Button
              fullWidth
              variant="contained"
              color="primary"
              style={{ backgroundColor: "#D0BD0F" }}
              className={classes.submit}
              href="https://drive.google.com/drive/folders/1Pf266wOcD7n7XbdNQEBS--djedHaJdMQ?usp=sharing"
              target="_blank"
            >
              Tải lên Clip tuyên truyền
            </Button> */}
            {/* <ButtonGroup fullWidth>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              style={{ backgroundColor: "#1EA061", fontSize:12}}
              onClick={() => getThongKeLuotThi()}
            >
              Thống kê đơn vị
            </Button>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              style={{ backgroundColor: "#FD8140",fontSize:12 }}
              onClick={() => getThongKeDiem()}
            >
              Thống kê điểm
            </Button>
           </ButtonGroup> */}

            {/* <Grid container justify="flex-end">
              <Grid item>
                <Link
                  onClick={() => setOpenLoginDialog(true)}
                  variant="body2"
                  style={{ color: "white" }}
                >
                  Bạn đã đăng ký tài khoản ? Đăng nhập
                </Link>
              </Grid>
            </Grid> */}
          </div>
        </Grid>

        {/* ======================================= Dialog  dang ky thong tin =================================== */}

        <Dialog open={openDialog} onClose={handleCloseDialog} maxWidth="sm">
          <DialogTitle
            id="form-dialog-title"
            align="center"
            onClose={handleCloseDialog}
          >
            ĐĂNG KÝ THÔNG TIN DỰ THI
          </DialogTitle>
          <DialogContent className={classes.formStyle}>
            {/* <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdtBQcctHjURSj1FUWcfoU-CluoEfff3xhkjPfeoVMVoSBgdw/viewform?embedded=true" width="720" height="1000" frameborder="0" marginheight="0" marginwidth="0">Đang tải...</iframe> */}
            <TextField
              variant="outlined"
              margin="normal"
              required
              label="Họ và tên"
              name={name}
              onChange={(input) => setName(input.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              label="Năm sinh"
              type="number"
              name={cmnd}
              // onChange={(input) => setPhoneNumber(input.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              label="Số điện thoại"
              type="number"
              name={phoneNumber}
              onChange={(input) => setPhoneNumber(input.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              label="CMND/CCCD"
              type="number"
              name={cmnd}
              onChange={(input) => setCmnd(input.target.value)}
            />
            {/* 
            <TextField
              variant="outlined"
              margin="normal"
              required
              select
              fullWidth
              label="Cơ quan, đơn vị"
              name={address}
              onChange={(input) => setAddress(input.target.value)}
            >
              <MenuItem value={"VP Đoàn ĐBQH, HĐND và UBND tỉnh"}>
                VP Đoàn ĐBQH, HĐND và UBND tỉnh
              </MenuItem>
            </TextField> */}

            <Autocomplete
              options={donvi}
              getOptionLabel={(option) => option.tendonvi}
              // style={{ width: "100%" }}
              name={address}
              onChange={(e, value) => setAddress(value.tendonvi)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  margin="normal"
                  required
                  style={{ width: "97%" }}
                  label="Cơ quan, đơn vị"
                  // name={address}
                  // onChange={(input) => console.log(input.target.value)}
                />
              )}
            />
            <TextField
              margin="normal"
              required
              select
              label="Nơi ở hiện tại"
              style={{ width: "97%" }}
              // value={idXaPhuong}
              onChange={(input) => setEmail(input.target.value)}
              variant="outlined"
            >
              <MenuItem value={"1"}>Phường 1</MenuItem>
              <MenuItem value={"2"}>Phường 2</MenuItem>
              <MenuItem value={"3"}>Phường 3</MenuItem>
              <MenuItem value={"4"}>Phường 4</MenuItem>
              <MenuItem value={"5"}>Phường Hiệp Ninh</MenuItem>
              <MenuItem value={"6"}>Phường Ninh Thạnh</MenuItem>
              <MenuItem value={"7"}>Phường Ninh Sơn</MenuItem>
              <MenuItem value={"8"}>Xã Bình Minh</MenuItem>
              <MenuItem value={"9"}>Xã Tân Bình</MenuItem>
              <MenuItem value={"10"}>Xã Thạnh Tân</MenuItem>
            </TextField>

            {/* <FormControl
              variant="outlined"
              margin="normal"
              required
              fullWidth
              // className={classes.formControl}
            >
              <InputLabel>Nơi ở</InputLabel>
              <Select
                value={email}
                onChange={(input) => setEmail(input.target.value)}
              >
                <MenuItem value={"1"}>
                Cán bộ - Công chức
                </MenuItem>
                <MenuItem value={"2"}>Viên chức - Người lao động</MenuItem>
              </Select>
            </FormControl> */}

            {/* <FormControl
              variant="outlined"
              margin="normal"
              required
              fullWidth
              // className={classes.formControl}
            >
              <InputLabel>Đơn vị</InputLabel>
              <Select
                value={address}
                onChange={(input) => setAddress(input.target.value)}
              >
                <MenuItem value={"VP Đoàn ĐBQH, HĐND và UBND tỉnh"}>
                  VP Đoàn ĐBQH, HĐND và UBND tỉnh
                </MenuItem>
                <MenuItem value={"Sở Công Thương"}>Sở Công Thương</MenuItem>
                <MenuItem value={"Sở Giáo dục và Đào tạo"}>
                  Sở Giáo dục và Đào tạo
                </MenuItem>
                <MenuItem value={"Sở Giao thông Vận tải"}>
                  Sở Giao thông Vận tải
                </MenuItem>
                <MenuItem value={"Sở Kế hoạch và Đầu tư"}>
                  Sở Kế hoạch và Đầu tư
                </MenuItem>
                <MenuItem value={"Sở Khoa học và Công nghệ"}>
                  Sở Khoa học và Công nghệ
                </MenuItem>
                <MenuItem value={"Sở Lao động - Thương binh và Xã hội"}>
                  Sở Lao động - Thương binh và Xã hội
                </MenuItem>
                <MenuItem value={"Sở Ngoại vụ"}>Sở Ngoại vụ</MenuItem>

                <MenuItem value={"Sở Nội vụ"}>Sở Nội vụ</MenuItem>
                <MenuItem value={"Sở Nông nghiệp và PTNT"}>
                  Sở Nông nghiệp và PTNT
                </MenuItem>
                <MenuItem value={"Sở Tài chính"}>Sở Tài chính</MenuItem>
                <MenuItem value={"Sở Tài nguyên và Môi trường"}>
                  Sở Tài nguyên và Môi trường
                </MenuItem>
                <MenuItem value={"Sở Thông tin và Truyền thông"}>
                  Sở Thông tin và Truyền thông
                </MenuItem>
                <MenuItem value={"Sở Tư pháp"}>Sở Tư pháp</MenuItem>
                <MenuItem value={"Sở Văn hóa - Thể thao - Du lịch"}>
                  Sở Văn hóa - Thể thao - Du lịch
                </MenuItem>
                <MenuItem value={"Sở Xây dựng"}>Sở Xây dựng</MenuItem>
                <MenuItem value={"Sở Y tế"}>Sở Y tế</MenuItem>
                <MenuItem value={"Thanh Tra Tỉnh"}>Thanh Tra Tỉnh</MenuItem>
                <MenuItem value={"Ban Quản lý khu kinh tế"}>Ban Quản lý khu kinh tế</MenuItem>
                <MenuItem value={"Huyện Châu Thành"}>Huyện Châu Thành</MenuItem>
                <MenuItem value={"Huyện Bến Cầu"}>Huyện Bến Cầu</MenuItem>
                <MenuItem value={"Huyện Dương Minh Châu"}>
                  Huyện Dương Minh Châu
                </MenuItem>
                <MenuItem value={"Huyện Gò Dầu"}>Huyện Gò Dầu</MenuItem>
                <MenuItem value={"Huyện Tân Biên"}>Huyện Tân Biên</MenuItem>
                <MenuItem value={"Huyện Tân Châu"}>Huyện Tân Châu</MenuItem>
                <MenuItem value={"Thành Phố Tây Ninh"}>
                  Thành Phố Tây Ninh
                </MenuItem>
                <MenuItem value={"Thị xã Hòa Thành"}>Thị xã Hòa Thành</MenuItem>
                <MenuItem value={"Thị xã Trảng Bàng"}>
                  Thị xã Trảng Bàng
                </MenuItem>
              </Select>
            </FormControl> */}
            <DialogContentText>
              Lưu ý: Các trường có dấu * là bắt buộc. Người dự thi điền đầy đủ,
              chính xác thông tin. Ban Tổ chức Hội thi không công nhận kết quả
              đối với người dự thi có thông tin đăng ký không chính xác.
            </DialogContentText>
            <ReCAPTCHA
              ref={recaptchaRef}
              sitekey="6LcFXawZAAAAAIW5D9--JfFT0bz2ytQwLrQO0-Sa"
              onChange={onChange}
              language="vi-vn"
              hl="vi"
              style={{
                paddingTop: 10,
                display: "flex",
                justifyContent: "center",
              }}
            />
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleFormSubmit}
              color="secondary"
              variant="contained"
            >
              Vào thi
            </Button>
          </DialogActions>
        </Dialog>

        {/* ======================================= Dialog Password ================================== */}

        <Dialog
          open={passworDialog}
          onClose={handleClosePassworDialog}
          maxWidth="sm"
          disableBackdropClick={true}
          disableEscapeKeyDown={true}
        >
          <DialogTitle
            id="form-dialog-title"
            align="center"
            onClose={handleClosePassworDialog}
          >
            Nhập mật khẩu OTP
          </DialogTitle>
          <DialogContent>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Mật khẩu"
              type="number"
              name={sbd}
              onChange={(input) => setSbd(input.target.value)}
            />
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleFormSubmit}
              color="secondary"
              variant="contained"
            >
              Vào thi
            </Button>
          </DialogActions>
        </Dialog>

        {/* ======================================= Dang nhap Password ================================== */}

        <Dialog
          open={loginDialog}
          onClose={handleCloseLoginDialog}
          maxWidth="sm"
        >
          <DialogTitle
            id="form-dialog-title"
            align="center"
            onClose={handleCloseLoginDialog}
          >
            Đăng nhập
          </DialogTitle>
          <DialogContent>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Số điện thoại"
              type="number"
              name={phoneNumber}
              onChange={(input) => setPhoneNumber(input.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Mật khẩu"
              type="number"
              name={passwordOTP}
              onChange={(input) => setPasswordOTP(input.target.value)}
            />
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleOTPSubmit}
              color="secondary"
              variant="contained"
            >
              Vào thi
            </Button>
          </DialogActions>
        </Dialog>

        {/* ======================================= Dialog  The Le =================================== */}

        <Dialog
          open={openTheLeDialog}
          onClose={handleCloseTheLeDialog}
          maxWidth="md"
        >
          <DialogTitle
            id="form-dialog-title"
            align="center"
            style={{ background: "#EFE3AF" }}
          >
            THỂ LỆ HỘI THI
            <div className={classes.hoavan}>
              <img src={HoaVan} className={classes.logo} />
            </div>
          </DialogTitle>
          <DialogContent style={{ background: "#EFE3AF" }}>
            <p>
              Căn cứ Kế hoạch số 150/KH-UBND ngày 29/9/2021 của UBND thành phố
              về việc tổ chức Cuộc thi trực tuyến tìm hiểu về cải cách hành
              chính, sáng tạo Biểu trưng (Logo) cải cách hành chính thành phố
              Tây Ninh năm 2021. <strong></strong>
            </p>
            <p>Ban Tổ chức cuộc thi ban hành Thể lệ cuộc thi như sau:</p>
            <p>
              <a name="bookmark3"></a>
              <a name="bookmark16"></a>
              <strong>I. </strong>
              <a name="bookmark31"></a>
              <a name="bookmark32"></a>
              <a name="bookmark34"></a>
              <a name="bookmark33"></a>
              <strong>Đối tượng dự thi</strong>
            </p>
            <p>
              Đối tượng dự thi là cán bộ, công chức, viên chức, người lao động
              làm việc tại các cơ quan hành chính nhà nước, đơn vị sự nghiệp,
              UBND phường, xã và người dân trên địa bàn thành phố, cụ thể như
              sau:
            </p>
            <p>
              - Đối với các cơ quan, đơn vị trực thuộc UBND thành phố Tây Ninh
              và UBND các phường, xã bắt buộc tham gia đầy đủ 02 nội dung cuộc
              thi.
            </p>
            <p>
              - Đối với người dân trên địa bàn thành phố Tây Ninh và các cơ quan
              trú đóng trên địa bàn thành phố Tây Ninh dự thi 01 trong 02 nội
              dung hoặc dự thi cả 02 nội dung{" "}
              <em>(không mang tính bắt buộc).</em>
              <em></em>
            </p>
            <p>Chia thành 02 Khối thi như sau:</p>
            <p>
              - Khối<em> </em>1: gồm các cơ quan Đảng, tổ chức chính trị-xã hội
              thành phố, cơ quan chuyên môn, các đơn vị sự nghiệp trực thuộc
              UBND thành phố Tây Ninh.
            </p>
            <p>
              - Khối 2:<em> </em>gồm<em> </em>các phường, xã thuộc thành phố và
              các các cơ quan, đơn vị trú đóng trên địa bàn thành phố{" "}
              <em>(kể cả đơn vị ngoài nhà nước).</em>
            </p>
            <p>
              Đối tượng không được tham gia cuộc thi: thành viên Ban Tổ chức, Tổ
              Thư ký, Tổ ra đề thi, Tổ thẩm định đề thi, cán bộ phụ trách kỹ
              thuật của hệ thống phần mềm thi trắc nghiệm.
            </p>
            <p>
              <strong>II. Nội dung thi</strong>
            </p>
            <p>
              <strong>1. Thi trực tuyến tìm hiểu cải cách hành chính</strong>
            </p>
            <p>
              Công tác chỉ đạo, điều hành cải cách hành chính tại các cơ quan,
              đơn vị, địa phương.
            </p>
            <p>
              <a name="bookmark28"></a>
              Xây dựng, rà soát văn bản quy phạm pháp luật; thi hành, phổ biến
              giáo dục pháp luật.
            </p>
            <p>
              <a name="bookmark29"></a>
              Công tác kiểm soát thủ tục hành chính, hoạt động của Bộ phận một
              cửa 02 cấp.
            </p>
            <p>
              Sắp xếp, đổi mới tổ chức bộ máy trong các cơ quan nhà nước để đảm
              bảo vận hành đồng bộ, thông suốt, chất lượng và hiệu quả.
            </p>
            <p>
              Công tác tuyển dụng, sử dụng, đào tạo, bồi dưỡng cán bộ, công
              chức, viên chức trong các cơ quan nhà nước.
            </p>
            <p>
              Quản lý và sử dụng tài sản công, tiết kiệm chi phí hành chính
              trong các cơ quan nhà nước, thực hiện tự chủ tài chính các đơn vị
              sự nghiệp công lập.
            </p>
            <p>
              Một số nội dung về cải cách thủ tục hành chính và thực hiện dịch
              vụ công trực tuyến trên Cổng Dịch vụ công tỉnh Tây Ninh (
              <a href="http://dichvucong.tayninh.gov.vn/">
                http://dichvucong.tayninh.gov.vn
              </a>
              ) và Cổng Dịch vụ công quốc gia ({" "}
              <a href="http://dichvucong.gov.vn/">http://dichvucong.gov.vn</a>).
            </p>
            <p>
              Các nội dung hiện đại hóa hành chính, xây dựng Chính phủ điện tử
              theo Nghị quyết số 17/NQ-CP ngày 17/3/2019 của Chính phủ về một số
              nhiệm vụ, giải pháp trọng tâm phát triển Chính phủ điện tử giai
              đoạn 2019 - 2020, định hướng đến 2025; triển khai Chương trình
              chuyển đổi số quốc gia đến năm 2025, định hướng đến năm 2030 được
              ban hành tại Quyết định số 749/QĐ-TTg ngày 03/6/2020 của Thủ tướng
              Chính phủ.
            </p>
            <p>
              <strong>
                2. Thiết kế Biểu trưng (Logo) cải cách hành chính thành phố Tây
                Ninh
              </strong>
              phải sáng tạo, có tính khái quát, tính thẩm mỹ cao, thể hiện được
              ý nghĩa, đặc trưng của thành phố Tây Ninh.
              <a name="bookmark18"></a> Logo ấn tượng, đơn giản, dễ hiểu, dễ
              nhận biết, dễ ghi nhớ. <a name="bookmark19"></a>
            </p>
            <p>
              Thuận tiện cho việc in ấn, phóng to, thu nhỏ, đắp nổi, gia công
              được trên mọi chất liệu, thuận tiện cho việc tuyên truyền và đảm
              bảo sử dụng lâu dài.
            </p>
            <p>
              Đối tượng dự thi: các tổ chức, cá nhân có khả năng thiết kế biểu
              tượng trong và ngoài tỉnh Tây Ninh.
            </p>
            <p>
              <strong>
                III. Hình thức, thời gian tổ chức và cách tính kết quả
              </strong>
            </p>
            <p>
              <strong>1.</strong>
              <strong> Hình thức thi </strong>
              <strong>
                <em></em>
              </strong>
            </p>
            <p>
              <strong>a)</strong>
              <strong> </strong>
              <strong>Thi tìm hiểu cải cách hành chính </strong>
              <strong>
                được tổ chức dưới hình thức trắc nghiệm trực tuyến
              </strong>
            </p>
            <p>
              - Cuộc thi được tổ chức theo hình thức thi trắc nghiệm online gồm
              20 câu hỏi; bắt đầu từ khi phát động cuộc thi vào lúc{" "}
              <strong>7 giờ 30 phút ngày 10 tháng 10 năm 2021 </strong>và kết
              thúc vào lúc <strong>17 giờ, ngày 10 tháng 11 năm 2021</strong>.
            </p>
            <p>
              - Nội dung câu hỏi thi trắc nghiệm online được công bố tại
              <a name="_Hlk80801153">
                Cổng thông tin điện tử thành phố Tây Ninh địa chỉ:
              </a>
              <a href="http://thanhpho.tayninh.gov.vn/">
                http://thanhpho.tayninh.gov.vn
              </a>
              .
            </p>
            <p>
              - Thi trắc nghiệm online dưới dạng biểu mẫu điện tử (đường link)
              được đăng và chuyển tải trên môi trường mạng Internet (qua mạng xã
              hội Facbook, Zalo).
            </p>
            <p>
              <strong>
                <em>
                  * Người dự thi sử dụng một trong hai hình thức sau để tham gia
                  dự thi:
                </em>
              </strong>
            </p>
            <p>
              (1)<strong> </strong>Thi trắc nghiệm online trên Cổng thông tin
              điện tử thành phố Tây Ninh tại địa chỉ
              <a href="http://thanhpho.tayninh.gov.vn/">
                http://thanhpho.tayninh.gov.vn
              </a>
              :
            </p>
            <p>
              - Bấm vào ô{" "}
              <u>
                “<strong>Vào thi”</strong>
              </u>
              .
            </p>
            <p>
              - Điền các thông tin cá nhân: Họ và tên; Năm sinh; Cơ quan, đơn
              vị; Nơi ở; Số điện thoại; Số CMND/CCCD.
            </p>
            <p>
              - Bấm vào ô kiểm tra{" "}
              <strong>
                <u>“Tôi không phải là người máy”</u>
              </strong>
              và bấm vào ô{" "}
              <strong>
                <u>“Bắt đầu thi”</u>
              </strong>
              .
            </p>
            <p>- Trả lời 20 câu hỏi trắc nghiệm.</p>
            <p>- Dự đoán số lượt trả lời đúng.</p>
            <p>
              - Bấm vào ô{" "}
              <strong>
                <u>“Hoàn thành”</u>
              </strong>
              .
            </p>
            <p>- Thời gian cho một lượt thi là 15 phút.</p>
            <p>
              (2)<strong> </strong>Thi trắc nghiệm online dưới dạng biểu mẫu
              điện tử (đường link) được đăng và chuyển tải trên môi trường mạng
              Internet (qua mạng xã hội Facbook, Zalo):
            </p>
            <p>
              Người tham gia dự thi click vào đường link được Ban Tổ chức Cuộc
              thi gửi qua mạng xã hội Facbook, Zalo để trả lời các câu hỏi
              (trình tự thực hiện như thi trắc nghiệm online qua Cổng thông tin
              điện tử), đồng thời đề nghị gửi đường link đó đến các nhóm, bạn
              bè, người thân, đồng nghiệp để mời tham gia cuộc thi.
            </p>
            <p>
              <strong>b) Thi thiết kế Biểu trưng (Logo) thành phố</strong>
              <strong> Tây Ninh</strong>
              <strong>: </strong>
              <strong>
                Tác giả gửi tác phẩm dự thi có kèm theo bản thuyết minh ý tưởng
                thiết kế Logo
              </strong>
            </p>
            <p>
              <strong>* Ý tưởng thể hiện</strong>
              <strong></strong>
            </p>
            <p>
              <a name="bookmark53"></a>
              Thể hiện trên khổ giấy A4 (29,7cm x 21cm). Mẫu biểu tượng phóng to
              đặt chính giữa tờ giấy khổ A4, kích thước không quá 15cm x 15cm.
              Phía dưới, bên phải và trái của mẫu biểu tượng phóng to, là 02 mẫu
              biểu tượng thu nhỏ (đen trắng và màu) kích thước không quá 3cm x
              3cm. Phía dưới bên góc trái tờ giấy là thông số màu của biểu
              tượng, phía dưới bên góc phải tờ giấy là phần mã số của Ban Giám
              khảo (theo Mẫu 2). Tác phẩm dự thi ngoài việc thể hiện trên giấy
              A4, cần sao chép file mềm vào VCD hoặc USB.
            </p>
            <p>
              <a name="bookmark54"></a>
              Chữ thể hiện trên biểu tượng (logo): “Thành phố Tây Ninh”.
            </p>
            <p>
              <a name="bookmark55"></a>
              Mỗi mẫu biểu tượng phải có bản thuyết minh kèm theo, không quá 02
              trang giấy A4 (theo Mẫu 3).
            </p>
            <p>
              <a name="bookmark56"></a>
              <em>Màu sắc:</em>
              Tối đa 03 (ba) màu (không kể màu trắng).
            </p>
            <p>
              <a name="bookmark57"></a>
              <em>Bố cục:</em>
              Chặt chẽ, hài hòa giữa màu sắc và hình khối (phù hợp với thể loại
              biểu tượng).
            </p>
            <p>
              <a name="bookmark58"></a>
              <em>Số lượng tác phẩm dự thi:</em>
              Không giới hạn.
            </p>
            <p>
              <a name="bookmark65"></a>
              <a name="bookmark66"></a>
              <a name="bookmark68"></a>
              <a name="bookmark67"></a>
              <strong>* Hồ sơ dự thi</strong>
              <strong></strong>
            </p>
            <p>
              <a name="bookmark69"></a>- 01 Phiếu đăng ký dự thi (theo Mẫu 1).
            </p>
            <p>
              <a name="bookmark70"></a>- 01 bản Mẫu thiết kế biểu tượng (logo)
              trên khổ giấy A4 (theo Mẫu 2).
            </p>
            <p>
              <a name="bookmark71"></a>- Bản thuyết minh nội dung, ý nghĩa tác
              phẩm dự thi (mỗi tác phẩm một bản thuyết minh) (theo Mẫu 3).
            </p>
            <p>
              <a name="bookmark72"></a>- 01 đĩa VCD hoặc USB chứa tất cả bản gốc
              thiết kế biểu tượng (logo) ở định dạng PDF, JPJ, CDR, bản thuyết
              minh ý tưởng ở định dạng doc hoặc docx.
            </p>
            <p>
              <a name="bookmark73"></a>- 01 bì thư có dán sẵn tem (ghi họ, tên,
              địa chỉ nơi cư trú hoặc đơn vị công tác).
            </p>
            <p>
              Tất cả thành phần hồ sơ dự thi nêu trên được đóng kín trong 01
              phong bì khổ lớn (kích thước 32cm x 42cm).
            </p>
            <p>
              <a name="bookmark74"></a>
              <a name="bookmark75"></a>
              <a name="bookmark77"></a>
              <a name="bookmark76"></a>
              <strong>* Nơi nhận hồ sơ dự thi</strong>
              <strong>:</strong>
              Hồ sơ dự thi nộp trực tiếp tại Cơ quan thường trực Ban Tổ chức
              hoặc theo đường Bưu điện. Bên ngoài ghi rõ thông tin nơi nhận. Địa
              chỉ: Số 82, đường Phạm Tung, Khu phố 1, Phường 3, thành phố Tây
              Ninh
              <strong>
                <em>
                  (Phòng Nội vụ thành phố trong Khu Hành chính thành phố Tây
                  Ninh)
                </em>
              </strong>
              hoặc liên hệ số điện thoại để được hướng dẫn trực tiếp.
            </p>
            <p>
              * Công chức phụ trách: Ông<strong> Nguyễn Quốc Ngân</strong>,
              chuyên viên Phòng Nội vụ thành phố Tây Ninh. Điện thoại:
              0276.3825250 - 0978972693.
            </p>
            <p>2. Thời gian</p>
            <p>
              a) Thời gian thi trực tuyến và nhận tác phẩm dự thi sáng tạo biểu
              trưng (Logo) thành phố Tây Ninh
            </p>
            <p>
              Bắt đầu từ{" "}
              <strong>
                <em>ngày 15 t</em>
              </strong>
              <strong>
                <em>háng </em>
              </strong>
              <strong>
                <em>10 năm </em>
              </strong>
              <strong>
                <em>2021</em>
              </strong>{" "}
              và kết thúc
              <strong>
                <em>ngày 15 tháng</em>
              </strong>{" "}
              <strong>
                <em>11</em>
              </strong>{" "}
              <strong>
                <em> năm</em>
              </strong>{" "}
              <strong>
                <em>2021</em>
              </strong>
              .
            </p>
            <p>
              b) Thời gian tổng kết, trao giải: dự kiến trong tháng 11/2021.
            </p>
            <p>3. Phương thức xét chọn tác phẩm dự thi</p>
            <p>Hội đồng thẩm định xét chọn theo 2 vòng, cụ thể:</p>
            <p>
              <a name="bookmark89"></a>
              a) Vòng sơ khảo: Ban Tổ chức Cuộc thi sao chụp, trình chiếu qua
              phần mềm Point các tác phẩm dự thi kèm theo bản thuyết minh. Hội
              đồng chấm và chọn ra 10 tác phẩm vào vòng chung khảo.
            </p>
            <p>
              <a name="bookmark90"></a>
              b) Vòng chung khảo: Ban Tổ chức Cuộc thi sao chụp, trình chiếu qua
              phần mềm Point các tác phẩm đã được chọn vào vòng chung khảo. Hội
              đồng chấm và chọn ra 01 tác phẩm sử dụng làm biểu tượng của thành
              phố Tây Ninh để trao giải Cuộc thi theo cơ cấu giải thưởng.
            </p>
            <p>
              <a name="bookmark91"></a>
              <a name="bookmark92"></a>
              <a name="bookmark94"></a>
              <a name="bookmark93"></a>
              4. Cơ cấu giải thưởng điều chỉnh
              <strong>
                <em> </em>
              </strong>
              thành{" "}
              <strong>
                Giải thưởng sáng tạo Biểu trưng (Logo) thành phố Tây Ninh{" "}
              </strong>
              dành cho cá nhân, tập thể có Biểu trưng (Logo) được UBND thành phố
              chọn sử dụng làm biểu tượng của thành phố Tây Ninh.
            </p>
            <p>
              01 giải:
              <em>
                tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>50.000.000 đồng</strong> (Năm mươi triệu đồng).
              </em>
            </p>
            <p>
              <strong>3. Cách tính kết quả</strong>
            </p>
            <p>
              <em>3.1.</em>
              <em> </em>
              <em>
                Thi tìm hiểu cải cách hành chính<strong> </strong>
              </em>
              <em>được tổ chức dưới hình thức trắc nghiệm trực tuyến</em>
            </p>
            <p>
              Người dự thi không giới hạn số lần tham gia
              <strong>
                <em>trả lời 20 câu hỏi trắc nghiệm trong thời gian 15 phút</em>
              </strong>
              và sẽ lấy kết quả của lần tham gia có số dự đoán đúng hoặc gần
              đúng nhất so với kết quả do Ban Tổ chức Cuộc thi công bố.
            </p>
            <p>
              <strong>
                <em>a) Đối với giải tập thể</em>
              </strong>
              <em>
                chia ra làm 02 khối: Khối (1) gồm các cơ quan Đảng, tổ chức
                chính trị-xã hội thành phố, cơ quan chuyên môn thành phố. Khối
                (2) gồm các phường, xã thuộc thành phố và các các cơ quan, đơn
                vị đóng trên địa bàn thành phố (kể cả đơn vị ngoài nhà nước).
                Mỗi khối được xét khen thưởng 03 giải tập thể.
              </em>
            </p>
            <p>
              - Cá nhân đăng ký thi cho cơ quan, đơn vị, tổ chức, địa phường nào
              thì sẽ tính số lượt thi tham gia cho cơ quan, đơn vị đó trên cơ sở
              thống kê theo bảng thông tin điện tử mà người dự thi đã đăng ký
              trước khi tham gia thi.
            </p>
            <p>
              - Kết quả được tính cho tập thể có bình quân số lượt người tham
              gia thi nhiều nhất, có đáp án đúng nhất và gửi về Ban Tổ chức Cuộc
              thi người với thời gian sớm nhất (thời gian thi được tính ngay khi
              người dự thi hoàn thành thành công bài thi online), tính từ cao
              xuống thấp.
            </p>
            <p>
              * Cách tính như sau: Bình quân số lượt người tham gia thi = Tổng
              số lượt các cá nhân tham gia thi cho tập thể chia cho tổng số biên
              chế được giao của đơn vị <em>(cơ quan, đơn vị)</em>.
            </p>
            <p>
              <strong>Ví dụ:</strong>
              Phòng Tư pháp thành phố có 100 lượt người dự thi (100 lần thi).
              Biên chế được giao là 05 người. Như vậy Bình quân lượt người tham
              gia của Phòng Tư pháp là 20 lượt.
            </p>
            <p>
              <strong>
                <em>b) Đối với giải cá nhân</em>
              </strong>
            </p>
            <p>
              - Kết quả được tính cho người có đáp án đúng nhất và gửi về Ban Tổ
              chức Cuộc thi với thời gian sớm nhất theo mã số điện tử dự thi
              (thời gian thi được tính ngay khi người dự thi hoàn thành thành
              công bài thi online).
            </p>
            <p>
              Trong trường hợp có số người dự thi (từ 02 người trở lên) trả lời
              đúng các câu hỏi và cùng dự đoán chính xác số người trả lời đúng,
              Ban Tổ chức Cuộc thi sẽ trao giải cho người có đáp án gửi đến Ban
              Tổ chức Cuộc thi sớm nhất theo mã số điện tử dự thi.
            </p>
            <p>
              <em>3.2. </em>
              <em>Thi thiết kế Biểu trưng (Logo) thành phố</em>
              <em> Tây Ninh</em>
            </p>
            <p>
              - Một tác phẩm của cá nhân hoặc tập thể dự thi được chọn làm Logo
              cải cách hành chính thành phố Tây Ninh phải đáp ứng các yêu cầu về
              hình thức, nội dung, ý nghĩa gắn với sự phát triển thành phố Tây
              Ninh, văn minh, hiện đại do Hội đồng Thẩm định Biểu trưng (Logo)
              thành phố trình UBND thành phố công nhận.
            </p>
            <p>
              - Bản quyền thiết kế tác phẩm Logo thuộc về UBND thành phố Tây
              Ninh, không mang tính chất thương mại.
            </p>
            <p>
              <strong>* Về tiêu chí chấm điểm Biểu trưng (Logo)</strong>
            </p>
            <p>a. Ý tưởng của mẫu thiết kế;</p>
            <p>b. Sự độc đáo sáng tạo của mẫu thiết kế;</p>
            <p>
              c. Thể hiện được giá trị cốt lõi của hình ảnh, ý nghĩa, đặc trưng
              văn hóa, khát vọng phát triển vươn lên của thành phố Tây Ninh.
            </p>
            <p>d. Tính thẩm mỹ, khoa học của mẫu thiết kế;</p>
            <p>
              e. Thuận lợi cho việc in ấn, phóng to, thu nhỏ, đắp nổi, gia công
              được bằng mọi chất liệu;
            </p>
            <p>g. Khả năng dễ nhớ, dễ nhận biết của mẫu thiết kế.</p>
            <p>
              <strong>IV. Giải thưởng</strong>
            </p>
            <p>
              <strong>1. Giải thưởng thi trực tuyến </strong>
              <strong>tìm hiểu cải cách hành chính</strong>
            </p>
            <p>
              <strong> a) Giải tập thể </strong>
            </p>
            <p>
              <strong> </strong>
              Có 06 giải tập thể dành cho các đơn vị có bình quân số lượt người
              tham gia
            </p>
            <p>
              thi nhiều, chất lượng thi cao nhất{" "}
              <em>(được chia theo Khối dự thi như trong Kế hoạch)</em>.
            </p>
            <p>
              <strong>- </strong>
              <strong>
                <em>02 Giải nhất</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>3.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>
                <em>- 02 Giải nhì</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>2.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>
                <em>- 02 Giải ba</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>1.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong> b) Giải cá nhân</strong>
            </p>
            <p>
              <strong> </strong>
              Có 15 giải thưởng dành cho các cá nhân có câu trả lời đúng nhất và
              dự đoán số lượt người tham gia chính xác nhất (không giới hạn số
              lượt tham gia trả lời), gồm:
            </p>
            <p>
              <strong>- </strong>
              <strong>
                <em>01 Giải nhất</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>2</strong>
                <strong>.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>
                <em>- 03 Giải nhì</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>1.500.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>
                <em>- 05 Giải ba</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>1.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>
                <em>- 06 Giải khuyến khích</em>
              </strong>
              <em>
                : tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>500.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>2</strong>
              <strong>
                <em>. </em>
              </strong>
              <strong>
                Giải thưởng sáng tạo Biểu trưng (Logo) thành phố Tây Ninh{" "}
              </strong>
              dành cho cá nhân, tập thể có Biểu trưng (Logo) được UBND thành phố
              chọn sử dụng làm Logo của thành phố.
            </p>
            <p>
              01 giải:
              <em>
                tặng giấy khen của Chủ tịch UBND thành phố và tiền thưởng{" "}
                <strong>50.000.000đ</strong>.
              </em>
            </p>
            <p>
              <strong>3. Nguồn kinh phí</strong>
            </p>
            <p>
              Từ nguồn kinh phí cải cách hành chính thành phố năm 2021 và nguồn
              xã hội hóa.
            </p>
            <p>
              <a name="bookmark95"></a>
              <a name="bookmark96"></a>
              <a name="bookmark98"></a>
              <a name="bookmark97"></a>
              <strong>V. Trách nhiệm pháp lý</strong>
            </p>
            <p>
              - Ban Tổ chức Cuộc thi không tính kết quả và không chịu trách
              nhiệm do các sự cố khách quan về kỹ thuật liên quan đến mạng
              Internet hoặc khi người dự thi vi phạm Thể lệ Cuộc thi.
            </p>
            <p>
              - Tác giả gửi tác phẩm dự thi được hiểu đã chấp thuận mọi quy định
              trong Thể lệ Cuộc thi. Người dự thi chịu trách nhiệm về quyền tác
              giả và quyền liên quan theo quy định của pháp luật.
            </p>
            <p>
              - Các tổ chức, cá nhân dự thi phải tuân thủ các quy định của Thể
              lệ cuộc thi, trong đó phải bảo đảm: Logo gửi dự thi không là đối
              tượng tranh chấp bản quyền tác giả; chưa được sử dụng, chưa xuất
              hiện trên bất kỳ phương tiện thông tin đại chúng nào.
            </p>
            <p>
              - Trong thời gian diễn ra cuộc thi, tác giả không được sử dụng
              logo tham dự cuộc thi này phục vụ các hoạt động thương mại - phi
              thương mại nào khác.
            </p>
            <p>
              - Tác giả đạt giải không được sử dụng Logo gửi dự thi trên bất kỳ
              ấn phẩm truyền thông hoặc cuộc thi nào khác.
            </p>
            <p>
              - Trong trường hợp có tranh chấp bản quyền tác giả phát sinh sau
              khi tác phẩm dự thi đạt giải được công bố, tác giả phải chịu hoàn
              toàn trách nhiệm trước pháp luật.
            </p>
            <p>
              - Ban Tổ chức không chịu trách nhiệm nếu tác phẩm dự thi không đến
              được Ban Tổ chức với bất kỳ lý do nào. Tác phẩm gửi đến Ban Tổ
              chức không đúng quy định được xem là không hợp lệ và sẽ không được
              đưa vào chấm thi dự giải. Ban Tổ chức không trả lại tác phẩm dự
              thi.
            </p>
            <p>VI. Giải quyết khiếu nại, tố cáo liên quan đến cuộc thi</p>
            <p>
              <a name="bookmark99"></a>
              <strong>1.</strong>
              Ban Tổ chức cuộc thi tiếp nhận và giải quyết khiếu nại, tố cáo
              liên quan đến kết quả cuộc thi trong thời gian 05 ngày, kể từ ngày
              công bố kết quả cuộc thi.
            </p>
            <p>
              <a name="bookmark100"></a>
              Quyết định của Ban Tổ chức Cuộc thi là kết quả giải quyết cuối
              cùng.
            </p>
            <p>
              <strong>2.</strong>
              Thể lệ cuộc thi được đăng tải trên Cổng thông tin điện tử thành
              phố: <strong>http://thanhpho.tayninh.gov.vn</strong> và được thông
              báo trên các phương tiện thông tin đại chúng, mạng xã hội.
            </p>
            <p>
              Trong quá trình thực hiện, nếu có nội dung phát sinh, Ban Tổ chức
              cuộc thi sẽ điều chỉnh, bổ sung cho phù hợp. Mọi vướng mắc (nếu
              có) liên hệ về Ban Tổ chức cuộc thi (qua Phòng Nội vụ Thành phố),
              số điện thoại 02763.825250 để được giải đáp./.
            </p>
          </DialogContent>
          <DialogActions
            style={{ justifyContent: "center", background: "#EFE3AF" }}
          >
            <Button
              onClick={handleCloseTheLeDialog}
              color="secondary"
              variant="contained"
            >
              Đồng ý
            </Button>
          </DialogActions>
        </Dialog>

        {/* ======================================= Dialog Thống kê =================================== */}

        <Dialog
          open={openThongKeDialog}
          onClose={handleCloseThongKeDialog}
          maxWidth="md"
        >
          <DialogTitle id="form-dialog-title" align="center">
            SỐ LƯỢNG THÍ SINH DỰ THI THEO ĐƠN VỊ
          </DialogTitle>
          <DialogContent dividers>
            <TableContainer>
              <Table aria-label="customized table" size="small">
                <TableHead>
                  <TableRow>
                    <StyledTableCell align="right">STT</StyledTableCell>
                    <StyledTableCell>Đơn vị</StyledTableCell>
                    <StyledTableCell align="right">Số lượng</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {thongKe.map((row, index) => (
                    <StyledTableRow key={row.donvi}>
                      <StyledTableCell align="right">
                        {index + 1}
                      </StyledTableCell>
                      <StyledTableCell component="th" scope="row">
                        {row.donvi}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.soluong}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                  <StyledTableRow>
                    <StyledTableCell align="right"></StyledTableCell>
                    <StyledTableCell
                      component="th"
                      scope="row"
                      style={{ fontWeight: "bold" }}
                    >
                      Tổng cộng
                    </StyledTableCell>
                    <StyledTableCell
                      align="right"
                      style={{ fontWeight: "bold" }}
                    >
                      {tong}
                    </StyledTableCell>
                  </StyledTableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleCloseThongKeDialog}
              color="secondary"
              variant="contained"
            >
              Đóng
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={openThongKeDiemDialog}
          onClose={handleCloseThongKeDiemDialog}
          maxWidth="md"
        >
          <DialogTitle id="form-dialog-title" align="center">
            THỐNG KÊ THEO SỐ ĐIỂM
          </DialogTitle>
          <DialogContent dividers>
            <TableContainer>
              <Table aria-label="customized table" size="small">
                <TableHead>
                  <TableRow>
                    <StyledTableCell align="right">STT</StyledTableCell>
                    <StyledTableCell>Số câu đúng</StyledTableCell>
                    <StyledTableCell align="right">Số lượng</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {thongKeDiem.map((row, index) => (
                    <StyledTableRow key={row.sodiem}>
                      <StyledTableCell align="right">
                        {index + 1}
                      </StyledTableCell>
                      <StyledTableCell component="th" scope="row">
                        {row.socaudung === -1
                          ? "Chưa làm bài"
                          : row.socaudung + "/30"}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.soluong}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions style={{ justifyContent: "center" }}>
            <Button
              onClick={handleCloseThongKeDiemDialog}
              color="secondary"
              variant="contained"
            >
              Đóng
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
}

{
  /* ====== Thông báo lỗi khi đăng nhập sai ========*/
}
function ErrorSnackBar(props) {
  const classes = useStyles();
  const { errMessOpen, handleErrMessOpen, messError, ...other } = props;

  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      open={errMessOpen}
      autoHideDuration={3000}
      onClose={handleErrMessOpen}
    >
      <SnackbarContent
        onClose={handleErrMessOpen}
        className={classes.error}
        message={
          <span className={classes.message}>
            <ErrorIcon className={classes.iconVariant} />
            {messError}
          </span>
        }
      />
    </Snackbar>
  );
}

{
  /* ====== Loading indicator ========*/
}
function LinearIndeterminate() {
  return (
    <div>
      <LinearProgress />
    </div>
  );
}

//============================================= Styles =============================================//
const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    alignContent: "center",
    backgroundImage: `url(${BannerLeft})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% 100%",
  },
  bannerText: {
    backgroundColor: "#007ACC",
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(2),
    color: theme.palette.background.paper,
    padding: theme.spacing(1),
  },
  image: {
    backgroundImage: `url(${BannerLeft})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% 100%",
    objectFit: "fill",
  },
  rightSide: {
    background: "linear-gradient(to right, #00c6ff, #0072ff)",
    // background: 'linear-gradient(to right, #00d2ff, #3a7bd5)',
    color: "white",
    marginRight: "auto",
    marginLeft: "auto",
    borderRadius: 5,
    //marginTop:10,
    //margin:"auto",
    //padding:"auto"
    // backgroundRepeat: 'no-repeat',
    // backgroundSize: 'cover',
    // backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    width: 130,
    height: 130,
    backgroundColor: "transparent",
    marginBottom: 20,
  },
  logo: {
    width: "100%",
    height: "100%",
  },
  hoavan: {
    width: 120,
    // height: 100,
    marginTop: -10,
    marginBottom: -30,
    backgroundColor: "transparent",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(1, 0, 1),
  },
  // timeRemain: {
  //   margin: theme.spacing(2, 0, 5),
  //   fontSize: 15,
  //   fontWeight: "bold",
  //   color: "white",
  // },
  timeRemain: {
    margin: theme.spacing(1, 0, 1),
    fontSize: "2.1em",
    fontWeight: "bold",
    fontFamily: "-apple-system",
    color: "red",
    textShadow: "-0.5px 0 black, 0 0.5px black, 0.5px 0 black, 0 -0.5px white",
  },
  timeRemain2: {
    margin: theme.spacing(1, 0, 3),
    fontSize: "1.8em",
    fontWeight: "bold",
    fontFamily: "Arial",
    color: "white",
    textShadow: "-0.5px 0 black, 0 0.5px black, 0.5px 0 black, 0 -0.5px black",
    // fontStyle:"italic"
  },
  testQuestion: {
    fontSize: 18,
    fontWeight: "bold",
    fontFamily: "-apple-system",
    color: "white",
  },

  // testName: {
  //   // margin: theme.spacing(0, 0, 5),
  //   fontSize: 20,
  //   fontWeight: "bold",
  //   fontFamily: "-apple-system",
  //   color: "#FFF401",
  //   textShadow: "-1px 1px 1px #0063B1",
  // },

  testName: {
    fontSize: "1.8em",
    margin: theme.spacing(0, -4, 0),
    fontWeight: "bold",
    fontFamily: "-apple-system",
    color: "yellow",
    textShadow: "-0.5px 0 black, 0 0.5px black, 0.5px 0 black, 0 -0.5px black",
  },
  progress: {
    margin: theme.spacing(2),
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: "flex",
    alignItems: "center",
  },
  formControl: {
    marginTop: 10,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  formStyle: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "33ch",
    },
  },
}));

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
