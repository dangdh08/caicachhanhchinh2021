import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import HomeIcon from "@material-ui/icons/Home";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { Redirect, Link } from "react-router-dom";

export default function HomePageNavBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [logedOut, setLogedOut] = React.useState(false);
  const open = Boolean(anchorEl);

  function handleMenu(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function logout() {
    localStorage.clear();
    setAnchorEl(null);
    setLogedOut(true);
  }

  if (logedOut) {
    return <Redirect to={{ pathname: "/" }} />;
  } else
    return (
      <AppBar
        position="absolute"
        elevation={2}
        className={classes.appBar}
        color="primary"
      >
        <Toolbar>
          {/* <IconButton */}
          {/*   aria-controls="menu-appbar" */}
          {/*   aria-haspopup="true" */}
          {/*   // onClick={handleMenu} */}
          {/*   component={Link}  */}
          {/*   to="/" */}
          {/*   color="inherit" */}
          {/* > */}
          {/*   <HomeIcon /> */}
          {/* </IconButton> */}

          <Typography variant="h6" className={classes.title}>
            CUỘC THI TÌM HIỂU CẢI CÁCH HÀNH CHÍNH NĂM 2021
          </Typography>
          
          <div>
            <IconButton
              aria-label="Tài khoản"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <AccountCircle />
              <Typography variant="body2" className={classes.titleIcon}>
                Tài khoản
              </Typography>
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right"
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right"
              }}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={logout}>Đăng xuất</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
    );
}

//============================================= Styles =============================================//

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  root2: {
    flexGrow: 1,
    paddingTop: 150
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1,
    justifyContent: "center",
    justifyItems: "center"
  },
  titleIcon: {
    flexGrow: 1,
    marginLeft: 5
  },
  paper: {
    height: 350,
    width: 250
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  }
}));
