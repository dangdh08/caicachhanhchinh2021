﻿using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public class BaiThiDataAccessLayer
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        public IEnumerable<BaiThiVaCauHoi> GetAllBaiThi()
        {
            var list = data.Baithi.Where(n => n.DelStt == true).OrderByDescending(a => a.Timestamps).ToList(); ;
            List<BaiThiVaCauHoi> subjects = new List<BaiThiVaCauHoi>();
            foreach (Baithi i in list)
            {
                BaiThiVaCauHoi ct = new BaiThiVaCauHoi();
                ct.TestCode = i.TestCode;
                ct.TestName = i.TestName;
                ct.IdStatus = i.IdStatus;
                ct.Password = i.Password;
                ct.TimeToDo = i.TimeToDo;
                ct.TotalQuestions = i.TotalQuestions;
                ct.Timestamps = i.Timestamps;
                ct.datacauhoi = data.Cauhoi.
                    Join(data.Chitietbaithi, t1 => t1.IdCauhoi, (t2 => t2.IdCauhoi),
                    (t1, t2) => new CauHoiEdit
                    {
                        IdCauhoi = t1.IdCauhoi,
                        Content = t1.Content,
                        ImgContent = t1.ImgContent,
                        AnswerA = t1.AnswerA,
                        AnswerB = t1.AnswerB,
                        AnswerC = t1.AnswerC,
                        AnswerD = t1.AnswerD,
                        CorrectAnswer = t1.CorrectAnswer,
                        Timestamps = t1.Timestamps,
                        IdLoaicauhoi = t1.IdLoaicauhoi,
                        TestCode = t2.TestCode,
                        DelStt=t1.DelStt
                    }).Where(n=>n.TestCode==i.TestCode && n.DelStt==true).ToList();
                ct.datathisinh= data.Students.
                    Join(data.StudentTestDetail, st => st.IdStudent, (ctl => ctl.IdStudent),
                    (st, ctl) => new ThiSinh
                    {
                        //chỉnh sửa lại thông tin lấy ra cho trac nghiem 60 nam tua 2, không lấy đầy đủ thông tin

                        IdStudent = st.IdStudent,
                        Avatar = st.Avatar,
                        //Birthday = st.Birthday,
                        Cmnd = st.Cmnd,
                        Donvi = st.Donvi,
                        //Email = st.Email,
                        //Gender = st.Gender,
                        Name = st.Name,
                        Phone = st.Phone,
                        //Sbd = st.Sbd,
                        //Username = st.Username,
                        Timelogin = st.Timelogin,
                        Idclass = ctl.TestCode
                    }).Where(t => t.Idclass == i.TestCode).Distinct().ToList();
                subjects.Add(ct);
            }
            return subjects;
        }
        public IEnumerable<Statuses> GetAllTrangThai()
        {
            return data.Statuses.ToList();
        }
        public int AddBaiThi(Baithi _context)
        {
            try
            {
                if (data.Baithi.Where(n => n.TestName == _context.TestName).Count() < 900)
                {
                    _context.Password = _context.Password;
                    data.Baithi.Add(_context);
                    data.SaveChanges();
                    return data.Baithi.Where(n => n.TestName == _context.TestName).OrderByDescending(n => n.TestCode).First().TestCode;
                    //return data.Baithi.FirstOrDefault(n => n.TestName == _context.TestName).TestCode;
                }
                return 2;
                //if (data.Baithi.Where(n => n.TestName == _context.TestName).Count() != 0)
                //    return 2;
                //_context.Password = _context.Password;
                //data.Baithi.Add(_context);
                //data.SaveChanges();
                //return data.Baithi.FirstOrDefault(n=>n.TestName== _context.TestName).TestCode;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteBaithi(int id)
        {
            try
            {
                Baithi _context = data.Baithi.Find(id);
                if (data.Chitietbaithi.Count(n => n.TestCode == id) != 0 || data.StudentTestDetail.Count(n => n.TestCode == id) != 0)
                {
                    _context.DelStt = false;
                    _context.Timestamps = DateTime.Now;
                    data.Entry(_context).State = EntityState.Modified;
                    data.SaveChanges();
                }
                else
                {
                    data.Baithi.Remove(_context);
                    data.SaveChanges();
                }
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateBaithi(Baithi _context)
        {
            try
            {
                _context.Password = _context.Password;
                _context.Timestamps = DateTime.Now;
                _context.DelStt = true;
                data.Entry(_context).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public Baithi GetDetail(int id)
        {
            Baithi _context = data.Baithi.Find(id);
            if (_context == null)
                return null;
            return _context;
        }
        public int AddCauHoiVaoBaiThi(Chitietbaithi _context)
        {
            try
            {
                if (data.Chitietbaithi.Where(n => n.IdCauhoi == _context.IdCauhoi && n.TestCode==_context.TestCode).Count() != 0)
                    return 2;
                if (data.Chitietbaithi.Count(n => n.TestCode == _context.TestCode)>=data.Baithi.SingleOrDefault(n=>n.TestCode==_context.TestCode).TotalQuestions)
                    return 3;
                data.Chitietbaithi.Add(_context);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteCauHoiTrongBaiThi(int id)
        {
            try
            {
                Chitietbaithi _context = data.Chitietbaithi.Find(id);
                    data.Chitietbaithi.Remove(_context);
                    data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public IEnumerable<CauHoiTheoBaiThi> GetCauHoiTheoBaiThi(int id)
        {
            var list = data.Cauhoi.
                Join(data.Chitietbaithi,
                o => o.IdCauhoi, od => od.IdCauhoi,
                (o, od) => new CauHoiTheoBaiThi
                {
                    IdCauhoi = o.IdCauhoi,
                    ImgContent = o.ImgContent,
                    IdLoaicauhoi = o.IdLoaicauhoi,
                    Content = o.Content,
                    AnswerA = o.AnswerA,
                    AnswerB = o.AnswerB,
                    AnswerC = o.AnswerC,
                    AnswerD = o.AnswerD,
                    CorrectAnswer = o.CorrectAnswer,
                    TestCode=od.TestCode,
                    DelStt = o.DelStt
                }).Where(m=>m.TestCode==id && m.DelStt==true).ToList();
            return list;
        }
        public int TaoDeThi(int Id,int TestCode)
        {
            try
            {
                var context = new trac_nghiem_onlineContext();
                //context.Database.ExecuteSqlCommand("TaoDeCho1ThiSinh @MaCode, @MaThisinh", parameters: new[] { TestCode, Id });
                if (data.StudentTestDetail.Count(n => n.IdStudent == Id && n.TestCode == TestCode) >= 1)
                {
                    context.Database.ExecuteSqlCommand($"XoaDeCho1ThiSinh {TestCode}, {Id}");
                }
                context.Database.ExecuteSqlCommand($"TaoDeCho1ThiSinh {TestCode}, {Id}");
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        public IEnumerable<GetDanhSachKetQua> GetDanhSachKetQua(int TestCode)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.GetDanhSachKetQua.FromSql("EXEC dbo.GetDanhSachKetQua @TestCode={0}", TestCode).ToList();
                return dbResults;
            }

        }

    }
}
