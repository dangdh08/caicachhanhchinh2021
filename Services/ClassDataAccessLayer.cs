﻿using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public class ClassDataAccessLayer
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        public IEnumerable<BaiThiTrongBang> GetAllBaiThi()
        {
            var list = data.Classes.Where(n => n.DelStt == true).OrderByDescending(a => a.Timestamps).ToList();
            List<BaiThiTrongBang> subjects = new List<BaiThiTrongBang>();
            foreach (Classes i in list)
            {
                BaiThiTrongBang ct= new BaiThiTrongBang();
                ct.ClassName = i.ClassName;
                ct.GradeName = i.GradeName;
                ct.IdClass = i.IdClass;
                ct.Timestamps = i.Timestamps;
                ct.datathisinh = data.Students.
                    Join(data.ChitietLop,st => st.IdStudent,(ctl => ctl.IdStudent),
                    (st,ctl)=>new ThiSinh
                    {
                        IdStudent=st.IdStudent,
                         Avatar=st.Avatar,
                         Birthday=st.Birthday,
                         Cmnd=st.Cmnd,
                         Donvi=st.Donvi,
                         Email=st.Email,
                         Gender=st.Gender,
                         Name=st.Name,
                         Phone=st.Phone,
                         Sbd=st.Sbd,
                         Username=st.Username,
                         Timelogin=st.Timelogin,
                         Idclass=ctl.IdClass
                    }).Where(t => t.Idclass == i.IdClass).ToList();
                subjects.Add(ct);
            }
            return subjects;
        }
        public int AddClasses(Classes _context)
        {
            try
            {
                if (data.Classes.Where(n => n.ClassName == _context.ClassName && n.GradeName==_context.GradeName).Count() != 0)
                    return 2;
                data.Classes.Add(_context);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteClasses(int id)
        {
            try
            {
                Classes _context = data.Classes.Find(id);
                if (data.ChitietLop.Count(n => n.IdClass == id) != 0 )
                {
                    _context.DelStt = false;
                    _context.Timestamps = DateTime.Now;
                    data.Entry(_context).State = EntityState.Modified;
                    data.SaveChanges();
                }
                else
                {
                    data.Classes.Remove(_context);
                    data.SaveChanges();
                }
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateClasses(Classes _context)
        {
            try
            {
                _context.Timestamps = DateTime.Now;
                _context.DelStt = true;
                data.Entry(_context).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public Classes GetDetail(int id)
        {
            Classes _context = data.Classes.Find(id);
            if (_context == null)
                return null;
            return _context;
        }
        public int AddThiSinhVaoLop(ChitietLop _context)
        {
            try
            {
                if (data.ChitietLop.Where(n => n.IdClass == _context.IdClass && n.IdStudent == _context.IdStudent).Count() != 0)
                    return 2;
                data.ChitietLop.Add(_context);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteThiSinhTrongLop(int id)
        {
            try
            {
                ChitietLop _context = data.ChitietLop.Find(id);
                if (data.ChitietLop.Count(n => n.IdClass == id) != 0)
                {
                    return 2;
                }
                    data.ChitietLop.Remove(_context);
                    data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
    }
}
