﻿using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public class DataAccessLayer
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        public List<ThiSinh> GetAllUser()
        {
            return data.Students.Select(x=>new ThiSinh { IdStudent=x.IdStudent, Avatar=x.Avatar, Birthday=x.Birthday,
                                         Cmnd=x.Cmnd, Donvi=x.Donvi,Email=x.Email,Gender=x.Gender,Name=x.Name,Phone=x.Phone,
                                          Sbd=x.Sbd,Timelogin=x.Timelogin, Username=x.Username}).ToList();
        }
        public int AddUser(Students st)
        {
            try
            {
                if (data.Students.Where(n => n.Username == st.Username).Count() != 0)
                    return 2;
                //st.Password = GenerateMD5(st.Sbd+st.Birthday.Year);
                st.Password = GenerateMD5(st.Cmnd);
                data.Students.Add(st);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteUser(int id)
        {
            try
            {
                if (data.Scores.Where(n => n.IdStudent == 0).Count() != 0 || data.StudentTestDetail.Where(n => n.IdStudent == 0).Count() != 0)
                    return 2;
                Students st = data.Students.Find(id);
                data.Students.Remove(st);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateUser(Students st)
        {
            try
            {
                if (st.Password == null)
                {
                //var pass = data.Students.Find(st.IdStudent).Password;
                st.Password = GenerateMD5(st.Sbd + st.Birthday.Year);
                }
                else
                {
                    st.Password = GenerateMD5(st.Password);
                }
                data.Entry(st).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public Students GetDetail(int id)
        {
            Students st = data.Students.Find(id);
            st.Password = "●●●●●●●●●●●●●●●●●●●●●●";
            st.Token = null;
            if (st == null)
                return null;
            return st;
        }

        public IEnumerable<DanhSachThi> DanhSachThi()
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.DanhSachThi.FromSql("EXEC dbo.DanhSachThiSinhThi").ToList();
                return dbResults;
            }

        }
        public IEnumerable<DanhSachThi> DanhSachThi2()
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.DanhSachThi.FromSql("EXEC dbo.DanhSachThiSinhThi_OTP").ToList();
                return dbResults;
            }

        }
        public IEnumerable<DanhSachThi> DanhSachThi_DonVi(string donvi)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.DanhSachThi.FromSql("EXEC dbo.DanhSachThiSinhThi_DonVi @TenDonVi={0}", donvi).ToList();
                return dbResults;
            }

        }

    }
}
