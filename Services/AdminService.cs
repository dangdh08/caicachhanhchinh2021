﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReactJsExample.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public interface IAdminService
    {
        Admins Authenticate(string username, string password);
        Admins Logout(string token);
        Admins CheckToken(string token);
    }
    public class AdminService : IAdminService
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        private readonly AppSettings _appSettings;
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        public AdminService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public Admins Authenticate(string username, string password)
        {
            var user = data.Admins.SingleOrDefault(x => x.Username == username && x.Password == GenerateMD5(password));

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, user.Gender.ToString()),
                    new Claim(ClaimTypes.GivenName, user.Name.ToString()),
                }),
                Expires = DateTime.UtcNow.AddHours(10000),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.Timelogin = DateTime.Now;
            data.Entry(user).State = EntityState.Modified;
            data.SaveChanges();

            // remove password before returning
            user.Password = null;

            return user;
        }
        public Admins Logout(string token)
        {
            var user = data.Admins.SingleOrDefault(x => x.Token == token);
            if (user == null)
                return null;
            user.Token = null;
            user.Timelogin = DateTime.Now;
            data.Entry(user).State = EntityState.Modified;
            data.SaveChanges();
            // remove password before returning
            user.Password = null;
            return user;
        }
        public Admins CheckToken(string token)
        {
            var user = data.Admins.SingleOrDefault(x => x.Token == token);
            if (user == null)
                return null;
            // remove password before returning
            user.Password = null;

            //// document-https://docs.microsoft.com/en-us/dotnet/api/system.identitymodel.tokens.jwt.jwtsecuritytokenhandler.readjwttoken?view=azure-dotnet

            //var handler = new JwtSecurityTokenHandler();
            //var jsonToken = handler.ReadToken(token);
            //var jsonToken1 = handler.ReadJwtToken(token);
            //var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            //var role = tokenS.Claims.First(claim => claim.Type == "role").Value;
            //var id_name = tokenS.Claims.First(claim => claim.Type == "unique_name").Value;
            return user;
        }

    }
}
