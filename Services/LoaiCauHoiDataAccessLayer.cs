﻿using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public class LoaiCauHoiDataAccessLayer
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        public IEnumerable<Loaicauhoi> GetAllLoaiCauHoi()
        {
            return data.Loaicauhoi.Where(n => n.DelStt == true).OrderByDescending(a => a.Timestamps).ToList();
        }
        public int AddLoaicauhoi(Loaicauhoi _context)
        {
            try
            {
                if (data.Loaicauhoi.Where(n => n.Tenloai == _context.Tenloai).Count() != 0)
                    return 2;
                data.Loaicauhoi.Add(_context);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteLoaicauhoi(int id)
        {
            try
            {
                Loaicauhoi _context = data.Loaicauhoi.Find(id);
                if (data.Cauhoi.Count(n => n.IdLoaicauhoi == id) != 0)
                {
                    _context.DelStt = false;
                    _context.Timestamps = DateTime.Now;
                    data.Entry(_context).State = EntityState.Modified;
                    data.SaveChanges();
                }
                else
                {
                    data.Loaicauhoi.Remove(_context);
                    data.SaveChanges();
                }
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateLoaicauhoi(Loaicauhoi _context)
        {
            try
            {
                _context.Timestamps = DateTime.Now;
                _context.DelStt = true;
                data.Entry(_context).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public Loaicauhoi GetDetail(int id)
        {
            Loaicauhoi _context = data.Loaicauhoi.Find(id);
            if (_context == null)
                return null;
            return _context;
        }
    }
}
