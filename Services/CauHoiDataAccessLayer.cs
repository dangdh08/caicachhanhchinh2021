﻿using Microsoft.EntityFrameworkCore;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public class CauHoiDataAccessLayer
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        public IEnumerable<CauHoimf> GetAllCauHoi()
        {
            //var context = new Sptrac_nghiem_onlineContext();
            //var list3 = context.CauHoimf.FromSql("EXEC dbo.GetAllCauHoi").ToList();
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.CauHoimf.FromSql("EXEC dbo.GetAllCauHoi").ToList();
                return dbResults;
            }
            //return data.Cauhoi.Where(n=>n.DelStt==true).OrderByDescending(a => a.Timestamps).ToList();
        }
        public IEnumerable<Cauhoi> GetCauHoiTheoLoai(int id)
        {
            return data.Cauhoi.Where(n=>n.IdLoaicauhoi==id &&n.DelStt==true).OrderByDescending(a => a.Timestamps).ToList();
        }
        public int AddCauhoi(Cauhoi _context)
        {
            try
            {
                if (data.Cauhoi.Where(n => n.Content == _context.Content).Count() != 0)
                    return 2;
                data.Cauhoi.Add(_context);
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int DeleteCauhoi(int id)
        {
            try
            {
                Cauhoi _context = data.Cauhoi.Find(id);
                if (data.StudentTestDetail.Count(n => n.IdCauhoi == id) != 0)
                {
                    _context.DelStt = false;
                    _context.Timestamps = DateTime.Now;
                    data.Entry(_context).State = EntityState.Modified;
                    data.SaveChanges();
                }
                else
                {
                    data.Cauhoi.Remove(_context);
                    data.SaveChanges();
                }
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateCauhoi(Cauhoi _context)
        {
            try
            {
                _context.DelStt = true;
                _context.Timestamps = DateTime.Now;
                data.Entry(_context).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public Cauhoi GetDetail(int id)
        {
            Cauhoi _context = data.Cauhoi.Find(id);
            if (_context == null)
                return null;
            return _context;
        }
    }
}
