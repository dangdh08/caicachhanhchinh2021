﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReactJsExample.Models;
using ReactJsExample.Models.StoredProcedure;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReactJsExample.Services
{
    public interface IUserService
    {
        Students Authenticate(string username, string password);
        Students AuthenticateCCHC(string username, string password);
        Students Logout(string token);
        Students CheckToken(string token);
        int UpdateDapAn(int Id, int IdStudent, string StudentAnswer, int TestCode);
        int CheckDiem(int IdStudent, int TestCode);
        IEnumerable<KetQuaThi> CheckTime(int Id, int TestCode);
    }
    public class UserService : IUserService
    {
        trac_nghiem_onlineContext data = new trac_nghiem_onlineContext();
        private readonly AppSettings _appSettings;
        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
        }
        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public Students Authenticate(string username, string password)
        {
            var user = data.Students.SingleOrDefault(x => x.Username == username && x.Password == GenerateMD5(password));

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, "User")
                }),
                Expires = DateTime.UtcNow.AddMinutes(180),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.Timelogin = DateTime.Now;
            data.Entry(user).State = EntityState.Modified;
            data.SaveChanges();

            // remove password before returning
            user.Password = null;
            return user;
        }
        //cải cách hành chính sửa lãi chek login phone/ passs)
        public Students AuthenticateCCHC(string phone, string password)
        {
            string a = GenerateMD5(password);
            string nda = phone;

            //var user = data.Students.SingleOrDefault(x => x.Phone == phone && x.Password == GenerateMD5(password));
            var user = data.Students.Where(x => x.Phone == phone && x.Password == GenerateMD5(password)).OrderByDescending(x => x.IdStudent).First();

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, "User")
                }),
                Expires = DateTime.UtcNow.AddMinutes(129600),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.Timelogin = DateTime.Now;
            data.Entry(user).State = EntityState.Modified;
            data.SaveChanges();

            // remove password before returning
            user.Password = null;
            return user;
        }
        public Students Logout(string token)
        {
            var user = data.Students.SingleOrDefault(x => x.Token == token);
            if (user == null)
                return null;
            user.Token = null;
            user.Timelogin = DateTime.Now;
            data.Entry(user).State = EntityState.Modified;
            data.SaveChanges();
            // remove password before returning
            user.Password = null;
            return user;
        }
        public Students CheckToken(string token)
        {
            var user = data.Students.SingleOrDefault(x => x.Token == token);
            if (user == null)
                return null;
            // remove password before returning
            user.Password = null;
            return user;
        }
        public int UpdateDapAn(int Id, int IdStudent, string StudentAnswer,int TestCode)
        {
            try
            {
                if(data.Scores.SingleOrDefault(n=>n.TestCode == TestCode && n.IdStudent == IdStudent).TimeFinish < DateTime.Now)
                    return 2;
                var detail = data.StudentTestDetail.FirstOrDefault(x => x.Id == Id && x.IdStudent == IdStudent);
                detail.Timestamps = DateTime.Now;
                detail.StudentAnswer = StudentAnswer;
                data.Entry(detail).State = EntityState.Modified;
                data.SaveChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        public int CheckDiem(int IdStudent, int TestCode)
        {
            try
            {
                if (data.Scores.SingleOrDefault(n => n.TestCode == TestCode && n.IdStudent == IdStudent) == null)
                    return 1;
                if (data.Scores.SingleOrDefault(n => n.TestCode == TestCode && n.IdStudent == IdStudent).Socaudung == null)
                    return 2;
                else
                    return 3;
            }
            catch
            {
                return -1;
            }
        }
        public IEnumerable<KetQuaThi> CheckTime(int Id, int TestCode)
        {
            using (var ctx = new Sptrac_nghiem_onlineContext())
            {
                var dbResults = ctx.KetQuaThi.FromSql("EXEC dbo.CheckThoiGianLamBai @MaThiSinh={0}, @TestCode ={1}", Id, TestCode).ToList();
                if (dbResults.First().time_finish > DateTime.Now)
                    return null;
                return dbResults;
            }

        }
    }
}
